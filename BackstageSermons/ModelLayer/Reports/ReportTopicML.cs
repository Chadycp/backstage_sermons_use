﻿using System;

namespace BackstageSermons
{
    public class ReportTopicML
    {
        public int TopicID { get; set; }
        public string Title { get; set; }
        public int TotalDownloads { get; set; }
        public int TotalListens { get; set; }
    }
}