﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace BackstageSermons.Settings
{
    public static class SermonAudio
    {
        public static string MemberID
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("SermonAudio_MemberID").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("SermonAudio_MemberID").Value = value;
            }
        }

        public static string Password
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("SermonAudio_Password").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("SermonAudio_Password").Value = value;
            }
        }

        public static void Update()
        {
            SettingCollectionML sc = new SettingCollectionML();
            SettingML s = SermonsCache.SettingCollection.Setting("SermonAudio_MemberID");
            sc.Add(s);
            s = SermonsCache.SettingCollection.Setting("SermonAudio_Password");
            sc.Add(s);
            SermonsBLL.Instance.UpdateSettingCollection(sc);
        }
    }
}
