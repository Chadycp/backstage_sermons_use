﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Remember set of selections used for a search
/// </summary>

namespace BackstageSermons
{
    public class SermonSearchStateML
    {
        #region Public fields

        public string SortField = string.Empty;
        public SortDirection SortDirection = SortDirection.Ascending;
        public int PageIndex = 0;

        #endregion

        #region Constructors

        #region SermonSearchState()

        public SermonSearchStateML()
        {
        }

        #endregion

        #endregion

        #region Static properties

        #region Current

        public static SermonSearchStateML Current
        {
            get
            {
                SermonSearchStateML s = (SermonSearchStateML)System.Web.HttpContext.Current.Session["SermonSearchStateML"];
                if (s == null)
                    s = new SermonSearchStateML();
                return s;
            }

            set
            {
                System.Web.HttpContext.Current.Session["SermonSearchStateML"] = value;
            }
        }

        #endregion

        #endregion
    }
}