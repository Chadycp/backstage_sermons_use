﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackstageSermons
{
    public interface IBackstageSermonsDataLayer
    {
        #region Sermon methods

        SermonCollectionML LoadAllSermons();
        SermonCollectionML LoadAllVisibleSermons();
        SermonCollectionML LoadAllFeaturedSermons();
        SermonCollectionML LoadLatestSermons(int numberOfSermons);
        SermonCollectionML LoadLatestVisibleSermons(int numberOfSermons);
        SermonCollectionML LoadSermonsNotInSeries();
        SermonCollectionML LoadAllVisibleFeaturedSermons();
        SermonCollectionML LoadAllVisibleSermonsBySpeakerID(int speakerID);
        SermonCollectionML LoadAllVisibleSermonsByTopicID(int topicID);
        SermonCollectionML LoadAllVisibleSermonsByPassageBook(string passageBook);
        SermonCollectionML LoadAllVisibleSermonsByDateRange(DateTime startDate, DateTime endDate);
        SermonCollectionML LoadAllSermonsByFieldValue(int fieldID, string value);
        SermonCollectionML LoadSermonsBySeriesID(int seriesID);
        SermonCollectionML LoadAllVisibleSermonsBySeriesID(int seriesID);
        //SermonCollectionML SearchAllVisibleSermons(int SpeakerID, int SeriesID, string PassageBook, string PassageChapter, string PassageVerse, DateTime StartDate, DateTime EndDate, string Title, string Keywords);
        //SermonCollectionML SearchAllSermons(string search);
        SermonCollectionML SearchSermons(bool loadInvisible, string search, string title, string keywords, int speakerID, List<int> seriesIDs, string passageBook, string passageChapter, string passageVerse, DateTime? startDate, DateTime? endDate, string where, string customFieldsWhere, string sortExpression, int rowsPerPage, int pageNumber);
        SermonML LoadSermon(int sermonID);
        void InsertSermon(SermonML sermon);
        void UpdateSermon(SermonML sermon);
        void DeleteSermon(int sermonID);

        #endregion

        #region Sermon Month Methods

        List<DateTime> LoadAllSermonMonths();
        List<DateTime> LoadAllVisibleSermonMonths();

        #endregion

        #region Sermon Passage Book Methods
        SermonPassageBookCollectionML LoadAllSermonPassageBooks();
        SermonPassageBookCollectionML LoadAllVisibleSermonPassageBooks();
        #endregion

        #region Series methods

        SeriesCollectionML LoadAllSeries();
        SeriesCollectionML LoadAllVisibleSeries();
        SeriesCollectionML LoadAllVisibleRootSeries();
        SeriesCollectionML LoadAllVisibleRootSeriesWithSermons();
        SeriesCollectionML LoadAllVisibleSeriesWithSermons();
        SeriesCollectionML LoadAllSeriesByFieldValue(int fieldID, string value);
        SeriesCollectionML LoadSeriesBySermonID(int SermonID);
        SeriesML LoadSeries(int seriesID);
        SeriesML LoadVisibleSeries(int seriesID);
        void InsertSeries(SeriesML series);
        void UpdateSeries(SeriesML series);
        void DeleteSeries(int seriesID);

        #endregion

        #region Sermon2Series Methods

        void DeleteSermon2Series(int SermonID, int SeriesID);
        void InsertSermon2Series(int SermonID, int SeriesID);

        #endregion

        #region Sermon Field methods
        SermonFieldCollectionML LoadAllSermonFields();
        SermonFieldCollectionML LoadAllSermonFieldsForSermon(int sermonID, int seriesID);
        void InsertSermonField(SermonFieldML item);
        void UpdateSermonField(SermonFieldML item);
        void DeleteSermonField(int fieldId);

        #endregion

        #region Sermon Field Value methods
        SermonFieldValueCollectionML LoadAllSermonFieldValues();
        SermonFieldValueCollectionML LoadAllSermonFieldValuesBySermonID(int sermonID);
        void InsertSermonFieldValue(SermonFieldValueML item);
        void UpdateSermonFieldValue(SermonFieldValueML item);
        void DeleteSermonFieldValue(int sermonID, int fieldID);

        #endregion

        #region Series Field methods

        SeriesFieldCollectionML LoadAllSeriesFields();
        SeriesFieldCollectionML LoadAllSeriesFieldsForSeries(int seriesID);
        void InsertSeriesField(SeriesFieldML item);
        void UpdateSeriesField(SeriesFieldML item);
        void DeleteSeriesField(int fieldID);

        #endregion

        #region Series Field Value methods

        SeriesFieldValueCollectionML LoadAllSeriesFieldValues();
        SeriesFieldValueCollectionML LoadAllSeriesFieldValuesBySeriesID(int seriesID);
        void InsertSeriesFieldValue(SeriesFieldValueML item);
        void UpdateSeriesFieldValue(SeriesFieldValueML item);
        void DeleteSeriesFieldValue(int seriesID, int fieldID);

        #endregion

        #region Speaker Methods

        SpeakerCollectionML LoadAllSpeakers();
        SpeakerCollectionML LoadAllVisibleSpeakers();
        SpeakerCollectionML LoadAllVisibleSpeakersWithSermons();
        SpeakerML LoadSpeaker(int speakerID);
        SpeakerML LoadVisibleSpeaker(int speakerID);
        void InsertSpeaker(SpeakerML speaker);
        void UpdateSpeaker(SpeakerML speaker);
        void DeleteSpeaker(int SpeakerID);

        #endregion
        
        #region Topic Methods

        TopicCollectionML LoadAllTopics();
        TopicCollectionML LoadAllVisibleTopics();
        TopicCollectionML LoadAllVisibleTopicsWithSermons();
        void InsertTopic(TopicML topic);
        void UpdateTopic(TopicML topic);
        void DeleteTopic(int TopicID);

        #endregion

        #region Topic2Sermon Methods

        TopicCollectionML LoadTopicsBySermonID(int SermonID);
        void InsertTopic2Sermon(int SermonID, int TopicID);
        void DeleteTopic2Sermon(int SermonID, int TopicID);

        #endregion

        #region Setting Methods

        SettingCollectionML LoadAllSettings();
        void InsertSetting(SettingML setting);
        void UpdateSetting(SettingML setting);
        void DeleteSetting(int SettingID);

        #endregion

        
        #region Activity Methods

        ActivityCollectionML LoadAllActivity();
        ActivityCollectionML LoadActivityByDateRange(DateTime startDate, DateTime endDate);
        void InsertActivity(ActivityML activity);
        void UpdateActivity(ActivityML activity);
        void DeleteActivity(int activityID);

        #endregion

        #region ReportSeries Methods

        ReportSeriesCollectionML LoadAllReportSeriesByDateRange(DateTime startDate, DateTime endDate);

        #endregion

        #region ReportTopic Methods

        ReportTopicCollectionML LoadAllReportTopicsByDateRange(DateTime startDate, DateTime endDate);

        #endregion
         
    }
}
