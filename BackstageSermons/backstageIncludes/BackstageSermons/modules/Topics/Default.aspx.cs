﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using backstage.bxpages;

namespace BackstageSermons.modules.Topics
{
    public partial class Default : BackstagePage, IComparer<TopicML>
    {
        #region Private Properties
        private TopicCollectionML searchTC;
        private TopicCollectionML _tc;
        #endregion

        #region Public Properties
        public string SortExpression
        {
            get
            {
                return backstage.modules.MemberManager.StringHelper.NZ(ViewState["SortExpression"]);
            }
            set
            {
                ViewState["SortExpression"] = value;
            }
        }

        public SortDirection SortDirection
        {
            get
            {
                if (ViewState["SortDirection"] == null)
                    return SortDirection.Ascending;
                return (SortDirection)ViewState["SortDirection"];
            }
            set
            {
                ViewState["SortDirection"] = value;
            }
        }

        public TopicSearchStateML searchState
        {
            get
            {
                TopicSearchStateML _searchState = (TopicSearchStateML)Session["TopicSearchStateML"];
                if (_searchState == null) {
                    _searchState = new TopicSearchStateML();
                    Session["TopicSearchStateML"] = _searchState;
                }
                return _searchState;
            }
            set
            {
                Session["TopicSearchStateML"] = value;
            }
        }

        public TopicCollectionML tc
        {
            get
            {
                if (_tc == null)
                    _tc = SermonsBLL.Instance.LoadAllTopics();
                return _tc;
            }
            set { _tc = value; }
        }
        #endregion



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) {
                ResetPaging();
                ResetSorting();

                SetDefaults();
                BindTopics();
            }
        }

        protected void SetDefaults()
        {
        }

        protected void txtSearch_OnTextChanged(object sender, EventArgs e)
        {
            BindTopics();
        }

        protected void ddlActive_IndexChanged(object sender, EventArgs e)
        {
            /*
            string strResult = string.Empty;
            foreach (RepeaterItem ri in rptTopics.Items)
            {
                try
                {
                    bool visible = Convert.ToBoolean(ddlActive.SelectedValue);
                    CheckBox cb = (CheckBox)ri.FindControl("cbTopic");
                    TextBox tb = (TextBox)ri.FindControl("txtTopicID");
                    if (cb.Checked)
                    {
                        int TopicID = int.Parse(tb.Text);
                        TopicML topic = TopicsBLL.Instance.LoadTopic(TopicID, false);
                        topic.Available = visible;
                        TopicsBLL.Instance.UpdateTopic(topic);
                    }
                }
                catch (Exception err)
                {
                    strResult += err.Message.ToString() + "<br/>";
                }
            }
            if (string.IsNullOrEmpty(strResult))
            {
                SetDefaults();
                BindTopics();
            }
            else
                lblResult.Text = strResult;
             */
        }

        protected void BindTopics()
        {
            searchTC = new TopicCollectionML();
            string search = txtSearch.Text.ToLower();
            if (search == "search topics")
                search = "";
            foreach (TopicML topic in tc) {
                if (topic.TopicID.ToString().ToLower().Contains(search) || topic.Title.ToLower().Contains(search))
                    searchTC.Add(topic);
            }

            SortTopics();

            gridTopics.DataSource = searchTC;
            gridTopics.DataBind();

            Session["TopicCollection"] = searchTC;
        }

        protected void btnAddTopic_Click(object sender, EventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            TextBox txtNewTitle = (TextBox)btn.Parent.FindControl("txtNewTitle");

            TopicML t = new TopicML();
            t.Title = txtNewTitle.Text;
            SermonsBLL.Instance.InsertTopic(t);
            tc = null;
            BindTopics();

            txtNewTitle.Text = "";
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "closeAddNew", "$('#trAddNew').children('td:first').children('div:first').css('display', 'block');ToggleNewTopic();", true);
        }

        protected void btnDeleteTopic_Click(object sender, EventArgs e)
        {
            try {
                foreach (GridViewRow gvRow in gridTopics.Rows) {
                    CheckBox cb = (CheckBox)gvRow.Cells[0].FindControl("cbTopic");
                    HiddenField hf = (HiddenField)gvRow.Cells[0].FindControl("hfTopicID");
                    if (cb != null && hf != null) {
                        if (cb.Checked) {
                            int TopicID = int.Parse(hf.Value);
                            SermonsBLL.Instance.DeleteTopic(TopicID);
                            tc.Remove(tc.FindTopicByID(TopicID));
                        }
                    }
                }
            } catch (Exception err) {
                AlertMessage(err.Message);
            }

            SetDefaults();
            BindTopics();
        }

        protected void SortTopics()
        {
            if (SortExpression.Length > 0) {

                searchState.SortField = SortExpression;
                searchState.SortDirection = SortDirection;
                searchTC.Sort(this);
            }
        }

        protected void gridTopics_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try {
                if (e.CommandName == "UpdateTopic") {
                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                    int tID = int.Parse(e.CommandArgument.ToString());

                    TopicML t = tc.FindTopicByID(tID);

                    if (t != null) {
                        CheckBox cbEditActive = (CheckBox)row.FindControl("cbEditActive");
                        TextBox txtEditTitle = (TextBox)row.FindControl("txtEditTitle");

                        t.Title = txtEditTitle.Text;

                        SermonsBLL.Instance.UpdateTopic(t);
                        SetDefaults();
                        BindTopics();
                    }
                }

                if (e.CommandName == "DeleteTopic") {
                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                    int tID = int.Parse(e.CommandArgument.ToString());

                    SermonsBLL.Instance.DeleteTopic(tID);
                    tc.Remove(tc.FindTopicByID(tID));
                    BindTopics();
                }

            } catch (Exception err) {
                AlertMessage(err.Message);
            }
        }

        protected void gridTopics_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridTopics.PageIndex = e.NewPageIndex;
            BindTopics();
        }

        public void gridTopics_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row != null && e.Row.RowType == DataControlRowType.Header) {
                foreach (TableCell cell in e.Row.Cells) {
                    if (cell.HasControls()) {
                        LinkButton button = cell.Controls[0] as LinkButton;

                        if (button != null) {
                            if (SortExpression == button.CommandArgument) {
                                if (SortDirection == SortDirection.Ascending)
                                    cell.CssClass = "asc";
                                else
                                    cell.CssClass = "desc";
                            }
                        }
                    }
                }
            }
        }

        protected void gridTopics_DataBound(object sender, System.EventArgs e)
        {
            TopicPrintListCollectionML splc = (TopicPrintListCollectionML)Session["TopicPrintListCollectionML"];

            if (splc == null) {
                //lblSelected.Text = "0";
                return;
            }

            foreach (GridViewRow gvRow in gridTopics.Rows) {
                CheckBox cb = (CheckBox)gvRow.Cells[0].FindControl("cbTopic");
                if (cb != null) {
                    TopicCollectionML sc = (TopicCollectionML)gridTopics.DataSource;
                    //throw new Exception(gvRow.DataItemIndex.ToString());
                    int sID = sc[gvRow.DataItemIndex].TopicID;
                    TopicPrintListML spl = splc.GetByTopicID(sID);
                    cb.Checked = (spl != null);
                }
            }


            //GridViewRow newRow = new GridViewRow();

            //lblSelected.Text = plc.Count.ToString();
        }

        protected void gridTopics_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (SortExpression == e.SortExpression) {
                SortDirection = (SortDirection == SortDirection.Ascending) ? SortDirection.Descending : SortDirection.Ascending;
            } else {
                SortExpression = e.SortExpression;
                SortDirection = SortDirection.Ascending;
            }
            ResetPaging();
            BindTopics();
            //BindData();
            updateTopics.Update();
        }

        /*
        protected void AlertMessage(string msg)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertMessage", "alert(\"" + msg + "\");", true);
        }
        */

        protected void rptTopics_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            int sID = int.Parse(e.CommandArgument.ToString());

            if (e.CommandName == "UpdateTopic") {
                TopicML t = tc.FindTopicByID(sID);

                if (t != null) {
                    CheckBox cbEditActive = (CheckBox)e.Item.FindControl("cbEditActive");
                    TextBox txtEditTitle = (TextBox)e.Item.FindControl("txtEditTitle");

                    t.Title = txtEditTitle.Text;

                    SermonsBLL.Instance.UpdateTopic(t);
                    SetDefaults();
                    BindTopics();
                }
            }

            if (e.CommandName == "DeleteTopic") {
                SermonsBLL.Instance.DeleteTopic(sID);
                tc.Remove(tc.FindTopicByID(sID));
                BindTopics();
            }
        }

        #region Select/Deselect All
        private void SelectAll()
        {
            try {
                TopicCollectionML scTemp = (TopicCollectionML)Session["TopicCollection"];
                if (scTemp == null)
                    return;
                TopicPrintListCollectionML splc = (TopicPrintListCollectionML)Session["TopicPrintListCollectionML"];

                if (splc == null) {
                    splc = new TopicPrintListCollectionML();
                    Session["TopicPrintListCollectionML"] = splc;
                }

                foreach (TopicML s in scTemp) {
                    TopicPrintListML spl = new TopicPrintListML();
                    spl.TopicID = s.TopicID;
                    splc.Add(spl);
                }
            } catch (Exception err) {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "errAlert", "alert('" + err.Message.ToString() + "');", true);
            }
            BindTopics();
        }

        private void DeselectAll()
        {
            try {
                Session.Remove("TopicPrintListCollectionML");
            } catch (Exception err) {
                AlertMessage(err.Message);
            }
            BindTopics();
        }

        protected void lbSelectAllTopics_Click(object sender, EventArgs e)
        {
            bool chckd = false;
            if (sender.GetType() == typeof(CheckBox))
                chckd = !cbSelectAllTopics.Checked;
            else
                chckd = (cbSelectAllTopics.Checked);

            if (chckd) {
                DeselectAll();
                lblSelectAllTopics.Text = "Select All";
                cbSelectAllTopics.Checked = false;
            } else {
                SelectAll();
                lblSelectAllTopics.Text = "Deselect All";
                cbSelectAllTopics.Checked = true;
            }
            TopicPrintListCollectionML slc = (TopicPrintListCollectionML)Session["TopicPrintListCollectionML"];
            /*
            if (slc == null)
                lblSelectedTopics.Text = "0";
            else
                lblSelectedTopics.Text = plc.Count.ToString();
             */

            updateTopics.Update();
        }
        #endregion

        private void ResetPaging()
        {
            try {
                gridTopics.PageIndex = 0;
            } catch {
            }
        }

        private void ResetSorting()
        {
            try {
                SortExpression = "Title";
                SortDirection = SortDirection.Ascending;
            } catch {
            }
        }

        #region Sorting Methods
        public int Compare(TopicML x, TopicML y)
        {
            try {
                int result = GetCompareResult(x, y);

                #region Handle sorting direction

                if (searchState.SortDirection == SortDirection.Descending)
                    result = result * -1;

                #endregion


                return result;

            } catch (Exception err) {
                System.Web.HttpContext.Current.Trace.Write(err.ToString());
            }

            return 0;
        }

        public int GetCompareResult(TopicML x, TopicML y)
        {
            int result = 0;

            if (searchState.SortField == "Title") {
                #region Sort by FirstName
                result = CompareStrings(x.Title, y.Title);
                #endregion
            }
            return result;
        }

        public int CompareStrings(string s1, string s2)
        {
            int result = 0;
            if (string.IsNullOrEmpty(s1)) {
                if (string.IsNullOrEmpty(s2)) {
                    return 0;
                } else {
                    return -1;
                }
            } else if (string.IsNullOrEmpty(s2)) {
                return 1;
            }

            int s1Length = s1.Length;
            int s2Length = s2.Length;

            bool sp1 = char.IsLetterOrDigit(s1[0]);
            bool sp2 = char.IsLetterOrDigit(s2[0]);

            if (sp1 && !sp2) {
                return 1;
            }
            if (!sp1 && sp2) {
                return -1;
            }

            char c1, c2;
            int i1 = 0, i2 = 0;
            int r = 0;
            bool letter1, letter2;

            while (true) {
                c1 = s1[i1];
                c2 = s2[i2];

                sp1 = char.IsDigit(c1);
                sp2 = char.IsDigit(c2);

                if (!sp1 && !sp2) {
                    if (c1 != c2) {
                        letter1 = char.IsLetter(c1);
                        letter2 = char.IsLetter(c2);

                        if (letter1 && letter2) {
                            c1 = char.ToUpper(c1);
                            c2 = char.ToUpper(c2);

                            r = c1 - c2;
                            if (0 != r) {
                                return r;
                            }
                        } else if (!letter1 && !letter2) {
                            r = c1 - c2;
                            if (0 != r) {
                                return r;
                            }
                        } else if (letter1) {
                            return 1;
                        } else if (letter2) {
                            return -1;
                        }
                    }

                } else if (sp1 && sp2) {
                    r = CompareNumbers(s1, s1Length, ref i1, s2, s2Length, ref i2);
                    if (0 != r) {
                        result = r;
                        break;
                        //return r;
                    }
                } else if (sp1) {
                    return -1;
                } else if (sp2) {
                    return 1;
                }

                i1++;
                i2++;

                if (i1 >= s1Length) {
                    if (i2 >= s2Length) {
                        return 0;
                    } else {
                        return -1;
                    }
                } else if (i2 >= s2Length) {
                    return 1;
                }
            }
            return result;
        }

        private static int CompareNumbers(
        string s1, int s1Length, ref int i1,
        string s2, int s2Length, ref int i2)
        {
            int nzStart1 = i1, nzStart2 = i2;
            int end1 = i1, end2 = i2;

            ScanNumber(s1, s1Length, i1, ref nzStart1, ref end1);
            ScanNumber(s2, s2Length, i2, ref nzStart2, ref end2);

            int start1 = i1;
            i1 = end1 - 1;
            int start2 = i2;
            i2 = end2 - 1;

            int length1 = end2 - nzStart2;
            int length2 = end1 - nzStart1;

            if (length1 == length2) {
                int r;
                for (int j1 = nzStart1, j2 = nzStart2; j1 <= i1; j1++, j2++) {
                    r = s1[j1] - s2[j2];
                    if (0 != r) return r;
                }

                length1 = end1 - start1;
                length2 = end2 - start2;

                if (length1 == length2) return 0;
            }

            if (length1 > length2) return -1;
            return 1;
        }

        private static void ScanNumber(string s, int length, int start, ref int nzStart, ref int end)
        {
            nzStart = start;
            end = start;

            bool countZeros = true;
            char c = s[end];

            while (true) {
                if (countZeros) {
                    if ('0' == c) {
                        nzStart++;
                    } else {
                        countZeros = false;
                    }
                }

                end++;
                if (end >= length) break;

                c = s[end];
                if (!char.IsDigit(c)) break;
            }
        }
        #endregion
    }
}
