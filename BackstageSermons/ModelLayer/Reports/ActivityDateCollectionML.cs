﻿using System;
using System.Collections.Generic;

namespace BackstageSermons
{
    public class ActivityDateCollectionML : List<ActivityDateML>
    {
        #region Public properties

        public int TotalListens
        {
            get
            {
                int total = 0;
                foreach (ActivityDateML activity in this)
                    total += activity.TotalListens;
                return total;
            }
        }

        public decimal TotalDownloads
        {
            get
            {
                decimal total = 0;
                foreach (ActivityDateML downloads in this)
                    total += downloads.TotalDownloads;
                return total;
            }
        }

        #endregion

        #region Public methods

        #region ActivityDateML FindByTitle(string title)

        public ActivityDateML FindByTitle(string title)
        {
            return this.Find(delegate(ActivityDateML activity) { return (activity.Title == title); });
        }

        #endregion

        #endregion
    }
}