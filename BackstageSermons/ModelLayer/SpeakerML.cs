﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.ComponentModel;

namespace BackstageSermons
{
    public enum NamePrefixes { [Description("Dr.")] Dr, [Description("Mr.")] Mr, [Description("Mrs.")] Mrs, [Description("Pastor")] Pastor, [Description("Rev.")] Rev }

    public class SpeakerML
    {
        public int SpeakerID { get; set; }
        public NamePrefixes? NamePrefix { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Photo { get; set; }
        public int PhotoID { get; set; }
        public string Bio { get; set; }
        public int SermonCount { get; set; }

        public string FullName(bool startWithFirstName, bool displayPrefix)
        {
            string n = "";
            string fName = displayPrefix && NamePrefix != null ? backstage.Classes.TypeHelper.GetEnumDescription(NamePrefix.Value) + " " + FirstName : FirstName;
            if (startWithFirstName)
            {
                n = fName;
                if (!string.IsNullOrEmpty(LastName) && !string.IsNullOrEmpty(n))
                    n += " ";
                n += LastName;
            }
            else
            {
                n = LastName;
                if (!string.IsNullOrEmpty(fName) && !string.IsNullOrEmpty(n))
                    n += ", ";
                n += fName;
            }
            return n;
        }

        public string FullDisplayName
        {
            get
            {
                return FullName(true, true);
            }
        }
    }
}
