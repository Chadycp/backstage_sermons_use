﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using BackstageControls;
using backstage.security;

namespace BackstageSermons.Controls
{
    public class SermonRepeater : SmartRepeater, IComparer<SermonML>
    {
        public SermonRepeater()
        {
        }

        private SermonCollectionML _sc;
        public SermonCollectionML sc
        {
            get
            {
                if (_sc == null)
                    _sc = GetSermonCollection();
                return _sc;
            }
            set { _sc = value; }
        }

        private SermonCollectionML renderSC;

        public override object DataSource
        {
            get
            {
                return base.DataSource;
            }
            set
            {
                base.DataSource = value;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (DetailView) {
                renderSC = new SermonCollectionML();
                renderSC.Add(CurrentSermon);
            } else if (RandomEntry) {
                renderSC = new SermonCollectionML();
                Random random = new Random();
                int r = random.Next(sc.Count);

                renderSC.Add(sc[r]);
            } else
                renderSC = sc;

            SortSermons();

            #region Paginator
            if (Repeat > 0 && System.Web.HttpContext.Current.Request.QueryString["ViewAll"] != "true") {
                if (!string.IsNullOrEmpty(PaginatorID))
                    SetPaginator(renderSC);
                _PaginatorCurrentPage = 1;
                try {
                    _PaginatorCurrentPage = int.Parse(System.Web.HttpContext.Current.Request.QueryString["Page"]);
                } catch { }
                int j = 0;
                int count = renderSC.Count;
                _PaginatorTotalItems = count;
                for (int i = 0; i < count; i++) {
                    try {
                        if (i < (Repeat * (PaginatorCurrentPage - 1)) || i > ((Repeat * _PaginatorCurrentPage) - 1))
                            renderSC.RemoveAt(j);
                        else {
                            j++;
                            if (j == 1)
                                _PaginatorFirstItem = i + 1;
                            _PaginatorLastItem = i + 1;
                        }
                    } catch (Exception err) {
                        System.Web.HttpContext.Current.Trace.Write("Paginator Error", err.ToString());
                    }
                }
            }
            #endregion

            

            this.DataSource = renderSC;
            this.DataBind();
        }

        protected SermonCollectionML GetSermonCollection()
        {
            SermonCollectionML sc = null;
            if (string.IsNullOrEmpty(AssociatedCategoryRepeaterID)) {
                string strSeriesID = System.Web.HttpContext.Current.Request.QueryString["SeriesID"];
                if (!string.IsNullOrEmpty(strSeriesID))
                    SeriesID = int.Parse(strSeriesID);
            } else {
                //CategoryRepeater cr = (CategoryRepeater)this.Parent.FindControl(AssociatedCategoryRepeaterID);
                //SeriesID = cr.SeriesID;
            }
                //sc = SermonsBLL.Instance.SearchAllVisibleSermons(0, SeriesID, "", "", "", DateTime.MinValue, DateTime.MaxValue, "", "", false);
            List<int> seriesIDs = new List<int>();
            if (SeriesID > 0)
                seriesIDs.Add(SeriesID);

            if (Repeat > 0)
                sc = SermonsBLL.Instance.SearchSermons(false, null, null, null, 0, seriesIDs, null, null, null, null, null, null, null, null, Repeat, 1, false);
            else if (SeriesID > 0)
                sc = SermonsBLL.Instance.LoadVisibleSermonsBySeriesID(SeriesID, false);
            else
                sc = SermonsBLL.Instance.LoadAllVisibleSermons(false);
            return sc;
        }

        protected void SetPaginator(SermonCollectionML pcml)
        {
            string cp = System.Web.HttpContext.Current.Request.QueryString["Page"];
            if (string.IsNullOrEmpty(cp))
                cp = "1";
            int currentPage = int.Parse(cp);
            Repeater rpt = (Repeater)this.Parent.FindControl(PaginatorID);
            PageCollectionML pc = new PageCollectionML();
            if (rpt != null) {
                int pg = 0;
                int i = 0;
                foreach (SermonML prod in pcml) {
                    if (i == 0) {
                        pg++;
                        PageML p = new PageML();
                        p.PageNum = pg;
                        if (pg == currentPage)
                            p.IsCurrentPage = true;
                        pc.Add(p);
                    }
                    if (i < (Repeat - 1))
                        i++;
                    else
                        i = 0;
                }
                _PaginatorTotalPages = pc.Count;
                rpt.DataSource = pc;
                rpt.DataBind();
            } else
                System.Web.HttpContext.Current.Trace.Write("rpt is null");
        }

        private void SortSermons()
        {
            if (!string.IsNullOrEmpty(SortField)) {
                //List<SermonML> sermonsToSort = new List<SermonML>();
                //foreach (SermonML s in renderSC)
                    //sermonsToSort.Add(s);
                //sermonsToSort.Sort(this);
                //renderSC.Clear();
                //foreach (SermonML s in sermonsToSort)
                    //renderSC.Add(s);
                renderSC.Sort(this);
            }
        }

        public int SeriesID { get; set; }

        private string _strSermonID;
        public string strSermonID
        {
            get
            {
                if (string.IsNullOrEmpty(_strSermonID)) {
                    _strSermonID = System.Web.HttpContext.Current.Request.QueryString["SermonID"];
                    if (string.IsNullOrEmpty(_strSermonID) && DisplayFirstEntry) {
                        if (CurrentSermon != null)
                            _strSermonID = CurrentSermon.SermonID.ToString();
                    }
                }
                return _strSermonID;
            }
            set { _strSermonID = value; }
        }
        private int _SermonID;
        public int SermonID
        {
            get
            {
                if (_SermonID == 0) {
                    if (!string.IsNullOrEmpty(strSermonID))
                        _SermonID = int.Parse(_strSermonID);
                }
                return _SermonID;
            }
            set { _SermonID = value; }
        }

        public string AssociatedCategoryRepeaterID { get; set; }

        private SermonML _CurrentSermon;
        private SermonML CurrentSermon
        {
            get
            {
                if (_CurrentSermon == null) {
                    if (string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["SermonID"])) {
                        if (DisplayFirstEntry && sc.Count > 0)
                            _CurrentSermon = sc[0];
                    } else {
                        _CurrentSermon = sc.FindSermonByID(SermonID);
                    }
                }
                return _CurrentSermon;
            }
            set { _CurrentSermon = value; }
        }

        [Description("Show first sermon if SermonID is not provided in the querystring"), DefaultValue(true)]
        public bool DisplayFirstEntry { get; set; }

        public bool DetailView { get; set; }
        /*
        public int Repeat { get; set; }

        public string PaginatorID { get; set; }
        */
        private int _PaginatorCurrentPage;
        public int PaginatorCurrentPage
        {
            get { return _PaginatorCurrentPage; }
        }

        private int _PaginatorFirstItem;
        public int PaginatorFirstItem
        {
            get
            {
                return _PaginatorFirstItem;
            }
        }

        private int _PaginatorLastItem;
        public int PaginatorLastItem
        {
            get
            {
                return _PaginatorLastItem;
            }
        }

        private int _PaginatorTotalItems;
        public int PaginatorTotalItems
        {
            get
            {
                return _PaginatorTotalItems;
            }
        }

        private int _PaginatorTotalPages;
        public int PaginatorTotalPages
        {
            get
            {
                return _PaginatorTotalPages;
            }
        }

        public bool RandomEntry { get; set; }

        public string SortField { get; set; }
        public string SortDirection { get; set; }

        #region IComparer<SermonML> Members

        public int Compare(SermonML x, SermonML y)
        {
            try {
                string[] sortElements = SortField.Split(',');
                string[] sortDirections = SortDirection.Split(',');

                if (sortElements.Length != sortDirections.Length)
                    throw new Exception(string.Format("SERMON_SORTER: SortField [{0}] SortDirections [{2}] counts do not match", sortElements, sortDirections));

                int sortIndex = 0;
                int result = 0;

                foreach (string sortingField in sortElements) {
                    Type sermonxType = x.GetType();
                    PropertyInfo pi = sermonxType.GetProperty(sortingField);

                    if (pi == null)
                        return 0;

                    Type fieldType = pi.PropertyType;
                    if (fieldType == typeof(DateTime)) {
                        #region Sort by date
                        if ((DateTime)pi.GetValue(x, null) < (DateTime)pi.GetValue(y, null))
                            result = -1;
                        else if ((DateTime)pi.GetValue(x, null) > (DateTime)pi.GetValue(y, null))
                            result = 1;
                        else
                            result = 0;

                        #endregion
                    } else if (fieldType == typeof(string)) {
                        #region Sort by string
                        result = string.Compare((string)pi.GetValue(x, null), (string)pi.GetValue(y, null));
                        #endregion

                    } else if (fieldType == typeof(int)) {
                        #region Sort by integer

                        int ix = (int)pi.GetValue(x, null);
                        int iy = (int)pi.GetValue(y, null);

                        if (ix < iy)
                            result = -1;
                        else if (ix > iy)
                            result = 1;
                        else
                            result = 0;

                        #endregion
                    }

                    #region Handle sorting direction
                    string sortingDirection = sortDirections[sortIndex];
                    if (sortingDirection.ToLower().StartsWith("desc"))
                        result = result * -1;

                    #endregion

                    #region Check result of sort

                    if (result != 0)
                        break;

                    sortIndex += 1;
                    #endregion
                }

                return result;

            } catch (Exception err) {
                System.Web.HttpContext.Current.Trace.Write(err.ToString());
            }

            return 0;
        }

        #endregion
    }
}
