﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Reflection;

namespace BackstageSermons
{
    public class SermonSorter : IComparer<SermonML>
    {
        public string SortField = string.Empty;
        public string SortDirection = string.Empty;

        #region IComparer<SermonML> Members

        public int Compare(SermonML x, SermonML y)
        {
            try {
                string[] sortElements = SortField.Split(',');
                string[] sortDirections = SortDirection.Split(',');

                if (sortElements.Length != sortDirections.Length)
                    throw new Exception(string.Format("SERMON_SORTER: SortField [{0}] SortDirections [{2}] counts do not match", sortElements, sortDirections));

                int sortIndex = 0;
                int result = 0;

                foreach (string sortingField in sortElements) {
                    Type sermonxType = x.GetType();
                    PropertyInfo pi = sermonxType.GetProperty(sortingField);

                    if (pi == null)
                        return 0;

                    Type fieldType = pi.PropertyType;
                    if(fieldType == typeof(DateTime))
                    {
                        #region Sort by date
                        if((DateTime)pi.GetValue(x, null) < (DateTime)pi.GetValue(y, null))
                            result = -1;
                        else if ((DateTime)pi.GetValue(x, null) > (DateTime)pi.GetValue(y, null))
                            result = 1;
                        else
                            result = 0;

                        #endregion
                    }
                    else if(fieldType == typeof(string))
                    {
                        #region Sort by string
                        result = string.Compare((string)pi.GetValue(x, null), (string)pi.GetValue(y, null));
                        #endregion

                    } else if (fieldType == typeof(int))
                    {
                        #region Sort by integer

                        int ix = (int)pi.GetValue(x, null);
                        int iy = (int)pi.GetValue(y, null);

                        if (ix < iy)
                            result = -1;
                        else if (ix > iy)
                            result = 1;
                        else
                            result = 0;

                        #endregion
                    }
                    /*
                    else if (sortingType.ToLower() == "time") {

                        #region Sort by time

                        int timeStartX = 0;
                        int timeEndX = 0;

                        if (!ParseTime(nx.InnerText, out timeStartX, out timeEndX))
                            return 0;

                        int timeStartY = 0;
                        int timeEndY = 0;

                        if (!ParseTime(ny.InnerText, out timeStartY, out timeEndY))
                            return 0;

                        if (timeStartX < timeStartY)
                            result = -1;
                        else if (timeStartX > timeStartY)
                            result = 1;
                        else
                            result = 0;

                        //System.Web.HttpContext.Current.Trace.Write(string.Format("CF: Comparing time [{0}][{1}]-[{2}] to [{3}][{4}]-[{5}]", nx.InnerText, timeStartX, timeEndX, ny.InnerText, timeStartY, timeEndY));

                        #endregion

                    }
                    else {

                        #region Default sorting

                        result = string.Compare(nx.InnerText, ny.InnerText);

                        #endregion
                    }
                     */

                    #region Handle sorting direction
                    string sortingDirection = sortDirections[sortIndex];
                    if (sortingDirection.ToLower().StartsWith("desc"))
                        result = result * -1;

                    #endregion

                    #region Check result of sort

                    if (result != 0)
                        break;

                    sortIndex += 1;
                    #endregion
                }

                return result;

            } catch (Exception err) {
                System.Web.HttpContext.Current.Trace.Write(err.ToString());
            }

            return 0;
        }


        #region bool ParseTime(string timeToParse, out int timeStart, out int timeEnd)

        private bool ParseTime(string timeToParse, out int timeStart, out int timeEnd)
        {
            timeStart = 0;
            timeEnd = 0;

            TimeSpan tsStart;
            TimeSpan tsEnd;

            if (!backstage.Classes.TimeHelper.ParseTime(timeToParse, out tsStart, out tsEnd))
                return false;

            if (tsStart != null)
                timeStart = (tsStart.Hours * 100) + tsStart.Minutes;

            if (tsEnd != null)
                timeEnd = (tsEnd.Hours * 100) + tsEnd.Minutes;

            return true;
        }

        #endregion

        #endregion
    }
}
