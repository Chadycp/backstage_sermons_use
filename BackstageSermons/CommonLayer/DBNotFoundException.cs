﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackstageSermons
{
    public class DBNotFoundException : System.Exception
    {
        public DBNotFoundException(string message)
            : base(message)
        {
        }

        public DBNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
