﻿using System;
using System.Collections.Generic;
using System.Linq;
using backstage.bxpages;
using backstage.Classes;
using BackstageSermons.Settings;

namespace BackstageSermons.modules.Settings
{
    public partial class Default : BackstagePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                SetDefaults();

        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected void SetDefaults()
        {
            cbEnableFeatured.Checked = Admin.EnableFeatured;
            cbEnableSharing.Checked = Admin.EnableSharing;
            cbEnableLivestream.Checked = Admin.EnableLivestream;
            ddlTimeZones.SelectedValue = Admin.TimeZone.Id;
            txtSeriesImageDimensions.Text = Admin.SeriesImageWidth.ToString() + "x" + Admin.SeriesImageHeight.ToString();
            txtSermonsPerPageModule.Text = Admin.SermonsPerPageModule.ToString();
            txtSermonsPerPageSite.Text = Admin.SermonsPerPageSite.ToString();
            txtSeriesDetailPage.Text = Admin.SeriesDetailPage;
            txtDetailPage.Text = Admin.SermonDetailPage;

            ddBibleVersion.DataSource = Enum.GetValues(typeof(Admin.BibleVersions))
                .Cast<Admin.BibleVersions>()
                .Select(v => v.ToString())
                .ToList();
            ddBibleVersion.DataBind();
            ddBibleVersion.SelectedValue = Admin.BibleVersion.ToString();

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Admin.EnableFeatured = cbEnableFeatured.Checked;
            Admin.EnableSharing = cbEnableSharing.Checked;
            Admin.EnableLivestream = cbEnableLivestream.Checked;
            Admin.TimeZone = TimeZoneInfo.FindSystemTimeZoneById(ddlTimeZones.SelectedValue);
            List<string> dim = StringEditor.StringToList(txtSeriesImageDimensions.Text, "x");
            Admin.SeriesImageWidth = dim.Count > 0 ? StringEditor.ToInt32(dim[0]) : 0;
            Admin.SeriesImageHeight = dim.Count > 1 ? StringEditor.ToInt32(dim[1]) : 0;
            Admin.SermonsPerPageModule = backstage.Classes.StringEditor.ToInt32(txtSermonsPerPageModule.Text);
            Admin.SermonsPerPageSite = backstage.Classes.StringEditor.ToInt32(txtSermonsPerPageSite.Text);
            Admin.SermonDetailPage = txtDetailPage.Text;
            Admin.SeriesDetailPage = txtSeriesDetailPage.Text;
            Admin.BibleVersion = (Admin.BibleVersions)StringEditor.ToEnum(ddBibleVersion.SelectedValue, typeof(Admin.BibleVersions));

            Admin.Update();
            RefreshPage();
        }

        protected void RefreshPage()
        {
            Response.Redirect("Default.aspx");
        }
    }
}
