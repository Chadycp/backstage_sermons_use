﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using backstage.security;

namespace BackstageSermons.components.SeriesFinder
{
    public partial class SeriesFinder : System.Web.UI.UserControl
    {

        #region Properties

        public int Height = 362;
        private int topHeight = 19;
        private int bottomHeight = 27;
        private int buttonBarHeight = 28;

        private string _Top;
        public int Top
        {
            get
            {
                if (_Top == null)
                {
                    if (ArrowPosition == Positions.left)
                        _Top = "-98";
                    else
                        _Top = "34";
                }
                return int.Parse(_Top);
            }
            set { _Top = ((int)value).ToString(); }
        }

        public enum Positions { top, right, bottom, left }
        private Positions _ArrowPosition = Positions.top;
        public Positions ArrowPosition
        {
            get { return _ArrowPosition; }
            set { _ArrowPosition = value; }
        }

        public enum FinderTypes { Picker, Manager }
        private FinderTypes _FinderType = FinderTypes.Manager;
        public FinderTypes FinderType
        {
            get { return _FinderType; }
            set { _FinderType = value; }
        }

        public string TriggerClientID;
        private string _TriggerID;
        public string TriggerID
        {
            get { return _TriggerID; }
            set
            {
                _TriggerID = value;
                if (string.IsNullOrEmpty(_TriggerID))
                {
                    Control ctrl = Page.FindControl(TriggerID);
                    if (ctrl != null)
                        TriggerClientID = ctrl.ClientID;
                }
            }
        }

        public string BreadCrumbRepeaterID
        {
            get
            {
                return (string)ViewState[this.ID + "BreadCrumbRepeaterID"];
            }
            set
            {
                ViewState[this.ID + "BreadCrumbRepeaterID"] = value;
            }
        }

        public string EditSeriesPanelID
        {
            get
            {
                return (string)ViewState[this.ID + "EditSeriesPanelID"];
            }
            set
            {
                ViewState[this.ID + "EditSeriesPanelID"] = value;
            }
        }

        private SeriesML _selectedSeries;
        public SeriesML selectedSeries
        {
            get
            {
                if (_selectedSeries == null)
                    _selectedSeries = seriescollection.FindSeriesByID(SeriesID);
                return _selectedSeries;
            }
            set
            {
                _selectedSeries = value;
            }
        }

        public string BasePage
        {
            get
            {
                return ViewState[this.ID + "BasePage"] == null ? "/backstageIncludes/BackstageEcommerce/modules/products/ProductManager.aspx" : (string)ViewState[this.ID + "BasePage"];
            }
            set
            {
                ViewState[this.ID + "BasePage"] = value;
            }
        }

        //Current Page SeriesID
        public int SeriesID
        {
            get
            {
                object vs = ViewState[this.ID + "SeriesID"];
                return vs == null ? 0 : (int)vs;
            }
            set
            {
                ViewState[this.ID + "SeriesID"] = value;
            }
        }

        public string UseFunction
        {
            get
            {
                return (string)ViewState[this.ID + "UseFunction"];
            }
            set { ViewState[this.ID + "UseFunction"] = value; }
        }

        public string AssociatedLabelID
        {
            get
            {
                return (string)ViewState[this.ID + "AssociatedLabelID"];
            }
            set { ViewState[this.ID + "AssociatedLabelID"] = value; }
        }

        public const string VISIBILITY_VISIBLE = "/backstage/images/icons/eyeIcon.png";
        public const string VISIBILITY_HIDDEN = "/backstage/images/icons/eyeIconClose.png";

        private SeriesCollectionML _seriescollection;
        public SeriesCollectionML seriescollection
        {
            get
            {
                if (_seriescollection == null)
                    _seriescollection = SermonsBLL.Instance.LoadSeriesTree(false);
                return _seriescollection;
            }
        }

        private BreadCrumbCollectionML bcc;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (!Page.IsPostBack)
                {
                    SetSeries();
                    if (Request.QueryString["focusSeries"] == "true")
                        pnlSeriesFinder.Style.Add("display", "block");
                }
                SetStyles();
                buttonAddFolder.Attributes.Add("onclick", "top.location = '/backstageIncludes/BackstageSermons/modules/Sermons/EditSeries.aspx?ParentSeriesID=" + SeriesID.ToString() + "';");
            }
        }

        FinderSeriesCollection fsc = new FinderSeriesCollection();
        public void SetSeries()
        {
            string baseSeriesId = Request.QueryString["baseSeriesId"];
            SeriesCollectionML sc = null;
            fsc = new FinderSeriesCollection();
            if (!string.IsNullOrEmpty(baseSeriesId))
            {
                SeriesML s = sc.FindSeriesByID(int.Parse(baseSeriesId));
                if (s != null)
                {
                    sc = s.ChildSeries;

                    FinderSeries fs = SeriesToFinderSeries(s);
                    fs.Indent = 0;
                    fsc.Add(fs);
                    if (s.IsOpen && (s.ChildSeries != null) && s.ChildSeries.Count > 0)
                    {
                        GetFinderSubSeries(s.ChildSeries, 1);
                    }
                }
            }
            if (sc == null)
            {
                GetFinderSubSeries(seriescollection, 0);
            }

            rptSeries.DataSource = fsc;
            rptSeries.DataBind();

            if (selectedSeries != null)
            {
                SetBreadCrumbs();
                //lblCurrentSeries.Text = backstage.Classes.StringEditor.Truncate(selectedSeries.Name, 20, "...", false);
                //lblCurrentSeries.ToolTip = selectedSeries.Name;
                //lblCurrentSeries.Text = "Select Main Level";
            }
        }

        public string GetFolderImage(string fID)
        {
            return "/backstage/modules/CatalogManager/components/CategoryFinder/images/folder.png";
        }

        protected void GetFinderSubSeries(SeriesCollectionML sc, int Indent)
        {
            foreach (SeriesML s in sc)
            {
                FinderSeries fs = SeriesToFinderSeries(s);
                fs.Indent = Indent;
                fsc.Add(fs);
                if (s.IsOpen && (s.ChildSeries != null) && s.ChildSeries.Count > 0)
                {
                    int SubIndent = Indent + 1;
                    GetFinderSubSeries(s.ChildSeries, SubIndent);
                }
            }
        }

        protected FinderSeries SeriesToFinderSeries(SeriesML series)
        {
            FinderSeries fs = new FinderSeries();
            fs.ID = series.SeriesID.ToString();
            fs.IsOpen = series.IsOpen;
            fs.Name = series.Title;
            fs.Visible = series.Active;
            fs.ChildCount = series.ChildSeries.Count;

            return fs;
        }

        protected void SetBreadCrumbs()
        {
            if (!string.IsNullOrEmpty(BreadCrumbRepeaterID))
            {
                Repeater rpt = (Repeater)this.Parent.FindControl(BreadCrumbRepeaterID);
                if (rpt != null)
                {
                    if (selectedSeries != null)
                    {
                        bcc = new BreadCrumbCollectionML();
                        AddBreadcrumb(selectedSeries);
                        bcc.Reverse();
                        rpt.DataSource = bcc;
                        rpt.DataBind();
                    }
                }
            }
        }

        protected void SetStyles()
        {
            if (FinderType == FinderTypes.Picker)
            {
                buttonAddFolder.Visible = false;
                pnlCurrentSeries.Visible = true;
                btnUseSeries.Visible = true;
            }
            pnlSeries.Style.Add("max-height", (Height - topHeight - bottomHeight - buttonBarHeight).ToString() + "px");
            ((Panel)updateProgressSeries.FindControl("pnlProgress")).Style.Add("height", (Height - topHeight - bottomHeight - buttonBarHeight).ToString() + "px");

            //Set Arrow Location
            if (ArrowPosition == Positions.top)
            {
                backstage.Classes.WebControlHelper.AddCssClass(pnlFinderArrow, "topFinderArrow");
            }
            if (ArrowPosition == Positions.right)
            {
                backstage.Classes.WebControlHelper.AddCssClass(pnlFinderArrow, "rightFinderArrow");
            }
            if (ArrowPosition == Positions.bottom)
            {
                backstage.Classes.WebControlHelper.AddCssClass(pnlFinderArrow, "bottomFinderArrow");
            }
            if (ArrowPosition == Positions.left)
            {
                backstage.Classes.WebControlHelper.AddCssClass(pnlFinderArrow, "leftFinderArrow");
                pnlSeriesFinder.Style.Add("left", "54px");
            }
            pnlSeriesFinder.Style.Add("top", Top.ToString() + "px");
        }

        #region Click Handlers

        protected void btnEditSeries_Click(object sender, EventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            string sID = btn.CommandArgument;
            ScriptManager.RegisterStartupScript(updateSeriesFinder, typeof(UpdatePanel), "editSeries", string.Format("top.window.location = '/backstageIncludes/BackstageSermons/modules/Sermons/EditSeries.aspx?SeriesID={0}';", sID), true);
        }

        protected void lblSeriesArrow_Click(object sender, EventArgs e)
        {
            pnlSeriesFinder.Style.Add("display", "block");
            try
            {
                LinkButton lb = (LinkButton)sender;
                string sID = lb.CommandArgument.ToString();
                SeriesML s = seriescollection.FindSeriesByID(int.Parse(sID));
                s.IsOpen = !s.IsOpen;
                SermonsBLL.Instance.UpdateSeries(s);
                SetSeries();
            }
            catch (Exception err)
            {
                AlertMessage(err.Message);
                Trace.Write("err", err.ToString());
            }
        }

        protected void AlertMessage(string msg)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "alertMessage", "alert(\"" + msg + "\");", true);
        }

        protected void lbVisibility_Click(object sender, EventArgs e)
        {
            pnlSeriesFinder.Style.Add("display", "block");
            try
            {
                LinkButton btn = (LinkButton)sender;
                string sID = btn.CommandArgument.ToString();
                SeriesML s = seriescollection.FindSeriesByID(int.Parse(sID));
                s.Active = !s.Active;

                SermonsBLL.Instance.UpdateSeries(s);
                SetSeries();
            }
            catch (Exception err)
            {
                Trace.Write("err", err.ToString());
            }
        }

        protected void btnUseSeries_Click(object sender, EventArgs e)
        {
            string strSID = "";
            if (sender.GetType() == typeof(ImageButton))
            {
                ImageButton btn = (ImageButton)sender;
                strSID = btn.CommandArgument.ToString();
            }
            else
            {
                LinkButton lb = (LinkButton)sender;
                strSID = lb.CommandArgument.ToString();
            }

            if (string.IsNullOrEmpty(strSID))
                strSID = "0";
            SeriesID = int.Parse(strSID);

            if (FinderType == FinderTypes.Manager)
            {
                string url = string.Format("{0}?SeriesID={1}", BasePage, SeriesID.ToString());

                #region Maintain Querystrings
                string type = Request.QueryString["type"];
                if (!string.IsNullOrEmpty(type))
                    url += "&type=" + type;
                /*
                if (SeriesType == SeriesTypes.Image || SeriesType == SeriesTypes.File) {
                    string PickerType = Request.QueryString["PickerType"];
                    if (!string.IsNullOrEmpty(PickerType))
                        url += "&PickerType=" + PickerType;
                    string Mode = Request.QueryString["Mode"];
                    if (!string.IsNullOrEmpty(Mode))
                        url += "&Mode=" + Mode;
                    string Filter = Request.QueryString["Filter"];
                    if (!string.IsNullOrEmpty(Filter))
                        url += "&Filter=" + Filter;
                    string Caller = Request.QueryString["Caller"];
                    if (!string.IsNullOrEmpty(Caller))
                        url += "&Caller=" + Caller;
                    string baseSeriesID = Request.QueryString["baseSeriesID"];
                    if (!string.IsNullOrEmpty(baseSeriesID))
                        url += "&baseSeriesID=" + baseSeriesID;
                }
                 */
                #endregion

                Response.Redirect(url);
            }
            else
            {
                string sName = "Main Level";
                if (SeriesID != 0)
                    sName = selectedSeries.Title;
                if (!string.IsNullOrEmpty(UseFunction))
                    ScriptManager.RegisterStartupScript(updateSeriesFinder, typeof(UpdatePanel), "useSeries", UseFunction + "(" + SeriesID.ToString() + ", '" + sName + "');", true);
                HideFinder();
                SetSeries();
            }
        }

        protected void btnDeleteSeries_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btn = (ImageButton)sender;
                string sID = btn.CommandArgument.ToString();

                #region Make sure there will be at least 1 series remaining after delete
                bool otherSeries = false;
                foreach (SeriesML se in seriescollection)
                {
                    if (se.SeriesID.ToString() != sID)
                    {
                        otherSeries = true;
                        break;
                    }
                }
                if (!otherSeries)
                    throw new Exception("You must leave at least one series in this manager.");
                #endregion
                SeriesML s = seriescollection.FindSeriesByID(int.Parse(sID));

                bool isCurrentSeries = false;
                if (sID == SeriesID.ToString() || CurrentSeriesIsChild(s))
                {
                    isCurrentSeries = true;
                    //Get parent
                    SeriesML pSeries = seriescollection.FindSeriesByID(s.ParentSeriesID);
                    if (pSeries != null)
                    {
                        //If parent has other children, pick first one. Otherwise, pick the parent
                        if (pSeries.ChildSeries.Count > 1)
                            SeriesID = GetFirstSiblingID(pSeries.ChildSeries);
                        else
                            SeriesID = pSeries.SeriesID;
                    }
                    else
                    {
                        SeriesID = GetFirstSiblingID(seriescollection);
                    }
                }

                SermonsBLL.Instance.DeleteSeries(s);

                if (isCurrentSeries)
                    ReloadPage();
                else
                    SetSeries();
            }
            catch (Exception err)
            {
                Page page = HttpContext.Current.Handler as Page;
                ScriptManager.RegisterStartupScript(page, typeof(Page), "alertError", "alert('" + err.Message.ToString() + "');", true);
            }
            pnlSeriesFinder.Style.Add("display", "block");
        }

        protected bool CurrentSeriesIsChild(SeriesML s)
        {
            bool b = false;
            foreach (SeriesML se in s.ChildSeries)
            {
                if (se.SeriesID == SeriesID)
                {
                    return true;
                }
                else
                    if (CurrentSeriesIsChild(se))
                        return true;
            }
            return b;
        }

        protected int GetFirstSiblingID(SeriesCollectionML sc)
        {
            int sID = 0;
            if (sc.Count > 1)
            {
                foreach (SeriesML se in sc)
                {
                    if (se.SeriesID != SeriesID)
                    {
                        sID = se.SeriesID;
                        break;
                    }
                }
            }
            return sID;
        }

        protected void btnMoveFolderObject_Click(object sender, EventArgs e)
        {
            /*
            ImageButton btn = (ImageButton)sender;
            string sID = Request.Form[hfFolderToMove.UniqueID];
            string wtmID = btn.CommandArgument.ToString();

            SeriesML moveSeries = seriescollection.FindSeriesByID(backstage.Classes.StringEditor.ToInt32(cID));
            SeriesML anchorSeries = seriescollection.FindSeriesByID(backstage.Classes.StringEditor.ToInt32(wtmID));
            if (moveSeries != null && anchorSeries != null)
            {
                switch (btn.CommandName)
                {
                    case "Before":
                        moveSeries.ParentSeriesID = anchorSeries.ParentSeriesID;
                        
                        anchorCat.ParentCategory = seriescollection.FindSeriesByID(anchorCat.ParentCategoryID);
                        CategoryCollectionML cc = null;
                        if (anchorCat.ParentCategory != null)
                            cc = anchorCat.ParentCategory.Childseriescollection;
                        else
                            cc = seriescollection;
                        int i = 0;

                        foreach (CategoryML c in cc)
                        {
                            if (c == anchorCat)
                            {
                                moveCat.Order = i;
                                EcommerceBLL.Instance.UpdateCategory(moveCat);
                                i++;
                            }
                            if (c == moveCat)
                                continue;

                            c.Order = i;
                            EcommerceBLL.Instance.UpdateCategory(c);

                            i++;
                        }
                        break;
                    case "After":
                        moveCat.ParentCategoryID = anchorCat.ParentCategoryID;
                        anchorCat.ParentCategory = seriescollection.FindSeriesByID(anchorCat.ParentCategoryID);
                        if (anchorCat.ParentCategory != null)
                            cc = anchorCat.ParentCategory.ChildCategories;
                        else
                            cc = seriescollection;
                        i = 0;

                        foreach (CategoryML c in anchorCat.ParentCategory.ChildCategories)
                        {
                            if (c == moveCat)
                                continue;

                            c.Order = i;
                            EcommerceBLL.Instance.UpdateCategory(c);

                            if (c == anchorCat)
                            {
                                i++;
                                moveCat.Order = i;
                                EcommerceBLL.Instance.UpdateCategory(moveCat);

                            }
                            i++;
                        }
                        break;
                    case "Into":
                        moveCat.ParentCategoryID = anchorCat.CategoryID;
                        i = 0;
                        foreach (CategoryML c in anchorCat.ChildCategories)
                        {
                            if (c == moveCat)
                                continue;
                            c.Order = i;
                            EcommerceBLL.Instance.UpdateCategory(c);

                            i++;
                        }
                        moveCat.Order = i;
                        EcommerceBLL.Instance.UpdateCategory(moveCat);

                        if (!anchorCat.IsOpen)
                        {
                            anchorCat.IsOpen = true;
                            EcommerceBLL.Instance.UpdateCategory(anchorCat);
                        }
                        break;
                }
            }

            _seriescollection = null;
            SetCats();
            pnlCategoryFinder.Style.Add("display", "block");
            */
        }

        protected void lbClose_Click(object sender, EventArgs e)
        {
            HideFinder();
        }

        #endregion

        protected void ReloadPage()
        {
            string url = string.Format("{0}?SeriesID={1}", BasePage, SeriesID.ToString());
            string type = Request.QueryString["type"];
            if (!string.IsNullOrEmpty(type))
                url += "&type=" + type;

            Response.Redirect(url);
        }

        protected void HideFinder()
        {
            pnlSeriesFinder.Style.Add("display", "none");
        }

        protected void AddBreadcrumb(SeriesML s)
        {
            BreadCrumbML bc = new BreadCrumbML();
            bc.ID = s.SeriesID.ToString();
            bc.Name = s.Title;

            string url = string.Format("{0}?SeriesID={1}", BasePage, s.SeriesID.ToString());

            bc.URL = url;
            bcc.Add(bc);
            if (s.SeriesID.ToString() != Request.QueryString["baseSeriesID"] && s.ParentSeriesID != 0)
            {
                SeriesML se = seriescollection.FindSeriesByID(s.ParentSeriesID);
                if (se != null)
                    AddBreadcrumb(se);
            }
        }
    }

    public class FinderSeries
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public bool Visible { get; set; }
        public bool IsOpen { get; set; }
        public int Indent { get; set; }
        public int ChildCount { get; set; }
    }

    public class FinderSeriesCollection : List<FinderSeries> { }
}