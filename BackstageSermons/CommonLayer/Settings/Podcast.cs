﻿using System;
using System.Collections.Generic;
using backstage.Classes;

namespace BackstageSermons.Settings
{
    public static class Podcast
    {
        public static string ID
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("PodcastID").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("PodcastID").Value = value;
            }
        }

        public static string Title
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("PodcastTitle").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("PodcastTitle").Value = value;
            }
        }

        public static string Link
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("PodcastLink").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("PodcastLink").Value = value;
            }
        }

        public static string Language
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("PodcastLanguage").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("PodcastLanguage").Value = value;
            }
        }

        public static string Copyright
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("PodcastCopyright").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("PodcastCopyright").Value = value;
            }
        }

        public static string Subtitle
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("PodcastSubtitle").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("PodcastSubtitle").Value = value;
            }
        }

        public static string Author
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("PodcastAuthor").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("PodcastAuthor").Value = value;
            }
        }

        public static string Summary
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("PodcastSummary").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("PodcastSummary").Value = value;
            }
        }

        public static string Description
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("PodcastDescription").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("PodcastDescription").Value = value;
            }
        }

        public static string OwnerName
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("PodcastOwnerName").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("PodcastOwnerName").Value = value;
            }
        }

        public static string OwnerEmail
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("PodcastOwnerEmail").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("PodcastOwnerEmail").Value = value;
            }
        }

        public static string ImageID
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("PodcastImageID").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("PodcastImageID").Value = value;
            }
        }

        public static int MaxSermons
        {
            get
            {
                return StringEditor.ToInt32(SermonsCache.SettingCollection.Setting("PodcastMaxSermons").Value);
            }
            set
            {
                SermonsCache.SettingCollection.Setting("PodcastMaxSermons").Value = value.ToString();
            }
        }

        public static List<int> SeriesIDs
        {
            get
            {
                return StringEditor.StringToIntList(SermonsCache.SettingCollection.Setting("PodcastSeriesIDs").Value, ",");
            }
            set
            {
                SermonsCache.SettingCollection.Setting("PodcastMaxSermons").Value = StringEditor.IEnumerableToString(value, ",");
            }
        }

        public enum PodcastDateRanges { None, Month, ThreeMonth, SixMonth, Year };
        public static PodcastDateRanges DateRange
        {
            get
            {
                return (PodcastDateRanges)StringEditor.ToEnum(SermonsCache.SettingCollection.Setting("PodcastDateRange").Value, typeof(PodcastDateRanges));
            }
            set
            {
                SermonsCache.SettingCollection.Setting("PodcastDateRange").Value = value.ToString();
            }
        }

        public static void Update()
        {
            SettingCollectionML sc = new SettingCollectionML();
            sc.Add(SermonsCache.SettingCollection.Setting("PodcastID"));
            sc.Add(SermonsCache.SettingCollection.Setting("PodcastTitle"));
            sc.Add(SermonsCache.SettingCollection.Setting("PodcastLink"));
            sc.Add(SermonsCache.SettingCollection.Setting("PodcastLanguage"));
            sc.Add(SermonsCache.SettingCollection.Setting("PodcastCopyright"));
            sc.Add(SermonsCache.SettingCollection.Setting("PodcastSubtitle"));
            sc.Add(SermonsCache.SettingCollection.Setting("PodcastAuthor"));
            sc.Add(SermonsCache.SettingCollection.Setting("PodcastSummary"));
            sc.Add(SermonsCache.SettingCollection.Setting("PodcastDescription"));
            sc.Add(SermonsCache.SettingCollection.Setting("PodcastOwnerName"));
            sc.Add(SermonsCache.SettingCollection.Setting("PodcastOwnerEmail"));
            sc.Add(SermonsCache.SettingCollection.Setting("PodcastImageID"));
            sc.Add(SermonsCache.SettingCollection.Setting("PodcastMaxSermons"));
            sc.Add(SermonsCache.SettingCollection.Setting("PodcastSeriesIDs"));
            sc.Add(SermonsCache.SettingCollection.Setting("PodcastDateRange"));
            SermonsBLL.Instance.UpdateSettingCollection(sc);
        }
    }
}
