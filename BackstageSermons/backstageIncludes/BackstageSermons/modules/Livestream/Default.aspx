﻿<%@ Page Language="C#" MasterPageFile="~/backstageIncludes/BackstageSermons/Sermons.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BackstageSermons.modules.Livestream.Default" EnableEventValidation="false" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <title>Backstage Sermons :: SermonAudio Manager</title>
    <link href="/backstageIncludes/BackstageSermons/modules/Sermons/css/SermonStyles.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
	    .copy p {
	    	line-height: 20px;
	    	}
	    .copy ul, .copy ol {
	    	line-height: 20px;
	    	padding-left: 25px;
	    	}
	    .leftColumn input[type="text"] {
	    	width: 200px;
	    	}
	    .leftColumn input[type="checkbox"] {
            margin-left: 0px;
        }
	</style>
    <script src="/backstageIncludes/BackstageSermons/modules/Livestream/js/livestream.js" type="text/javascript"></script>
    <script type="text/javascript">
        function equalHeight(group) {
            var tallest = 0;
            group.each(function () {
                var thisHeight = $(this).height();
                if (thisHeight > tallest) {
                    tallest = thisHeight;
                }
            });
            group.height(tallest);
        }
        $(document).ready(function () {
            equalHeight($("div.sectionContent").children('div').children('div'));
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
<asp:Label runat="server" ID="lblResult" style="color: #990000;"></asp:Label>

<div class="sectionContent clearfix">
	<div class="leftColumnWrapper" style="width:270px">
		<div class="leftColumn">
			<div style="margin-left:1px;border-bottom:1px solid #bec7cd">
				<div class="padding20">
					<label class="full"><asp:Label ID="lblLink" runat="server" Text="Please paste in the link to your streaming server."></asp:Label></label>
					<asp:TextBox runat="server" ID="txtLink"></asp:TextBox><br /><br />
                    OR<br />
                    <label class="full"><asp:Label ID="lblEmbedCode" runat="server" Text="Please paste in the embed code from your livestream provider."></asp:Label></label>
                    <asp:TextBox runat="server" ID="txtEmbedCode" TextMode="MultiLine" Width="200" Height="150" style="max-height: 200px;max-width: 200px;"></asp:TextBox><br />

					<label class="full"><asp:Label ID="lblTitle" runat="server" Text="Announce your stream on the sermons page. Please enter text below."></asp:Label></label>
					<asp:TextBox runat="server" ID="txtTitle" TextMode="MultiLine" Width="200" Height="50" style="max-height: 100px;max-width: 200px;"></asp:TextBox><br />
					
					<label class="full"><asp:CheckBox runat="server" ID="cbEnablePassword" Text="Enable Stream Password" AutoPostBack="true" OnCheckedChanged="cbEnablePassword_OnCheckedChanged" /></label>
					<asp:Panel runat="server" ID="pnlPassword" Visible="false">
                        <asp:Repeater runat="server" ID="rptLiveStreamPWs">
                            <HeaderTemplate>
                                <label class="full"><asp:Label ID="Label1" runat="server" Text="Please enter the password/s for your stream."></asp:Label></label>
                            </HeaderTemplate>
                            <ItemTemplate>
					            <input type="text" value="<%# Container.DataItem %>"/>
                                <label class="trash">
									<asp:LinkButton runat="server" ID="lbRemovePassword" OnClick="lbDeletePassword_Click" CommandArgument='<%# Container.ItemIndex %>'><img src="/backstage/images/icon-trash.png" alt="Delete Password" title="Delete Password" style="cursor:pointer" /></asp:LinkButton>
								</label>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:TextBox ID="txtAddPassword" runat="server"></asp:TextBox>
                        <label class="add">
							<asp:LinkButton runat="server" ID="lbAddPassword" OnClick="lbAddPassword_Click" ><img src="/backstage/images/icon-duplicate.png" alt="Add Password" title="Add Password" style="cursor:pointer" /></asp:LinkButton>
						</label>
					</asp:Panel>
					
					<label class="full"><asp:CheckBox runat="server" ID="cbEnableLimit" Text="Enable Stream Limit" AutoPostBack="true" OnCheckedChanged="cbEnableLimit_OnCheckedChanged" /></label>
					<asp:Panel runat="server" ID="pnlLimit" Visible="false">
					    <label class="full"><asp:Label ID="lblLimit" runat="server" Text="Please enter the max number of simultaneous streamers."></asp:Label></label>
					    <asp:TextBox runat="server" ID="txtLimit"></asp:TextBox>
					</asp:Panel>
				</div>
			</div>
			<div style="margin-left:1px;border-top:1px solid #ecf2f5">
				<h1>Live Time Ranges</h1>
				<ul>
					<cms:SmartRepeater runat="server" ID="rptDateRanges">
						<ItemTemplate>
							<li>
								<span><%# Container.DataItem %>
									<label class="trash">
									    <asp:LinkButton runat="server" ID="lbDeleteDataRange" OnClick="lbDeleteDateRange_Click" CommandArgument='<%# Container.ItemIndex %>'><img src="/backstage/images/icon-trash.png" alt="Delete Time Range" title="Delete Time Range" style="cursor:pointer" /></asp:LinkButton>
									</label>
								</span>
							</li>
						</ItemTemplate>
					</cms:SmartRepeater>
				</ul>
				<div style="padding:0 20px 30px">
					<label class="full">Day of week</label>
					<asp:DropDownList runat="server" ID="ddlDayOfWeek" CssClass="chosen-select" style="width: 220px">
					    <asp:ListItem Value="0" Text="Sunday"></asp:ListItem>
					    <asp:ListItem Value="1" Text="Monday"></asp:ListItem>
					    <asp:ListItem Value="2" Text="Tuesday"></asp:ListItem>
					    <asp:ListItem Value="3" Text="Wednesday"></asp:ListItem>
					    <asp:ListItem Value="4" Text="Thursday"></asp:ListItem>
					    <asp:ListItem Value="5" Text="Friday"></asp:ListItem>
					    <asp:ListItem Value="6" Text="Saturday"></asp:ListItem>
					</asp:DropDownList>
					
					<label class="full">Start Time</label>
					<asp:DropDownList runat="server" ID="ddlStartHour" CssClass="chosen-select" style="width: 50px;">
					    <asp:ListItem Value="0" Text="12"></asp:ListItem>
					    <asp:ListItem Text="1"></asp:ListItem>
					    <asp:ListItem Text="2"></asp:ListItem>
					    <asp:ListItem Text="3"></asp:ListItem>
					    <asp:ListItem Text="4"></asp:ListItem>
					    <asp:ListItem Text="5"></asp:ListItem>
					    <asp:ListItem Text="6"></asp:ListItem>
					    <asp:ListItem Text="7"></asp:ListItem>
					    <asp:ListItem Text="8"></asp:ListItem>
					    <asp:ListItem Text="9"></asp:ListItem>
					    <asp:ListItem Text="10"></asp:ListItem>
					    <asp:ListItem Text="11"></asp:ListItem>
					</asp:DropDownList>
					<asp:DropDownList runat="server" ID="ddlStartMinute" CssClass="chosen-select" style="width: 50px;" >
					    <asp:ListItem Text="00"></asp:ListItem>
					    <asp:ListItem Text="05"></asp:ListItem>
					    <asp:ListItem Text="10"></asp:ListItem>
					    <asp:ListItem Text="15"></asp:ListItem>
					    <asp:ListItem Text="20"></asp:ListItem>
					    <asp:ListItem Text="25"></asp:ListItem>
					    <asp:ListItem Text="30"></asp:ListItem>
					    <asp:ListItem Text="35"></asp:ListItem>
					    <asp:ListItem Text="40"></asp:ListItem>
					    <asp:ListItem Text="45"></asp:ListItem>
					    <asp:ListItem Text="50"></asp:ListItem>
					    <asp:ListItem Text="55"></asp:ListItem>
					</asp:DropDownList>
					<asp:DropDownList runat="server" ID="ddlStartTimePeriod" CssClass="chosen-select" style="width: 60px;">
					    <asp:ListItem Text="AM"></asp:ListItem>
					    <asp:ListItem Text="PM"></asp:ListItem>
					</asp:DropDownList>
					
					<label class="full" style="clear: both; margin-top: 12px;">End Time</label>
					<asp:DropDownList runat="server" ID="ddlEndHour" CssClass="chosen-select" style="width: 50px;">
					    <asp:ListItem Value="0" Text="12"></asp:ListItem>
					    <asp:ListItem Text="1"></asp:ListItem>
					    <asp:ListItem Text="2"></asp:ListItem>
					    <asp:ListItem Text="3"></asp:ListItem>
					    <asp:ListItem Text="4"></asp:ListItem>
					    <asp:ListItem Text="5"></asp:ListItem>
					    <asp:ListItem Text="6"></asp:ListItem>
					    <asp:ListItem Text="7"></asp:ListItem>
					    <asp:ListItem Text="8"></asp:ListItem>
					    <asp:ListItem Text="9"></asp:ListItem>
					    <asp:ListItem Text="10"></asp:ListItem>
					    <asp:ListItem Text="11"></asp:ListItem>
					</asp:DropDownList>
					<asp:DropDownList runat="server" ID="ddlEndMinute" CssClass="chosen-select" style="width: 50px;">
					    <asp:ListItem Text="00"></asp:ListItem>
					    <asp:ListItem Text="05"></asp:ListItem>
					    <asp:ListItem Text="10"></asp:ListItem>
					    <asp:ListItem Text="15"></asp:ListItem>
					    <asp:ListItem Text="20"></asp:ListItem>
					    <asp:ListItem Text="25"></asp:ListItem>
					    <asp:ListItem Text="30"></asp:ListItem>
					    <asp:ListItem Text="35"></asp:ListItem>
					    <asp:ListItem Text="40"></asp:ListItem>
					    <asp:ListItem Text="45"></asp:ListItem>
					    <asp:ListItem Text="50"></asp:ListItem>
					    <asp:ListItem Text="55"></asp:ListItem>
					</asp:DropDownList>						
					<asp:DropDownList runat="server" ID="ddlEndTimePeriod" CssClass="chosen-select" style="width: 60px;">
					    <asp:ListItem Text="AM"></asp:ListItem>
					    <asp:ListItem Text="PM"></asp:ListItem>
					</asp:DropDownList>
					<br /><br />
						<asp:Button runat="server" ID="btnAddDateRange" Text="+ Add Date Range" CssClass="bxButton2 green" style="margin-top:8px; margin-bottom: 45px;" OnClick="btnAddDateRange_Click" />
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="rightColumnWrapper" style="width:710px">
		<div class="rightColumn">
			<div class="toolbarContainer">
				<div class="toolbar clearfix">
					<span class="sectionTitle">
						<span>Live Streaming Settings (Audio/Video)</span>
					</span>
					<div class="rightButtons clearfix">
						<asp:LinkButton runat="server" ID="btnUpdate" CssClass="bxButton2 green" OnClick="btnUpdate_Click" Text="Save Changes" />
					</div>
				</div>
			</div>
			<div class="tableContainer padding20">
				<div class="copy">
					<h1>How to setup streaming on your site.</h1>
					<p>Live streaming is very helpful for events where some people cannot be present. Here are the steps to setup live streaming.</p>
					<ol>
						<li>You will need a webcam or a video camera to record the event.</li>
						<li>You will also need an internet connection. This will enable you to stream the video for other viewers to see. You must have a stable broadband Internet connection and enough bandwidth to support your streaming.</li>
						<li>You will need a computer. The next step is to use your computer as a media encoder for your streaming. You will need to use a fast computer so that the streaming media will be able to process the data that is being encoded faster. Setup your media encoder to be able to send quality videos online.</li>
						<li>Streaming server. After your setup is ready, you will have to find a streaming server (such as <a style="font-weight: bold;" href="http://www.livestream.com/platform/broadcastlive">www.livestream.com</a>) where you can stream your live videos online to your audience.</li>
						<li>After everything is setup, paste in the link to your streaming server on the left.</li>
					</ol>

                    <h1>Live Stream Copy</h1>
                    <cms:CKEditor runat="server" ID="ckCopy" Height="400"></cms:CKEditor>
				</div>
			</div>
		</div>
	</div>
</div>
</asp:Content>