﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using backstage.bxpages;
using backstage.security;
using BackstageSermons.Settings;
using backstage.Classes;

namespace BackstageSermons.modules.Sermons
{
    public partial class EditSermon : BackstagePage
    {
        #region Public Objects
        private string _strSermonID;
        public string strSermonID
        {
            get
            {
                if (_strSermonID == null)
                    _strSermonID = Request.QueryString["SermonID"];
                return _strSermonID;
            }
            set { _strSermonID = value; }
        }

        private int _SermonID;
        public int SermonID
        {
            get
            {
                if (_SermonID == 0 && !string.IsNullOrEmpty(strSermonID))
                    _SermonID = int.Parse(strSermonID);
                return _SermonID;
            }
            set { _SermonID = value; }
        }

        private string _strSeriesID;
        public string strSeriesID
        {
            get
            {
                if (_strSeriesID == null)
                    _strSeriesID = Request.QueryString["SeriesID"];
                return _strSeriesID;
            }
            set { _strSeriesID = value; }
        }

        private int _SeriesID;
        public int SeriesID
        {
            get
            {
                if (_SeriesID == 0 && !string.IsNullOrEmpty(strSeriesID))
                    _SeriesID = int.Parse(strSeriesID);
                return _SeriesID;
            }
            set { _SeriesID = value; }
        }

        private SeriesCollectionML _series;
        public SeriesCollectionML series
        {
            get
            {
                if (_series == null)
                    _series = SermonsBLL.Instance.LoadSeriesTree(false);
                return _series;
            }
            set { _series = value; }
        }

        public string visibleImage = "/backstage/images/icons/eyeIcon.png";
        public string invisibleImage = "/backstage/images/icons/eyeIconClose.png";
        public string visibleLabel = "This sermon is visible.";
        public string invisibleLabel = "This sermon is invisible.";
        public string choosePhoto = "/backstageincludes/backstageecommerce/images/choosePhoto.jpg";

        private SermonML _sermon;
        public SermonML sermon
        {
            get
            {
                if (_sermon == null)
                {
                    if (!string.IsNullOrEmpty(strSermonID))
                    {
                        try
                        {
                            _sermon = SermonsBLL.Instance.LoadSermon(SermonID, true);

                            _sermon.FieldValues = SermonsBLL.Instance.LoadAllSermonFieldValuesBySermonID(SermonID);
                        }
                        catch (Exception err)
                        {
                            Trace.Write("Error Loading Sermon", err.ToString());
                        }
                    }
                }
                return _sermon;
            }
            set
            {
                _sermon = value;
            }
        }

        private SermonFieldCollectionML _sermonFields = null;
        public SermonFieldCollectionML SermonFields
        {
            get
            {
                if (_sermonFields == null)
                {
                    int sermonId = -1;
                    if (!string.IsNullOrEmpty(strSermonID))
                    {
                        if (!int.TryParse(strSermonID, out sermonId))
                            sermonId = -1;
                    }

                    //-- Should add in option for multiple series later, instead of -1
                    _sermonFields = SermonsBLL.Instance.LoadAllSermonFieldsForSermon(sermonId, -1);
                }

                return _sermonFields;
            }
            set
            {
                _sermonFields = value;
            }
        }

        private TopicCollectionML _tc;
        public TopicCollectionML tc
        {
            get
            {
                if (_tc == null)
                {
                    _tc = SermonsBLL.Instance.LoadAllTopics();
                    if (_tc == null)
                        _tc = new TopicCollectionML();
                }
                return _tc;
            }
            set { _tc = value; }
        }

        public string topicIDs
        {
            get
            {
                return ViewState["sermonTopicIDs"] == null ? "" : (string)ViewState["sermonTopicIDs"];
            }
            set { ViewState["sermonTopicIDs"] = value; }
        }

        public string topicTitles
        {
            get
            {
                return ViewState["sermonTopicTitles"] == null ? "" : (string)ViewState["sermonTopicTitles"];
            }
            set { ViewState["sermonTopicTitles"] = value; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SetVisibility();
            if (sermon == null)
            {
                sermon = new SermonML();
                sermon.Series = new SeriesCollectionML();
                sermon.Topics = new TopicCollectionML();
            }
            if (Page.IsPostBack)
            {
                if (!ScriptManager.GetCurrent(this.Page).IsInAsyncPostBack)
                {
                    SetSeriesDefaults();
                }
            }
            else
            {
                backstage.Classes.CommonInfo.BibleBookCollectionML bcc = new backstage.Classes.CommonInfo.BibleBookCollectionML();
                ddlPassageBook.DataSource = bcc;
                ddlPassageBook.DataBind();

                ddlSpeaker.DataSource = SermonsBLL.Instance.LoadAllSpeakers();
                ddlSpeaker.DataBind();


                LoadTopics();
                LoadSermon();
                SetSeriesDefaults();
            }

        }

        #region Default Methods

        protected void SetVisibility()
        {
            phFeatured.Visible = Admin.EnableFeatured;
        }

        protected void LoadSermon()
        {
            if (sermon.SermonID == 0)
            {
                h1Title.InnerHtml = "Create New Sermon";
                btnCreate1.Visible = true;
                //btnCreate2.Visible = true;
                imgVisibility.ImageUrl = invisibleImage;
                lblVisibility.Text = invisibleLabel;
                txtVisibility.Text = "false";
                txtDate.Text = DateTime.Now.ToString("MM/dd/yyyy");

                //imgPhoto.ImageUrl = choosePhoto;

                MarkUsedTopics();

                //Select Current Series
                ScriptManager.RegisterStartupScript(this, this.GetType(), "checkSeries", "$(document).ready(function(){$('#ctl00_ctl00_cphContent_cphContent_series" + SeriesID.ToString() + "').click();});", true);
            }
            else
            {
                FillSermonInfo();
                btnUpdate1.Visible = true;
                //btnUpdate2.Visible = true;
            }

            FillSermonFields();
        }

        protected void FillSermonInfo()
        {
            #region Visibility
            h1Title.InnerHtml = "You Are Editing";
            spanTitle.InnerText = sermon.Title;
            if (sermon.Available)
            {
                imgVisibility.ImageUrl = visibleImage;
                lblVisibility.Text = visibleLabel;
                txtVisibility.Text = "true";
            }
            else
            {
                imgVisibility.ImageUrl = invisibleImage;
                lblVisibility.Text = invisibleLabel;
                txtVisibility.Text = "false";
            }
            #endregion

            #region Sermon Information
            //if (string.IsNullOrEmpty(sermon.TitleGraphic))
            //sermon.TitleGraphic = choosePhoto;
            //imgPhoto.ImageUrl = sermon.TitleGraphic;
            //txtImgSrc.Text = sermon.TitleGraphic;
            if (!string.IsNullOrEmpty(sermon.TitleGraphic))
                ipTitleGraphic.ImageID = sermon.TitleGraphic;

            txtTitle.Text = sermon.Title;
            cbFeatured.Checked = sermon.Featured;
            ddlSpeaker.SelectedValue = sermon.Speaker.ToString();
            txtDate.Text = sermon.Date.ToString("MM/dd/yyyy");
            txtTime.Text = sermon.Time;
            ddlPassageBook.Text = sermon.PassageBook;
            txtPassageChapter.Text = sermon.PassageChapter;
            txtPassageVerse.Text = sermon.PassageVerse;
            ckPassageCopy.Text = sermon.PassageCopy;

            ctrlSEOEditor.txtPageTitle.Text = sermon.SEOTitle;
            ctrlSEOEditor.lblPageTitleCount.Text = "(" + ctrlSEOEditor.txtPageTitle.Text.Length.ToString() + " characters)";
            //ctrlSEOEditor.pageTitleWarning.Style.Add("display", ctrlSEOEditor.txtPageTitle.Text.Length > 70 ? "inline" : "none");
            //ctrlSEOEditor.pageTitleWarning.Attributes.Add("title", "Title should be under 70 characters. You currently have " + ctrlSEOEditor.txtPageTitle.Text.Length.ToString() + ".");

            ctrlSEOEditor.txtPageDescription.Text = sermon.SEODescription;
            ctrlSEOEditor.lblPageDescriptionCount.Text = "(" + ctrlSEOEditor.txtPageDescription.Text.Length.ToString() + " characters)";
            //ctrlSEOEditor.pageDescriptionWarning.Style.Add("display", ctrlSEOEditor.txtPageDescription.Text.Length > 155 ? "inline" : "none");
            //ctrlSEOEditor.pageDescriptionWarning.Attributes.Add("title", "Description should be under 155 characters. You currently have " + ctrlSEOEditor.txtPageDescription.Text.Length.ToString() + ".");

            ckSermonNotes.Text = sermon.Description;
            #endregion

            #region Files

            #region AudioFiles
            ddlAudioLocation.SelectedValue = sermon.AudioLocation.ToString();

            txtBxAudioFile.Text = sermon.BxAudioFile;
            txtExternalAudioLink.Text = sermon.ExternalAudioLink;
            txtSermonAudioID.Text = sermon.SermonAudioID;
            #endregion

            #region VideoFiles
            ddlVideoLocation.SelectedValue = sermon.VideoLocation.ToString();

            txtBxVideoFile.Text = sermon.BxVideoFile;
            txtExternalVideoLink.Text = sermon.ExternalVideoLink;
            txtYouTubeID.Text = sermon.YouTubeID;
            #endregion

            txtOutline.Text = sermon.Outline;

            #endregion

            #region Sermon Topics


            StringBuilder sbIDs = new StringBuilder();
            StringBuilder sbTitles = new StringBuilder();

            foreach (TopicML topic in sermon.Topics)
            {
                if (sbIDs.Length > 0)
                {
                    sbIDs.Append(",");
                    sbTitles.Append(",");
                }
                sbIDs.Append(topic.TopicID.ToString());
                sbTitles.Append(Server.UrlEncode(topic.Title));
            }

            topicIDs = sbIDs.ToString();
            topicTitles = sbTitles.ToString();

            BindSelectedTopics();
            MarkUsedTopics();

            #endregion
        }

        protected void FillSermonFields()
        {
            foreach (SermonFieldML field in SermonFields)
            {
                SermonFieldValueML val = sermon.FieldValues.FindBySermonIDAndFieldID(sermon.SermonID, field.FieldID);
                if (val != null)
                    field.Value = val.Value;
                else
                    field.Value = field.DefaultValue;
            }

            rptCustomFields.DataSource = SermonFields;
            rptCustomFields.DataBind();
        }

        protected void rptCustomFields_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                SermonFieldML field = e.Item.DataItem as SermonFieldML;

                switch (field.FieldType.ToLower())
                {
                    case "richtext":
                        Control c = e.Item.FindControl("ckCustom");
                        if (c != null)
                            c.Visible = true;
                        break;
                    case "text":
                        c = e.Item.FindControl("txtCustom");
                        if (c != null)
                            c.Visible = true;
                        break;
                    case "bool":
                        c = e.Item.FindControl("rblBool");
                        if (c != null)
                            c.Visible = true;
                        break;
                    case "file":
                        c = e.Item.FindControl("fpCustom");
                        if (c != null)
                            c.Visible = true;
                        break;
                    case "image":
                        c = e.Item.FindControl("ipCustom");
                        if (c != null)
                            c.Visible = true;
                        break;
                    case "multipleimages":
                        c = e.Item.FindControl("mipCustom");
                        if (c != null)
                            c.Visible = true;
                        break;
                    case "usercontrol":
                        c = e.Item.FindControl("cfCustom");
                        if (c != null)
                            c.Visible = true;
                        break;
                }
            }
            catch (Exception err)
            {
                Trace.Write("err", err.ToString());
            }
        }


        #endregion

        #region Topic Methods

        protected void LoadTopics()
        {
            rptTopics.DataSource = tc;
            rptTopics.DataBind();
        }

        protected void MarkUsedTopics()
        {
            TopicCollectionML topics = new TopicCollectionML();

            if (topicIDs.Length > 0)
            {
                string[] ids = topicIDs.Split(',');
                string[] titles = topicTitles.Split(',');

                for (int i = 0; i < ids.Length; i++)
                {
                    TopicML topic = new TopicML();
                    topic.TopicID = int.Parse(ids[i]);
                    topic.Title = titles[i];
                    topics.Add(topic);
                }
            }

            foreach (RepeaterItem ri in rptTopics.Items)
            {
                LinkButton lb = (LinkButton)ri.FindControl("lbTopic");
                bool b = false;
                foreach (TopicML t in topics)
                {
                    if (t.TopicID.ToString() == lb.CommandArgument)
                        b = true;
                }
                lb.Attributes.Add("sermonTopic", b.ToString());
            }
        }

        protected void AddTopicToSelectedList(TopicML topic)
        {
            if (topicIDs.Length > 0)
                topicIDs += ",";
            topicIDs += topic.TopicID.ToString();

            if (topicTitles.Length > 0)
                topicTitles += ",";
            topicTitles += Server.UrlEncode(topic.Title);

            BindSelectedTopics();
            MarkUsedTopics();

            txtTopic.Text = "";
            txtTopic.Focus();
        }

        protected void BindSelectedTopics()
        {
            TopicCollectionML topics = new TopicCollectionML();

            if (topicIDs.Length > 0)
            {

                string[] ids = topicIDs.Split(',');
                string[] titles = topicTitles.Split(',');

                for (int i = 0; i < ids.Length; i++)
                {
                    TopicML topic = new TopicML();
                    topic.TopicID = int.Parse(ids[i]);
                    topic.Title = titles[i];
                    topics.Add(topic);
                }
            }
            rptSelectedTopics.DataSource = topics;
            rptSelectedTopics.DataBind();
        }

        protected void lbTopic_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            TopicML topic = new TopicML();
            topic.Title = lb.CommandName;
            topic.TopicID = int.Parse(lb.CommandArgument);

            AddTopicToSelectedList(topic);
        }

        protected void btnNewTopic_Click(object sender, EventArgs e)
        {
            try
            {
                bool b = false;
                foreach (TopicML t in tc)
                {
                    if (t.Title.ToLower() == txtTopic.Text.ToLower())
                    {
                        b = true;
                        break;
                    }
                }
                if (b)
                    throw new Exception("The topic you tried to create already exists! Please select a topic from the suggestion menu or create a new one.");

                TopicML topic = new TopicML();
                topic.Title = txtTopic.Text;
                SermonsBLL.Instance.InsertTopic(topic);

                //reset Topic Collection
                _tc = null;
                LoadTopics();

                AddTopicToSelectedList(topic);
            }
            catch (Exception err)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "errorMessage", "alert(\"" + err.Message + "\");", true);
            }
        }

        protected void btnDeleteSermonTopic_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            string id = btn.CommandArgument;
            string[] ids = topicIDs.Split(',');
            string[] titles = topicTitles.Split(',');
            StringBuilder sbIDs = new StringBuilder();
            StringBuilder sbTitles = new StringBuilder();

            for (int i = 0; i < ids.Length; i++)
            {
                string s = ids[i];
                if (s != id)
                {
                    if (sbIDs.Length > 0)
                        sbIDs.Append(",");
                    sbIDs.Append(s);
                    if (sbTitles.Length > 0)
                        sbTitles.Append(",");
                    sbTitles.Append(titles[i]);
                }
            }

            topicIDs = sbIDs.ToString();
            topicTitles = sbTitles.ToString();


            BindSelectedTopics();
            MarkUsedTopics();

        }

        #endregion

        #region Set Methods
        protected void SetSermonValues()
        {
            #region Visibility
            if (txtVisibility.Text == "true")
            {
                sermon.Available = true;
            }
            else
            {
                sermon.Available = false;
            }
            #endregion

            #region Sermon Information
            //sermon.TitleGraphic = txtImgSrc.Text;
            sermon.TitleGraphic = ipTitleGraphic.ImageID;
            sermon.Title = txtTitle.Text;
            sermon.Featured = cbFeatured.Checked;
            sermon.Speaker = int.Parse(ddlSpeaker.SelectedValue);
            sermon.Date = Convert.ToDateTime(txtDate.Text);
            sermon.Time = txtTime.Text;
            sermon.PassageBook = ddlPassageBook.Text;
            sermon.PassageChapter = txtPassageChapter.Text;
            sermon.PassageVerse = txtPassageVerse.Text;
            sermon.PassageCopy = ckPassageCopy.Text;
            sermon.Description = Server.HtmlDecode(ckSermonNotes.Text);
            sermon.SEODescription = ctrlSEOEditor.txtPageDescription.Text;
            sermon.SEOTitle = ctrlSEOEditor.txtPageTitle.Text;
            #endregion

            #region Files

            #region Audio Files
            string al = ddlAudioLocation.SelectedValue;
            if (Enum.IsDefined(typeof(AudioLocation), al))
                sermon.AudioLocation = (AudioLocation)Enum.Parse(typeof(AudioLocation), al, true);
            else
                sermon.AudioLocation = AudioLocation.Bx;
            sermon.BxAudioFile = txtBxAudioFile.Text;
            sermon.ExternalAudioLink = txtExternalAudioLink.Text;
            sermon.SermonAudioID = StringEditor.RemoveWhitespace(txtSermonAudioID.Text).Replace(" ", "");
            #endregion

            #region Video Files
            string vl = ddlVideoLocation.SelectedValue;
            if (Enum.IsDefined(typeof(VideoLocation), vl))
                sermon.VideoLocation = (VideoLocation)Enum.Parse(typeof(VideoLocation), vl, true);
            else
                sermon.VideoLocation = VideoLocation.Bx;
            sermon.BxVideoFile = txtBxVideoFile.Text;
            sermon.ExternalVideoLink = txtExternalVideoLink.Text;
            if (sermon.VideoLocation == VideoLocation.YouTube)
            {
                string YoutubeID = rfvYouTubeID.YoutubeID;
                if (rfvYouTubeID.IsValid && !string.IsNullOrEmpty(YoutubeID))
                {
                    sermon.YouTubeID = YoutubeID;
                }
                else
                {
                    pnlDigitalFields.Style.Add("display", "block");
                    throw new Exception(rfvYouTubeID.InvalidMessage);
                }
            }
            #endregion

            sermon.Outline = txtOutline.Text;
            //sermon.Bulletin = txtBulletin.Text;

            #endregion

            #region Topics
            sermon.Topics = new TopicCollectionML();

            if (topicIDs.Length > 0)
            {
                string[] ids = topicIDs.Split(',');
                string[] titles = topicTitles.Split(',');
                for (int i = 0; i < ids.Length; i++)
                {
                    TopicML topic = new TopicML();
                    topic.TopicID = int.Parse(ids[i]);
                    topic.Title = titles[i];
                    sermon.Topics.Add(topic);
                }
            }

            #endregion

            #region Series
            sermon.Series = new SeriesCollectionML();
            foreach (Control ctrl in pnlSeriesHTML.Controls)
            {
                if (ctrl.GetType() == typeof(CheckBox))
                {
                    CheckBox cb = (CheckBox)ctrl;
                    if (cb.Checked)
                    {
                        SeriesML s = new SeriesML();
                        s.SeriesID = int.Parse(cb.InputAttributes["value"]);
                        s.Title = cb.Text;
                        sermon.Series.Add(s);
                    }
                }
            }
            if (sermon.Series.Count == 0)
                throw new Exception("Must select at least one Series for this sermon");
            #endregion

            #region SermonFields

            try
            {
                if (SermonFields != null)
                {

                    for (int i = 0; i < SermonFields.Count; i += 1)
                    {
                        SermonFieldML field = SermonFields[i];
                        RepeaterItem rptItem = rptCustomFields.Items[i];

                        SermonFieldValueML val = sermon.FieldValues.FindBySermonIDAndFieldID(sermon.SermonID, field.FieldID);

                        string type = field.FieldType.ToLower();
                        if (val == null)
                        {
                            val = new SermonFieldValueML();
                            val.SermonID = sermon.SermonID;
                            val.FieldID = field.FieldID;
                            sermon.FieldValues.Add(val);
                        }

                        if (type == "richtext")
                        {
                            BackstageControls.CKEditor ck = rptItem.FindControl("ckCustom") as BackstageControls.CKEditor;
                            if (ck != null)
                                val.Value = ck.Text;

                        }
                        else if (type == "text")
                        {
                            TextBox txt = rptItem.FindControl("txtCustom") as TextBox;
                            if (txt != null)
                                val.Value = txt.Text.Trim();
                        }
                        else if (type == "bool")
                        {
                            RadioButtonList rbl = rptItem.FindControl("rblBool") as RadioButtonList;
                            if (rbl != null)
                                val.Value = rbl.SelectedValue;
                        }
                        else if (type == "file")
                        {
                            bx.components.FilePicker fp = rptItem.FindControl("fpCustom") as bx.components.FilePicker;
                            if (fp != null)
                                val.Value = fp.FileID;
                        }
                        else if (type == "image")
                        {
                            bx.components.ImagePicker ip = rptItem.FindControl("ipCustom") as bx.components.ImagePicker;
                            if (ip != null)
                                val.Value = ip.ImageID;
                        }
                        else if (type == "multipleimages")
                        {
                            bx.components.MultipleImagePicker mip = rptItem.FindControl("mipCustom") as bx.components.MultipleImagePicker;
                            if (mip != null)
                                val.Value = backstage.Classes.StringEditor.IEnumerableToString(mip.ImageIDs, ",");
                        }
                        else if (type == "usercontrol")
                        {
                            BackstageSermons.Controls.CustomField cf = rptItem.FindControl("cfCustom") as BackstageSermons.Controls.CustomField;
                            if (cf != null)
                                val.Value = cf.Value;
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Trace.Write("err", err.ToString());
            }

            #endregion

        }
        #endregion

        protected Object TryConvert(Object obj, string value, Label lbl, string errMsg)
        {
            Object o = new Object();
            try
            {
                int objInt = 0;
                decimal objDec = 0;
                bool objBool = false;
                short objShort = 0;
                float objFloat = 0;
                double objDouble = 0;

                bool stringIsNull = string.IsNullOrEmpty(value);

                Type type = obj.GetType();

                if (type == objInt.GetType())
                {
                    o = objInt;
                    if (!stringIsNull)
                        o = int.Parse(value);
                }
                if (type == objDec.GetType())
                {
                    o = objDec;
                    if (!stringIsNull)
                        o = Convert.ToDecimal(value);
                }
                if (type == objBool.GetType())
                {
                    o = objBool;
                    if (!stringIsNull)
                        o = Convert.ToBoolean(value);
                }
                if (type == objShort.GetType())
                {
                    o = objShort;
                    if (!stringIsNull)
                        o = short.Parse(value);
                }
                if (type == objFloat.GetType())
                {
                    o = objFloat;
                    if (!stringIsNull)
                        o = float.Parse(value);
                }
                if (type == objDouble.GetType())
                {
                    o = objDouble;
                    if (!stringIsNull)
                        o = double.Parse(value);
                }
                lbl.Attributes.Add("style", "");
            }
            catch
            {
                o = null;
                lbl.Attributes.Add("style", "color: #990000;");
                throw new Exception(errMsg);
            }
            return o;
        }

        #region Series Methods

        protected void SetSeriesDefaults()
        {
            ddlSeries.Items.Clear();
            ddlSeries.Items.Add(new ListItem("root", "root"));

            pnlSeriesHTML.Controls.Clear();

            if (series != null)
            {
                if (series.Count > 0)
                {
                    series.Sort(delegate(SeriesML s1, SeriesML s2) { return s1.Title.CompareTo(s2.Title); });
                    RenderSeriesCollection(series, "");
                }
            }

            #region Series
            foreach (Control ctrl in pnlSeriesHTML.Controls)
            {
                if (ctrl.GetType() == typeof(CheckBox))
                {
                    CheckBox cb = (CheckBox)ctrl;
                    int sID = int.Parse(cb.InputAttributes["value"]);
                    foreach (SeriesML s in sermon.Series)
                    {
                        if (s.SeriesID == sID)
                            cb.Checked = true;
                    }
                }
            }
            #endregion
        }

        protected void RenderSeriesCollection(SeriesCollectionML sc, string preSpaces)
        {
            foreach (SeriesML series in sc)
            {
                pnlSeriesHTML.Controls.Add(new LiteralControl("<div style=\"margin: 0 0 0 25px;\">"));
                CheckBox cb = new CheckBox();
                cb.ID = "series" + series.SeriesID;
                cb.Text = backstage.Classes.StringEditor.Truncate(series.Title, 32 - ((preSpaces.Length / 2) * 3), "...", false);
                cb.Attributes.Add("title", series.Title);
                cb.InputAttributes.Add("value", series.SeriesID.ToString());
                cb.InputAttributes.Add("name", "series");
                cb.InputAttributes.Add("onClick", "CheckParentAndChildSeries(this);");

                pnlSeriesHTML.Controls.Add(cb);

                #region DropDownList
                ListItem li = new ListItem(preSpaces + series.Title, series.SeriesID.ToString());
                if (series.SeriesID == SeriesID)
                    li.Selected = true;
                ddlSeries.Items.Add(li);
                series.ChildSeries.Sort(delegate(SeriesML s1, SeriesML s2) { return s1.Title.CompareTo(s2.Title); });
                #endregion

                RenderSeriesCollection(series.ChildSeries, preSpaces + "--");

                pnlSeriesHTML.Controls.Add(new LiteralControl("</div>"));
            }
        }

        #endregion

        #region Button Events

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                SetSermonValues();

                SermonsBLL.Instance.InsertSermon(sermon);
                Response.Redirect("Default.aspx?SeriesID=" + strSeriesID);
            }
            catch (Exception err)
            {
                Trace.Write("err", err.ToString());
                lblResult.Text = err.Message.ToString() + "<br/>";
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                SetSermonValues();
                /*
                if (!string.IsNullOrEmpty(Settings.SermonAudio.MemberID) && !string.IsNullOrEmpty(Settings.SermonAudio.Password))
                {
                    if (string.IsNullOrEmpty(sermon.SermonAudioID))
                        SermonsBLL.Instance.SermonAudio_SubmitSermon(sermon);
                }
                 */
                SermonsBLL.Instance.UpdateSermon(sermon);
                SecurityBAC.Instance.UpdateRecentEdit("/backstageIncludes/BackstageSermons/modules/Sermons", "Url");

                Response.Redirect("Default.aspx?SeriesID=" + strSeriesID);
            }
            catch (Exception err)
            {
                lblResult.Text = err.Message.ToString() + "<br/>";
                Trace.Write(err.ToString());
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx?SeriesID=" + strSeriesID);
        }

        protected void btnNewSeries_Click(object sender, EventArgs e)
        {
            SeriesML s = new SeriesML();
            s.Title = txtNewSeries.Text;
            if (ddlSeries.SelectedValue != "root")
                try
                {
                    s.ParentSeriesID = int.Parse(ddlSeries.SelectedValue);
                }
                catch { }
            SermonsBLL.Instance.InsertSeries(s);
            SeriesID = s.SeriesID;
            strSeriesID = s.SeriesID.ToString();

            //Reset SeriesCollectionML
            _series = null;

            SetSeriesDefaults();

            SetSeriesJquery();
        }

        #endregion

        #region Update Events

        protected void SetSeriesJquery()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "settingCatScripts", "setCatJquery();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "openCategory", "openCategories();", true);
        }

        protected void updateSeries_Load(object sender, EventArgs e)
        {
        }

        #endregion

        protected void btnShowAddSpeaker_Click(object sender, EventArgs e)
        {
            pnlAddSpeaker.Visible = true;
        }

        protected void btnCancelAddSpeaker_Click(object sender, EventArgs e)
        {
            pnlAddSpeaker.Visible = false;
        }

        protected void btnAddSpeaker_Click(object sender, EventArgs e)
        {
            SpeakerML s = new SpeakerML();
            s.NamePrefix = (NamePrefixes?)backstage.Classes.StringEditor.ToEnum(ddlAddSpeakerNamePrefix.SelectedValue, typeof(NamePrefixes?));
            s.FirstName = txtAddSpeakerFirstName.Text;
            s.LastName = txtAddSpeakerLastName.Text;
            s.PhotoID = StringEditor.ToInt32(ipSpeaker.ImageID);
            //s.Photo = ipSpeaker.Image.Src;
            s.Bio = Server.HtmlDecode(ckEditAddSpeakerBio.Text);
            SermonsBLL.Instance.InsertSpeaker(s);
            //Clear session so speaker tab will update.
            Session["SpeakerCollection"] = null;

            ddlSpeaker.DataSource = SermonsBLL.Instance.LoadAllSpeakers();
            ddlSpeaker.DataBind();
            ddlSpeaker.SelectedValue = s.SpeakerID.ToString();
            pnlAddSpeaker.Visible = false;
        }
    }
}