﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace BackstageSermons
{
    public class TopicCollectionML : List<TopicML>
    {
        public TopicML FindTopicByID(int TopicID)
        {
            TopicML topic = null;
            foreach (TopicML t in this)
            {
                if (t.TopicID == TopicID)
                {
                    topic = t;
                    break;
                }
            }

            return topic;
        }
    }
}
