﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace BackstageSermons
{
    public class SettingCollectionML : List<SettingML>
    {
        public SettingML Setting(string Name)
        {
            SettingML s = null;
            foreach (SettingML setting in this)
            {
                if (setting.Name == Name)
                    s = setting;
            }
            if (s == null)
            {
                s = new SettingML(Name);
                this.Add(s);
            }
            return s;
        }
    }
}