﻿<%@ Page Language="C#" MasterPageFile="~/backstageIncludes/BackstageSermons/modules/Sermons/SermonsNav.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BackstageSermons.modules.Topics.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <title>Backstage Sermons :: Topic Manager</title>
    
	
	<script src="/backstageIncludes/BackstageSermons/modules/Topics/js/topics.js" type="text/javascript"></script>
    <script type="text/javascript">
        function ConfirmVisibility(ddl){
            if(ddl.value != "-"){
                if(!confirm('Are you sure you wish to change the visiblity of all the selected sermons?')) return false;
            }else{
                return false;
            }
        }
        function BounceFocus(){ var element = document.getElementById('<%=txtSearch.ClientID%>'); element.blur(); element.focus(); }
        $(document).ready(function(){
            $("#browser").find("span").hover(function(){
                $(this).children("span .imgSeriesAction").css({"display":"inline"});
            }, function(){
                $(this).children("span .imgSeriesAction").css({"display":"none"});
            });
            
            $(".pencil").click(function(){
                //$(this).parent().children("a:first").css({"display":"none"});
                //$(this).parent().parent().parent().children("input").css({"display":"inline"});
                //$(this).parent().parent().parent().children("a").css({"display":"inline"});
                $(this).parent().parent().parent().children("#inputBox").show("fast");
            });
            $(".closeButton").click(function(){
                $(this).parent().hide("fast");
            });
            $('#subNavTopics').addClass('subActive');
        });
        
        function ToggleNewTopic()
        {
            $('.trInsert').toggle();
        }
        
        function toggleTopicEdit(img)
        {
            $img = $(img);
            $tr = ($img.get(0).nodeName == "IMG") ? $(img).parent().parent().parent() : $(img).parent().parent();
            $tr.children("td").each(function(){
                $(this).find('.SermonInfoView').css({'display':'none'});
                $(this).find('.SermonEditView').css({'display':'block'});
                $(this).addClass("tdEdit");
            });
        }
        function closeTopicEditView(img){
            $(img).parent().parent().parent().children("td").each(function(){
                $(this).find('.SermonInfoView').css({'display':'block'});
                $(this).find('.SermonEditView').css({'display':'none'});
                $(this).removeClass("tdEdit");
            });
        }
        
        
    </script>
    <style type="text/css">
            .trInsert{ display: none; }
            .SermonInfoView{ cursor: pointer; }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
<asp:Label runat="server" ID="lblResult" style="color: #990000;"></asp:Label>


<asp:UpdateProgress ID="udProgress" runat="server" DisplayAfter="100" Visible="true" DynamicLayout="true">
    <ProgressTemplate>
        <div class="processing">
            <img alt="Processing" src="/backstage/images/NewButtons/ajax-loader.gif">
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>

<div class="sectionContent clearfix">
	<div class="topBarSearch">
	    <asp:TextBox ID="txtSearch" CssClass="search" Text="Search Topics" onfocus="if(this.value == 'Search Topics'){this.value='';}" onblur="if(this.value==''){this.value='Search Topics';}" runat="server" AutoPostBack="True" onkeyup="BounceFocus()" OnTextChanged="txtSearch_OnTextChanged" />
	</div>
	<div class="rightColumnWrapper" style="width:980px">
		<div class="rightColumn">
			<div class="toolbarContainer">
				<div class="toolbar clearfix">
					<div class="leftButtons clearfix" style="position: relative;">
					    <asp:CheckBox runat="server" ID="cbSelectAllTopics" AutoPostBack="true" OnCheckedChanged="lbSelectAllTopics_Click" style="position: absolute; z-index: 1; left: 5px; top: 3px; margin: 0; border: none;" />
					    <asp:LinkButton runat="server" ID="lbSelectAll" CssClass="bxButton2" OnClick="lbSelectAllTopics_Click" style="padding-left: 24px;">
				                <asp:Label runat="server" ID="lblSelectAllTopics" Text="Select All"></asp:Label>
				        </asp:LinkButton>
					    <asp:LinkButton runat="server" data-icon="minuscircle" CssClass="bxButton2" ToolTip="Delete Selected Topics" ID="lbDeleteTopic" OnClick="btnDeleteTopic_Click" OnClientClick="return confirm('Are you sure you wish to delete all the selected topics?');" Text="Delete Selected" />
					</div>
					<div class="rightButtons clearfix">
						<div data-icon="printer" onclick="print()" class="bxButton2">Print List</div>
						<div class="bxButton2 green" onclick="ToggleNewTopic()">+ Add New Topic</div>
					</div>
				</div>
			</div>
			<div class="tableContainer">
				<asp:UpdatePanel runat="server" ID="updateTopics" ChildrenAsTriggers="True" UpdateMode="Conditional">
					<ContentTemplate>
					
					    <cms:SmartGridView runat="server" ID="gridTopics" AllowPaging="True" AllowInserting="true" CssClass="list" AllowSorting="True" OnRowCommand="gridTopics_OnRowCommand"
                            AutoGenerateColumns="False" CellPadding="0" DataKeyNames="TopicID" GridLines="None" OnPageIndexChanging="gridTopics_PageIndexChanging"
                            OnRowCreated="gridTopics_OnRowCreated" OnDataBound="gridTopics_DataBound" OnSorting="gridTopics_Sorting" PageSize="12" PagerSettings-PageButtonCount="15">
                            <EmptyDataTemplate>
                            	<div style="padding:30px;text-align:center;font-size:20px;color:#999999">There are currently no topics to show.</div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField>
                                    <InsertItemTemplate></InsertItemTemplate>
                                    <ItemTemplate>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="border-right: none;">
                                                    <asp:CheckBox runat="server" ID="cbTopic" cID='<%# Eval("TopicID") %>' />
                                                    <asp:HiddenField runat="server" ID="hfTopicID" Value='<%# Eval("TopicID") %>' />
                                                </td>
                                            </tr>
                                        </table> 
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Topic/Tag" SortExpression="Title">
                                    <InsertItemTemplate>
                                        <span><asp:TextBox runat="server" ID="txtNewTitle" width="400"></asp:TextBox></span>
                                    </InsertItemTemplate>
                                    <ItemTemplate>
                                        <span class="SermonInfoView" onclick="toggleTopicEdit(this)"><%# Eval("Title") %></span>
										<div class="SermonEditView" style="display: none;">
											<asp:TextBox runat="server" ID="txtEditTitle" CssClass="txtEditTitle" Text='<%# Eval("Title") %>'></asp:TextBox>
										</div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <InsertItemTemplate>
								        <div>
									        <a>
									            <asp:ImageButton runat="server" ID="btnAddTopic" ImageUrl="/backstage/images/icon-save.png" OnClick="btnAddTopic_Click" />
									        </a>
									        <a>
									            <img alt="cancel insert" src="/backstage/images/icon-cancel.png" style="cursor: pointer;" onclick="ToggleNewTopic()" />
									        </a>
								        </div>
                                    </InsertItemTemplate>
                                    <ItemTemplate>
                                        <div class="functions SermonInfoView">
											<img alt="edit sermon" title="Edit this sermon" src="/backstage/images/icon-edit.png" onclick="toggleTopicEdit(this)" />
											<asp:ImageButton runat="server" ID="btnDelete" ImageUrl="/backstage/images/icon-trash.png" OnClientClick="return confirm('Are you sure you wish to delete this topic?');" CommandName="DeleteTopic" CommandArgument='<%# Eval("TopicID") %>' ToolTip="Delete this topic" style="margin: 0px; padding: 0px;" />
										</div>
										<div class="functions SermonEditView" style="display: none;">
											<asp:LinkButton runat="server" ID="lbUpdateTopic" Text="Update" class="pmUpdateBtn" CommandName="UpdateTopic" CommandArgument='<%# Eval("TopicID") %>'><img src="/backstage/images/icon-save.png" /></asp:LinkButton>
											<img alt="cancel edit" src="/backstage/images/icon-cancel.png" onclick="closeTopicEditView(this)" />
										</div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                
                            </Columns<%@ Page Language="C#" MasterPageFile="~/backstageIncludes/BackstageSermons/Sermons.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BackstageSermons.modules.Topics.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <title>Backstage Sermons :: Topic Manager</title>
    
	
	<script src="/backstageIncludes/BackstageSermons/modules/Topics/js/topics.js" type="text/javascript"></script>
    <script type="text/javascript">
        function ConfirmVisibility(ddl){
            if(ddl.value != "-"){
                if(!confirm('Are you sure you wish to change the visiblity of all the selected sermons?')) return false;
            }else{
                return false;
            }
        }
        function BounceFocus(){ var element = document.getElementById('<%=txtSearch.ClientID%>'); element.blur(); element.focus(); }
        $(document).ready(function(){
            $("#browser").find("span").hover(function(){
                $(this).children("span .imgSeriesAction").css({"display":"inline"});
            }, function(){
                $(this).children("span .imgSeriesAction").css({"display":"none"});
            });
            
            $(".pencil").click(function(){
                //$(this).parent().children("a:first").css({"display":"none"});
                //$(this).parent().parent().parent().children("input").css({"display":"inline"});
                //$(this).parent().parent().parent().children("a").css({"display":"inline"});
                $(this).parent().parent().parent().children("#inputBox").show("fast");
            });
            $(".closeButton").click(function(){
                $(this).parent().hide("fast");
            });
            $('#subNavTopics').addClass('subActive');
        });
        
        function ToggleNewTopic()
        {
            $('.trInsert').toggle();
        }
        
        function toggleTopicEdit(img)
        {
            $img = $(img);
            $tr = ($img.get(0).nodeName == "IMG") ? $(img).parent().parent().parent() : $(img).parent().parent();
            $tr.children("td").each(function(){
                $(this).find('.SermonInfoView').css({'display':'none'});
                $(this).find('.SermonEditView').css({'display':'block'});
                $(this).addClass("tdEdit");
            });
        }
        function closeTopicEditView(img){
            $(img).parent().parent().parent().children("td").each(function(){
                $(this).find('.SermonInfoView').css({'display':'block'});
                $(this).find('.SermonEditView').css({'display':'none'});
                $(this).removeClass("tdEdit");
            });
        }
        
        
    </script>
    <style type="text/css">
            .trInsert{ display: none; }
            .SermonInfoView{ cursor: pointer; }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
<asp:Label runat="server" ID="lblResult" style="color: #990000;"></asp:Label>


<asp:UpdateProgress ID="udProgress" runat="server" DisplayAfter="100" Visible="true" DynamicLayout="true">
    <ProgressTemplate>
        <div class="processing">
            <img alt="Processing" src="/backstage/images/NewButtons/ajax-loader.gif">
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>

<div class="sectionContent clearfix">
	<div class="topBarSearch">
	    <asp:TextBox ID="TextBox1" CssClass="search" Text="Search Topics" onfocus="if(this.value == 'Search Topics'){this.value='';}" onblur="if(this.value==''){this.value='Search Topics';}" runat="server" AutoPostBack="True" onkeyup="BounceFocus()" OnTextChanged="txtSearch_OnTextChanged" />
	</div>
	<div class="rightColumnWrapper" style="width:980px">
		<div class="rightColumn">
			<div class="toolbarContainer">
				<div class="toolbar clearfix">
					<div class="leftButtons clearfix" style="position: relative;">
					    <asp:CheckBox runat="server" ID="CheckBox1" AutoPostBack="true" OnCheckedChanged="lbSelectAllTopics_Click" style="position: absolute; z-index: 1; left: 5px; top: 3px; margin: 0; border: none;" />
					    <asp:LinkButton runat="server" ID="LinkButton1" CssClass="bxButton2" OnClick="lbSelectAllTopics_Click" style="padding-left: 24px;">
				                <asp:Label runat="server" ID="Label1" Text="Select All"></asp:Label>
				        </asp:LinkButton>
					    <asp:LinkButton runat="server" data-icon="minuscircle" CssClass="bxButton2" ToolTip="Delete Selected Topics" ID="LinkButton2" OnClick="btnDeleteTopic_Click" OnClientClick="return confirm('Are you sure you wish to delete all the selected topics?');" Text="Delete Selected" />
					</div>
					<div class="rightButtons clearfix">
						<div data-icon="printer" onclick="print()" class="bxButton2">Print List</div>
						<div class="bxButton2 green" onclick="ToggleNewTopic()">+ Add New Topic</div>
					</div>
				</div>
			</div>
			<div class="tableContainer">
				<asp:UpdatePanel runat="server" ID="UpdatePanel1" ChildrenAsTriggers="True" UpdateMode="Conditional">
					<ContentTemplate>
					
					    <cms:SmartGridView runat="server" ID="SmartGridView1" AllowPaging="True" AllowInserting="true" CssClass="list" AllowSorting="True" OnRowCommand="gridTopics_OnRowCommand"
                            AutoGenerateColumns="False" CellPadding="0" DataKeyNames="TopicID" GridLines="None" OnPageIndexChanging="gridTopics_PageIndexChanging"
                            OnRowCreated="gridTopics_OnRowCreated" OnDataBound="gridTopics_DataBound" OnSorting="gridTopics_Sorting" PageSize="12" PagerSettings-PageButtonCount="15">
                            <EmptyDataTemplate>
                            	<div style="padding:30px;text-align:center;font-size:20px;color:#999999">There are currently no topics to show.</div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField>
                                    <InsertItemTemplate></InsertItemTemplate>
                                    <ItemTemplate>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="border-right: none;">
                                                    <asp:CheckBox runat="server" ID="cbTopic" cID='<%# Eval("TopicID") %>' />
                                                    <asp:HiddenField runat="server" ID="hfTopicID" Value='<%# Eval("TopicID") %>' />
                                                </td>
                                            </tr>
                                        </table> 
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Topic/Tag" SortExpression="Title">
                                    <InsertItemTemplate>
                                        <span><asp:TextBox runat="server" ID="txtNewTitle" width="400"></asp:TextBox></span>
                                    </InsertItemTemplate>
                                    <ItemTemplate>
                                        <span class="SermonInfoView" onclick="toggleTopicEdit(this)"><%# Eval("Title") %></span>
										<div class="SermonEditView" style="display: none;">
											<asp:TextBox runat="server" ID="txtEditTitle" CssClass="txtEditTitle" Text='<%# Eval("Title") %>'></asp:TextBox>
										</div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <InsertItemTemplate>
								        <div>
									        <a>
									            <asp:ImageButton runat="server" ID="btnAddTopic" ImageUrl="/backstage/images/icon-save.png" OnClick="btnAddTopic_Click" />
									        </a>
									        <a>
									            <img alt="cancel insert" src="/backstage/images/icon-cancel.png" style="cursor: pointer;" onclick="ToggleNewTopic()" />
									        </a>
								        </div>
                                    </InsertItemTemplate>
                                    <ItemTemplate>
                                        <div class="functions SermonInfoView">
											<img alt="edit sermon" title="Edit this sermon" src="/backstage/images/icon-edit.png" onclick="toggleTopicEdit(this)" />
											<asp:ImageButton runat="server" ID="btnDelete" ImageUrl="/backstage/images/icon-trash.png" OnClientClick="return confirm('Are you sure you wish to delete this topic?');" CommandName="DeleteTopic" CommandArgument='<%# Eval("TopicID") %>' ToolTip="Delete this topic" style="margin: 0px; padding: 0px;" />
										</div>
										<div class="functions SermonEditView" style="display: none;">
											<asp:LinkButton runat="server" ID="lbUpdateTopic" Text="Update" class="pmUpdateBtn" CommandName="UpdateTopic" CommandArgument='<%# Eval("TopicID") %>'><img src="/backstage/images/icon-save.png" /></asp:LinkButton>
											<img alt="cancel edit" src="/backstage/images/icon-cancel.png" onclick="closeTopicEditView(this)" />
										</div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                
                            </Columns>
                            <PagerStyle CssClass="trPager" />
                            <RowStyle CssClass="trContent" />
                            <InsertRowStyle CssClass="trContent trInsert" />
                        </cms:SmartGridView>
					</ContentTemplate>
					<Triggers>
						<asp:AsyncPostBackTrigger ControlID="txtSearch" EventName="TextChanged" />
						<asp:AsyncPostBackTrigger ControlID="lbDeleteTopic" EventName="Click" />
					</Triggers>
				</asp:UpdatePanel>
			</div>
		</div>
	</div>
</div>
</asp:Content>
                            <PagerStyle CssClass="trPager" />
                            <RowStyle CssClass="trContent" />
                            <InsertRowStyle CssClass="trContent trInsert" />
                        </cms:SmartGridView>
					</ContentTemplate>
					<Triggers>
						<asp:AsyncPostBackTrigger ControlID="txtSearch" EventName="TextChanged" />
						<asp:AsyncPostBackTrigger ControlID="lbDeleteTopic" EventName="Click" />
					</Triggers>
				</asp:UpdatePanel>
			</div>
		</div>
	</div>
</div>
</asp:Content>