﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackstageSermons
{
    public class SpeakerPrintListML
    {
        #region Public fields

        public int SpeakerID { get; set; }
        public string Name { get; set; }

        #endregion

        #region Construction

        #region SpeakerPrintListML()

        public SpeakerPrintListML()
        {
        }

        #endregion

        #endregion
    }
}