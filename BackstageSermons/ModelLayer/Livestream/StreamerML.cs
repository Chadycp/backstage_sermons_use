﻿using System;

namespace BackstageSermons.Livestream
{
    public class StreamerML
    {
        public StreamerML(string SessionID)
        {
            this.SessionID = SessionID;
            this.LastUpdated = DateTime.Now;
        }

        public string SessionID { get; set; }
        public DateTime LastUpdated { get; set; }
        public bool IsExpired(TimeSpan timeSpan)//whether the streamer is expired
        {
            return DateTime.Now - this.LastUpdated >= timeSpan;
        }

        public void Update()//upate the expiration date
        {
            this.LastUpdated = DateTime.Now;
        }
    }
}
