﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace BackstageSermons
{
    public class SpeakerCollectionML : List<SpeakerML>
    {
        public SpeakerML FindSpeakerByID(int SpeakerID)
        {
            SpeakerML speaker = null;
            foreach (SpeakerML s in this)
            {
                if (s.SpeakerID == SpeakerID)
                {
                    speaker = s;
                    break;
                }
            }
            
            return speaker;
        }
        
        public SpeakerML FindSpeakerByName(string firstName, string lastName)
        {
            firstName = firstName.ToLower();
            lastName = lastName.ToLower();
            foreach (SpeakerML s in this)
            {
                if (s.FirstName.ToLower() == firstName && s.LastName.ToLower() == lastName)
                    return s;
            }
            return null;
        }
    }
}