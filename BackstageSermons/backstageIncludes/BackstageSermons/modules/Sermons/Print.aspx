﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Print.aspx.cs" Inherits="BackstageSermons.modules.Sermons.Print" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>Backstage Sermons :: Sermon Manager</title>
<link href="/backstage/css/styles.common.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    
table.list tr th a {
    background-image: none;
}

</style>
</head>
<body>
    <form id="form1" runat="server">

        <asp:GridView runat="server" ID="gridSermons" CssClass="list" AllowSorting="True"
            AutoGenerateColumns="False" CellPadding="0" DataKeyNames="SermonID" GridLines="None">
            <EmptyDataTemplate>
        	    <div style="padding:30px;text-align:center;font-size:20px;color:#999999">There are no sermons in this series.</div>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate></HeaderTemplate>
                    <ItemTemplate>
	                    <asp:ImageButton runat="server" ID="btnVisibility" CommandName="ChangeVisibility" CommandArgument='<%# Eval("SermonID") %>' ImageUrl='<%# (bool)Eval("Available") ? "/backstage/images/icons/eyeIcon.png" : "/backstage/images/icons/eyeIconClose.png" %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Title" SortExpression="Title">
                    <ItemTemplate>
                        <div class="SermonInfoView">
                            <%# backstage.Classes.StringEditor.Truncate(Eval("Title").ToString(), 32, "...", false) %>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Passage" SortExpression="Passage">
                    <ItemTemplate>
                        <div class="SermonInfoView">
	                        <%# Eval("PassageFull") %>
	                    </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Speaker" SortExpression="Speaker">
                    <ItemTemplate>
                        <div class="SermonInfoView">
                            <%# Eval("SpeakerName") %>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <div class="functions" style="position:relative">
                    	    <div class="SermonInfoView">
	                            <img alt="edit" title="Edit this sermon" src="/backstage/images/icon-edit.png" onclick="showHideEditControls(this)" />
	                            <asp:ImageButton runat="server" ID="btnDeleteSermon" CommandName="DeleteSermon" CommandArgument='<%# Eval("SermonID") %>' ImageUrl="/backstage/images/icon-trash.png" ToolTip="Delete this sermon" OnClientClick="return confirm('Are you sure you wish to delete this sermon?');" />
	                        </div>
	                        <div class="SermonEditView" style="display: none;">
	                            <asp:LinkButton runat="server" ID="lbUpdateSermon" Text="Update" class="pmUpdateBtn" CommandName="UpdateSermon" CommandArgument='<%# Eval("SermonID") %>'><img alt="save" src="/backstage/images/icon-save.png" /></asp:LinkButton>
	                            <img alt="cancel" src="/backstage/images/icon-cancel.png" onclick="closeSermonEditView(this)" />
	                        </div>
	                    </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="trPager" />
            <RowStyle CssClass="trContent" />
        </asp:GridView>
    </form>
</body>
</html>
