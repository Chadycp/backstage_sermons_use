﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace BackstageSermons.Settings
{
    public static class Livestream
    {
        public static string Link
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("Livestream_Link").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("Livestream_Link").Value = value;
            }
        }

        public static string EmbedCode
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("Livestream_EmbedCode").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("Livestream_EmbedCode").Value = value;
            }
        }

        public static string Copy
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("Livestream_Copy").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("Livestream_Copy").Value = value;
            }
        }

        public static string Title
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("Livestream_Title").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("Livestream_Title").Value = value;
            }
        }

        public static string Password
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("Livestream_Password").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("Livestream_Password").Value = value;
            }
        }

        public static int Limit
        {
            get
            {
                return backstage.Classes.StringEditor.ToInt32(SermonsCache.SettingCollection.Setting("Livestream_Limit").Value);
            }
            set
            {
                SermonsCache.SettingCollection.Setting("Livestream_Limit").Value = value.ToString();
            }
        }

        public static string DateRanges
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("Livestream_DateRanges").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("Livestream_DateRanges").Value = value;
            }
        }

        public static bool IsLive
        {
            get
            {
                DateTime dt = TimeZoneInfo.ConvertTime(DateTime.Now, Admin.TimeZone);
                if (!string.IsNullOrEmpty(DateRanges)) {
                    string[] drs = Regex.Split(DateRanges, "\t");
                    foreach (string dr in drs) {
                        string[] times = dr.Split(',');

                        //Check Day Of Week
                        if (times[0] != ((int)dt.DayOfWeek).ToString())
                            continue;

                        //Check Start Time
                        DateTime dtStart = Convert.ToDateTime(times[1]);
                        if (dt.Hour < dtStart.Hour)
                            continue;
                        if (dt.Hour == dtStart.Hour && dt.Minute < dtStart.Minute)
                            continue;

                        //Check End Time
                        DateTime dtEnd = Convert.ToDateTime(times[2]);
                        if (dt.Hour > dtEnd.Hour)
                            continue;
                        if (dt.Hour == dtEnd.Hour && dt.Minute > dtEnd.Minute)
                            continue;

                        return true;
                    }
                }

                return false;
            }
        }

        public static void Update()
        {
            SettingCollectionML sc = new SettingCollectionML();
            SettingML s = SermonsCache.SettingCollection.Setting("Livestream_Link");
            sc.Add(s);
            s = SermonsCache.SettingCollection.Setting("Livestream_EmbedCode");
            sc.Add(s);
            s = SermonsCache.SettingCollection.Setting("Livestream_Copy");
            sc.Add(s);
            s = SermonsCache.SettingCollection.Setting("Livestream_Title");
            sc.Add(s);
            s = SermonsCache.SettingCollection.Setting("Livestream_Password");
            sc.Add(s);
            s = SermonsCache.SettingCollection.Setting("Livestream_Limit");
            sc.Add(s);
            s = SermonsCache.SettingCollection.Setting("Livestream_DateRanges");
            sc.Add(s);
            SermonsBLL.Instance.UpdateSettingCollection(sc);
        }
    }
}
