﻿<%@ Page Language="C#" MasterPageFile="~/backstageIncludes/BackstageSermons/modules/Sermons/SermonsNav.Master" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="BackstageSermons.modules.SermonAudio.Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <title>Backstage Sermons :: SermonAudio Manager</title>
    <link href="/backstageIncludes/BackstageSermons/modules/Sermons/css/SermonStyles.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
	    .inpt{width: 204px;}    
	</style>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#subNavSermonAudio').addClass('subActive');
        });
        imgSrcImg = "ctl00_ctl00_cphContent_cphContent_imgPhoto";
        imgSrcTxt = "ctl00_ctl00_cphContent_cphContent_txtImgSrc";
    </script>
    <style type="text/css">
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <asp:Label runat="server" ID="lblResult" style="color: #990000;"></asp:Label>
    <div class="panel" style="padding: 0 20px 20px 20px;">
        <br />
        <h1>Sermon Audio Settings</h1>
        
        <div style="float: left; width: 220px;">
            <asp:Label ID="Label9" runat="server" Text="Member ID"></asp:Label><br />
            <asp:TextBox runat="server" ID="txtMemberID" CssClass="inpt"></asp:TextBox><br />
            
            <asp:Label ID="Label1" runat="server" Text="Password"></asp:Label><br />
            <asp:TextBox runat="server" ID="txtPassword" TextMode="Password" CssClass="inpt"></asp:TextBox><br />
            
            <asp:ImageButton runat="server" ID="btnUpdate" CssClass="eUpdate2" OnClick="btnUpdate_Click" Text="Update" ImageUrl="/backstage/images/NewButtons/update2.gif" />
        </div>
        
        <div style="float: left; width: 672px; margin: 12px 0 0 0;">
        <!--
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ornare ligula vitae tortor interdum nec malesuada nunc placerat. Aenean dictum egestas ligula, nec porttitor risus pulvinar non. Etiam nisl mauris, sagittis quis faucibus ut, varius at risus. Ut a diam turpis, non facilisis erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nunc iaculis iaculis ligula, et adipiscing dolor semper in. Etiam tincidunt sapien at diam sollicitudin sagittis nec vitae mi. Praesent placerat diam id nisi hendrerit vel consequat risus euismod. Duis luctus purus sit amet purus varius fermentum. Maecenas sit amet eros molestie enim pulvinar rhoncus. Vestibulum ac tellus massa, vitae volutpat nunc. Mauris adipiscing magna sed libero sagittis in varius quam dictum. Quisque elit sem, semper eu congue ut, sodales ac libero. Aenean tellus felis, gravida eu aliquet sed, sagittis adipiscing justo.
            </p>
            <p>
                Integer eu mattis nisi. Nam at volutpat tortor. Duis sed lectus ante. Maecenas vulputate rutrum lacus, eget rhoncus dolor elementum ut. Vivamus eget magna odio. Quisque sed arcu mi, aliquam iaculis lorem. Nam cursus pretium sollicitudin. Morbi ac mi sem, nec volutpat magna. Nam rhoncus molestie consequat. Aliquam eleifend purus at risus viverra nec porta erat ullamcorper. Aliquam blandit, odio in mollis pharetra, est ante feugiat dui, ac porta dolor enim non arcu.
            </p>
            <p>
                Fusce sed ante est. Fusce malesuada pretium turpis, et scelerisque enim sodales quis. Etiam sed mattis arcu. In hac habitasse platea dictumst. Aliquam ac ipsum nibh, ac aliquet risus. Nullam convallis, neque nec lacinia mollis, neque nibh laoreet eros, tincidunt condimentum orci sem eget justo. Duis mattis magna sit amet massa molestie varius nec at lorem. Integer enim odio, congue in consectetur eu, egestas a est. Morbi quis mauris sit amet lacus euismod tincidunt. In rutrum pretium dolor sed tincidunt. In non felis ligula, quis venenatis lectus. 
            </p>
            -->
        </div>
        
        <div class="clearBoth"></div>
        
    </div>
    <br /><br /><br />
</asp:Content>