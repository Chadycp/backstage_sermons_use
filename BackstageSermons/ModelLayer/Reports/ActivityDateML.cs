﻿using System;

namespace BackstageSermons
{
    public class ActivityDateML
    {
        #region Private fields

        private DateTime _Date;
        private string _Title = string.Empty;
        private int _TotalDownloads = 0;
        private int _TotalListens = 0;
        private int _TotalLivestreamViews = 0;
        private int _TotalPodcastHits = 0;

        #endregion

        #region Public properties

        public DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        public int TotalDownloads
        {
            get { return _TotalDownloads; }
            set { _TotalDownloads = value; }
        }

        public int TotalListens
        {
            get { return _TotalListens; }
            set { _TotalListens = value; }
        }

        public int TotalLivestreamViews
        {
            get { return _TotalLivestreamViews; }
            set { _TotalLivestreamViews = value; }
        }

        public int TotalPodcastHits
        {
            get { return _TotalPodcastHits; }
            set { _TotalPodcastHits = value; }
        }

        #endregion
    }
}