﻿using System;

namespace BackstageSermons.Controls
{
    public class CustomField : System.Web.UI.UserControl
    {
        public bool AutoBind
        {
            get
            {
                if (ViewState["PPAutoBind"] == null)
                    return true;
                return Convert.ToBoolean(ViewState["PPAutoBind"]);
            }
            set { ViewState["PPAutoBind"] = value; }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

        }

        protected override void OnLoad(EventArgs e)
        {
            if (AutoBind)
                BindSmartControl();

            base.OnLoad(e);
        }

        public void BindSmartControl()
        {
            //Controls.Clear();
            //System.Web.HttpContext.Current.Trace.Write("src", Src);
            if (!string.IsNullOrEmpty(Src))
            {
                System.Web.UI.Control ctrl = this.TemplateControl.LoadControl(Src);
                ICustomField cf = (ICustomField)ctrl;
                cf.Value = (string)ViewState[this.ClientID + "_Value"];
                this.Controls.Add(ctrl);
            }
            //this.Controls.Add(this.TemplateControl.LoadControl(Src));
        }

        public string Src
        {
            get
            {
                return ViewState[this.ClientID + "_Src"] == null ? "" : (string)ViewState[this.ClientID + "_Src"];
            }
            set { ViewState[this.ClientID + "_Src"] = value; }
        }

        public string Value
        {
            get
            {
                foreach (System.Web.UI.Control ctrl in this.TemplateControl.Controls)
                {
                    if (ctrl is ICustomField)
                    {
                        ICustomField cf = (ICustomField)ctrl;
                        return cf.Value;
                    }
                }
                return "";
                //return ViewState[this.ClientID + "_Value"] == null ? "" : (string)ViewState[this.ClientID + "_Value"];
            }
            set { ViewState[this.ClientID + "_Value"] = value; }
        }
    }
}
