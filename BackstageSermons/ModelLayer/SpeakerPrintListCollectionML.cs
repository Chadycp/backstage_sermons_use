﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Security.Cryptography;
using backstage.security;

namespace BackstageSermons
{
    public class SpeakerPrintListCollectionML : List<SpeakerPrintListML>
    {
        #region Construction

        #region SpeakerPrintListCollectionML()

        public SpeakerPrintListCollectionML()
        {
        }

        #endregion

        #endregion

        #region Public methods

        #region public void Add(SpeakerPrintList spl)

        public new void Add(SpeakerPrintListML spl)
        {
            foreach (SpeakerPrintListML s in this) {
                if (s.SpeakerID == spl.SpeakerID)
                    return;
            }
            base.Add(spl);
        }

        #endregion

        #region SpeakerPrintListML GetBySpeakerID(int SpeakerID)

        public SpeakerPrintListML GetBySpeakerID(int SpeakerID)
        {
            foreach (SpeakerPrintListML spl in this)
                if (spl.SpeakerID == SpeakerID)
                    return spl;

            return null;
        }

        #endregion

        #endregion
    }
}