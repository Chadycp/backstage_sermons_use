﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackstageSermons
{
    public class SermonReportML
    {
        #region Private Properties
        private DateTime _BeginDate = DateTime.Now;
        private DateTime _EndDate = DateTime.Now;
        private ActivityDateCollectionML _ActivityByDate = new ActivityDateCollectionML();
        #region Sermons
        private ReportSermonCollectionML _ReportSermons = new ReportSermonCollectionML();
        private ReportSermonML _MostDownloadedSermon = new ReportSermonML();
        private ReportSermonML _MostListenedToSermon = new ReportSermonML();
        #endregion
        #region Speakers
        private ReportSpeakerCollectionML _ReportSpeakers = new ReportSpeakerCollectionML();
        private ReportSpeakerML _MostDownloadedSpeaker = new ReportSpeakerML();
        private ReportSpeakerML _MostListenedToSpeaker = new ReportSpeakerML();
        #endregion
        #region Series
        private ReportSeriesCollectionML _ReportSeries = new ReportSeriesCollectionML();
        private ReportSeriesML _MostDownloadedSeries = new ReportSeriesML();
        private ReportSeriesML _MostListenedToSeries = new ReportSeriesML();
        #endregion
        #region Topics
        private ReportTopicCollectionML _ReportTopics = new ReportTopicCollectionML();
        private ReportTopicML _MostDownloadedTopic = new ReportTopicML();
        private ReportTopicML _MostListenedToTopic = new ReportTopicML();
        #endregion
        #endregion

        #region Public Properties
        public DateTime BeginDate
        {
            get { return _BeginDate; }
            set { _BeginDate = value; }
        }

        public DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }
        public int TotalDownloads { get; set; }
        public int TotalListens { get; set; }
        public int TotalLivestreamViews { get; set; }
        public int TotalPodcastHits { get; set; }
        public ActivityDateCollectionML ActivityByDate
        {
            get { return _ActivityByDate; }
            set { _ActivityByDate = value; }
        }

        #region Sermons
        public ReportSermonCollectionML ReportSermons
        {
            get { return _ReportSermons; }
            set { _ReportSermons = value; }
        }
        public ReportSermonML MostDownloadedSermon
        {
            get { return _MostDownloadedSermon; }
            set { _MostDownloadedSermon = value; }
        }
        public ReportSermonML MostListenedToSermon
        {
            get { return _MostListenedToSermon; }
            set { _MostListenedToSermon = value; }
        }
        #endregion

        #region Speakers
        public ReportSpeakerCollectionML ReportSpeakers
        {
            get { return _ReportSpeakers; }
            set { _ReportSpeakers = value; }
        }
        public ReportSpeakerML MostDownloadedSpeaker
        {
            get { return _MostDownloadedSpeaker; }
            set { _MostDownloadedSpeaker = value; }
        }
        public ReportSpeakerML MostListenedToSpeaker
        {
            get { return _MostListenedToSpeaker; }
            set { _MostListenedToSpeaker = value; }
        }
        #endregion

        #region Series
        public ReportSeriesCollectionML ReportSeries
        {
            get { return _ReportSeries; }
            set { _ReportSeries = value; }
        }
        public ReportSeriesML MostDownloadedSeries
        {
            get { return _MostDownloadedSeries; }
            set { _MostDownloadedSeries = value; }
        }
        public ReportSeriesML MostListenedToSeries
        {
            get { return _MostListenedToSeries; }
            set { _MostListenedToSeries = value; }
        }
        #endregion

        #region Topics
        public ReportTopicCollectionML ReportTopics
        {
            get { return _ReportTopics; }
            set { _ReportTopics = value; }
        }
        public ReportTopicML MostDownloadedTopic
        {
            get { return _MostDownloadedTopic; }
            set { _MostDownloadedTopic = value; }
        }
        public ReportTopicML MostListenedToTopic
        {
            get { return _MostListenedToTopic; }
            set { _MostListenedToTopic = value; }
        }
        #endregion
        #endregion

    }

}