﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackstageSermons
{
    public interface ICustomField
    {
        #region Methods
        string Value { get; set; }
        #endregion
    }
}
