﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackstageSermons
{
    public class SeriesFieldValueCollectionML : List<SeriesFieldValueML>
    {
        public SeriesFieldValueML FindBySeriesIDAndFieldID(int seriesID, int fieldID)
        {
            foreach (SeriesFieldValueML sfv in this) {
                if (
                   (seriesID == sfv.SeriesID) &&
                   (fieldID == sfv.FieldID)
                ) {
                    return sfv;
                }
            }
            return null;
        }

        public void UpdateSeriesID(int seriesID)
        {
            foreach (SeriesFieldValueML item in this)
                item.SeriesID = seriesID;
        }
    }
}