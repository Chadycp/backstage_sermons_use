﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Security.Cryptography;
using backstage.security;

namespace BackstageSermons
{
    public class SermonPrintListCollectionML : List<SermonPrintListML>
    {
        #region Construction

        #region SermonPrintListCollectionML()

        public SermonPrintListCollectionML()
        {
        }

        #endregion

        #endregion

        #region Public methods

        #region public void Add(SermonPrintList spl)

        public new void Add(SermonPrintListML spl)
        {
            foreach (SermonPrintListML s in this) {
                if (s.SermonID == spl.SermonID)
                    return;
            }
            base.Add(spl);
        }

        #endregion

        #region SermonPrintListML GetBySermonID(int SermonID)

        public SermonPrintListML GetBySermonID(int SermonID)
        {
            foreach (SermonPrintListML spl in this)
                if (spl.SermonID == SermonID)
                    return spl;

            return null;
        }

        #endregion

        #endregion
    }
}