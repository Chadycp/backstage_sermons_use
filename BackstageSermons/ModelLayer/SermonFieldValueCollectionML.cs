﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackstageSermons
{
    public class SermonFieldValueCollectionML : List<SermonFieldValueML>
    {
        public SermonFieldValueML FindBySermonIDAndFieldID(int sermonID, int fieldID)
        {
            foreach (SermonFieldValueML sfv in this) {
                if (
                   (sermonID == sfv.SermonID) &&
                   (fieldID == sfv.FieldID)
                ) {
                    return sfv;
                }
            }
            return null;
        }

        public void UpdateSermonID(int sermonID)
        {
            foreach (SermonFieldValueML item in this)
                item.SermonID = sermonID;
        }
    }
}