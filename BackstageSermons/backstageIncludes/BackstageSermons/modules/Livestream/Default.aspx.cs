﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using BackstageSermons.Settings;

namespace BackstageSermons.modules.Livestream
{
    public partial class Default : System.Web.UI.Page
    {
        public string DateRanges {
            get {
                if (ViewState["lsDateRange"] == null)
                    ViewState["lsDateRange"] = BackstageSermons.Settings.Livestream.DateRanges;
                return (string)ViewState["lsDateRange"];
            }
            set { ViewState["lsDateRange"] = value; }
        }

        public string LiveStreamPasswordString {
            get {
                if (ViewState["lsPassword"] == null)
                    ViewState["lsPassword"] = BackstageSermons.Settings.Livestream.Password;
                return (string)ViewState["lsPassword"];
            }
            set { ViewState["lsPassword"] = value; }
        }

        public int PasswordCount { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                SetDefaults();
        }

        protected void SetDefaults()
        {
            txtLink.Text = BackstageSermons.Settings.Livestream.Link;
            txtEmbedCode.Text = BackstageSermons.Settings.Livestream.EmbedCode;
            ckCopy.Text = BackstageSermons.Settings.Livestream.Copy;

            txtTitle.Text = BackstageSermons.Settings.Livestream.Title;

            BindPasswordList();
           
            pnlPassword.Visible = PasswordCount > 0;
            cbEnablePassword.Checked = pnlPassword.Visible;

            int limit = BackstageSermons.Settings.Livestream.Limit;
            txtLimit.Text = limit != 0 ? limit.ToString() : "";
            pnlLimit.Visible = (limit != 0);
            cbEnableLimit.Checked = (limit != 0);

            BindDateRanges();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            BackstageSermons.Settings.Livestream.Link = txtLink.Text;
            BackstageSermons.Settings.Livestream.EmbedCode = txtEmbedCode.Text;
            BackstageSermons.Settings.Livestream.Copy = ckCopy.Text;
            BackstageSermons.Settings.Livestream.Title = txtTitle.Text;
            BackstageSermons.Settings.Livestream.Limit = cbEnableLimit.Checked ? backstage.Classes.StringEditor.ToInt32(txtLimit.Text) : 0;

            BackstageSermons.Settings.Livestream.Password = cbEnablePassword.Checked ? LiveStreamPasswordString : "";
            BackstageSermons.Settings.Livestream.DateRanges = DateRanges;

            BackstageSermons.Settings.Livestream.Update();
            RefreshPage();
        }

        protected void BindDateRanges()
        {
            List<string> ranges = new List<string>();
            string str = DateRanges;
            if (!string.IsNullOrEmpty(str))
            {
                string[] drs = Regex.Split(str, "\t");
                foreach (string dr in drs)
                {
                    string[] times = dr.Split(',');

                    DateTime dtStart = Convert.ToDateTime(times[1]);
                    DateTime dtEnd = Convert.ToDateTime(times[2]);

                    ranges.Add(new backstage.Classes.CommonInfo.DayOfWeekML(times[0]).AbbreviationThreeLetter + " " + dtStart.ToString("hh:mmt").ToLower() + " - " + dtEnd.ToString("hh:mmt").ToLower());
                }
            }
            rptDateRanges.DataSource = ranges;
            rptDateRanges.DataBind();
        }

        protected void btnAddDateRange_Click(object sender, EventArgs e)
        {
            try
            {
                string dr = DateRanges;
                if (dr != null && dr.Length > 0)
                    dr += "\t";
                int startHour = ddlStartTimePeriod.SelectedValue == "PM" ? (int.Parse(ddlStartHour.SelectedValue) + 12) : int.Parse(ddlStartHour.SelectedValue);
                int startMinute = int.Parse(ddlStartMinute.SelectedValue);
                int endHour = ddlEndTimePeriod.SelectedValue == "PM" ? (int.Parse(ddlEndHour.SelectedValue) + 12) : int.Parse(ddlEndHour.SelectedValue);
                int endMinute = int.Parse(ddlEndMinute.SelectedValue);
                DateTime dtStart = DateTime.MinValue.Add(new TimeSpan(startHour, startMinute, 0));
                DateTime dtEnd = DateTime.MinValue.Add(new TimeSpan(endHour, endMinute, 0));
                dr += string.Format("{0},{1},{2}", ddlDayOfWeek.SelectedValue, dtStart, dtEnd);
                DateRanges = dr;

                BindDateRanges();
            }
            catch (Exception err)
            {
                Trace.Write("err", err.ToString());
                ScriptManager.RegisterStartupScript(this, typeof(Page), "err", "alert(\"" + err.Message + "\");", true);
            }
        }

        protected void lbDeleteDateRange_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string[] drs = Regex.Split(DateRanges, "\t");
            List<string> ranges = new List<string>(drs);
            ranges.RemoveAt(int.Parse(lb.CommandArgument));
            StringBuilder sb = new StringBuilder();
            foreach (string s in ranges)
            {
                if (sb.Length > 0)
                    sb.Append("\t");
                sb.Append(s);
            }
            DateRanges = sb.ToString();
            BindDateRanges();
        }

        protected void RefreshPage()
        {
            Response.Redirect("Default.aspx");
        }

        protected void cbEnablePassword_OnCheckedChanged(object sender, EventArgs e)
        {
            pnlPassword.Visible = cbEnablePassword.Checked;
        }

        protected void cbEnableLimit_OnCheckedChanged(object sender, EventArgs e)
        {
            pnlLimit.Visible = cbEnableLimit.Checked;
        }

        protected void BindPasswordList()
        {
            List<string> passwords = new List<string>();
            string str = LiveStreamPasswordString;
            if (!string.IsNullOrEmpty(str))
            {
                string[] drs = Regex.Split(str, ";");
                foreach (string dr in drs)
                {
                   passwords.Add(dr);
                }
            }

            PasswordCount = passwords.Count;

            rptLiveStreamPWs.DataSource = passwords;
            rptLiveStreamPWs.DataBind();
        }

        protected void lbDeletePassword_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string[] pws = Regex.Split(LiveStreamPasswordString, ";");
            List<string> ranges = new List<string>(pws);
            ranges.RemoveAt(int.Parse(lb.CommandArgument));
            StringBuilder sb = new StringBuilder();

            foreach (string s in ranges)
            {
                if (sb.Length > 0)
                    sb.Append(";");
                sb.Append(s);
            }
            LiveStreamPasswordString = sb.ToString();
            BindPasswordList();
        }

        protected void lbAddPassword_Click(object sender, EventArgs e)
        {
            try
            {
                string passwordString = LiveStreamPasswordString;

                if (passwordString != null && passwordString.Length > 0)
                    passwordString += ";";

                string newPw = txtAddPassword.Text;
                passwordString += newPw;
                LiveStreamPasswordString = passwordString;
                txtAddPassword.Text = "";

                BindPasswordList();
            }
            catch (Exception err)
            {
                Trace.Write("err", err.ToString());
                ScriptManager.RegisterStartupScript(this, typeof(Page), "err", "alert(\"" + err.Message + "\");", true);
            }
            
        }

    }
}
