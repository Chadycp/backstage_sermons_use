﻿using System;

namespace BackstageSermons
{
    public class ReportSpeakerML
    {
        public int SpeakerID { get; set; }
        public string SpeakerName { get; set; }
        public int TotalDownloads { get; set; }
        public int TotalListens { get; set; }
    }
}