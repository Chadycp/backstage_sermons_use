﻿using System;

namespace BackstageSermons
{
    public class ReportSeriesML
    {
        public int SeriesID { get; set; }
        public string Title { get; set; }
        public int TotalDownloads { get; set; }
        public int TotalListens { get; set; }
    }
}