﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using System.Data;
using System.Data.SqlClient;
using BackstageSermons.DataLayer.SQLServer.DataSets;
using BackstageSermons.DataLayer.SQLServer.DataSets.BackstageSermonsDataSetTableAdapters;
using BackstageSermons;
using BackstageSQLHelper;
using backstage.Classes;

namespace BackstageSermons.DataLayer.SQLServer
{
    public class SermonsDLL : IBackstageSermonsDataLayer
    {
        #region Constructors

        #region SermonsDLL()

        /// <summary>
        /// Default constructor
        /// </summary>
        public SermonsDLL()
        {
        }

        #endregion

        #endregion

        #region Instance data

        private static readonly SermonsDLL instance = new SermonsDLL();

        /// <summary>
        /// returns the SermonsDLL singleton
        /// </summary>
        public static SermonsDLL Instance
        {
            get { return instance; }
        }

        #endregion

        #region Private methods

        #region Sermon methods

        private SermonML SermonFromRow(BackstageSermonsDataSet.SermonsRow row)
        {
            SermonML sermon = new SermonML();

            //ints
            sermon.SermonID = DBHelper.GetInt32(row, "SermonID");
            sermon.Speaker = DBHelper.GetInt32(row, "Speaker");
            sermon.SeriesID = DBHelper.GetInt32(row, "SeriesID");
           

            //decimals
            sermon.UnitPrice = DBHelper.GetDecimal(row, "UnitPrice");

            //bools
            sermon.Available = DBHelper.GetBool(row, "Available");
            sermon.Featured = DBHelper.GetBool(row, "Featured");

            //DateTimes
            if (!row.IsDateNull()) { sermon.Date = row.Date; }

            //Enums
            string al = DBHelper.GetString(row, "AudioLocation");
            if (Enum.IsDefined(typeof(AudioLocation), al))
                sermon.AudioLocation = (AudioLocation)Enum.Parse(typeof(AudioLocation), al, true);
            else
                sermon.AudioLocation = AudioLocation.Bx;

            string vl = DBHelper.GetString(row, "VideoLocation");
            if (Enum.IsDefined(typeof(VideoLocation), vl))
                sermon.VideoLocation = (VideoLocation)Enum.Parse(typeof(VideoLocation), vl, true);
            else
                sermon.VideoLocation = VideoLocation.Bx;
            sermon.SpeakerNamePrefix = (NamePrefixes?)DBHelper.GetEnum(row, "SpeakerNamePrefix", typeof(NamePrefixes?));
            
            //strings
            sermon.BxAudioFile = DBHelper.GetString(row, "BxAudioFile");
            sermon.ExternalAudioLink = DBHelper.GetString(row, "ExternalAudioLink");
            sermon.SermonAudioID = DBHelper.GetString(row, "SermonAudioID");
            sermon.Bulletin = DBHelper.GetString(row, "Bulletin");
            sermon.Description = DBHelper.GetString(row, "Description");
            sermon.Outline = DBHelper.GetString(row, "Outline");
            sermon.PassageBook = DBHelper.GetString(row, "PassageBook");
            sermon.PassageChapter = DBHelper.GetString(row, "PassageChapter");
            sermon.PassageVerse = DBHelper.GetString(row, "PassageVerse");
            sermon.PassageCopy = DBHelper.GetString(row, "PassageCopy");
            sermon.Time = DBHelper.GetString(row, "Time");
            sermon.Title = DBHelper.GetString(row, "Title");
            sermon.TitleGraphic = DBHelper.GetString(row, "TitleGraphic");
            sermon.BxVideoFile = DBHelper.GetString(row, "BxVideoFile");
            sermon.ExternalVideoLink = DBHelper.GetString(row, "ExternalVideoLink");
            sermon.YouTubeID = DBHelper.GetString(row, "YouTubeID");
            sermon.VimeoID = DBHelper.GetString(row, "VimeoID");
            sermon.SpeakerFirstName = DBHelper.GetString(row, "SpeakerFirstName");
            sermon.SpeakerLastName = DBHelper.GetString(row, "SpeakerLastName");
            sermon.SEODescription = DBHelper.GetString(row, "SEODescription");
            sermon.SEOTitle = DBHelper.GetString(row, "SEOTitle");
            sermon.SeriesTitle = DBHelper.GetString(row, "SeriesTitle");

            return sermon;
        }

        private void SermonRowFromML(BackstageSermonsDataSet.SermonsRow row, SermonML sermon)
        {
            //ints
            row.SermonID = sermon.SermonID;
            row.Speaker = sermon.Speaker;

            //bools
            row.Available = sermon.Available;
            row.Featured = sermon.Featured;

            //decimals
            row.UnitPrice = sermon.UnitPrice;

            //DateTime
            //if (sermon.Date == null || sermon.Date <= DateTime.MinValue)
            System.Web.HttpContext.Current.Trace.Write("sermon date", sermon.Date.ToString());
            if (sermon.Date <= DateTime.MinValue)
            {
                System.Web.HttpContext.Current.Trace.Write("setting date null");
                row.SetDateNull();
            }
            else
            {
                System.Web.HttpContext.Current.Trace.Write("setting sermon date");
                row.Date = sermon.Date;
            }

            //strings

            //AudioLocation
            if (string.IsNullOrEmpty(sermon.AudioLocation.ToString()))
                row.SetAudioLocationNull();
            else
                row.AudioLocation = sermon.AudioLocation.ToString();
            
            //BxAudioFile
            if (string.IsNullOrEmpty(sermon.BxAudioFile))
                row.SetBxAudioFileNull();
            else
                row.BxAudioFile = sermon.BxAudioFile;

            //ExternalAudioLink
            if (string.IsNullOrEmpty(sermon.ExternalAudioLink))
                row.SetExternalAudioLinkNull();
            else
                row.ExternalAudioLink = sermon.ExternalAudioLink;

            //SermonAudioID
            if (string.IsNullOrEmpty(sermon.SermonAudioID))
                row.SetSermonAudioIDNull();
            else
                row.SermonAudioID = sermon.SermonAudioID;
            
            //Bulletin
            if (string.IsNullOrEmpty(sermon.Bulletin))
                row.SetBulletinNull();
            else
                row.Bulletin = sermon.Bulletin;
            
            //Description
            if (string.IsNullOrEmpty(sermon.Description))
                row.SetDescriptionNull();
            else
                row.Description = sermon.Description;

            //Outline
            if (string.IsNullOrEmpty(sermon.Outline))
                row.SetOutlineNull();
            else
                row.Outline = sermon.Outline;

            //PassageBook
            if (string.IsNullOrEmpty(sermon.PassageBook))
                row.SetPassageBookNull();
            else
                row.PassageBook = sermon.PassageBook;

            //PassageChapter
            if (string.IsNullOrEmpty(sermon.PassageChapter))
                row.SetPassageChapterNull();
            else
                row.PassageChapter = sermon.PassageChapter;

            //PassageVerse
            if (string.IsNullOrEmpty(sermon.PassageVerse))
                row.SetPassageVerseNull();
            else
                row.PassageVerse = sermon.PassageVerse;

            //PassageCopy
            if (string.IsNullOrEmpty(sermon.PassageCopy))
                row.SetPassageCopyNull();
            else
                row.PassageCopy = sermon.PassageCopy;

            //Time
            if (string.IsNullOrEmpty(sermon.Time))
                row.SetTimeNull();
            else
                row.Time = sermon.Time;

            //Title
            if (string.IsNullOrEmpty(sermon.Title))
                row.SetTitleNull();
            else
                row.Title = sermon.Title;

            //TitleGraphic
            if (string.IsNullOrEmpty(sermon.TitleGraphic))
                row.SetTitleGraphicNull();
            else
                row.TitleGraphic = sermon.TitleGraphic;
            
            //VideoLocation
            if(string.IsNullOrEmpty(sermon.VideoLocation.ToString()))
                row.SetVideoLocationNull();
            else
                row.VideoLocation = sermon.VideoLocation.ToString();

            //BxVideoFile
            if (string.IsNullOrEmpty(sermon.BxVideoFile))
                row.SetBxVideoFileNull();
            else
                row.BxVideoFile = sermon.BxVideoFile;

            //ExternalVideoLink
            if (string.IsNullOrEmpty(sermon.ExternalVideoLink))
                row.SetExternalVideoLinkNull();
            else
                row.ExternalVideoLink = sermon.ExternalVideoLink;

            //YouTubeID
            if (string.IsNullOrEmpty(sermon.YouTubeID))
                row.SetYouTubeIDNull();
            else
                row.YouTubeID = sermon.YouTubeID;

            //VimeoID
            if (string.IsNullOrEmpty(sermon.VimeoID))
                row.SetVimeoIDNull();
            else
                row.VimeoID = sermon.VimeoID;

            //SEODescription
            if (string.IsNullOrEmpty(sermon.SEODescription))
                row.SetSEODescriptionNull();
            else
                row.SEODescription = sermon.SEODescription;

            //SEOTitle
            if (string.IsNullOrEmpty(sermon.SEOTitle))
                row.SetSEOTitleNull();
            else
                row.SEOTitle = sermon.SEOTitle;
        }

        private SermonML Sermon2SeriesFromRow(BackstageSermonsDataSet.Sermon2SeriesRow row)
        {
            SermonML sermon = new SermonML();

            //ints
            sermon.SermonID = DBHelper.GetInt32(row, "SermonID");
            sermon.Speaker = DBHelper.GetInt32(row, "Speaker");

            //decimals
            sermon.UnitPrice = DBHelper.GetDecimal(row, "UnitPrice");

            //bools
            sermon.Available = DBHelper.GetBool(row, "Available");
            sermon.Featured = DBHelper.GetBool(row, "Featured");

            //DateTimes
            if (!row.IsDateNull()) { sermon.Date = row.Date; }

            //Enums
            string al = DBHelper.GetString(row, "AudioLocation");
            if (Enum.IsDefined(typeof(AudioLocation), al))
                sermon.AudioLocation = (AudioLocation)Enum.Parse(typeof(AudioLocation), al, true);
            else
                sermon.AudioLocation = AudioLocation.Bx;

            string snp = DBHelper.GetString(row, "SpeakerNamePrefix");
            if (Enum.IsDefined(typeof(NamePrefixes), snp))
                sermon.SpeakerNamePrefix = (NamePrefixes)Enum.Parse(typeof(NamePrefixes), snp, true);

            string vl = DBHelper.GetString(row, "VideoLocation");
            if (Enum.IsDefined(typeof(VideoLocation), vl))
                sermon.VideoLocation = (VideoLocation)Enum.Parse(typeof(VideoLocation), vl, true);
            else
                sermon.VideoLocation = VideoLocation.Bx;

            //strings
            sermon.BxAudioFile = DBHelper.GetString(row, "BxAudioFile");
            sermon.ExternalAudioLink = DBHelper.GetString(row, "ExternalAudioLink");
            sermon.SermonAudioID = DBHelper.GetString(row, "SermonAudioID");
            sermon.Bulletin = DBHelper.GetString(row, "Bulletin");
            sermon.Description = DBHelper.GetString(row, "Description");
            sermon.Outline = DBHelper.GetString(row, "Outline");
            sermon.PassageBook = DBHelper.GetString(row, "PassageBook");
            sermon.PassageChapter = DBHelper.GetString(row, "PassageChapter");
            sermon.PassageVerse = DBHelper.GetString(row, "PassageVerse");
            sermon.Time = DBHelper.GetString(row, "Time");
            sermon.Title = DBHelper.GetString(row, "Title");
            sermon.TitleGraphic = DBHelper.GetString(row, "TitleGraphic");
            sermon.BxVideoFile = DBHelper.GetString(row, "VideoLink");
            sermon.ExternalVideoLink = DBHelper.GetString(row, "ExternalVideoLink");
            sermon.YouTubeID = DBHelper.GetString(row, "YouTubeID");
            sermon.SpeakerFirstName = DBHelper.GetString(row, "SpeakerFirstName");
            sermon.SpeakerLastName = DBHelper.GetString(row, "SpeakerLastName");
            sermon.SEODescription = DBHelper.GetString(row, "SEODescription");
            sermon.SEOTitle = DBHelper.GetString(row, "SEOTitle");

            return sermon;
        }

        private SeriesML Series2SermonFromRow(BackstageSermonsDataSet.Series2SermonRow row)
        {
            SeriesML series = new SeriesML();
            series.SeriesID = row.SeriesID;
            series.ParentSeriesID = DBHelper.GetInt32(row, "ParentSeriesID");
            series.UnitPrice = DBHelper.GetDecimal(row, "UnitPrice");

            series.Title = DBHelper.GetString(row, "Title");

            return series;
        }

        #endregion

        #region Sermon Month methods

        private DateTime SermonMonthFromRow(BackstageSermonsDataSet.SermonMonthsRow row)
        {
            return DBHelper.GetDateTime(row, "Month");
        }

        #endregion

        #region SermonPassageBook methods

        private SermonPassageBookML SermonPassageBookFromRow(BackstageSermonsDataSet.SermonPassageBooksRow row)
        {
            SermonPassageBookML book = new SermonPassageBookML();
            book.Name = DBHelper.GetString(row, "PassageBook");
            book.SermonCount = DBHelper.GetInt32(row, "SermonCount");
            return book;
        }

        #endregion

        #region Series methods

        public SeriesML SeriesFromRow(DataRow row)
        {
            SeriesML series = new SeriesML();

            //ints
            series.SeriesID = DBHelper.GetInt32(row, "SeriesID");
            series.ParentSeriesID = DBHelper.GetInt32(row, "ParentSeriesID");
            series.SermonCount = DBHelper.GetInt32(row, "SermonCount");
            series.PodcastImageID = DBHelper.GetInt32(row, "PodcastImageID");

            //decimals
            series.UnitPrice = DBHelper.GetDecimal(row, "UnitPrice");
            
            //bools
            series.Active = DBHelper.GetBool(row, "Active");
            series.IsOpen = DBHelper.GetBool(row, "IsOpen");
            series.Featured = DBHelper.GetBool(row, "Featured");
            
            //strings
            series.Title = DBHelper.GetString(row, "Title");
            series.Description = DBHelper.GetString(row, "Description");
            series.PodcastID = DBHelper.GetString(row, "PodcastID");
            series.PodcastTitle = DBHelper.GetString(row, "PodcastTitle");
            series.PodcastSubtitle = DBHelper.GetString(row, "PodcastSubtitle");
            series.PodcastDescription = DBHelper.GetString(row, "PodcastDescription");
            series.SEOTitle = DBHelper.GetString(row, "SEOTitle");
            series.SEODescription = DBHelper.GetString(row, "SEODescription");
            
            //collections
            string imageIDs = DBHelper.GetString(row, "ImageIDs");
            series.Images = new backstage.security.ImageCollectionML(imageIDs);

            return series;
        }

        private void SeriesRowFromML(BackstageSermonsDataSet.SeriesRow row, SeriesML series)
        {
            //ints
            row.SeriesID = series.SeriesID;
            row.ParentSeriesID = series.ParentSeriesID;
            if (series.PodcastImageID > 0)
                row.PodcastImageID = series.PodcastImageID;
            else
                row.SetPodcastImageIDNull();
            

            //decimals
            row.UnitPrice = series.UnitPrice;

            //bools
            row.Active = series.Active;
            row.IsOpen = series.IsOpen;
            row.Featured = series.Featured;

            //strings
            if (string.IsNullOrEmpty(series.Title))
                row.SetTitleNull();
            else
                row.Title = series.Title;
            if (string.IsNullOrEmpty(series.Description))
                row.SetDescriptionNull();
            else
                row.Description = series.Description;
            if (string.IsNullOrEmpty(series.PodcastID))
                row.SetPodcastIDNull();
            else
                row.PodcastID = series.PodcastID;
            if (string.IsNullOrEmpty(series.PodcastTitle))
                row.SetPodcastTitleNull();
            else
                row.PodcastTitle = series.PodcastTitle;
            if (string.IsNullOrEmpty(series.PodcastSubtitle))
                row.SetPodcastSubtitleNull();
            else
                row.PodcastSubtitle = series.PodcastSubtitle;
            if (string.IsNullOrEmpty(series.PodcastDescription))
                row.SetPodcastDescriptionNull();
            else
                row.PodcastDescription = series.PodcastDescription;
            if (string.IsNullOrEmpty(series.SEOTitle))
                row.SetSEOTitleNull();
            else
                row.SEOTitle = series.SEOTitle;
            if (string.IsNullOrEmpty(series.SEODescription))
                row.SetSEODescriptionNull();
            else
                row.SEODescription = series.SEODescription;

            //collections
            if (series.Images == null)
                row.SetImageIDsNull();
            else
                row.ImageIDs = series.Images.ImageIDs;
        }

        #endregion

        #region SermonFieldValue methods

        private SermonFieldValueML SermonFieldValueFromRow(BackstageSermonsDataSet.SermonFieldValuesRow row)
        {
            SermonFieldValueML val = new SermonFieldValueML();
            val.SermonID = row.SermonID;
            val.FieldID = row.FieldID;
            val.Value = DBHelper.GetString(row, "Value");
            val.IsCreated = true;
            return val;
        }

        private void SermonFieldValuesRowFromML(BackstageSermonsDataSet.SermonFieldValuesRow row, SermonFieldValueML val)
        {
            row.SermonID = val.SermonID;
            row.FieldID = val.FieldID;
            if (string.IsNullOrEmpty(val.Value))
                row.SetValueNull();
            else
                row.Value = val.Value;
        }

        #endregion

        #region SermonField methods

        private SermonFieldML SermonFieldFromRow(BackstageSermonsDataSet.SermonFieldsRow row)
        {
            SermonFieldML field = new SermonFieldML();
            field.FieldID = row.FieldID;
            field.Name = DBHelper.GetString(row, "Name");
            field.FieldType = DBHelper.GetString(row, "FieldType");
            field.Title = DBHelper.GetString(row, "Title");
            field.Description = DBHelper.GetString(row, "Description");
            field.SeriesID = DBHelper.GetInt32(row, "SeriesID");
            field.SermonID = DBHelper.GetInt32(row, "SermonID");
            field.Location = DBHelper.GetString(row, "Location");
            field.DefaultValue = DBHelper.GetString(row, "DefaultValue");
            field.IsCreated = true;
            return field;
        }

        private void SermonFieldRowFromML(BackstageSermonsDataSet.SermonFieldsRow row, SermonFieldML field)
        {
            row.FieldID = field.FieldID;
            if (string.IsNullOrEmpty(field.Name))
                row.SetNameNull();
            else
                row.Name = field.Name;
            if (string.IsNullOrEmpty(field.FieldType))
                row.SetFieldTypeNull();
            else
                row.FieldType = field.FieldType;
            if (string.IsNullOrEmpty(field.Title))
                row.SetTitleNull();
            else
                row.Title = field.Title;
            if (string.IsNullOrEmpty(field.Description))
                row.SetDescriptionNull();
            else
                row.Description = field.Description;
            if (string.IsNullOrEmpty(field.Location))
                row.SetLocationNull();
            else
                row.Location = field.Location;
            if (string.IsNullOrEmpty(field.DefaultValue))
                row.SetDefaultValueNull();
            else
                row.DefaultValue = field.DefaultValue;
            row.SeriesID = field.SeriesID;
            row.SermonID = field.SermonID;
        }

        #endregion

        #region SeriesField methods

        private SeriesFieldML SeriesFieldFromRow(BackstageSermonsDataSet.SeriesFieldsRow row)
        {
            SeriesFieldML field = new SeriesFieldML();
            field.FieldID = row.FieldID;
            field.Name = DBHelper.GetString(row, "Name");
            field.FieldType = DBHelper.GetString(row, "FieldType");
            field.Title = DBHelper.GetString(row, "Title");
            field.Description = DBHelper.GetString(row, "Description");
            field.SeriesID = DBHelper.GetInt32(row, "SeriesID");
            field.Location = DBHelper.GetString(row, "Location");
            field.DefaultValue = DBHelper.GetString(row, "DefaultValue");
            
            field.IsCreated = true;
            return field;
        }

        private void SeriesFieldRowFromML(BackstageSermonsDataSet.SeriesFieldsRow row, SeriesFieldML field)
        {
            row.FieldID = field.FieldID;
            if (string.IsNullOrEmpty(field.Name))
                row.SetNameNull();
            else
                row.Name = field.Name;
            if (string.IsNullOrEmpty(field.FieldType))
                row.SetFieldTypeNull();
            else
                row.FieldType = field.FieldType;
            if (string.IsNullOrEmpty(field.Title))
                row.SetTitleNull();
            else
                row.Title = field.Title;
            if (string.IsNullOrEmpty(field.Description))
                row.SetDescriptionNull();
            else
                row.Description = field.Description;
            if (string.IsNullOrEmpty(field.Location))
                row.SetLocationNull();
            else
                row.Location = field.Location;
            if (string.IsNullOrEmpty(field.DefaultValue))
                row.SetDefaultValueNull();
            else
                row.DefaultValue = field.DefaultValue;
            row.SeriesID = field.SeriesID;
        }

        #endregion

        #region SeriesFieldValue methods

        private SeriesFieldValueML SeriesFieldValueFromRow(BackstageSermonsDataSet.SeriesFieldValuesRow row)
        {
            SeriesFieldValueML val = new SeriesFieldValueML();
            val.SeriesID = row.SeriesID;
            val.FieldID = row.FieldID;
            val.Value = DBHelper.GetString(row, "Value");
            val.IsCreated = true;
            return val;
        }

        private void SeriesFieldValueRowFromML(BackstageSermonsDataSet.SeriesFieldValuesRow row, SeriesFieldValueML val)
        {
            row.SeriesID = val.SeriesID;
            row.FieldID = val.FieldID;
            if (string.IsNullOrEmpty(val.Value))
                row.SetValueNull();
            else
                row.Value = val.Value;
        }

        #endregion

        #region Speaker Methods

        public SpeakerML SpeakerFromRow(DataRow row)
        {
            SpeakerML speaker = new SpeakerML();

            //ints
            speaker.SpeakerID = DBHelper.GetInt32(row, "SpeakerID");
            speaker.SermonCount = DBHelper.GetInt32(row, "SermonCount");
            speaker.PhotoID = DBHelper.GetInt32(row, "PhotoID");

            //strings
            speaker.Bio = DBHelper.GetString(row, "Bio");
            speaker.FirstName = DBHelper.GetString(row, "FirstName");
            speaker.LastName = DBHelper.GetString(row, "LastName");
            speaker.Photo = DBHelper.GetString(row, "Photo");

            //enums
            speaker.NamePrefix = (NamePrefixes?)DBHelper.GetEnum(row, "NamePrefix", typeof(NamePrefixes?));

            return speaker;
        }

        private void SpeakersRowFromML(BackstageSermonsDataSet.SpeakersRow row, SpeakerML speaker)
        {
            //ints
            row.SpeakerID = speaker.SpeakerID;
            row.PhotoID = speaker.PhotoID;

            //strings
            if (string.IsNullOrEmpty(speaker.Bio))
                row.SetBioNull();
            else
                row.Bio = speaker.Bio;

            if (string.IsNullOrEmpty(speaker.FirstName))
                row.SetFirstNameNull();
            else
                row.FirstName = speaker.FirstName;

            if (string.IsNullOrEmpty(speaker.LastName))
                row.SetLastNameNull();
            else
                row.LastName = speaker.LastName;

            if (string.IsNullOrEmpty(speaker.Photo))
                row.SetPhotoNull();
            else
                row.Photo = speaker.Photo;
            if (speaker.NamePrefix == null || string.IsNullOrEmpty(speaker.NamePrefix.Value.ToString()))
                row.SetNamePrefixNull();
            else
                row.NamePrefix = speaker.NamePrefix.Value.ToString();
        }

        #endregion
        
        #region Topic Methods

        private TopicML TopicFromRow(BackstageSermonsDataSet.TopicsRow row)
        {
            TopicML topic = new TopicML();

            //ints
            topic.TopicID = DBHelper.GetInt32(row, "TopicID");
            topic.SermonCount = DBHelper.GetInt32(row, "SermonCount");

            //strings
            topic.Title = DBHelper.GetString(row, "Title");

            return topic;
        }

        private void TopicsRowFromML(BackstageSermonsDataSet.TopicsRow row, TopicML topic)
        {
            row.TopicID = topic.TopicID;
            if (string.IsNullOrEmpty(topic.Title))
                row.SetTitleNull();
            else
                row.Title = topic.Title;
        }

        #endregion

        #region Topic2Sermon Methods

        private TopicML Topic2SermonFromRow(BackstageSermonsDataSet.Topic2SermonRow row)
        {
            TopicML topic = new TopicML();

            //ints
            topic.TopicID = row.TopicID;

            //strings
            topic.Title = DBHelper.GetString(row, "Title");

            return topic;
        }

        #endregion

        #region Setting Methods

        private SettingML SettingFromRow(BackstageSermonsDataSet.SettingsRow row)
        {
            SettingML setting = new SettingML();

            //ints
            setting.SettingID = DBHelper.GetInt32(row, "SettingID");

            //strings
            setting.Name = DBHelper.GetString(row, "Name");
            setting.Value = DBHelper.GetString(row, "Value");

            return setting;
        }

        private void SettingRowFromML(BackstageSermonsDataSet.SettingsRow row, SettingML setting)
        {
            //ints
            row.SettingID = setting.SettingID;

            //string
            row.Name = setting.Name;
            row.Value = setting.Value;
        }

        #endregion

        #region Activity Methods

        private ActivityML ActivityFromRow(BackstageSermonsDataSet.ActivityRow row)
        {
            ActivityML a = new ActivityML();
            a.ActivityDate = DBHelper.GetDateTime(row, "ActivityDate");
            a.ActivityID = DBHelper.GetInt32(row, "ActivityID");
            a.ActivityType = (ActivityTypes)DBHelper.GetEnum(row, "ActivityType", typeof(ActivityTypes));
            a.Location = DBHelper.GetString(row, "Location");
            a.SermonID = DBHelper.GetInt32(row, "SermonID");
            a.SermonTitle = DBHelper.GetString(row, "SermonTitle");
            a.SpeakerID = DBHelper.GetInt32(row, "SpeakerID");
            a.SpeakerFirstName = DBHelper.GetString(row, "SpeakerFirstName");
            a.SpeakerLastName = DBHelper.GetString(row, "SpeakerLastName");
            a.UserAgent = DBHelper.GetString(row, "UserAgent");
            a.IPAddress = DBHelper.GetString(row, "IPAddress");

            return a;
        }

        private void ActivityRowFromML(BackstageSermonsDataSet.ActivityRow row, ActivityML a)
        {
            //ints
            row.ActivityID = a.ActivityID;
            row.SermonID = a.SermonID;

            if (a.ActivityDate <= DateTime.MinValue)
                row.ActivityDate = DateTime.Now;
            else
                row.ActivityDate = a.ActivityDate;

            row.ActivityType = a.ActivityType.ToString();
            if (string.IsNullOrEmpty(a.Location))
                row.SetLocationNull();
            else
                row.Location = a.Location;
            if (string.IsNullOrEmpty(a.UserAgent))
                row.SetUserAgentNull();
            else
                row.UserAgent = a.UserAgent;
            if (string.IsNullOrEmpty(a.IPAddress))
                row.SetIPAddressNull();
            else
                row.IPAddress = a.IPAddress;
        }
        
        #endregion

        #region ReportSeries Methods

        private ReportSeriesML ReportSeriesFromRow(BackstageSermonsDataSet.ReportSeriesRow row)
        {
            ReportSeriesML r = new ReportSeriesML();
            r.SeriesID = DBHelper.GetInt32(row, "SeriesID");
            r.Title = DBHelper.GetString(row, "Title");
            r.TotalDownloads = DBHelper.GetInt32(row, "TotalDownloads");
            r.TotalListens = DBHelper.GetInt32(row, "TotalListens");

            return r;
        }

        #endregion


        #region ReportTopic Methods

        private ReportTopicML ReportTopicFromRow(BackstageSermonsDataSet.ReportTopicsRow row)
        {
            ReportTopicML r = new ReportTopicML();
            r.TopicID = DBHelper.GetInt32(row, "TopicID");
            r.Title = DBHelper.GetString(row, "Title");
            r.TotalDownloads = DBHelper.GetInt32(row, "TotalDownloads");
            r.TotalListens = DBHelper.GetInt32(row, "TotalListens");

            return r;
        }

        #endregion

        #endregion

        #region Public methods

        #region Sermon methods

        public SermonCollectionML LoadAllSermons()
        {
            #region Load from database
            using (SermonsTableAdapter ta = new SermonsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.SermonsDataTable dt = ta.LoadAllSermons();
                SermonCollectionML sermons = new SermonCollectionML();
                foreach (BackstageSermonsDataSet.SermonsRow row in dt)
                {
                    #region Load item

                    SermonML sermon = SermonFromRow(row);
                    sermons.Add(sermon);

                    #endregion
                }

                return sermons;
            }
            #endregion
        }

        public SermonCollectionML LoadAllVisibleSermons()
        {
            #region Load from database
            using (SermonsTableAdapter ta = new SermonsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.SermonsDataTable dt = ta.LoadAllVisibleSermons();
                SermonCollectionML sermons = new SermonCollectionML();
                foreach (BackstageSermonsDataSet.SermonsRow row in dt)
                {
                    #region Load item

                    SermonML sermon = SermonFromRow(row);
                    sermons.Add(sermon);

                    #endregion
                }

                return sermons;
            }
            #endregion
        }

        public SermonCollectionML LoadAllFeaturedSermons()
        {
            #region Load from database
            using (SermonsTableAdapter ta = new SermonsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.SermonsDataTable dt = ta.LoadAllFeaturedSermons();
                SermonCollectionML sermons = new SermonCollectionML();
                foreach (BackstageSermonsDataSet.SermonsRow row in dt)
                {
                    #region Load item

                    SermonML sermon = SermonFromRow(row);
                    sermons.Add(sermon);

                    #endregion
                }

                return sermons;
            }
            #endregion
        }

        public SermonCollectionML LoadLatestSermons(int numberOfSermons)
        {
            #region Load from database
            using (SermonsTableAdapter ta = new SermonsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.SermonsDataTable dt = ta.LoadLatestSermons(numberOfSermons);
                SermonCollectionML sermons = new SermonCollectionML();
                foreach (BackstageSermonsDataSet.SermonsRow row in dt)
                {
                    #region Load item

                    SermonML sermon = SermonFromRow(row);
                    sermons.Add(sermon);

                    #endregion
                }

                return sermons;
            }
            #endregion
        }

        public SermonCollectionML LoadLatestVisibleSermons(int numberOfSermons)
        {
            #region Load from database
            using (SermonsTableAdapter ta = new SermonsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.SermonsDataTable dt = ta.LoadLatestVisibleSermons(numberOfSermons);
                SermonCollectionML sermons = new SermonCollectionML();
                foreach (BackstageSermonsDataSet.SermonsRow row in dt)
                {
                    #region Load item

                    SermonML sermon = SermonFromRow(row);
                    sermons.Add(sermon);

                    #endregion
                }

                return sermons;
            }
            #endregion
        }

        public SermonCollectionML LoadSermonsNotInSeries()
        {
            #region Load from database
            using (SermonsTableAdapter ta = new SermonsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.SermonsDataTable dt = ta.LoadSermonsNotInSeries();
                SermonCollectionML sermons = new SermonCollectionML();
                foreach (BackstageSermonsDataSet.SermonsRow row in dt)
                {
                    #region Load item

                    SermonML sermon = SermonFromRow(row);
                    sermons.Add(sermon);

                    #endregion
                }

                return sermons;
            }
            #endregion
        }

        public SermonCollectionML LoadAllVisibleFeaturedSermons()
        {
            #region Load from database
            using (SermonsTableAdapter ta = new SermonsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.SermonsDataTable dt = ta.LoadAllVisibleFeaturedSermons();
                SermonCollectionML sermons = new SermonCollectionML();
                foreach (BackstageSermonsDataSet.SermonsRow row in dt)
                {
                    #region Load item

                    SermonML sermon = SermonFromRow(row);
                    sermons.Add(sermon);

                    #endregion
                }

                return sermons;
            }
            #endregion
        }

        public SermonCollectionML LoadAllVisibleSermonsBySpeakerID(int speakerID)
        {
            #region Load from database
            using (SermonsTableAdapter ta = new SermonsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.SermonsDataTable dt = ta.LoadAllVisibleSermonsBySpeakerID(speakerID);
                SermonCollectionML sermons = new SermonCollectionML();
                foreach (BackstageSermonsDataSet.SermonsRow row in dt)
                {
                    #region Load item

                    SermonML sermon = SermonFromRow(row);
                    sermons.Add(sermon);

                    #endregion
                }

                return sermons;
            }
            #endregion
        }

        public SermonCollectionML LoadAllVisibleSermonsByTopicID(int topicID)
        {
            #region Load from database
            using (SermonsTableAdapter ta = new SermonsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.SermonsDataTable dt = ta.LoadAllVisibleSermonsByTopicID(topicID);
                SermonCollectionML sermons = new SermonCollectionML();
                foreach (BackstageSermonsDataSet.SermonsRow row in dt)
                {
                    #region Load item

                    SermonML sermon = SermonFromRow(row);
                    sermons.Add(sermon);

                    #endregion
                }

                return sermons;
            }
            #endregion
        }

        public SermonCollectionML LoadAllVisibleSermonsBySeriesID(int seriesID)
        {
            #region Load from database
            using (SermonsTableAdapter ta = new SermonsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.SermonsDataTable dt = ta.LoadAllVisibleSermonsBySeriesID(seriesID);
                SermonCollectionML sermons = new SermonCollectionML();
                foreach (BackstageSermonsDataSet.SermonsRow row in dt)
                {
                    #region Load item

                    SermonML sermon = SermonFromRow(row);
                    sermons.Add(sermon);

                    #endregion
                }

                return sermons;
            }
            #endregion
        }

        public SermonCollectionML LoadAllVisibleSermonsByPassageBook(string passageBook)
        {
            #region Load from database
            using (SermonsTableAdapter ta = new SermonsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.SermonsDataTable dt = ta.LoadAllVisibleSermonsByPassageBook(passageBook);
                SermonCollectionML sermons = new SermonCollectionML();
                foreach (BackstageSermonsDataSet.SermonsRow row in dt)
                {
                    #region Load item

                    SermonML sermon = SermonFromRow(row);
                    sermons.Add(sermon);

                    #endregion
                }

                return sermons;
            }
            #endregion
        }

        public SermonCollectionML LoadAllVisibleSermonsByDateRange(DateTime startDate, DateTime endDate)
        {
            #region Load from database
            using (SermonsTableAdapter ta = new SermonsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.SermonsDataTable dt = ta.LoadAllVisibleSermonsByDateRange(startDate, endDate);
                SermonCollectionML sermons = new SermonCollectionML();
                foreach (BackstageSermonsDataSet.SermonsRow row in dt)
                {
                    #region Load item

                    SermonML sermon = SermonFromRow(row);
                    sermons.Add(sermon);

                    #endregion
                }

                return sermons;
            }
            #endregion
        }

        public SermonCollectionML LoadAllSermonsByFieldValue(int fieldID, string value)
        {
            #region Load from database
            using (SermonsTableAdapter ta = new SermonsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.SermonsDataTable dt = ta.LoadAllSermonsByFieldValue(fieldID, value);
                SermonCollectionML sermons = new SermonCollectionML();
                foreach (BackstageSermonsDataSet.SermonsRow row in dt)
                {
                    #region Load item

                    SermonML sermon = SermonFromRow(row);
                    sermons.Add(sermon);

                    #endregion
                }

                return sermons;
            }
            #endregion
        }

        public SermonCollectionML SearchSermons(bool loadInvisible, string search, string title, string keywords, int speakerID, List<int> seriesIDs, string passageBook, string passageChapter, string passageVerse, DateTime? startDate, DateTime? endDate, string where, string customFieldsWhere, string sortExpression, int rowsPerPage, int pageNumber)
        {
            #region Load from database
            using (SermonsTableAdapter ta = new SermonsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.SermonsDataTable dt = ta.SearchSermons(loadInvisible, search, title, keywords, speakerID, StringEditor.IEnumerableToString(seriesIDs, ","), passageBook, passageChapter, passageVerse, startDate, endDate, where, customFieldsWhere, sortExpression, rowsPerPage, pageNumber);
                SermonCollectionML sermons = new SermonCollectionML();
                foreach (BackstageSermonsDataSet.SermonsRow row in dt)
                {
                    #region Load item

                    SermonML sermon = SermonFromRow(row);
                    sermons.Add(sermon);

                    #endregion
                }

                return sermons;
            }
            #endregion
        }

        #region SermonCollectionML LoadSermonsBySeriesID(int seriesID, bool loadRelatedData)

        public SermonCollectionML LoadSermonsBySeriesID(int SeriesID)
        {
            #region Load from database

            using (Sermon2SeriesTableAdapter ta = new Sermon2SeriesTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.Sermon2SeriesDataTable dt = ta.LoadSermonsBySeriesID(SeriesID);

                SermonCollectionML sermons = new SermonCollectionML();

                foreach (BackstageSermonsDataSet.Sermon2SeriesRow row in dt)
                {

                    #region Load item

                    SermonML sermon = Sermon2SeriesFromRow(row);
                    sermons.Add(sermon);

                    #endregion
                }

                return sermons;
            }

            #endregion
        }

        #endregion

        #region SermonML LoadSermon(int sermonID)

        public SermonML LoadSermon(int sermonID)
        {
            #region Load from database

            using (SermonsTableAdapter ta = new SermonsTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SermonsDataTable dt = ta.LoadSermon(sermonID);
                if (dt.Rows.Count == 0)
                    throw new DBNotFoundException(string.Format("Sermon [{0}] not found", sermonID));

                SermonML sermon = SermonFromRow(dt[0]);

                return sermon;
            }

            #endregion
        }

        #endregion

        #region void InsertSermon(SermonML sermon)

        public void InsertSermon(SermonML sermon)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.SermonsRow row = ds.Sermons.NewSermonsRow();
                    SermonRowFromML(row, sermon);
                    ds.Sermons.AddSermonsRow(row);

                    try
                    {
                        using (SermonsTableAdapter ta = new SermonsTableAdapter())
                        {
                            ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                            ta.Update(ds);
                        }
                    }
                    catch (Exception exInsert)
                    {

                        if (DBHelper.IsDuplicateException(exInsert))
                            throw new DBDuplicateException(exInsert.Message, exInsert);

                        throw exInsert;
                    }

                    ds.AcceptChanges();
                    sermon.SermonID = row.SermonID; // retrieve SeriesID of series that we just inserted...
                }

                #endregion

                ts.Complete();
            }
        }

        #endregion

        public void UpdateSermon(SermonML sermon)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Update item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.SermonsRow row = ds.Sermons.NewSermonsRow();
                    SermonRowFromML(row, sermon);
                    ds.Sermons.AddSermonsRow(row);

                    ds.AcceptChanges();
                    row.SetModified();

                    using (SermonsTableAdapter ta = new SermonsTableAdapter())
                    {
                        ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                        ta.Update(ds);
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        public void DeleteSermon(int sermonID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Delete item

                using (SermonsTableAdapter ta = new SermonsTableAdapter())
                {
                    ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                    ta.Delete(sermonID);
                }

                #endregion

                ts.Complete();
            }
        }

        #endregion

        #region Sermon Month methods

        public List<DateTime> LoadAllSermonMonths()
        {
            #region Load from database
            using (SermonMonthsTableAdapter ta = new SermonMonthsTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SermonMonthsDataTable dt = ta.LoadAllSermonMonths();

                List<DateTime> months = new List<DateTime>();

                foreach (BackstageSermonsDataSet.SermonMonthsRow row in dt)
                {
                    #region Load item
                    months.Add(SermonMonthFromRow(row));

                    #endregion
                }
                return months;
            }

            #endregion
        }

        public List<DateTime> LoadAllVisibleSermonMonths()
        {
            #region Load from database
            using (SermonMonthsTableAdapter ta = new SermonMonthsTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SermonMonthsDataTable dt = ta.LoadAllVisibleSermonMonths();

                List<DateTime> months = new List<DateTime>();

                foreach (BackstageSermonsDataSet.SermonMonthsRow row in dt)
                {
                    #region Load item
                    months.Add(SermonMonthFromRow(row));

                    #endregion
                }
                return months;
            }

            #endregion
        }

        #endregion

        #region SermonPassageBook methods

        public SermonPassageBookCollectionML LoadAllSermonPassageBooks()
        {
            #region Load from database
            using (SermonPassageBooksTableAdapter ta = new SermonPassageBooksTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SermonPassageBooksDataTable dt = ta.LoadAllSermonPassageBooks();

                SermonPassageBookCollectionML books = new SermonPassageBookCollectionML();

                foreach (BackstageSermonsDataSet.SermonPassageBooksRow row in dt)
                {
                    #region Load item
                    books.Add(SermonPassageBookFromRow(row));
                    #endregion
                }
                return books;
            }

            #endregion
        }

        public SermonPassageBookCollectionML LoadAllVisibleSermonPassageBooks()
        {
            #region Load from database
            using (SermonPassageBooksTableAdapter ta = new SermonPassageBooksTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SermonPassageBooksDataTable dt = ta.LoadAllVisibleSermonPassageBooks();

                SermonPassageBookCollectionML books = new SermonPassageBookCollectionML();

                foreach (BackstageSermonsDataSet.SermonPassageBooksRow row in dt)
                {
                    #region Load item
                    books.Add(SermonPassageBookFromRow(row));
                    #endregion
                }
                return books;
            }

            #endregion
        }

        #endregion

        #region Series methods

        public SeriesCollectionML LoadAllSeries()
        {
            #region Load from database

            using (SeriesTableAdapter ta = new SeriesTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SeriesDataTable dt = ta.LoadAllSeries();

                SeriesCollectionML series = new SeriesCollectionML();

                foreach (BackstageSermonsDataSet.SeriesRow row in dt)
                {

                    #region Load item
                    series.Add(SeriesFromRow(row));
                    #endregion
                }

                return series;
            }

            #endregion
        }

        public SeriesCollectionML LoadAllVisibleSeries()
        {
            #region Load from database

            using (SeriesTableAdapter ta = new SeriesTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SeriesDataTable dt = ta.LoadAllVisibleSeries();

                SeriesCollectionML series = new SeriesCollectionML();

                foreach (BackstageSermonsDataSet.SeriesRow row in dt)
                {

                    #region Load item
                    series.Add(SeriesFromRow(row));
                    #endregion
                }

                return series;
            }

            #endregion
        }

        public SeriesCollectionML LoadAllVisibleRootSeries()
        {
            #region Load from database

            using (SeriesTableAdapter ta = new SeriesTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SeriesDataTable dt = ta.LoadAllVisibleRootSeries();

                SeriesCollectionML series = new SeriesCollectionML();

                foreach (BackstageSermonsDataSet.SeriesRow row in dt)
                {

                    #region Load item
                    series.Add(SeriesFromRow(row));
                    #endregion
                }

                return series;
            }

            #endregion
        }

        public SeriesCollectionML LoadAllVisibleRootSeriesWithSermons()
        {
            #region Load from database

            using (SeriesTableAdapter ta = new SeriesTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SeriesDataTable dt = ta.LoadAllVisibleRootSeriesWithSermons();

                SeriesCollectionML series = new SeriesCollectionML();

                foreach (BackstageSermonsDataSet.SeriesRow row in dt)
                {

                    #region Load item
                    series.Add(SeriesFromRow(row));
                    #endregion
                }

                return series;
            }

            #endregion
        }

        public SeriesCollectionML LoadAllVisibleSeriesWithSermons()
        {
            #region Load from database

            using (SeriesTableAdapter ta = new SeriesTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SeriesDataTable dt = ta.LoadAllVisibleSeriesWithSermons();

                SeriesCollectionML series = new SeriesCollectionML();

                foreach (BackstageSermonsDataSet.SeriesRow row in dt)
                {

                    #region Load item
                    series.Add(SeriesFromRow(row));
                    #endregion
                }

                return series;
            }

            #endregion
        }

        public SeriesCollectionML LoadAllSeriesByFieldValue(int fieldID, string value)
        {
            #region Load from database

            using (SeriesTableAdapter ta = new SeriesTableAdapter())
            {
                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SeriesDataTable dt = ta.LoadAllSeriesByFieldValue(fieldID, value);

                SeriesCollectionML series = new SeriesCollectionML();

                foreach (BackstageSermonsDataSet.SeriesRow row in dt)
                {

                    #region Load item
                    series.Add(SeriesFromRow(row));
                    #endregion
                }

                return series;
            }

            #endregion
        }

        public SeriesCollectionML LoadSeriesBySermonID(int SermonID)
        {
            #region Load from database

            using (SeriesTableAdapter ta = new SeriesTableAdapter())
            {
                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SeriesDataTable dt = ta.LoadSeriesBySermonID(SermonID);

                SeriesCollectionML series = new SeriesCollectionML();

                foreach(BackstageSermonsDataSet.SeriesRow row in dt)
                {

                    #region Load item

                    series.Add(SeriesFromRow(row));

                    #endregion
                }

                return series;
            }

            #endregion
        }

        public SeriesML LoadSeries(int seriesId)
        {
            #region Load from database
            using (SeriesTableAdapter ta = new SeriesTableAdapter())
            {
                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SeriesDataTable dt = ta.LoadSeries(seriesId);

                if (dt.Rows.Count == 0)
                    throw new DBNotFoundException(string.Format("Series not found [{0}] not found", seriesId));

                SeriesML series = SeriesFromRow(dt[0]);

                return series;
            }

            #endregion
        }

        public SeriesML LoadVisibleSeries(int seriesId)
        {
            #region Load from database

            using (SeriesTableAdapter ta = new SeriesTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SeriesDataTable dt = ta.LoadVisibleSeries(seriesId);

                if (dt.Rows.Count == 0)
                    throw new DBNotFoundException(string.Format("Series not found [{0}] not found", seriesId));

                SeriesML series = SeriesFromRow(dt[0]);

                return series;
            }

            #endregion
        }

        public void InsertSeries(SeriesML series)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.SeriesRow row = ds.Series.NewSeriesRow();
                    SeriesRowFromML(row, series);
                    ds.Series.AddSeriesRow(row);

                    try
                    {
                        using (SeriesTableAdapter ta = new SeriesTableAdapter())
                        {
                            ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                            ta.Update(ds);
                        }
                        ds.AcceptChanges();
                        series.SeriesID = row.SeriesID; // retrieve SeriesID of series that we just inserted...

                    }
                    catch (Exception exInsert)
                    {

                        if (DBHelper.IsDuplicateException(exInsert))
                            throw new DBDuplicateException(exInsert.Message, exInsert);

                        throw exInsert;
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        public void UpdateSeries(SeriesML series)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {

                    BackstageSermonsDataSet.SeriesRow row = ds.Series.NewSeriesRow();
                    SeriesRowFromML(row, series);
                    ds.Series.AddSeriesRow(row);

                    ds.AcceptChanges();
                    row.SetModified();

                    using (SeriesTableAdapter ta = new SeriesTableAdapter())
                    {
                        ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                        ta.Update(ds);
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        public void DeleteSeries(int seriesId)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Delete item

                using (SeriesTableAdapter ta = new SeriesTableAdapter())
                {
                    ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                    ta.Delete(seriesId);
                }

                #endregion

                ts.Complete();
            }
        }

        #endregion

        #region Sermon2Series Methods

        public void DeleteSermon2Series(int SermonID, int SeriesID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Delete item

                using (Sermon2SeriesTableAdapter ta = new Sermon2SeriesTableAdapter())
                {
                    ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                    ta.Delete(SermonID, SeriesID);
                }

                #endregion

                ts.Complete();
            }
        }

        public void InsertSermon2Series(int SermonID, int SeriesID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.Sermon2SeriesRow row = ds.Sermon2Series.NewSermon2SeriesRow();
                    row.SeriesID = SeriesID;
                    row.SermonID = SermonID;
                    ds.Sermon2Series.AddSermon2SeriesRow(row);

                    try
                    {
                        using (Sermon2SeriesTableAdapter ta = new Sermon2SeriesTableAdapter())
                        {
                            ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                            ta.Update(ds);
                        }
                        ds.AcceptChanges();

                    }
                    catch (Exception exInsert)
                    {

                        if (DBHelper.IsDuplicateException(exInsert))
                            throw new DBDuplicateException(exInsert.Message, exInsert);

                        throw exInsert;
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        #endregion

        #region SermonField methods

        public SermonFieldCollectionML LoadAllSermonFields()
        {
            #region Load from database

            using (SermonFieldsTableAdapter ta = new SermonFieldsTableAdapter())
            {
                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;

                BackstageSermonsDataSet.SermonFieldsDataTable dt = ta.LoadAllSermonFields();
                SermonFieldCollectionML items = new SermonFieldCollectionML();

                foreach (BackstageSermonsDataSet.SermonFieldsRow row in dt)
                {
                    items.Add(SermonFieldFromRow(row));
                }

                return items;
            }
            #endregion
        }

        public SermonFieldCollectionML LoadAllSermonFieldsForSermon(int sermonID, int seriesID)
        {
            #region Load from database

            using (SermonFieldsTableAdapter ta = new SermonFieldsTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SermonFieldsDataTable dt = ta.LoadSermonFieldsForSermon(sermonID, seriesID);

                SermonFieldCollectionML items = new SermonFieldCollectionML();

                foreach (BackstageSermonsDataSet.SermonFieldsRow row in dt)
                {
                    items.Add(SermonFieldFromRow(row));
                }

                return items;
            }
            #endregion
        }

        public void InsertSermonField(SermonFieldML item)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.SermonFieldsRow row = ds.SermonFields.NewSermonFieldsRow();

                    SermonFieldRowFromML(row, item);
                    ds.SermonFields.AddSermonFieldsRow(row);

                    try
                    {
                        using (SermonFieldsTableAdapter ta = new SermonFieldsTableAdapter())
                        {
                            ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                            ta.Update(ds);
                        }
                        ds.AcceptChanges();

                        item.FieldID = ds.SermonFields[0].FieldID;

                    }
                    catch (Exception exInsert)
                    {
                        if (DBHelper.IsDuplicateException(exInsert))
                            throw new DBDuplicateException(exInsert.Message, exInsert);

                        throw exInsert;
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        public void UpdateSermonField(SermonFieldML item)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            int rowsAffected = 0;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Update item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.SermonFieldsRow row = ds.SermonFields.NewSermonFieldsRow();
                    SermonFieldRowFromML(row, item);
                    ds.SermonFields.AddSermonFieldsRow(row);

                    ds.AcceptChanges();
                    row.SetModified();

                    using (SermonFieldsTableAdapter ta = new SermonFieldsTableAdapter())
                    {
                        ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                        rowsAffected = ta.Update(ds);
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        public void DeleteSermonField(int fieldId)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Delete item

                using (SermonFieldsTableAdapter ta = new SermonFieldsTableAdapter())
                {
                    ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                    ta.Delete(fieldId);
                }

                #endregion

                ts.Complete();
            }
        }

        #endregion

        #region SermonFieldValue methods

        public SermonFieldValueCollectionML LoadAllSermonFieldValues()
        {
            #region Load from database

            using (SermonFieldValuesTableAdapter ta = new SermonFieldValuesTableAdapter())
            {
                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;

                BackstageSermonsDataSet.SermonFieldValuesDataTable dt = ta.LoadAllSermonFieldValues();
                SermonFieldValueCollectionML items = new SermonFieldValueCollectionML();

                foreach (BackstageSermonsDataSet.SermonFieldValuesRow row in dt)
                {
                    items.Add(SermonFieldValueFromRow(row));
                }

                return items;
            }
            #endregion
        }

        public SermonFieldValueCollectionML LoadAllSermonFieldValuesBySermonID(int sermonID)
        {
            #region Load from database

            using (SermonFieldValuesTableAdapter ta = new SermonFieldValuesTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SermonFieldValuesDataTable dt = ta.LoadAllSermonFieldValuesBySermonID(sermonID);

                SermonFieldValueCollectionML items = new SermonFieldValueCollectionML();

                foreach (BackstageSermonsDataSet.SermonFieldValuesRow row in dt)
                {
                    items.Add(SermonFieldValueFromRow(row));
                }

                return items;
            }
            #endregion
        }

        public void InsertSermonFieldValue(SermonFieldValueML item)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.SermonFieldValuesRow row = ds.SermonFieldValues.NewSermonFieldValuesRow();

                    SermonFieldValuesRowFromML(row, item);
                    ds.SermonFieldValues.AddSermonFieldValuesRow(row);

                    try
                    {
                        using (SermonFieldValuesTableAdapter ta = new SermonFieldValuesTableAdapter())
                        {
                            ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                            ta.Update(ds);
                        }
                        ds.AcceptChanges();

                    }
                    catch (Exception exInsert)
                    {

                        if (DBHelper.IsDuplicateException(exInsert))
                            throw new DBDuplicateException(exInsert.Message, exInsert);

                        throw exInsert;
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        public void UpdateSermonFieldValue(SermonFieldValueML item)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            int rowsAffected = 0;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Update item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.SermonFieldValuesRow row = ds.SermonFieldValues.NewSermonFieldValuesRow();
                    SermonFieldValuesRowFromML(row, item);
                    ds.SermonFieldValues.AddSermonFieldValuesRow(row);

                    ds.AcceptChanges();
                    row.SetModified();

                    using (SermonFieldValuesTableAdapter ta = new SermonFieldValuesTableAdapter())
                    {
                        ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                        rowsAffected = ta.Update(ds);
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        public void DeleteSermonFieldValue(int sermonID, int fieldID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Delete item

                using (SermonFieldValuesTableAdapter ta = new SermonFieldValuesTableAdapter())
                {
                    ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                    ta.Delete(sermonID, fieldID);
                }

                #endregion

                ts.Complete();
            }
        }

        #endregion

        #region Series Field methods

        public SeriesFieldCollectionML LoadAllSeriesFields()
        {
            #region Load from database

            using (SeriesFieldsTableAdapter ta = new SeriesFieldsTableAdapter())
            {
                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;

                BackstageSermonsDataSet.SeriesFieldsDataTable dt = ta.LoadAllSeriesFields();
                SeriesFieldCollectionML items = new SeriesFieldCollectionML();

                foreach (BackstageSermonsDataSet.SeriesFieldsRow row in dt)
                {
                    items.Add(SeriesFieldFromRow(row));
                }

                return items;
            }
            #endregion
        }

        public SeriesFieldCollectionML LoadAllSeriesFieldsForSeries(int seriesID)
        {
            #region Load from database

            using (SeriesFieldsTableAdapter ta = new SeriesFieldsTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SeriesFieldsDataTable dt = ta.LoadSeriesFieldsForSeries(seriesID);

                SeriesFieldCollectionML items = new SeriesFieldCollectionML();

                foreach (BackstageSermonsDataSet.SeriesFieldsRow row in dt)
                {
                    items.Add(SeriesFieldFromRow(row));
                }

                return items;
            }
            #endregion
        }

        public void InsertSeriesField(SeriesFieldML item)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.SeriesFieldsRow row = ds.SeriesFields.NewSeriesFieldsRow();

                    SeriesFieldRowFromML(row, item);
                    ds.SeriesFields.AddSeriesFieldsRow(row);

                    try
                    {
                        using (SeriesFieldsTableAdapter ta = new SeriesFieldsTableAdapter())
                        {
                            ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                            ta.Update(ds);
                        }
                        ds.AcceptChanges();

                        item.FieldID = ds.SeriesFields[0].FieldID;

                    }
                    catch (Exception exInsert)
                    {
                        if (DBHelper.IsDuplicateException(exInsert))
                            throw new DBDuplicateException(exInsert.Message, exInsert);

                        throw exInsert;
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        public void UpdateSeriesField(SeriesFieldML item)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            int rowsAffected = 0;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Update item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.SeriesFieldsRow row = ds.SeriesFields.NewSeriesFieldsRow();
                    SeriesFieldRowFromML(row, item);
                    ds.SeriesFields.AddSeriesFieldsRow(row);

                    ds.AcceptChanges();
                    row.SetModified();

                    using (SeriesFieldsTableAdapter ta = new SeriesFieldsTableAdapter())
                    {
                        ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                        rowsAffected = ta.Update(ds);
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        public void DeleteSeriesField(int fieldID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Delete item

                using (SeriesFieldsTableAdapter ta = new SeriesFieldsTableAdapter())
                {
                    ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                    ta.Delete(fieldID);
                }

                #endregion

                ts.Complete();
            }
        }

        #endregion

        #region SeriesFieldValue methods

        public SeriesFieldValueCollectionML LoadAllSeriesFieldValues()
        {
            #region Load from database

            using (SeriesFieldValuesTableAdapter ta = new SeriesFieldValuesTableAdapter())
            {
                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;

                BackstageSermonsDataSet.SeriesFieldValuesDataTable dt = ta.LoadAllSeriesFieldValues();
                SeriesFieldValueCollectionML items = new SeriesFieldValueCollectionML();

                foreach (BackstageSermonsDataSet.SeriesFieldValuesRow row in dt)
                {
                    items.Add(SeriesFieldValueFromRow(row));
                }

                return items;
            }
            #endregion
        }

        public SeriesFieldValueCollectionML LoadAllSeriesFieldValuesBySeriesID(int seriesID)
        {
            #region Load from database

            using (SeriesFieldValuesTableAdapter ta = new SeriesFieldValuesTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SeriesFieldValuesDataTable dt = ta.LoadAllSeriesFieldValuesBySeriesID(seriesID);

                SeriesFieldValueCollectionML items = new SeriesFieldValueCollectionML();

                foreach (BackstageSermonsDataSet.SeriesFieldValuesRow row in dt)
                {
                    items.Add(SeriesFieldValueFromRow(row));
                }

                return items;
            }
            #endregion
        }

        public void InsertSeriesFieldValue(SeriesFieldValueML item)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.SeriesFieldValuesRow row = ds.SeriesFieldValues.NewSeriesFieldValuesRow();

                    SeriesFieldValueRowFromML(row, item);
                    ds.SeriesFieldValues.AddSeriesFieldValuesRow(row);

                    try
                    {
                        using (SeriesFieldValuesTableAdapter ta = new SeriesFieldValuesTableAdapter())
                        {
                            ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                            ta.Update(ds);
                        }
                        ds.AcceptChanges();

                    }
                    catch (Exception exInsert)
                    {

                        if (DBHelper.IsDuplicateException(exInsert))
                            throw new DBDuplicateException(exInsert.Message, exInsert);

                        throw exInsert;
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        public void UpdateSeriesFieldValue(SeriesFieldValueML item)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            int rowsAffected = 0;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Update item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.SeriesFieldValuesRow row = ds.SeriesFieldValues.NewSeriesFieldValuesRow();
                    SeriesFieldValueRowFromML(row, item);
                    ds.SeriesFieldValues.AddSeriesFieldValuesRow(row);

                    ds.AcceptChanges();
                    row.SetModified();

                    using (SeriesFieldValuesTableAdapter ta = new SeriesFieldValuesTableAdapter())
                    {
                        ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                        rowsAffected = ta.Update(ds);
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        public void DeleteSeriesFieldValue(int seriesID, int fieldID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Delete item

                using(SeriesFieldValuesTableAdapter ta = new SeriesFieldValuesTableAdapter())
                {
                    ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                    ta.Delete(seriesID, fieldID);
                }

                #endregion

                ts.Complete();
            }
        }

        #endregion

        #region Speaker Methods

        public SpeakerCollectionML LoadAllSpeakers()
        {
            #region Load from database

            using (SpeakersTableAdapter ta = new SpeakersTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.SpeakersDataTable dt = ta.LoadAllSpeakers();
                SpeakerCollectionML speakers = new SpeakerCollectionML();
                foreach (BackstageSermonsDataSet.SpeakersRow row in dt)
                {
                    #region Load item

                    SpeakerML speaker = SpeakerFromRow(row);
                    speakers.Add(speaker);

                    #endregion
                }

                return speakers;
            }
            #endregion
        }

        public SpeakerCollectionML LoadAllVisibleSpeakers()
        {
            #region Load from database

            using (SpeakersTableAdapter ta = new SpeakersTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.SpeakersDataTable dt = ta.LoadAllVisibleSpeakers();
                SpeakerCollectionML speakers = new SpeakerCollectionML();
                foreach (BackstageSermonsDataSet.SpeakersRow row in dt)
                {
                    #region Load item

                    SpeakerML speaker = SpeakerFromRow(row);
                    speakers.Add(speaker);

                    #endregion
                }

                return speakers;
            }
            #endregion
        }

        public SpeakerCollectionML LoadAllVisibleSpeakersWithSermons()
        {
            #region Load from database

            using (SpeakersTableAdapter ta = new SpeakersTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.SpeakersDataTable dt = ta.LoadAllVisibleSpeakersWithSermons();
                SpeakerCollectionML speakers = new SpeakerCollectionML();
                foreach (BackstageSermonsDataSet.SpeakersRow row in dt)
                {
                    #region Load item

                    SpeakerML speaker = SpeakerFromRow(row);
                    speakers.Add(speaker);

                    #endregion
                }

                return speakers;
            }
            #endregion
        }

        public SpeakerML LoadSpeaker(int speakerID)
        {
            #region Load from database

            using (SpeakersTableAdapter ta = new SpeakersTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SpeakersDataTable dt = ta.LoadSpeaker(speakerID);
                if (dt.Rows.Count == 0)
                    throw new DBNotFoundException(string.Format("Speaker [{0}] not found", speakerID));

                SpeakerML speaker = SpeakerFromRow(dt[0]);

                return speaker;
            }

            #endregion
        }

        public SpeakerML LoadVisibleSpeaker(int speakerID)
        {
            #region Load from database

            using (SpeakersTableAdapter ta = new SpeakersTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.SpeakersDataTable dt = ta.LoadVisibleSpeaker(speakerID);
                if (dt.Rows.Count == 0)
                    throw new DBNotFoundException(string.Format("Speaker [{0}] not found", speakerID));

                SpeakerML speaker = SpeakerFromRow(dt[0]);

                return speaker;
            }

            #endregion
        }

        public void InsertSpeaker(SpeakerML speaker)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.SpeakersRow row = ds.Speakers.NewSpeakersRow();

                    SpeakersRowFromML(row, speaker);
                    ds.Speakers.AddSpeakersRow(row);

                    try
                    {
                        using (SpeakersTableAdapter ta = new SpeakersTableAdapter())
                        {
                            ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                            ta.Update(ds);
                        }
                    }
                    catch (Exception exInsert)
                    {

                        if (DBHelper.IsDuplicateException(exInsert))
                            throw new DBDuplicateException(exInsert.Message, exInsert);

                        throw exInsert;
                    }

                    ds.AcceptChanges();
                    speaker.SpeakerID = row.SpeakerID; // retrieve SpeakerID of Speaker that we just inserted...
                }

                #endregion

                ts.Complete();
            }
        }

        public void UpdateSpeaker(SpeakerML speaker)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Update item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.SpeakersRow row = ds.Speakers.NewSpeakersRow();
                    SpeakersRowFromML(row, speaker);
                    ds.Speakers.AddSpeakersRow(row);

                    ds.AcceptChanges();
                    row.SetModified();

                    using (SpeakersTableAdapter ta = new SpeakersTableAdapter())
                    {
                        ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                        ta.Update(ds);
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        public void DeleteSpeaker(int SpeakerID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Delete item

                using (SpeakersTableAdapter ta = new SpeakersTableAdapter())
                {
                    ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                    ta.Delete(SpeakerID);
                }

                #endregion

                ts.Complete();
            }
        }

        #endregion

        #region Topic Methods

        public TopicCollectionML LoadAllTopics()
        {
            #region Load from database

            using (TopicsTableAdapter ta = new TopicsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.TopicsDataTable dt = ta.LoadAllTopics();
                TopicCollectionML topics = new TopicCollectionML();
                foreach (BackstageSermonsDataSet.TopicsRow row in dt)
                {
                    #region Load item

                    TopicML topic = TopicFromRow(row);
                    topics.Add(topic);

                    #endregion
                }

                return topics;
            }
            #endregion
        }

        public TopicCollectionML LoadAllVisibleTopics()
        {
            #region Load from database

            using (TopicsTableAdapter ta = new TopicsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.TopicsDataTable dt = ta.LoadAllVisibleTopics();
                TopicCollectionML topics = new TopicCollectionML();
                foreach (BackstageSermonsDataSet.TopicsRow row in dt)
                {
                    #region Load item

                    TopicML topic = TopicFromRow(row);
                    topics.Add(topic);

                    #endregion
                }

                return topics;
            }
            #endregion
        }

        public TopicCollectionML LoadAllVisibleTopicsWithSermons()
        {
            #region Load from database

            using (TopicsTableAdapter ta = new TopicsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.TopicsDataTable dt = ta.LoadAllVisibleTopicsWithSermons();
                TopicCollectionML topics = new TopicCollectionML();
                foreach (BackstageSermonsDataSet.TopicsRow row in dt)
                {
                    #region Load item

                    TopicML topic = TopicFromRow(row);
                    topics.Add(topic);

                    #endregion
                }

                return topics;
            }
            #endregion
        }

        public void InsertTopic(TopicML topic)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.TopicsRow row = ds.Topics.NewTopicsRow();

                    TopicsRowFromML(row, topic);
                    ds.Topics.AddTopicsRow(row);

                    try
                    {
                        using (TopicsTableAdapter ta = new TopicsTableAdapter())
                        {
                            ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                            ta.Update(ds);
                        }
                    }
                    catch (Exception exInsert)
                    {

                        if (DBHelper.IsDuplicateException(exInsert))
                            throw new DBDuplicateException(exInsert.Message, exInsert);

                        throw exInsert;
                    }

                    ds.AcceptChanges();
                    topic.TopicID = row.TopicID; // retrieve TopicID of Topic that we just inserted...
                }

                #endregion

                ts.Complete();
            }
        }

        public void UpdateTopic(TopicML topic)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Update item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.TopicsRow row = ds.Topics.NewTopicsRow();
                    TopicsRowFromML(row, topic);
                    ds.Topics.AddTopicsRow(row);

                    ds.AcceptChanges();
                    row.SetModified();

                    using (TopicsTableAdapter ta = new TopicsTableAdapter())
                    {
                        ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                        ta.Update(ds);
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        public void DeleteTopic(int TopicID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Delete item

                using (TopicsTableAdapter ta = new TopicsTableAdapter())
                {
                    ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                    ta.Delete(TopicID);
                }

                #endregion

                ts.Complete();
            }
        }

        #endregion

        #region Topic2Sermon Methods

        public TopicCollectionML LoadTopicsBySermonID(int SermonID)
        {
            #region Load from database

            using (Topic2SermonTableAdapter ta = new Topic2SermonTableAdapter())
            {

                ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                BackstageSermonsDataSet.Topic2SermonDataTable dt = ta.LoadTopicsBySermonID(SermonID);

                TopicCollectionML sets = new TopicCollectionML();

                foreach (BackstageSermonsDataSet.Topic2SermonRow row in dt)
                {

                    #region Load item

                    sets.Add(Topic2SermonFromRow(row));

                    #endregion
                }

                return sets;
            }

            #endregion
        }

        public void DeleteTopic2Sermon(int SermonID, int SetID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Delete item

                using (Topic2SermonTableAdapter ta = new Topic2SermonTableAdapter())
                {
                    ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                    ta.Delete(SermonID, SetID);
                }

                #endregion

                ts.Complete();
            }
        }

        public void InsertTopic2Sermon(int SermonID, int SetID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.Topic2SermonRow row = ds.Topic2Sermon.NewTopic2SermonRow();
                    row.SermonID = SermonID;
                    row.TopicID = SetID;
                    ds.Topic2Sermon.AddTopic2SermonRow(row);

                    try
                    {
                        using (Topic2SermonTableAdapter ta = new Topic2SermonTableAdapter())
                        {
                            ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                            ta.Update(ds);
                        }
                        ds.AcceptChanges();

                    }
                    catch (Exception exInsert)
                    {

                        if (DBHelper.IsDuplicateException(exInsert))
                            throw new DBDuplicateException(exInsert.Message, exInsert);

                        throw exInsert;
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        #endregion

        #region Settings Methods

        public SettingCollectionML LoadAllSettings()
        {
            #region Load from database

            using (SettingsTableAdapter ta = new SettingsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.SettingsDataTable dt = ta.LoadAllSettings();
                SettingCollectionML settings = new SettingCollectionML();
                foreach (BackstageSermonsDataSet.SettingsRow row in dt)
                {
                    #region Load item
                    settings.Add(SettingFromRow(row));
                    #endregion
                }

                return settings;
            }
            #endregion
        }

        public void InsertSetting(SettingML setting)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.SettingsRow row = ds.Settings.NewSettingsRow();

                    SettingRowFromML(row, setting);
                    ds.Settings.AddSettingsRow(row);

                    try
                    {
                        using (SettingsTableAdapter ta = new SettingsTableAdapter())
                        {
                            ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                            ta.Update(ds);
                        }
                        ds.AcceptChanges();
                        setting.SettingID = row.SettingID; // retrieve SettingID of SettingML that we just inserted...

                    }
                    catch (Exception exInsert)
                    {

                        if (DBHelper.IsDuplicateException(exInsert))
                            throw new DBDuplicateException(exInsert.Message, exInsert);

                        throw exInsert;
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        public void UpdateSetting(SettingML setting)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Update item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.SettingsRow row = ds.Settings.NewSettingsRow();
                    SettingRowFromML(row, setting);
                    ds.Settings.AddSettingsRow(row);

                    ds.AcceptChanges();
                    row.SetModified();

                    using (SettingsTableAdapter ta = new SettingsTableAdapter())
                    {
                        ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                        ta.Update(ds);
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        public void DeleteSetting(int SettingID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Delete item

                using (SettingsTableAdapter ta = new SettingsTableAdapter())
                {
                    ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                    ta.Delete(SettingID);
                }

                #endregion

                ts.Complete();
            }
        }

        #endregion

        #region Activity Methods

        public ActivityCollectionML LoadAllActivity()
        {
            #region Load from database

            using (ActivityTableAdapter ta = new ActivityTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.ActivityDataTable dt = ta.LoadAllActivity();
                ActivityCollectionML ac = new ActivityCollectionML();
                foreach (BackstageSermonsDataSet.ActivityRow row in dt)
                {
                    #region Load item
                    ac.Add(ActivityFromRow(row));
                    #endregion
                }

                return ac;
            }
            #endregion
        }

        public ActivityCollectionML LoadActivityByDateRange(DateTime startDate, DateTime endDate)
        {
            #region Load from database

            using (ActivityTableAdapter ta = new ActivityTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.ActivityDataTable dt = ta.LoadActivityByDateRange(startDate, endDate);
                ActivityCollectionML ac = new ActivityCollectionML();
                foreach (BackstageSermonsDataSet.ActivityRow row in dt)
                {
                    #region Load item
                    ac.Add(ActivityFromRow(row));
                    #endregion
                }

                return ac;
            }
            #endregion
        }

        public void InsertActivity(ActivityML activity)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.ActivityRow row = ds.Activity.NewActivityRow();

                    ActivityRowFromML(row, activity);
                    ds.Activity.AddActivityRow(row);

                    try
                    {
                        using (ActivityTableAdapter ta = new ActivityTableAdapter())
                        {
                            ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                            ta.Update(ds);
                        }
                        ds.AcceptChanges();
                        activity.ActivityID = row.ActivityID; // retrieve ActivityID of ActivityML that we just inserted...

                    }
                    catch (Exception exInsert)
                    {

                        if (DBHelper.IsDuplicateException(exInsert))
                            throw new DBDuplicateException(exInsert.Message, exInsert);

                        throw exInsert;
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        public void UpdateActivity(ActivityML activity)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Update item

                using (BackstageSermonsDataSet ds = new BackstageSermonsDataSet())
                {
                    BackstageSermonsDataSet.ActivityRow row = ds.Activity.NewActivityRow();
                    ActivityRowFromML(row, activity);
                    ds.Activity.AddActivityRow(row);

                    ds.AcceptChanges();
                    row.SetModified();

                    using (ActivityTableAdapter ta = new ActivityTableAdapter())
                    {
                        ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                        ta.Update(ds);
                    }
                }

                #endregion

                ts.Complete();
            }
        }

        public void DeleteActivity(int activityID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Delete item

                using (ActivityTableAdapter ta = new ActivityTableAdapter())
                {
                    ta.Connection.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                    ta.Delete(activityID);
                }

                #endregion

                ts.Complete();
            }
        }

        #endregion

        #region ReportSeries Methods

        public ReportSeriesCollectionML LoadAllReportSeriesByDateRange(DateTime startDate, DateTime endDate)
        {
            #region Load from database

            using (ReportSeriesTableAdapter ta = new ReportSeriesTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.ReportSeriesDataTable dt = ta.LoadAllReportSeriesByDateRange(startDate, endDate);
                ReportSeriesCollectionML rsc = new ReportSeriesCollectionML();
                foreach (BackstageSermonsDataSet.ReportSeriesRow row in dt)
                {
                    #region Load item
                    rsc.Add(ReportSeriesFromRow(row));
                    #endregion
                }

                return rsc;
            }
            #endregion
        }

        #endregion

        #region ReportTopic Methods

        public ReportTopicCollectionML LoadAllReportTopicsByDateRange(DateTime startDate, DateTime endDate)
        {
            #region Load from database

            using (ReportTopicsTableAdapter ta = new ReportTopicsTableAdapter())
            {
                SqlConnection sc = new SqlConnection();
                sc.ConnectionString = ConnectionBAC.Instance.ConnectionString;
                ta.Connection.ConnectionString = sc.ConnectionString;
                BackstageSermonsDataSet.ReportTopicsDataTable dt = ta.LoadAllReportTopicsByDateRange(startDate, endDate);
                ReportTopicCollectionML rtc = new ReportTopicCollectionML();
                foreach (BackstageSermonsDataSet.ReportTopicsRow row in dt)
                {
                    #region Load item
                    rtc.Add(ReportTopicFromRow(row));
                    #endregion
                }

                return rtc;
            }
            #endregion
        }

        #endregion

        #endregion
    }
}