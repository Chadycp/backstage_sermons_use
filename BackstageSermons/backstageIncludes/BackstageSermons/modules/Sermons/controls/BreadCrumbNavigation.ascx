﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="BreadCrumbNavigation.ascx.cs" Inherits="BackstageSermons.modules.Sermons.controls.BreadCrumbNavigation" %>
<%@ Register Src="~/backstageIncludes/BackstageSermons/components/SeriesFinder/SeriesFinder.ascx" TagName="SeriesFinder" TagPrefix="ctrl" %>

<asp:UpdatePanel runat="server" ID="updateBreadCrumbs" UpdateMode="Always">
	<ContentTemplate>
		<div id="divCategories" class="tab first arrow">
			<div class="bgLeft"></div>
			<div class="chevron"></div>
			All Series
			<!--<div class="arrow"></div>-->
		</div>
		<e:BreadCrumbs runat="server" ID="rptBreadCrumbs" AutoBind="false">
			<ItemTemplate>
				<a class="tab<%# Container.ItemIndex == (((backstage.security.BreadCrumbCollectionML)rptBreadCrumbs.DataSource).Count - 1) ? " selected" : "" %>" href="/backstageIncludes/BackstageSermons/modules/Sermons/Default.aspx?SermonID=<%# Eval("ID") %>">
					<div class="bgLeft"></div>
					<div class="chevron"></div>
					<%# Eval("Name") %>
				</a>
			</ItemTemplate>
		</e:BreadCrumbs>
	</ContentTemplate>
</asp:UpdatePanel>

<ctrl:SeriesFinder runat="server" ID="ctrlSeriesFinder" TriggerClientID="divCategories" BreadCrumbRepeaterID="rptBreadCrubs"></ctrl:SeriesFinder>