﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using backstage.bxpages;
using backstage.Classes;

namespace BackstageSermons.modules.Speakers
{
    public partial class Default : BackstagePage, IComparer<SpeakerML>
    {
        #region Private Properties
        private static SpeakerCollectionML searchSC;
        private static SpeakerCollectionML _sc;
        #endregion

        #region Public Properties
        public string SortExpression
        {
            get
            {
                return backstage.modules.MemberManager.StringHelper.NZ(ViewState["SortExpression"]);
            }
            set
            {
                ViewState["SortExpression"] = value;
            }
        }

        public SortDirection SortDirection
        {
            get
            {
                if (ViewState["SortDirection"] == null)
                    return SortDirection.Ascending;
                return (SortDirection)ViewState["SortDirection"];
            }
            set
            {
                ViewState["SortDirection"] = value;
            }
        }

        public SpeakerSearchStateML searchState
        {
            get
            {
                SpeakerSearchStateML _searchState = (SpeakerSearchStateML)Session["SpeakerSearchStateML"];
                if (_searchState == null) {
                    _searchState = new SpeakerSearchStateML();
                    Session["SpeakerSearchStateML"] = _searchState;
                }
                return _searchState;
            }
            set
            {
                Session["SpeakerSearchStateML"] = value;
            }
        }
        public SpeakerCollectionML sc
        {
            get
            {
                if (_sc == null)
                    _sc = SermonsBLL.Instance.LoadAllSpeakers();
                return _sc;
            }
            set { _sc = value; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) {
                ResetPaging();
                ResetSorting();
                SetDefaults();
                BindSpeakers();

                string sID = Request.QueryString["SpeakerID"];
                if (!string.IsNullOrEmpty(sID))
                    EditSpeaker(StringEditor.ToInt32(sID));
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

        }

        protected void SetDefaults()
        {
        }

        protected void txtSearch_OnTextChanged(object sender, EventArgs e)
        {
            BindSpeakers();
        }

        protected void ddlActive_IndexChanged(object sender, EventArgs e)
        {
            /*
            string strResult = string.Empty;
            foreach (RepeaterItem ri in rptSpeakers.Items)
            {
                try
                {
                    bool visible = Convert.ToBoolean(ddlActive.SelectedValue);
                    CheckBox cb = (CheckBox)ri.FindControl("cbSpeaker");
                    TextBox tb = (TextBox)ri.FindControl("txtSpeakerID");
                    if (cb.Checked)
                    {
                        int SpeakerID = int.Parse(tb.Text);
                        SpeakerML speaker = SpeakersBLL.Instance.LoadSpeaker(SpeakerID, false);
                        speaker.Available = visible;
                        SpeakersBLL.Instance.UpdateSpeaker(speaker);
                    }
                }
                catch (Exception err)
                {
                    strResult += err.Message.ToString() + "<br/>";
                }
            }
            if (string.IsNullOrEmpty(strResult))
            {
                SetDefaults();
                SearchSpeakers();
            }
            else
                lblResult.Text = strResult;
             */
        }

        protected void BindSpeakers()
        {
            searchSC = new SpeakerCollectionML();
            string search = txtSearch.Text.ToLower();
            if (search == "search speakers")
                search = "";
            foreach (SpeakerML speaker in sc) {
                if (string.IsNullOrEmpty(search) || speaker.SpeakerID.ToString().ToLower().Contains(search) || speaker.FullDisplayName.ToLower().Contains(search))
                    searchSC.Add(speaker);
            }

            SortSpeakers();

            gridSpeakers.DataSource = searchSC;
            gridSpeakers.DataBind();

            Session["SpeakerCollection"] = searchSC;
        }

        protected void btnDeleteSpeaker_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow gvRow in gridSpeakers.Rows)
            {
                try
                {
                    CheckBox cb = (CheckBox)gvRow.FindControl("cbSpeaker");
                    HiddenField hf = (HiddenField)gvRow.FindControl("hfSpeakerID");
                    if (cb.Checked)
                    {
                        int SpeakerID = int.Parse(hf.Value);
                        SermonsBLL.Instance.DeleteSpeaker(SpeakerID);
                        sc.Remove(sc.FindSpeakerByID(SpeakerID));
                    }
                }
                catch (Exception err)
                {
                    lblResult.Text = err.Message.ToString();
                }
            }
            SetDefaults();
            BindSpeakers();

            /*
            foreach (RepeaterItem ri in rptSpeakers.Items)
            {
                try
                {
                    CheckBox cb = (CheckBox)ri.FindControl("cbSpeaker");
                    TextBox tb = (TextBox)ri.FindControl("txtSpeakerID");
                    if (cb.Checked)
                    {
                        int SpeakerID = int.Parse(tb.Text);
                        SermonsBLL.Instance.DeleteSpeaker(SpeakerID);
                        sc.Remove(sc.FindSpeakerByID(SpeakerID));
                    }
                }
                catch (Exception err)
                {
                    Trace.Write(err.ToString());
                    lblResult.Text = err.Message.ToString();
                }
            }
            SetDefaults();
            BindSpeakers();
             */
        }

        protected void SortSpeakers()
        {
            if (SortExpression.Length > 0) {

                searchState.SortField = SortExpression;
                searchState.SortDirection = SortDirection;
                searchSC.Sort(this);
            }
            /*
            //Reset Arrows
            imgSortName.Visible = false;
            lbSortNameClear.Visible = false;

            if (curHeader != null)
            {
                bool hasSort = (!string.IsNullOrEmpty(curHeader.SortDirection));
                switch (curHeader.Name)
                {
                    case "Name":
                        if (curHeader.SortDirection == "Descending")
                        {
                            searchSC.Sort(delegate(SpeakerML speaker1, SpeakerML speaker2) { return speaker2.Name.CompareTo(speaker1.Name); });
                            imgSortName.ImageUrl = "/backstage/images/upsimple.png";
                        }
                        else if (curHeader.SortDirection == "Ascending")
                        {
                            searchSC.Sort(delegate(SpeakerML speaker1, SpeakerML speaker2) { return speaker1.Name.CompareTo(speaker2.Name); });
                            imgSortName.ImageUrl = "/backstage/images/downsibtnUpdateSpeaker_Clickmple.png";
                        }
                        imgSortName.Visible = hasSort;
                        lbSortNameClear.Visible = hasSort;
                        break;
                }
            }
            */
        }


        protected void gridSpeakers_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try {
                if (e.CommandName == "DeleteSpeaker") {
                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                    int sID = int.Parse(e.CommandArgument.ToString());
                    SermonsBLL.Instance.DeleteSpeaker(sID);
                    sc.Remove(sc.FindSpeakerByID(sID));
                    BindSpeakers();
                }
            } catch (Exception err) {
                AlertMessage(err.Message);
            }
        }

        protected void gridSpeakers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridSpeakers.PageIndex = e.NewPageIndex;
            //BindData();
            BindSpeakers();
        }

        public void gridSpeakers_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row != null && e.Row.RowType == DataControlRowType.Header) {
                foreach (TableCell cell in e.Row.Cells) {
                    if (cell.HasControls()) {
                        LinkButton button = cell.Controls[0] as LinkButton;

                        if (button != null) {
                            if (SortExpression == button.CommandArgument) {
                                if (SortDirection == SortDirection.Ascending)
                                    cell.CssClass = "asc";
                                else
                                    cell.CssClass = "desc";
                            }
                        }
                    }
                }
            }
        }

        protected void gridSpeakers_DataBound(object sender, System.EventArgs e)
        {
            SpeakerPrintListCollectionML splc = (SpeakerPrintListCollectionML)Session["SpeakerPrintListCollectionML"];

            if (splc == null) {
                //lblSelected.Text = "0";
                return;
            }

            foreach (GridViewRow gvRow in gridSpeakers.Rows) {
                CheckBox cb = (CheckBox)gvRow.Cells[0].FindControl("cbSpeaker");
                if (cb != null) {
                    SpeakerCollectionML sc = (SpeakerCollectionML)gridSpeakers.DataSource;
                    //throw new Exception(gvRow.DataItemIndex.ToString());
                    int sID = sc[gvRow.DataItemIndex].SpeakerID;
                    SpeakerPrintListML spl = splc.GetBySpeakerID(sID);
                    cb.Checked = (spl != null);
                }
            }


            //GridViewRow newRow = new GridViewRow();

            //lblSelected.Text = plc.Count.ToString();
        }

        protected void gridSpeakers_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (SortExpression == e.SortExpression) {
                SortDirection = (SortDirection == SortDirection.Ascending) ? SortDirection.Descending : SortDirection.Ascending;
            } else {
                SortExpression = e.SortExpression;
                SortDirection = SortDirection.Ascending;
            }
            ResetPaging();
            BindSpeakers();
            //BindData();
            updateSpeakers.Update();
        }

        private void ResetPaging()
        {
            try {
                gridSpeakers.PageIndex = 0;
            } catch {
            }
        }

        private void ResetSorting()
        {
            try {
                SortExpression = "LastName";
                SortDirection = SortDirection.Ascending;
            } catch {
            }
        }

        #region Sorting Methods
        public int Compare(SpeakerML x, SpeakerML y)
        {
            try {
                int result = GetCompareResult(x, y);

                #region Handle sorting direction

                if (searchState.SortDirection == SortDirection.Descending)
                    result = result * -1;

                #endregion


                return result;

            } catch (Exception err) {
                System.Web.HttpContext.Current.Trace.Write(err.ToString());
            }

            return 0;
        }

        public int GetCompareResult(SpeakerML x, SpeakerML y)
        {
            int result = 0;

            if (searchState.SortField == "FirstName") {
                result = CompareStrings(x.FirstName, y.FirstName);
            }
            else if (searchState.SortField == "LastName")
            {
                result = CompareStrings(x.LastName, y.LastName);
            }
            else if (searchState.SortField == "NamePrefix")
            {
                result = CompareStrings((x.NamePrefix == null ? "" : x.NamePrefix.ToString()), (y.NamePrefix == null ? "" : y.NamePrefix.ToString()));
            }
            return result;
        }

        public int CompareStrings(string s1, string s2)
        {
            int result = 0;
            if (string.IsNullOrEmpty(s1)) {
                if (string.IsNullOrEmpty(s2)) {
                    return 0;
                } else {
                    return -1;
                }
            } else if (string.IsNullOrEmpty(s2)) {
                return 1;
            }

            int s1Length = s1.Length;
            int s2Length = s2.Length;

            bool sp1 = char.IsLetterOrDigit(s1[0]);
            bool sp2 = char.IsLetterOrDigit(s2[0]);

            if (sp1 && !sp2) {
                return 1;
            }
            if (!sp1 && sp2) {
                return -1;
            }

            char c1, c2;
            int i1 = 0, i2 = 0;
            int r = 0;
            bool letter1, letter2;

            while (true) {
                c1 = s1[i1];
                c2 = s2[i2];

                sp1 = char.IsDigit(c1);
                sp2 = char.IsDigit(c2);

                if (!sp1 && !sp2) {
                    if (c1 != c2) {
                        letter1 = char.IsLetter(c1);
                        letter2 = char.IsLetter(c2);

                        if (letter1 && letter2) {
                            c1 = char.ToUpper(c1);
                            c2 = char.ToUpper(c2);

                            r = c1 - c2;
                            if (0 != r) {
                                return r;
                            }
                        } else if (!letter1 && !letter2) {
                            r = c1 - c2;
                            if (0 != r) {
                                return r;
                            }
                        } else if (letter1) {
                            return 1;
                        } else if (letter2) {
                            return -1;
                        }
                    }

                } else if (sp1 && sp2) {
                    r = CompareNumbers(s1, s1Length, ref i1, s2, s2Length, ref i2);
                    if (0 != r) {
                        result = r;
                        break;
                        //return r;
                    }
                } else if (sp1) {
                    return -1;
                } else if (sp2) {
                    return 1;
                }

                i1++;
                i2++;

                if (i1 >= s1Length) {
                    if (i2 >= s2Length) {
                        return 0;
                    } else {
                        return -1;
                    }
                } else if (i2 >= s2Length) {
                    return 1;
                }
            }
            return result;
        }

        private static int CompareNumbers(
        string s1, int s1Length, ref int i1,
        string s2, int s2Length, ref int i2)
        {
            int nzStart1 = i1, nzStart2 = i2;
            int end1 = i1, end2 = i2;

            ScanNumber(s1, s1Length, i1, ref nzStart1, ref end1);
            ScanNumber(s2, s2Length, i2, ref nzStart2, ref end2);

            int start1 = i1;
            i1 = end1 - 1;
            int start2 = i2;
            i2 = end2 - 1;

            int length1 = end2 - nzStart2;
            int length2 = end1 - nzStart1;

            if (length1 == length2) {
                int r;
                for (int j1 = nzStart1, j2 = nzStart2; j1 <= i1; j1++, j2++) {
                    r = s1[j1] - s2[j2];
                    if (0 != r) return r;
                }

                length1 = end1 - start1;
                length2 = end2 - start2;

                if (length1 == length2) return 0;
            }

            if (length1 > length2) return -1;
            return 1;
        }

        private static void ScanNumber(string s, int length, int start, ref int nzStart, ref int end)
        {
            nzStart = start;
            end = start;

            bool countZeros = true;
            char c = s[end];

            while (true) {
                if (countZeros) {
                    if ('0' == c) {
                        nzStart++;
                    } else {
                        countZeros = false;
                    }
                }

                end++;
                if (end >= length) break;

                c = s[end];
                if (!char.IsDigit(c)) break;
            }
        }
        #endregion

        protected void lbPrint_Click(object sender, EventArgs e)
        {
            if (Session["SpeakerCollection"] == null)
                BindSpeakers();
            ScriptManager.RegisterStartupScript(this, typeof(Page), "printTrigger", "printPage();", true);
        }

        protected void btnEditSpeaker_Click(object sender, EventArgs e)
        {
            int sID = 0;
            if (sender.GetType() == typeof(ImageButton))
                sID = int.Parse(((ImageButton)sender).CommandArgument);
            else
                sID = int.Parse(((LinkButton)sender).CommandArgument);
            EditSpeaker(sID);
            updatePnlEditSpeaker.Update();

            if (sID == 0)
                editSpeakerTitle.InnerText = "Add Speaker";
            else
                editSpeakerTitle.InnerText = "Edit Speaker";

        }

        protected void EditSpeaker(int sID)
        {
            txtEditSpeakerID.Text = sID.ToString();

            if (sID == 0)
            {
                ipPhoto.ImageID = "";
                ddlEditNamePrefix.SelectedValue = "";
                txtEditFirstName.Text = "";
                txtEditLastName.Text = "";
                ckEditBio.Text = "";
            }
            else
            {
                SpeakerML s = sc.FindSpeakerByID(sID);
                ipPhoto.ImageID = s.PhotoID.ToString();
                if (s.NamePrefix == null)
                    ddlEditNamePrefix.SelectedValue = "";
                else
                    ddlEditNamePrefix.SelectedValue = s.NamePrefix.Value.ToString();
                txtEditFirstName.Text = s.FirstName;
                txtEditLastName.Text = s.LastName;
                ckEditBio.Text = s.Bio;
            }
            pnlEditSpeaker.Visible = true;
        }

        protected void rptSpeakers_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            int sID = int.Parse(e.CommandArgument.ToString());

            if (e.CommandName == "DeleteSpeaker") {
                SermonsBLL.Instance.DeleteSpeaker(sID);
                sc.Remove(sc.FindSpeakerByID(sID));
                BindSpeakers();
            }
        }

        protected void btnUpdateSpeaker_Click(object sender, EventArgs e)
        {
            int sID = int.Parse(txtEditSpeakerID.Text);
            SpeakerML s = null;
            if (sID == 0)
                s = new SpeakerML();
            else
                s = sc.FindSpeakerByID(sID);

            s.PhotoID = StringEditor.ToInt32(ipPhoto.ImageID);
            s.NamePrefix = (NamePrefixes?)backstage.Classes.StringEditor.ToEnum(ddlEditNamePrefix.SelectedValue, typeof(NamePrefixes?));
            s.FirstName = txtEditFirstName.Text;
            s.LastName = txtEditLastName.Text;
            s.Bio = Server.HtmlDecode(ckEditBio.Text);

            if (sID == 0) {
                SermonsBLL.Instance.InsertSpeaker(s);
                sc = null;
            } else
                SermonsBLL.Instance.UpdateSpeaker(s);

            ipPhoto.ImageID = "";
            ddlEditNamePrefix.SelectedValue = "";
            txtEditFirstName.Text = "";
            txtEditLastName.Text = "";
            //ckEditBio.Text = "";
            txtEditSpeakerID.Text = "";

            BindSpeakers();
            pnlEditSpeaker.Visible = false;
            updateSpeakers.Update();
        }

        /*
        protected void AlertMessage(string msg)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertMessage", "alert(\"" + msg + "\");", true);
        }
        */

        protected void btnCancelEdit_Click(object sender, EventArgs e)
        {
            pnlEditSpeaker.Visible = false;
            updatePnlEditSpeaker.Update();
        }

        #region Select/Deselect All
        private void SelectAll()
        {
            try {
                SpeakerCollectionML scTemp = (SpeakerCollectionML)Session["SpeakerCollection"];
                if (scTemp == null)
                    return;
                SpeakerPrintListCollectionML splc = (SpeakerPrintListCollectionML)Session["SpeakerPrintListCollectionML"];

                if (splc == null) {
                    splc = new SpeakerPrintListCollectionML();
                    Session["SpeakerPrintListCollectionML"] = splc;
                }

                foreach (SpeakerML s in scTemp) {
                    SpeakerPrintListML spl = new SpeakerPrintListML();
                    spl.SpeakerID = s.SpeakerID;
                    splc.Add(spl);
                }
            } catch (Exception err) {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "errAlert", "alert('" + err.Message.ToString() + "');", true);
            }
            BindSpeakers();
        }

        private void DeselectAll()
        {
            try {
                Session.Remove("SpeakerPrintListCollectionML");
            } catch (Exception err) {
                AlertMessage(err.Message);
            }
            BindSpeakers();
        }

        protected void lbSelectAllSpeakers_Click(object sender, EventArgs e)
        {
            bool chckd = false;
            if (sender.GetType() == typeof(CheckBox))
                chckd = !cbSelectAllSpeakers.Checked;
            else
                chckd = (cbSelectAllSpeakers.Checked);

            if (chckd) {
                DeselectAll();
                lblSelectAllSpeakers.Text = "Select All";
                cbSelectAllSpeakers.Checked = false;
            } else {
                SelectAll();
                lblSelectAllSpeakers.Text = "Deselect All";
                cbSelectAllSpeakers.Checked = true;
            }
            SpeakerPrintListCollectionML slc = (SpeakerPrintListCollectionML)Session["SpeakerPrintListCollectionML"];
            /*
            if (slc == null)
                lblSelectedSpeakers.Text = "0";
            else
                lblSelectedSpeakers.Text = plc.Count.ToString();
             */

            updateSpeakers.Update();
        }
        #endregion
    }
}
