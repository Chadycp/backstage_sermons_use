﻿<%@ Page Language="C#" MasterPageFile="~/backstageIncludes/BackstageSermons/SermonsNew.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BackstageSermons.modules.Settings.Default" %>

<asp:Content ID="contentStyles" ContentPlaceHolderID="stylesheets" runat="server">
    <%
        //Style Bundles
        Bundles.Reference("/backstageIncludes/BackstageSermons/css/modules/Sermons/SermonStyles.css");
    %>
    <style type="text/css">   
	
		.subNavContainer .bxIcon {
			font-size: 20px;
			float: left;
			display: block;
			margin-top: -4px;
			margin-right: 6px;
			margin-left: -25px;
			
		}
	</style>
</asp:Content>

<asp:Content ID="contentScripts" ContentPlaceHolderID="scripts" runat="server">
    <%
        //Script Bundles
        Bundles.Reference("/backstageIncludes/BackstageSermons/js/modules/Settings/settings.js");
    %>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <title>Backstage Sermons :: SermonAudio Manager</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
<asp:Label runat="server" ID="lblResult" style="color: #990000;"></asp:Label>

<div class="sectionContent clearfix">
	<div class="leftColumnWrapper" style="width:225px;position:relative;z-index:3">
		<div class="leftColumn">
			<div class="subNavContainer">
		        <div class="subNav subActive"><a href="#"><span class="bxIcon bxIcon-Gear"></span>General</a></div>
			</div>
		</div>
	</div>
	<div class="rightColumnWrapper" style="width:755px;position:relative;z-index:2">
		<div class="rightColumn">
			<div class="toolbarContainer">
				<div class="toolbar clearfix">
					<div class="rightButtons clearfix">
						<asp:LinkButton runat="server" ID="btnUpdate" CssClass="bxButton2 green" OnClick="btnUpdate_Click" Text="Save Changes" />
					</div>
				</div>
			</div>
			<div class="tableContainer" style="padding:30px">
				<h1>Settings</h1>
                <div class="grid">
                    <div class="col-1-3">
				        <asp:CheckBox runat="server" ID="cbEnableFeatured" CssClass="bxInput" Text="Enable Featured?" />
                    </div>
                    <div class="col-1-3">
                        <asp:CheckBox runat="server" ID="cbEnableSharing" CssClass="bxInput" Text="Enable Sharing?" />
                    </div>
                    <div class="col-1-3">
                        <asp:CheckBox runat="server" ID="cbEnableLivestream" CssClass="bxInput" Text="Enable Livestream?" />
                    </div>
                </div>
				<div class="grid">
                    <div class="col-1-2">
                        <label class="full">Time Zone:</label>
                        <cms:TimeZoneDropDownList runat="server" ID="ddlTimeZones" CssClass="chosen-select" Style="width: 50%;" DisplayUSCanadaTimeZonesFirst="true"></cms:TimeZoneDropDownList>
                    </div>
                    <div class="col-1-2">
                        <label class="full">Series Image Dimensions (400x200)</label>
                        <asp:TextBox runat="server" ID="txtSeriesImageDimensions"></asp:TextBox>
                    </div>
				</div>
				<div class="grid">
                    <div class="col-1-2">
                        <label class="full">Sermons Per Page (in Sermon Module)</label>
                        <asp:TextBox runat="server" ID="txtSermonsPerPageModule"></asp:TextBox>
                    </div>
                    <div class="col-1-2">
                        <label class="full">Sermons Per Page (Web-site)</label>
                        <asp:TextBox runat="server" ID="txtSermonsPerPageSite"></asp:TextBox>
                    </div>
				</div>

                <div class="grid">
                    <div class="col-1-2">
                        <label class="full">SeriesDetail Page  [/pages/sermons/series-detail.aspx]</label>
                        <asp:TextBox runat="server" ID="txtSeriesDetailPage"></asp:TextBox>
                    </div>
                    <div class="col-1-2">
                        <label class="full">Detail Page  [/pages/sermons/detail.aspx]</label>
                        <asp:TextBox runat="server" ID="txtDetailPage"></asp:TextBox>
                    </div>
				</div>

                <label class="full">Bible Version (for Ministry Compass)</label>
                <asp:DropDownList runat="server" ID="ddBibleVersion" CssClass="chosen-select" Style="width: 100px;"></asp:DropDownList>

                
			</div>
		</div>
	</div>
</div>  

</asp:Content>