﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace BackstageSermons
{
    public static class SermonsCache
    {
        #region private
        private static SettingCollectionML _SettingCollection;
        #endregion

        #region public
        public static SettingCollectionML SettingCollection
        {
            get
            {
                if (_SettingCollection == null)
                    _SettingCollection = SermonsBLL.Instance.LoadAllSettings();
                return _SettingCollection;
            }
            set { _SettingCollection = value; }
        }
        #endregion
    }
}
