﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackstageSermons
{
    public class TopicPrintListML
    {
        #region Public fields

        public int TopicID { get; set; }
        public string Title { get; set; }

        #endregion

        #region Construction

        #region TopicPrintListML()

        public TopicPrintListML()
        {
        }

        #endregion

        #endregion
    }
}