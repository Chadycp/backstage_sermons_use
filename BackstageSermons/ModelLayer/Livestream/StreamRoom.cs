﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace BackstageSermons.Livestream
{
    public class StreamRoom
    {
        public StreamRoom()
        {
            this.streamers = new StreamerCollectionML();
            //periodically kick out the expired streamer
            TimerCallback callback = new TimerCallback(this.OnClearExpiredStreamer);
            this.timer = new Timer(callback, null, ClearStreamerInterval, ClearStreamerInterval);
        }

        private static StreamRoom _Instance = new StreamRoom();
        public static StreamRoom Instance//singleton design pattern to ensure there is only a StreamRoom instance
        {
            get
            {
                return _Instance;
            }
        }

        private static readonly long ClearStreamerInterval = (long)(100);//set the interval to 1/2 minute to invoke the timer function
        private static readonly TimeSpan ExpiringInterval = new TimeSpan(0, 1, 0);// set the expiration limit to 1 minutes
        private Timer timer = null;
        private StreamerCollectionML streamers = null;//the collection of the streamers
        private object islock = new object();

        private static DateTime LastClear { get; set; }
        public DateTime GetLastClear()
        {
            return LastClear;
        }
        //execute the function periodically, and kick out the expired streamer
        private void OnClearExpiredStreamer(object state)
        {
            LastClear = DateTime.Now;
            lock (islock)//prevent cooccurrence
            {
                List<StreamerML> expiredStreamers = new List<StreamerML>();//the expired streamer
                foreach (StreamerML s in this.streamers)//loop through all the streamers and find out the expired streamer
                {
                    if (s.IsExpired(ExpiringInterval))
                    {
                        expiredStreamers.Add(s);
                    }
                }
                //kick out the expired streamer
                foreach (StreamerML s in expiredStreamers)
                {
                    this.streamers.Remove(s);
                }
            }
        }

        //judge whether the streamer is in the room
        public bool StreamerIsInRoom(string sessionID)
        {
            return (GetStreamer(sessionID) != null);
        }
        //get the streamer
        public StreamerML GetStreamer(string sessionID)
        {
            return this.streamers.GetStreamerBySessionID(sessionID);
        }
        //add a streamer
        public StreamerML AddStreamer(string streamerID)
        {
            lock (islock)//Lock it
            {
                StreamerML u = new StreamerML(streamerID);
                this.streamers.Add(u);
                return u;
            }
        }

        public void RemoveStreamer(StreamerML streamer)
        {
            lock (islock)//Lock it
            {
                this.streamers.Remove(streamer);
            }
        }

        //Get All Streamers
        public StreamerCollectionML GetAllStreamers()
        {
            return this.streamers;
        }
    }
}
