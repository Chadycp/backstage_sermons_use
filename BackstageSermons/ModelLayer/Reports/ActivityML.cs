﻿using System;

namespace BackstageSermons
{
    public enum ActivityTypes { Unknown, Download, Listen, Livestream, Podcast, Share }
    public class ActivityML
    {
        public int ActivityID { get; set; }
        public DateTime ActivityDate { get; set; }
        public ActivityTypes ActivityType { get; set; }
        public int SermonID { get; set; }
        public string SermonTitle { get; set; }
        public int SpeakerID { get; set; }
        public string SpeakerFirstName { get; set; }
        public string SpeakerLastName { get; set; }
        public string Location { get; set; }
        public string UserAgent { get; set; }
        public string IPAddress { get; set; }
    }
}