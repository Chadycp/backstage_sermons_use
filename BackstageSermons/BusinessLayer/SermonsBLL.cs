﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Transactions;
using BackstageSQLHelper;

namespace BackstageSermons
{
    public class SermonsBLL
    {

        #region Constructors

        #region SermonsBLL()

        /// <summary>
        /// Default constructor
        /// </summary>
        public SermonsBLL()
        {
        }

        #endregion

        #endregion

        #region Instance data

        private static readonly SermonsBLL instance = new SermonsBLL();

        /// <summary>
        /// returns the SermonsBLL singleton
        /// </summary>
        public static SermonsBLL Instance
        {
            get { return instance; }
        }

        #endregion

        #region Public properties
        private string _domain;
        public string domain
        {
            get
            {
                if (string.IsNullOrEmpty(_domain))
                    _domain = System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Host;
                return _domain;
            }
        }
        #endregion

        #region Public methods

        #region Series methods

        public SeriesCollectionML LoadAllSeries(bool filterActiveSeries, bool loadRelatedData)
        {
            SeriesCollectionML sc;
            if (filterActiveSeries)
            {
                sc = SermonsDLL.Instance.LoadAllVisibleSeries();
            }
            else
                sc = SermonsDLL.Instance.LoadAllSeries();


            if (loadRelatedData)
            {
                foreach (SeriesML s in sc)
                {
                    s.Sermons = LoadSermonsBySeriesID(s.SeriesID, false);
                }
            }

            return sc;
        }

        public SeriesCollectionML LoadAllVisibleRootSeries(bool loadRelatedData)
        {
            SeriesCollectionML sc = SermonsDLL.Instance.LoadAllVisibleRootSeries();

            if (loadRelatedData)
            {
                foreach (SeriesML s in sc)
                {
                    s.Sermons = LoadSermonsBySeriesID(s.SeriesID, false);
                }
            }

            return sc;
        }

        public SeriesCollectionML LoadAllVisibleRootSeriesWithSermons(bool loadRelatedData)
        {
            SeriesCollectionML sc = SermonsDLL.Instance.LoadAllVisibleRootSeriesWithSermons();

            if (loadRelatedData)
            {
                foreach (SeriesML s in sc)
                {
                    s.Sermons = LoadSermonsBySeriesID(s.SeriesID, false);
                }
            }

            return sc;
        }

        public SeriesCollectionML LoadAllVisibleSeriesWithSermons(bool loadRelatedData)
        {
            SeriesCollectionML sc = SermonsDLL.Instance.LoadAllVisibleSeriesWithSermons();

            if (loadRelatedData)
            {
                foreach (SeriesML s in sc)
                {
                    s.Sermons = LoadSermonsBySeriesID(s.SeriesID, false);
                }
            }

            return sc;
        }

        public SeriesCollectionML LoadAllSeriesByFieldValue(int fieldID, string value)
        {
            return LoadAllSeriesByFieldValue(fieldID, value, false);
        }

        public SeriesCollectionML LoadAllSeriesByFieldValue(int fieldID, string value, bool loadRelatedData)
        {
            SeriesCollectionML sc = SermonsDLL.Instance.LoadAllSeriesByFieldValue(fieldID, value);
            if (loadRelatedData)
            {
                foreach (SeriesML s in sc)
                {
                    s.Sermons = LoadSermonsBySeriesID(s.SeriesID, false);
                }
            }
            return sc;
        }

        public SeriesCollectionML LoadAllVisibleSeriesByFieldValue(int fieldID, string value, bool loadRelatedData)
        {
            SeriesCollectionML sc = SermonsDLL.Instance.LoadAllSeriesByFieldValue(fieldID, value);
            SeriesCollectionML series = new SeriesCollectionML();
            foreach (SeriesML s in sc)
            {
                if (s.Active)
                    series.Add(s);
            }
            if (loadRelatedData)
            {
                foreach (SeriesML s in sc)
                {
                    s.Sermons = LoadSermonsBySeriesID(s.SeriesID, false);
                }
            }
            return series;
        }

        public SeriesCollectionML LoadSeriesTree(bool filterActiveSeries)
        {
            SeriesCollectionML sc = SermonsBLL.Instance.LoadAllSeries(filterActiveSeries, false);

            SeriesCollectionML scTree = GetChildSeries(sc, 0, true);
            return scTree;
        }

        public SeriesCollectionML LoadSeriesBySermonID(int SermonID)
        {
            SeriesCollectionML sc = SermonsDLL.Instance.LoadSeriesBySermonID(SermonID);
            return sc;
        }

        public SeriesCollectionML GetChildSeries(SeriesCollectionML sc, int parentID, bool multilevel)
        {
            SeriesCollectionML newSC = new SeriesCollectionML();
            foreach (SeriesML s in sc)
            {
                if (s.ParentSeriesID == parentID)
                {
                    if (multilevel)
                        s.ChildSeries = GetChildSeries(sc, s.SeriesID, true);
                    newSC.Add(s);

                }
            }
            return newSC;
        }

        public SeriesML LoadSeries(int seriesId, bool loadRelatedData)
        {
            SeriesML s = SermonsDLL.Instance.LoadSeries(seriesId);
            if(loadRelatedData)
                s.Sermons = LoadSermonsBySeriesID(s.SeriesID, false);
            return s;
        }

        public SeriesML LoadVisibleSeries(int seriesId, bool loadRelatedData)
        {
            SeriesML s = SermonsDLL.Instance.LoadVisibleSeries(seriesId);
            if(loadRelatedData)
                s.Sermons = LoadSermonsBySeriesID(s.SeriesID, false);
            return s;
        }

        public void InsertSeries(SeriesML series)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                SermonsDLL.Instance.InsertSeries(series);

                if (series.FieldValues != null)
                {
                    series.FieldValues.UpdateSeriesID(series.SeriesID);
                    UpdateSeriesFieldValues(series);
                }

                #endregion

                ts.Complete();
            }
        }

        public void UpdateSeries(SeriesML series)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Update item

                SermonsDLL.Instance.UpdateSeries(series);

                if (series.FieldValues != null)
                {
                    series.FieldValues.UpdateSeriesID(series.SeriesID);
                    UpdateSeriesFieldValues(series);
                }

                #endregion

                ts.Complete();
            }
        }

        public void DeleteSeries(SeriesML series)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                foreach(SeriesML s in series.ChildSeries)
                {
                    DeleteSeries(s);
                }
                SermonsDLL.Instance.DeleteSeries(series.SeriesID);

                ts.Complete();
            }
        }

        #endregion

        #region Sermons Methods

        public SermonCollectionML LoadAllSermons()
        {
            return LoadAllSermons(false);
        }

        public SermonCollectionML LoadAllSermons(bool loadRelatedData)
        {
            SermonCollectionML sc = SermonsDLL.Instance.LoadAllSermons();
            if (loadRelatedData)
            {
                foreach (SermonML s in sc)
                {
                    s.Series = LoadSeriesBySermonID(s.SermonID);
                    s.Topics = LoadTopicsBySermonID(s.SermonID);
                }
            }
            return sc;
        }

        public SermonCollectionML LoadAllVisibleSermons(bool loadRelatedData)
        {
            SermonCollectionML sermons = SermonsDLL.Instance.LoadAllVisibleSermons();
            if (loadRelatedData)
            {
                foreach (SermonML s in sermons)
                {
                    s.Series = LoadSeriesBySermonID(s.SermonID);
                    s.Topics = LoadTopicsBySermonID(s.SermonID);
                }
            }
            return sermons;
        }

        public SermonCollectionML LoadAllFeaturedSermons(bool loadRelatedData)
        {
            SermonCollectionML sc = SermonsDLL.Instance.LoadAllFeaturedSermons();
            if (loadRelatedData)
            {
                foreach (SermonML s in sc)
                {
                    s.Series = LoadSeriesBySermonID(s.SermonID);
                    s.Topics = LoadTopicsBySermonID(s.SermonID);
                }
            }
            return sc;
        }

        public SermonCollectionML LoadLatestSermons(int numberOfSermons, bool loadRelatedData)
        {
            SermonCollectionML sermons = SermonsDLL.Instance.LoadLatestSermons(numberOfSermons);
            if (loadRelatedData)
            {
                foreach (SermonML s in sermons)
                {
                    s.Series = LoadSeriesBySermonID(s.SermonID);
                    s.Topics = LoadTopicsBySermonID(s.SermonID);
                }
            }
            return sermons;
        }

        public SermonCollectionML LoadLatestVisibleSermons(int numberOfSermons, bool loadRelatedData)
        {
            SermonCollectionML sermons = SermonsDLL.Instance.LoadLatestVisibleSermons(numberOfSermons);
            if (loadRelatedData)
            {
                foreach (SermonML s in sermons)
                {
                    s.Series = LoadSeriesBySermonID(s.SermonID);
                    s.Topics = LoadTopicsBySermonID(s.SermonID);
                }
            }
            return sermons;
        }

        public SermonCollectionML LoadSermonsNotInSeries(bool loadRelatedData)
        {
            SermonCollectionML sc = SermonsDLL.Instance.LoadSermonsNotInSeries();
            if (loadRelatedData)
            {
                foreach (SermonML s in sc)
                {
                    s.Series = LoadSeriesBySermonID(s.SermonID);
                    s.Topics = LoadTopicsBySermonID(s.SermonID);
                }
            }
            return sc;
        }

        public SermonCollectionML LoadAllVisibleFeaturedSermons(bool loadRelatedData)
        {
            SermonCollectionML sc = SermonsDLL.Instance.LoadAllVisibleFeaturedSermons();
            if (loadRelatedData)
            {
                foreach (SermonML s in sc)
                {
                    s.Series = LoadSeriesBySermonID(s.SermonID);
                    s.Topics = LoadTopicsBySermonID(s.SermonID);
                }
            }
            return sc;
        }

        public SermonCollectionML LoadAllVisibleSermonsBySpeakerID(int speakerID, bool loadRelatedData)
        {
            SermonCollectionML sermons = SermonsDLL.Instance.LoadAllVisibleSermonsBySpeakerID(speakerID);
            if (loadRelatedData)
            {
                foreach (SermonML s in sermons)
                {
                    s.Series = LoadSeriesBySermonID(s.SermonID);
                    s.Topics = LoadTopicsBySermonID(s.SermonID);
                }
            }
            return sermons;
        }

        public SermonCollectionML LoadAllVisibleSermonsByTopicID(int topicID, bool loadRelatedData)
        {
            SermonCollectionML sermons = SermonsDLL.Instance.LoadAllVisibleSermonsByTopicID(topicID);
            if (loadRelatedData)
            {
                foreach (SermonML s in sermons)
                {
                    s.Series = LoadSeriesBySermonID(s.SermonID);
                    s.Topics = LoadTopicsBySermonID(s.SermonID);
                }
            }
            return sermons;
        }

        public SermonCollectionML LoadAllVisibleSermonsBySeriesID(int seriesID, bool loadRelatedData)
        {
            SermonCollectionML sermons = SermonsDLL.Instance.LoadAllVisibleSermonsBySeriesID(seriesID);
            if (loadRelatedData)
            {
                foreach (SermonML s in sermons)
                {
                    s.Series = LoadSeriesBySermonID(s.SermonID);
                    s.Topics = LoadTopicsBySermonID(s.SermonID);
                }
            }
            return sermons;
        }

        public SermonCollectionML LoadAllVisibleSermonsByPassageBook(string passageBook, bool loadRelatedData)
        {
            SermonCollectionML sermons = SermonsDLL.Instance.LoadAllVisibleSermonsByPassageBook(passageBook);
            if (loadRelatedData)
            {
                foreach (SermonML s in sermons)
                {
                    s.Series = LoadSeriesBySermonID(s.SermonID);
                    s.Topics = LoadTopicsBySermonID(s.SermonID);
                }
            }
            return sermons;
        }

        public SermonCollectionML LoadAllVisibleSermonsByDateRange(DateTime startDate, DateTime endDate, bool loadRelatedData)
        {
            SermonCollectionML sermons = SermonsDLL.Instance.LoadAllVisibleSermonsByDateRange(startDate, endDate);
            if (loadRelatedData)
            {
                foreach (SermonML s in sermons)
                {
                    s.Series = LoadSeriesBySermonID(s.SermonID);
                    s.Topics = LoadTopicsBySermonID(s.SermonID);
                }
            }
            return sermons;
        }

        public SermonCollectionML LoadAllSermonsByFieldValue(int fieldID, string value)
        {
            return LoadAllSermonsByFieldValue(fieldID, value, false);
        }

        public SermonCollectionML LoadAllSermonsByFieldValue(int fieldID, string value, bool loadRelatedData)
        {
            SermonCollectionML sc = SermonsDLL.Instance.LoadAllSermonsByFieldValue(fieldID, value);
            if (loadRelatedData)
            {
                foreach (SermonML s in sc)
                {
                    s.Series = LoadSeriesBySermonID(s.SermonID);
                    s.Topics = LoadTopicsBySermonID(s.SermonID);
                }
            }
            return sc;
        }

        public SermonCollectionML LoadAllVisibleSermonsByFieldValue(int fieldID, string value, bool loadRelatedData)
        {
            SermonCollectionML sc = SermonsDLL.Instance.LoadAllSermonsByFieldValue(fieldID, value);
            SermonCollectionML sermons = new SermonCollectionML();
            foreach (SermonML s in sc)
            {
                if (s.Available)
                    sermons.Add(s);
            }
            if (loadRelatedData)
            {
                foreach (SermonML s in sermons)
                {
                    s.Series = LoadSeriesBySermonID(s.SermonID);
                    s.Topics = LoadTopicsBySermonID(s.SermonID);
                }
            }
            return sermons;
        }

        public SermonCollectionML SearchAllVisibleSermons(int SpeakerID, int SeriesID, string PassageBook, string PassageChapter, string PassageVerse, DateTime StartDate, DateTime EndDate, string Title, string Keywords, bool loadRelatedData)
        {
            List<int> seriesIDs = null;
            if (SeriesID > 0)
                seriesIDs = new List<int>() { SeriesID };
            SermonCollectionML sermons = SermonsDLL.Instance.SearchSermons(false, null, Title, Keywords, SpeakerID, seriesIDs, PassageBook, PassageChapter, PassageVerse, StartDate, EndDate, null, null, null, 100000, 1);
            if (loadRelatedData)
            {
                foreach (SermonML s in sermons)
                {
                    s.Series = LoadSeriesBySermonID(s.SermonID);
                    s.Topics = LoadTopicsBySermonID(s.SermonID);
                }
            }
            return sermons;
        }

        public SermonCollectionML SearchAllSermons(string search, bool loadRelatedData)
        {
            SermonCollectionML sermons = SermonsDLL.Instance.SearchSermons(true, search, null, null, 0, null, null, null, null, null, null, null, null, null, 100000, 1);
            if (loadRelatedData)
            {
                foreach (SermonML s in sermons)
                {
                    s.Series = LoadSeriesBySermonID(s.SermonID);
                    s.Topics = LoadTopicsBySermonID(s.SermonID);
                }
            }
            return sermons;
        }

        public SermonCollectionML SearchSermons(bool loadInvisible, string search, string title, string keywords, int speakerID, List<int> seriesIDs, string passageBook, string passageChapter, string passageVerse, DateTime? startDate, DateTime? endDate, string where, string customFieldsWhere, string sortExpression, int rowsPerPage, int pageNumber, bool loadRelatedData)
        {
            SermonCollectionML sermons = SermonsDLL.Instance.SearchSermons(loadInvisible, search, title, keywords, speakerID, seriesIDs, passageBook, passageChapter, passageVerse, startDate, endDate, where, customFieldsWhere, sortExpression, rowsPerPage, pageNumber);
            if (loadRelatedData)
            {
                foreach (SermonML s in sermons)
                {
                    s.Series = LoadSeriesBySermonID(s.SermonID);
                    s.Topics = LoadTopicsBySermonID(s.SermonID);
                }
            }
            return sermons;
        }

        public SermonCollectionML LoadSermonsBySeriesID(int seriesID, bool loadRelatedData)
        {
            SermonCollectionML sc = SermonsDLL.Instance.LoadSermonsBySeriesID(seriesID);
            if (loadRelatedData)
            {

            }

            return sc;
        }

        public SermonCollectionML LoadVisibleSermonsBySeriesID(int seriesID, bool loadRelatedData)
        {
            SermonCollectionML sc = SermonsDLL.Instance.LoadSermonsBySeriesID(seriesID);
            SermonCollectionML sermons = new SermonCollectionML();
            foreach (SermonML s in sc)
            {
                if (s.Available)
                    sermons.Add(s);
            }
            if (loadRelatedData)
            {

            }

            return sermons;
        }

        public SermonML LoadSermon(int sermonId, bool loadRelatedData)
        {
            SermonML s = SermonsDLL.Instance.LoadSermon(sermonId);
            if (loadRelatedData)
            {
                s.Series = LoadSeriesBySermonID(s.SermonID);
                s.Topics = LoadTopicsBySermonID(s.SermonID);
            }

            return s;
        }

        public void LoadSermonRelatedData(SermonML sermon)
        {

        }

        public void InsertSermon(SermonML sermon)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                SermonsDLL.Instance.InsertSermon(sermon);

                if (sermon.Series != null)
                    UpdateSermon2Series(sermon);

                if (sermon.Topics != null)
                    UpdateSermonTopics(sermon);

                if (sermon.FieldValues != null)
                {
                    sermon.FieldValues.UpdateSermonID(sermon.SermonID);
                    UpdateSermonFieldValues(sermon);
                }

                #endregion

                ts.Complete();
            }
        }

        public void UpdateSermon(SermonML sermon)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Update item

                SermonsDLL.Instance.UpdateSermon(sermon);
                if (sermon.Series != null)
                    UpdateSermon2Series(sermon);

                if (sermon.Topics != null)
                    UpdateSermonTopics(sermon);

                if (sermon.FieldValues != null)
                {
                    sermon.FieldValues.UpdateSermonID(sermon.SermonID);
                    UpdateSermonFieldValues(sermon);
                }

                #endregion

                ts.Complete();
            }
        }

        public void UpdateSermon2Series(SermonML sermon)
        {
            foreach (SeriesML series in LoadAllSeries(false, true))
            {
                bool isInDB = false;
                foreach (SermonML s in series.Sermons)
                {
                    if (s.SermonID == sermon.SermonID)
                        isInDB = true;
                }

                bool isInML = false;
                foreach (SeriesML s in sermon.Series)
                {
                    if (series.SeriesID == s.SeriesID)
                        isInML = true;
                }

                if (isInDB)
                {
                    if (!isInML)
                        DeleteSermon2Series(sermon.SermonID, series.SeriesID);
                }
                else
                {
                    if (isInML)
                        InsertSermon2Series(sermon.SermonID, series.SeriesID);
                }
            }

        }

        public void UpdateSermonTopics(SermonML sermon)
        {
            foreach(TopicML topic in LoadAllTopics())
            {
                bool isInDB = false;
                foreach(TopicML t in LoadTopicsBySermonID(sermon.SermonID))
                {
                    if (t.TopicID == topic.TopicID)
                        isInDB = true;
                }

                bool isInML = false;
                foreach (TopicML t in sermon.Topics)
                {
                    if (t.TopicID == topic.TopicID)
                        isInML = true;
                }

                if (isInDB)
                {
                    if (!isInML)
                        DeleteTopic2Sermon(sermon.SermonID, topic.TopicID);
                }
                else
                {
                    if (isInML)
                        InsertTopic2Sermon(sermon.SermonID, topic.TopicID);
                }
            }
        }

        public void DeleteSermon(int sermonID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                SermonsDLL.Instance.DeleteSermon(sermonID);

                if (!string.IsNullOrEmpty(BackstageSermons.Settings.Admin.SermonDetailPage))
                {
                    try
                    {
                        backstage.SEORuleHelper.Instance.DeleteCustomSermonDetailRule(sermonID);
                    }
                    catch (Exception err)
                    {
                        System.Web.HttpContext.Current.Trace.Write("err", err.ToString());
                    }
                }

                ts.Complete();
            }
        }

        #endregion

        #region Sermon Month Methods

        public List<DateTime> LoadAllSermonMonths()
        {
            return SermonsDLL.Instance.LoadAllSermonMonths();
        }

        public List<DateTime> LoadAllVisibleSermonMonths()
        {
            return SermonsDLL.Instance.LoadAllVisibleSermonMonths();
        }

        #endregion

        #region SermonPassageBook Methods

        public SermonPassageBookCollectionML LoadAllSermonPassageBooks()
        {
            return SermonsDLL.Instance.LoadAllSermonPassageBooks();
        }

        public SermonPassageBookCollectionML LoadAllVisibleSermonPassageBooks()
        {
            return SermonsDLL.Instance.LoadAllVisibleSermonPassageBooks();
        }

        #endregion

        #region Sermon2Series Methods

        protected void DeleteSermon2Series(int SermonID, int SeriesID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                SermonsDLL.Instance.DeleteSermon2Series(SermonID, SeriesID);

                ts.Complete();
            }
        }

        public void InsertSermon2Series(int SermonID, int SeriesID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                SermonsDLL.Instance.InsertSermon2Series(SermonID, SeriesID);

                #endregion

                ts.Complete();
            }
        }

        #endregion

        #region Speaker Methods

        public SpeakerCollectionML LoadAllSpeakers()
        {
            return SermonsDLL.Instance.LoadAllSpeakers();
        }

        public SpeakerCollectionML LoadAllVisibleSpeakers()
        {
            return SermonsDLL.Instance.LoadAllVisibleSpeakers();
        }

        public SpeakerCollectionML LoadAllVisibleSpeakersWithSermons()
        {
            return SermonsDLL.Instance.LoadAllVisibleSpeakersWithSermons();
        }

        public SpeakerML LoadSpeaker(int speakerID)
        {
            return SermonsDLL.Instance.LoadSpeaker(speakerID);
        }

        public SpeakerML LoadVisibleSpeaker(int speakerID)
        {
            return SermonsDLL.Instance.LoadVisibleSpeaker(speakerID);
        }

        public void InsertSpeaker(SpeakerML speaker)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                SermonsDLL.Instance.InsertSpeaker(speaker);

                #endregion

                ts.Complete();
            }
        }

        public void UpdateSpeaker(SpeakerML speaker)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Update item

                SermonsDLL.Instance.UpdateSpeaker(speaker);

                #endregion

                ts.Complete();
            }
        }

        public void DeleteSpeaker(int SpeakerID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                SermonsDLL.Instance.DeleteSpeaker(SpeakerID);

                ts.Complete();
            }
        }

        #endregion
        
        #region Topic Methods

        public TopicCollectionML LoadAllTopics()
        {
            return SermonsDLL.Instance.LoadAllTopics();
        }

        public TopicCollectionML LoadAllVisibleTopics()
        {
            return SermonsDLL.Instance.LoadAllVisibleTopics();
        }

        public TopicCollectionML LoadAllVisibleTopicsWithSermons()
        {
            return SermonsDLL.Instance.LoadAllVisibleTopicsWithSermons();
        }

        public void InsertTopic(TopicML topic)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                SermonsDLL.Instance.InsertTopic(topic);

                #endregion

                ts.Complete();
            }
        }

        public void UpdateTopic(TopicML topic)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Update item

                SermonsDLL.Instance.UpdateTopic(topic);

                #endregion

                ts.Complete();
            }
        }

        public void DeleteTopic(int TopicID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                SermonsDLL.Instance.DeleteTopic(TopicID);

                ts.Complete();
            }
        }

        #endregion

        #region Topic2Sermon Methods

        public TopicCollectionML LoadTopicsBySermonID(int SermonID)
        {
            return SermonsDLL.Instance.LoadTopicsBySermonID(SermonID);
        }

        protected void InsertTopic2Sermon(int SermonID, int TopicID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {
                SermonsDLL.Instance.InsertTopic2Sermon(SermonID, TopicID);
                ts.Complete();
            }
        }

        protected void DeleteTopic2Sermon(int SermonID, int TopicID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {
                SermonsDLL.Instance.DeleteTopic2Sermon(SermonID, TopicID);
                ts.Complete();
            }
        }

        #endregion

        #region Setting methods

        public SettingCollectionML LoadAllSettings()
        {
            return SermonsDLL.Instance.LoadAllSettings();
        }

        public void InsertSetting(SettingML setting)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item
                SermonsDLL.Instance.InsertSetting(setting);
                #endregion

                ts.Complete();
            }
        }

        public void UpdateSetting(SettingML setting)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Update item
                SermonsDLL.Instance.UpdateSetting(setting);
                #endregion

                ts.Complete();
            }
        }

        public void UpdateSettingCollection(SettingCollectionML cacheSettings)
        {
            SettingCollectionML settings = LoadAllSettings();
            foreach (SettingML setting in cacheSettings)
            {
                bool isInDB = false;
                SettingML sDB = new SettingML();
                foreach (SettingML s in settings)
                {
                    if (s.Name == setting.Name)
                    {
                        isInDB = true;
                        sDB = s;
                        setting.SettingID = s.SettingID;
                        break;
                    }
                }
                if (isInDB)
                {
                    if (sDB.Value != setting.Value)
                        UpdateSetting(setting);
                }
                else
                    InsertSetting(setting);
            }
        }

        public void DeleteSetting(int SettingID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {
                SermonsDLL.Instance.DeleteSetting(SettingID);
                ts.Complete();
            }
        }

        #endregion

        #region SermonAudioMethods
        public string SermonAudio_SubmitSermon(SermonML sermon)
        {
            com.sa_media.web4.Service service = new BackstageSermons.com.sa_media.web4.Service();
            string keywords = string.Empty;
            if (sermon.Topics != null)
            {
                foreach (TopicML t in sermon.Topics)
                {
                    if (keywords.Length > 0)
                        keywords += ", ";
                    keywords += t.Title;
                }
            }
            SpeakerML speaker = LoadAllSpeakers().FindSpeakerByID(sermon.Speaker);
            sermon.SermonAudioID = service.SubmitSermon(Settings.SermonAudio.MemberID, Settings.SermonAudio.Password, sermon.Title, sermon.Title, sermon.Description, "Sunday Service", sermon.Date, speaker.FirstName + " " + speaker.LastName, sermon.PassageFull, "", keywords, sermon.Description);
            return sermon.SermonAudioID;
        }
        #endregion

        #region SermonField methods

        public SermonFieldCollectionML LoadAllSermonFields()
        {
            return SermonsDLL.Instance.LoadAllSermonFields();
        }

        public SermonFieldCollectionML LoadAllSermonFieldsForSermon(int sermonID, int seriesID)
        {
            return SermonsDLL.Instance.LoadAllSermonFieldsForSermon(sermonID, seriesID);
        }

        public void InsertSermonField(SermonFieldML item)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                SermonsDLL.Instance.InsertSermonField(item);

                ts.Complete();
            }
        }

        public void UpdateSermonField(SermonFieldML item)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                SermonsDLL.Instance.UpdateSermonField(item);

                ts.Complete();
            }

        }

        public void DeleteSermonField(int fieldID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                SermonsDLL.Instance.DeleteSermonField(fieldID);

                ts.Complete();
            }
        }

        #endregion

        #region SermonFieldValue methods

        public SermonFieldValueCollectionML LoadAllSermonFieldValues()
        {
            return SermonsDLL.Instance.LoadAllSermonFieldValues();
        }

        public SermonFieldValueCollectionML LoadAllSermonFieldValuesBySermonID(int sermonID)
        {
            return SermonsDLL.Instance.LoadAllSermonFieldValuesBySermonID(sermonID);
        }

        public void InsertSermonFieldValue(SermonFieldValueML item)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                SermonsDLL.Instance.InsertSermonFieldValue(item);

                ts.Complete();
            }
        }

        public void UpdateSermonFieldValue(SermonFieldValueML item)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                SermonsDLL.Instance.UpdateSermonFieldValue(item);

                ts.Complete();
            }
        }

        public void DeleteSermonFieldValue(int sermonID, int fieldID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                SermonsDLL.Instance.DeleteSermonFieldValue(sermonID, fieldID);

                ts.Complete();
            }
        }

        public void UpdateSermonFieldValues(SermonML sermon)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                SermonFieldValueCollectionML existing = LoadAllSermonFieldValuesBySermonID(sermon.SermonID);
                List<SermonFieldValueML> valuesToDelete = existing.FindAll(delegate(SermonFieldValueML toCheck) { return (sermon.FieldValues.FindBySermonIDAndFieldID(toCheck.SermonID, toCheck.FieldID) == null); });

                foreach (SermonFieldValueML toUpdate in sermon.FieldValues)
                {
                    if (toUpdate.IsCreated)
                        UpdateSermonFieldValue(toUpdate);
                    else
                        InsertSermonFieldValue(toUpdate);
                }

                foreach (SermonFieldValueML toDelete in valuesToDelete)
                    DeleteSermonFieldValue(toDelete.SermonID, toDelete.FieldID);

                ts.Complete();
            }
        }

        #endregion

        #region SeriesField methods

        public SeriesFieldCollectionML LoadAllSeriesFields()
        {
            return SermonsDLL.Instance.LoadAllSeriesFields();
        }

        public SeriesFieldCollectionML LoadAllSeriesFieldsForSeries(int seriesID)
        {
            return SermonsDLL.Instance.LoadAllSeriesFieldsForSeries(seriesID);
        }

        public void InsertSeriesField(SeriesFieldML item)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                SermonsDLL.Instance.InsertSeriesField(item);

                ts.Complete();
            }
        }

        public void UpdateSeriesField(SeriesFieldML item)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                SermonsDLL.Instance.UpdateSeriesField(item);

                ts.Complete();
            }
        }

        public void DeleteSeriesField(int fieldID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                SermonsDLL.Instance.DeleteSeriesField(fieldID);

                ts.Complete();
            }
        }

        #endregion

        #region SeriesFieldValue methods

        public SeriesFieldValueCollectionML LoadAllSeriesFieldValues()
        {
            return SermonsDLL.Instance.LoadAllSeriesFieldValues();
        }

        public SeriesFieldValueCollectionML LoadAllSeriesFieldValuesBySeriesID(int seriesID)
        {
            return SermonsDLL.Instance.LoadAllSeriesFieldValuesBySeriesID(seriesID);
        }

        public void InsertSeriesFieldValue(SeriesFieldValueML item)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                SermonsDLL.Instance.InsertSeriesFieldValue(item);

                ts.Complete();
            }
        }

        public void UpdateSeriesFieldValue(SeriesFieldValueML item)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                SermonsDLL.Instance.UpdateSeriesFieldValue(item);

                ts.Complete();
            }
        }

        public void DeleteSeriesFieldValue(int seriesID, int fieldID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                SermonsDLL.Instance.DeleteSeriesFieldValue(seriesID, fieldID);

                ts.Complete();
            }
        }

        public void UpdateSeriesFieldValues(SeriesML item)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                SeriesFieldValueCollectionML existing = LoadAllSeriesFieldValuesBySeriesID(item.SeriesID);
                List<SeriesFieldValueML> valuesToDelete = existing.FindAll(delegate(SeriesFieldValueML toCheck) { return (item.FieldValues.FindBySeriesIDAndFieldID(toCheck.SeriesID, toCheck.FieldID) == null); });

                foreach (SeriesFieldValueML toUpdate in item.FieldValues)
                {
                    if (toUpdate.IsCreated)
                        UpdateSeriesFieldValue(toUpdate);
                    else
                        InsertSeriesFieldValue(toUpdate);
                }

                foreach (SeriesFieldValueML toDelete in valuesToDelete)
                    DeleteSeriesFieldValue(toDelete.SeriesID, toDelete.FieldID);

                ts.Complete();
            }
        }

        #endregion

        #region Activity methods

        public ActivityCollectionML LoadAllActivity()
        {
            return SermonsDLL.Instance.LoadAllActivity();
        }

        public ActivityCollectionML LoadActivityByDateRange(DateTime startDate, DateTime endDate)
        {
            return SermonsDLL.Instance.LoadActivityByDateRange(startDate, endDate);
        }

        public void InsertActivity(ActivityML activity)
        {
            InsertActivity(activity, true);
        }

        public void InsertActivity(ActivityML activity, bool filterCrawlers)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Insert item

                if (string.IsNullOrEmpty(activity.UserAgent) && System.Web.HttpContext.Current != null)
                {
                    if (filterCrawlers && backstage.Classes.BrowserHelper.RequestIsCrawler)
                        return;
                    activity.UserAgent = System.Web.HttpContext.Current.Request.UserAgent;
                }
                else if (filterCrawlers && backstage.Classes.BrowserHelper.UserAgentIsCrawler(activity.UserAgent))
                    return;

                if (string.IsNullOrEmpty(activity.IPAddress) && System.Web.HttpContext.Current != null)
                    activity.IPAddress = backstage.Classes.BrowserHelper.IPAddress;

                if (filterCrawlers && !string.IsNullOrEmpty(activity.IPAddress) && backstage.Classes.BrowserHelper.IPAddressIsCrawler(activity.IPAddress))
                    return;

                SermonsDLL.Instance.InsertActivity(activity);
                #endregion

                ts.Complete();
            }
        }

        public void UpdateActivity(ActivityML activity)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {

                #region Update item
                SermonsDLL.Instance.UpdateActivity(activity);
                #endregion

                ts.Complete();
            }
        }

        public void DeleteActivity(int activityID)
        {
            TransactionOptions to = new TransactionOptions();
            to.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (MyTransactionScope ts = new MyTransactionScope(TransactionScopeOption.Required))
            { //, to, EnterpriseServicesInteropOption.Full)) {
                SermonsDLL.Instance.DeleteActivity(activityID);
                ts.Complete();
            }
        }

        #endregion

        #region ReportSeries methods

        public ReportSeriesCollectionML LoadAllReportSeriesByDateRange(DateTime startDate, DateTime endDate)
        {
            return SermonsDLL.Instance.LoadAllReportSeriesByDateRange(startDate, endDate);
        }

        #endregion

        #region ReportTopic methods

        public ReportTopicCollectionML LoadAllReportTopicsByDateRange(DateTime startDate, DateTime endDate)
        {
            return SermonsDLL.Instance.LoadAllReportTopicsByDateRange(startDate, endDate);
        }

        #endregion

        #endregion
    }
}