﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackstageSermons
{
    public class DBDuplicateException : System.Exception
    {
        public DBDuplicateException(string message)
            : base(message)
        {
        }

        public DBDuplicateException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
