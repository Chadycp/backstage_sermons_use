﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackstageSermons
{
    public class SermonPrintListML
    {
        #region Public fields

        public int SermonID { get; set; }
        public string Name { get; set; }

        #endregion

        #region Construction

        #region SermonPrintListML()

        public SermonPrintListML()
        {
        }

        #endregion

        #endregion
    }
}