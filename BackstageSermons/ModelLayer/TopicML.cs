﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace BackstageSermons
{
    public class TopicML
    {
        public int TopicID { get; set; }
        public string Title { get; set; }
        public int SermonCount { get; set; }
    }
}
