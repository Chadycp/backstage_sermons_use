﻿<%@ Page Language="C#" MasterPageFile="~/backstageIncludes/BackstageSermons/SermonsNew.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BackstageSermons.modules.Speakers.Default" EnableEventValidation="false" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <title>Backstage Sermons :: Speaker Manager</title>
</asp:Content>

<asp:Content ID="contentStyles" ContentPlaceHolderID="stylesheets" runat="server">
    <style type="text/css">
        #cphContent_cphContent_pnlEditSpeaker_Popup{ z-index: 990 !important; }
    </style>
</asp:Content>

<asp:Content ID="contentScripts" ContentPlaceHolderID="scripts" runat="server">
    <script type="text/javascript" src="/backstageIncludes/BackstageSermons/modules/Speakers/js/speakers.js"></script>
    <script type="text/javascript" src="/backstage/js/user/bxPrintHelper.js"></script>

    <script type="text/javascript">
        function ConfirmVisibility(ddl) {
            if (ddl.value != "-") {
                if (!confirm('Are you sure you wish to change the visiblity of all the selected sermons?')) return false;
            } else {
                return false;
            }
        }
        $(document).ready(function () {
            $(".printTrigger").printHelper();
            $("#browser").find("span").hover(function () {
                $(this).children("span .imgSeriesAction").css({ "display": "inline" });
            }, function () {
                $(this).children("span .imgSeriesAction").css({ "display": "none" });
            });

            $(".pencil").click(function () {
                //$(this).parent().children("a:first").css({"display":"none"});
                //$(this).parent().parent().parent().children("input").css({"display":"inline"});
                //$(this).parent().parent().parent().children("a").css({"display":"inline"});
                $(this).parent().parent().parent().children("#inputBox").show("fast");
            });
            $(".closeButton").click(function () {
                $(this).parent().hide("fast");
            });
            $('#subNavSpeakers').addClass('subActive');
        });

        function ToggleNewSpeaker() {
            $('#trAddNew').toggle();
        }


    </script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">

<asp:Label runat="server" ID="lblResult" style="color: #990000;"></asp:Label>

<asp:UpdatePanel ID="updatePnlEditSpeaker" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="True">
	<ContentTemplate>
	    <cms:PopupPanel runat="server" ID="pnlEditSpeaker" Visible="false" ZIndex="998">
	        <ContentTemplate>
			    <div class="editSpeaker">
			    	<h1 runat="server" ID="editSpeakerTitle" style="margin-bottom: 19px;">Add Speaker</h1>
					<div class="editSpeaker-Image">
                        <bx:ImagePicker runat="server" ID="ipPhoto"></bx:ImagePicker>
					</div>
					
					<div class="clearfix"></div>
					<div class="editSpeaker-Info">
					    <span>Prefix:</span>
	                    <asp:DropDownList runat="server" ID="ddlEditNamePrefix">
	                        <asp:ListItem Value="" Text="--"></asp:ListItem>
	                        <asp:ListItem Value="Dr" Text="Dr."></asp:ListItem>
	                        <asp:ListItem Value="Mr" Text="Mr."></asp:ListItem>
	                        <asp:ListItem Value="Mrs" Text="Mrs."></asp:ListItem>
	                        <asp:ListItem Value="Pastor" Text="Pastor"></asp:ListItem>
	                        <asp:ListItem Value="Rev" Text="Rev."></asp:ListItem>
	                    </asp:DropDownList>
						<div class="clearfix"></div>
					    <asp:TextBox runat="server" ID="txtEditFirstName" placeholder="First Name" CssClass="editSpeaker-Input"></asp:TextBox>
					    <asp:TextBox runat="server" ID="txtEditLastName" placeholder="Last Name" CssClass="editSpeaker-Input"></asp:TextBox>
					    <asp:TextBox runat="server" ID="txtEditSpeakerID" style="display: none;"></asp:TextBox>
						
					    <div class="editSpeaker-Bio">
						    <span>Bio:</span>
						    <div style="margin-top: 7px;">
						    	<cms:CKEditor runat="server" ID="ckEditBio" Toolbar="Basic" height="200"></cms:CKEditor>
						    </div>
						    <div class="editSpeaker-BioButtons">
							    <asp:LinkButton runat="server" ID="ImageButton1" CssClass="bxButton2" Text="Update" OnClick="btnUpdateSpeaker_Click" />
							    <asp:LinkButton runat="server" ID="btnCancelEdit" CssClass="bxButton2" Text="Cancel" OnClick="btnCancelEdit_Click" />
						    </div>
					    </div>
					</div>
			    </div>
			</ContentTemplate>
		</cms:PopupPanel>
	</ContentTemplate>
</asp:UpdatePanel>
        
        

<asp:UpdateProgress ID="udProgress" runat="server" DisplayAfter="100" Visible="true" DynamicLayout="true">
    <ProgressTemplate>
        <div class="processing">
            <img alt="Processing" src="/backstage/images/NewButtons/ajax-loader.gif">
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>

<div class="sectionContent clearfix">
	<div class="topBarSearch">
	    <cms:DelayedSubmitExtender ID="DelayedSubmitExtender1" runat="server" Timeout="400" TargetControlID="txtSearch"/>
	    <asp:TextBox ID="txtSearch" CssClass="search" Text="Search Speakers" onfocus="if(this.value == 'Search Speakers'){this.value='';}" onblur="if(this.value==''){this.value='Search Speakers';}" runat="server" AutoPostBack="True" OnTextChanged="txtSearch_OnTextChanged" />
	</div>
	<div class="rightColumnWrapper" style="width:980px">
		<div class="rightColumn">
			<div class="toolbarContainer">
				<div class="toolbar clearfix">
					<div class="leftButtons clearfix" style="position: relative;">
					    <asp:CheckBox runat="server" ID="cbSelectAllSpeakers" AutoPostBack="true" OnCheckedChanged="lbSelectAllSpeakers_Click" style="position: absolute; z-index: 1; left: 5px; top: 3px; margin: 0; border: none;" />
					    <asp:LinkButton runat="server" ID="lbSelectAll" CssClass="bxButton2" OnClick="lbSelectAllSpeakers_Click" style="padding-left: 24px;">
				                <asp:Label runat="server" ID="lblSelectAllSpeakers" Text="Select All"></asp:Label>
				        </asp:LinkButton>
					    <asp:LinkButton runat="server" CssClass="bxButton2 red" ToolTip="Delete Selected Speakers" ID="lbDeleteSpeaker" OnClick="btnDeleteSpeaker_Click" OnClientClick="return confirm('Are you sure you wish to delete all the selected speakers?');"><span class="bxIcon bxIcon-Trash"></span>Delete Selected</asp:LinkButton>
					</div>
					<div class="rightButtons clearfix">
						<asp:LinkButton runat="server" ID="lbPrint" CssClass="bxButton2" OnClick="lbPrint_Click"><span class="bxIcon bxIcon-Print"></span>Print List</asp:LinkButton>
						<a data-icon="printer" id="A1" data-icon="printer" class="bxButton2 printTrigger" style="display: none;" href="Print.aspx">Print List</a>
						<asp:LinkButton runat="server" ID="lbAddSpeaker" CssClass="bxButton2 green" CommandArgument="0" OnClick="btnEditSpeaker_Click">+ Add New Speaker</asp:LinkButton>
					</div>
				</div>
			</div>
			<div class="tableContainer">
				<asp:UpdatePanel runat="server" ID="updateSpeakers" ChildrenAsTriggers="True" UpdateMode="Conditional">
					<ContentTemplate>
					    
					    <asp:GridView runat="server" ID="gridSpeakers" AllowPaging="True" CssClass="list" AllowSorting="True" OnRowCommand="gridSpeakers_OnRowCommand"
                            AutoGenerateColumns="False" CellPadding="0" DataKeyNames="SpeakerID" GridLines="None" OnPageIndexChanging="gridSpeakers_PageIndexChanging"
                            OnRowCreated="gridSpeakers_OnRowCreated" OnDataBound="gridSpeakers_DataBound" OnSorting="gridSpeakers_Sorting" PageSize="12" PagerSettings-PageButtonCount="15">
                            <EmptyDataTemplate>
                            	<div style="padding:30px;text-align:center;font-size:20px;color:#999999">There are currently no speakers to show.</div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField>
            	                    <HeaderTemplate></HeaderTemplate>
                                    <ItemTemplate>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="border-right: none;">
                                                    <asp:CheckBox runat="server" ID="cbSpeaker" cID='<%# Eval("SpeakerID") %>' />
                                                    <asp:HiddenField runat="server" ID="hfSpeakerID" Value='<%# Eval("SpeakerID") %>' />
                                                </td>
                                            </tr>
                                        </table> 
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="speaker photo" src="<%# string.IsNullOrEmpty(Eval("Photo").ToString()) ? "/backstageincludes/backstagesermons/images/choosePhoto.jpg" : Eval("Photo").ToString() %>" width="40" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Prefix" SortExpression="NamePrefix">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="lbNamePrefix" CssClass="fillCell grey"  OnClick="btnEditSpeaker_Click" CommandArgument='<%# Eval("SpeakerID") %>'>
                                            <%# Eval("NamePrefix") == null ? "" : backstage.Classes.StringEditor.Truncate(Eval("NamePrefix").ToString(), 40, "...", false) %>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="FirstName" SortExpression="FirstName">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="lbFirstName" CssClass="fillCell grey"  OnClick="btnEditSpeaker_Click" CommandArgument='<%# Eval("SpeakerID") %>'>
                                            <%# backstage.Classes.StringEditor.Truncate(Eval("FirstName").ToString(), 40, "...", false) %>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="LastName" SortExpression="LastName">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="lbLastName" CssClass="fillCell grey"  OnClick="btnEditSpeaker_Click" CommandArgument='<%# Eval("SpeakerID") %>'>
                                            <%# backstage.Classes.StringEditor.Truncate(Eval("LastName").ToString(), 40, "...", false) %>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate><span>Bio</span></HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="lbBio" CssClass="fillCell grey"  OnClick="btnEditSpeaker_Click" CommandArgument='<%# Eval("SpeakerID") %>'>
                                            <%# backstage.Classes.StringEditor.Truncate(backstage.Classes.StringEditor.RemoveHTML(Eval("Bio").ToString()), 80, "...", true) %>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div class="functions">
											<asp:ImageButton runat="server" ID="btnEditSpeaker" ImageUrl="/backstage/images/icon-edit.png" OnClick="btnEditSpeaker_Click" CommandArgument='<%# Eval("SpeakerID") %>' />
											<asp:ImageButton runat="server" ID="btnDelete" ImageUrl="/backstage/images/icon-trash.png" OnClientClick="return confirm('Are you sure you wish to delete this speaker?');" CommandName="DeleteSpeaker" CommandArgument='<%# Eval("SpeakerID") %>' ToolTip="Delete this speaker" />
										</div>
                                    </ItemTemplate>
                                </asp:TemplateField>
					    
					    
					    </Columns>
                            <PagerStyle CssClass="trPager" />
                            <RowStyle CssClass="trContent" />
                        </asp:GridView>
					    
					</ContentTemplate>
					<Triggers>
						<asp:AsyncPostBackTrigger ControlID="txtSearch" EventName="TextChanged" />
						<asp:AsyncPostBackTrigger ControlID="lbDeleteSpeaker" EventName="Click" />
						<asp:AsyncPostBackTrigger ControlID="lbAddSpeaker" EventName="Click" />
					</Triggers>
				</asp:UpdatePanel>
			</div>
		</div>
	</div>
</div>
</asp:Content>