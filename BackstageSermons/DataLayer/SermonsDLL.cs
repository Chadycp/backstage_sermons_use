﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace BackstageSermons
{
    public class SermonsDLL : IBackstageSermonsDataLayer
    {

        #region Constructors

        #region SermonsDLL()

        /// <summary>
        /// Default constructor
        /// </summary>
        public SermonsDLL()
        {
        }

        #endregion

        #endregion

        #region Instance data

        private static readonly SermonsDLL instance = new SermonsDLL();

        /// <summary>
        /// returns the SermonsDLL singleton
        /// </summary>
        public static SermonsDLL Instance
        {
            get { return instance; }
        }

        #endregion

        #region IBackstageDataLayer Methods

        #region Series methods

        public SeriesCollectionML LoadAllSeries()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllSeries();

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SeriesCollectionML LoadAllVisibleSeries()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllVisibleSeries();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SeriesCollectionML LoadAllVisibleRootSeries()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllVisibleRootSeries();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SeriesCollectionML LoadAllVisibleRootSeriesWithSermons()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllVisibleRootSeriesWithSermons();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SeriesCollectionML LoadAllVisibleSeriesWithSermons()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllVisibleSeriesWithSermons();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SeriesCollectionML LoadAllSeriesByFieldValue(int fieldID, string value)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllSeriesByFieldValue(fieldID, value);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SeriesCollectionML LoadSeriesBySermonID(int SermonID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadSeriesBySermonID(SermonID);

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public SeriesML LoadSeries(int seriesID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadSeries(seriesID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public SeriesML LoadVisibleSeries(int seriesID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadVisibleSeries(seriesID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public void InsertSeries(SeriesML series)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.InsertSeries(series);

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public void UpdateSeries(SeriesML series)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.UpdateSeries(series);

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public void DeleteSeries(int seriesID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.DeleteSeries(seriesID);

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        #endregion

        #region Sermon Methods

        public SermonCollectionML LoadAllSermons()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllSermons();

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SermonCollectionML LoadAllVisibleSermons()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllVisibleSermons();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SermonCollectionML LoadAllFeaturedSermons()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllFeaturedSermons();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SermonCollectionML LoadLatestSermons(int numberOfSermons)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadLatestSermons(numberOfSermons);

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SermonCollectionML LoadLatestVisibleSermons(int numberOfSermons)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadLatestVisibleSermons(numberOfSermons);

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SermonCollectionML LoadSermonsNotInSeries()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadSermonsNotInSeries();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SermonCollectionML LoadAllVisibleFeaturedSermons()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllVisibleFeaturedSermons();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SermonCollectionML LoadAllVisibleSermonsBySpeakerID(int speakerID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllVisibleSermonsBySpeakerID(speakerID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SermonCollectionML LoadAllVisibleSermonsByTopicID(int topicID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllVisibleSermonsByTopicID(topicID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SermonCollectionML LoadAllVisibleSermonsBySeriesID(int seriesID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllVisibleSermonsBySeriesID(seriesID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SermonCollectionML LoadAllVisibleSermonsByPassageBook(string passageBook)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllVisibleSermonsByPassageBook(passageBook);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SermonCollectionML LoadAllVisibleSermonsByDateRange(DateTime startDate, DateTime endDate)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllVisibleSermonsByDateRange(startDate, endDate);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SermonCollectionML LoadAllSermonsByFieldValue(int fieldID, string value)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllSermonsByFieldValue(fieldID, value);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SermonCollectionML SearchSermons(bool loadInvisible, string search, string title, string keywords, int speakerID, List<int> seriesIDs, string passageBook, string passageChapter, string passageVerse, DateTime? startDate, DateTime? endDate, string where, string customFieldsWhere, string sortExpression, int rowsPerPage, int pageNumber)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.SearchSermons(loadInvisible, search, title, keywords, speakerID, seriesIDs, passageBook, passageChapter, passageVerse, startDate, endDate, where, customFieldsWhere, sortExpression, rowsPerPage, pageNumber);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SermonCollectionML LoadSermonsBySeriesID(int seriesID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadSermonsBySeriesID(seriesID);

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SermonML LoadSermon(int sermonID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadSermon(sermonID);

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public void InsertSermon(SermonML sermon)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.InsertSermon(sermon);

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public void UpdateSermon(SermonML sermon)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.UpdateSermon(sermon);

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public void DeleteSermon(int sermonID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.DeleteSermon(sermonID);

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        #endregion

        #region Sermon Month Methods

        public List<DateTime> LoadAllSermonMonths()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllSermonMonths();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public List<DateTime> LoadAllVisibleSermonMonths()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllVisibleSermonMonths();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        #endregion

        #region SermonPassageBook Methods

        public SermonPassageBookCollectionML LoadAllSermonPassageBooks()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllSermonPassageBooks();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SermonPassageBookCollectionML LoadAllVisibleSermonPassageBooks()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllVisibleSermonPassageBooks();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        #endregion

        #region Sermon2Series Methods

        public void InsertSermon2Series(int SermonID, int SeriesID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.InsertSermon2Series(SermonID, SeriesID);

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public void DeleteSermon2Series(int SermonID, int SeriesID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.DeleteSermon2Series(SermonID, SeriesID);

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        #endregion

        #region SermonField methods

        public SermonFieldCollectionML LoadAllSermonFields()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllSermonFields();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public SermonFieldCollectionML LoadAllSermonFieldsForSermon(int sermonID, int seriesID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllSermonFieldsForSermon(sermonID, seriesID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public void InsertSermonField(SermonFieldML item)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.InsertSermonField(item);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public void UpdateSermonField(SermonFieldML item)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.UpdateSermonField(item);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public void DeleteSermonField(int fieldID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.DeleteSermonField(fieldID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        #endregion

        #region SermonFieldValue methods

        public SermonFieldValueCollectionML LoadAllSermonFieldValues()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllSermonFieldValues();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public SermonFieldValueCollectionML LoadAllSermonFieldValuesBySermonID(int sermonID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllSermonFieldValuesBySermonID(sermonID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public void InsertSermonFieldValue(SermonFieldValueML item)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.InsertSermonFieldValue(item);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public void UpdateSermonFieldValue(SermonFieldValueML item)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.UpdateSermonFieldValue(item);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public void DeleteSermonFieldValue(int sermonID, int fieldID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.DeleteSermonFieldValue(sermonID, fieldID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        #endregion

        #region SeriesField methods

        public SeriesFieldCollectionML LoadAllSeriesFields()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllSeriesFields();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SeriesFieldCollectionML LoadAllSeriesFieldsForSeries(int seriesID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllSeriesFieldsForSeries(seriesID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public void InsertSeriesField(SeriesFieldML item)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.InsertSeriesField(item);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public void UpdateSeriesField(SeriesFieldML item)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.UpdateSeriesField(item);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public void DeleteSeriesField(int fieldID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.DeleteSeriesField(fieldID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        #endregion

        #region SeriesFieldValue methods

        public SeriesFieldValueCollectionML LoadAllSeriesFieldValues()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllSeriesFieldValues();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SeriesFieldValueCollectionML LoadAllSeriesFieldValuesBySeriesID(int seriesID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllSeriesFieldValuesBySeriesID(seriesID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public void InsertSeriesFieldValue(SeriesFieldValueML item)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.InsertSeriesFieldValue(item);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public void UpdateSeriesFieldValue(SeriesFieldValueML item)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.UpdateSeriesFieldValue(item);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public void DeleteSeriesFieldValue(int seriesID, int fieldID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.DeleteSeriesFieldValue(seriesID, fieldID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        #endregion

        #region Speaker Methods

        public SpeakerCollectionML LoadAllSpeakers()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllSpeakers();

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SpeakerCollectionML LoadAllVisibleSpeakers()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllVisibleSpeakers();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SpeakerCollectionML LoadAllVisibleSpeakersWithSermons()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllVisibleSpeakersWithSermons();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SpeakerML LoadSpeaker(int speakerID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadSpeaker(speakerID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public SpeakerML LoadVisibleSpeaker(int speakerID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadVisibleSpeaker(speakerID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public void InsertSpeaker(SpeakerML speaker)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.InsertSpeaker(speaker);

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public void UpdateSpeaker(SpeakerML speaker)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.UpdateSpeaker(speaker);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public void DeleteSpeaker(int SpeakerID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.DeleteSpeaker(SpeakerID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        #endregion

        #region Topic Methods

        public TopicCollectionML LoadAllTopics()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllTopics();

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public TopicCollectionML LoadAllVisibleTopics()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllVisibleTopics();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public TopicCollectionML LoadAllVisibleTopicsWithSermons()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllVisibleTopicsWithSermons();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public void InsertTopic(TopicML topic)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.InsertTopic(topic);

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public void UpdateTopic(TopicML topic)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.UpdateTopic(topic);

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public void DeleteTopic(int TopicID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.DeleteTopic(TopicID);

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        #endregion

        #region Topic2Sermon Methods

        public TopicCollectionML LoadTopicsBySermonID(int SermonID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadTopicsBySermonID(SermonID);

            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public void InsertTopic2Sermon(int SermonID, int SetID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.InsertTopic2Sermon(SermonID, SetID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        public void DeleteTopic2Sermon(int SermonID, int SetID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.DeleteTopic2Sermon(SermonID, SetID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;

            }
            finally
            {
                service = null;
            }
        }

        #endregion

        #region Setting methods

        public SettingCollectionML LoadAllSettings()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllSettings();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public void InsertSetting(SettingML setting)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.InsertSetting(setting);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public void UpdateSetting(SettingML setting)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.UpdateSetting(setting);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public void DeleteSetting(int SettingID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.DeleteSetting(SettingID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        #endregion

        #region Activity methods

        public ActivityCollectionML LoadAllActivity()
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllActivity();
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public ActivityCollectionML LoadActivityByDateRange(DateTime startDate, DateTime endDate)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadActivityByDateRange(startDate, endDate);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public void InsertActivity(ActivityML activity)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.InsertActivity(activity);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public void UpdateActivity(ActivityML activity)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.UpdateActivity(activity);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        public void DeleteActivity(int activityID)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                service.DeleteActivity(activityID);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        #endregion

        #region ReportSeries methods

        public ReportSeriesCollectionML LoadAllReportSeriesByDateRange(DateTime startDate, DateTime endDate)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllReportSeriesByDateRange(startDate, endDate);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        #endregion

        #region ReportTopic methods

        public ReportTopicCollectionML LoadAllReportTopicsByDateRange(DateTime startDate, DateTime endDate)
        {
            IBackstageSermonsDataLayer service = getImplementation();

            try
            {
                return service.LoadAllReportTopicsByDateRange(startDate, endDate);
            }
            catch (Exception exInvoke)
            {
                throw exInvoke;
            }
            finally
            {
                service = null;
            }
        }

        #endregion


        #endregion

        #region Private methods

        #region IBackstageSermonsDataLayer getImplementation()

        /// <summary>
        /// Dynamically load configured business actions assembly
        /// </summary>
        /// <returns>Interface pointer to business actions object that implements IBackstageDataLayer</returns>
        private IBackstageSermonsDataLayer getImplementation()
        {
            IBackstageSermonsDataLayer service = null;

            try
            {
                // eg: BackstageSermons.DataLayer.SQLServer.SermonsDLL, BackstageSermons.DataLayer.SQLServer, Version=1.0.0.0, Culture=neutral

                string strTypeName = string.Empty; // As2Settings.Instance.DataLayerTypeName;

                if (string.IsNullOrEmpty(strTypeName))
                {
                    //signed
                    //strTypeName = "BackstageSermons.DataLayer.SQLServer.SermonsDLL, BackstageSermons, Version=1.0.0.0, Culture=neutral, PublicKeyToken=525568904810dc8c";
                    strTypeName = "BackstageSermons.DataLayer.SQLServer.SermonsDLL, BackstageSermons, Version=1.0.0.0, Culture=neutral";
                }

                Type t = Type.GetType(strTypeName);

                if (t == null)
                    throw new Exception(string.Format("Unable to load type [{0}]", strTypeName));

                service = (IBackstageSermonsDataLayer)Activator.CreateInstance(t); // uses fully qualified type name

            }
            catch (Exception exTypeLoad)
            {
                throw exTypeLoad;
            }

            return service;
        }

        #endregion

        #endregion
    }
}
