﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackstageSermons
{
    public class SeriesFieldCollectionML : List<SeriesFieldML>
    {
        public SeriesFieldML FindByFieldID(int fieldID)
        {
            foreach (SeriesFieldML sf in this) {
                if (
                   (fieldID == sf.FieldID)
                ) {
                    return sf;
                }
            }
            return null;
        }
    }
}