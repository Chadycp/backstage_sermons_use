﻿using System;
using BackstageSermons.Settings;

namespace BackstageSermons.modules.SermonAudio
{
    public partial class Settings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                SetDefaults();

        }

        protected void SetDefaults()
        {
            txtMemberID.Text = BackstageSermons.Settings.SermonAudio.MemberID;
            txtPassword.Text = BackstageSermons.Settings.SermonAudio.Password;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            BackstageSermons.Settings.SermonAudio.MemberID = txtMemberID.Text;
            BackstageSermons.Settings.SermonAudio.Password = txtPassword.Text;

            BackstageSermons.Settings.SermonAudio.Update();
            RefreshPage();
        }

        protected void RefreshPage()
        {
            Response.Redirect("Settings.aspx");
        }
    }
}
