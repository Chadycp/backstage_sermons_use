﻿<%@ Page Language="C#" MasterPageFile="~/backstageIncludes/BackstageSermons/Sermons.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BackstageSermons.modules.Sermons.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <title>Backstage Sermons :: Sermon Manager</title>
    <link href="css/SermonStyles.css" rel="stylesheet" type="text/css" />
    
    <!--For Series Folders-->	
	<link rel="stylesheet" href="/backstageIncludes/BackstageSermons/modules/Sermons/css/jquery.treeview.css" />
	<script src="/backstage/modules/ImageManager/jquery.treeview/lib/jquery.cookie.js" type="text/javascript"></script>
	<script src="/backstage/modules/ImageManager/jquery.treeview/jquery.treeview.js" type="text/javascript"></script>
	<!--End Series Folders-->
	
	<script src="/backstage/js/PrintHelper.js" type="text/javascript"></script>
	<script src="js/SermonManager.js" type="text/javascript"></script>
    <script type="text/javascript">

        function ConfirmVisibility(ddl) {
            if (ddl.value != "-") {
                if (!confirm('Are you sure you wish to change the visiblity of all the selected sermons?')) return false;
            } else {
                return false;
            }
        }
        function equalHeight(group) {
            var tallest = 0;
            group.css("height", "auto");
            group.each(function () {
                var thisHeight = $(this).height();
                if (thisHeight > tallest) {
                    tallest = thisHeight;
                }
            });
            group.height(tallest);
        }
        $(document).ready(function () {
            $(".printTrigger").printHelper();
            $("#browser").treeview({
                //animated:"normal",
                animated: "fast",
                //persist: "cookie"
                persist: "location",
                toggle: checkHeight
            });

            checkHeight();

            $("#browser").find("span").hover(function () {
                $(this).children("span .imgSeriesAction").css({ "display": "inline" });
            }, function () {
                $(this).children("span .imgSeriesAction").css({ "display": "none" });
            });

            $(".pencil").click(function () {
                $(this).parent().parent().parent().children("#inputBox").show("fast");
            });
            $(".closeButton").click(function () {
                $(this).parent().hide("fast");
            });
            $('#subNavSermons').addClass('subActive');
        });

        function checkHeight() {
            equalHeight($("div.sectionContent").children('div').children('div'));
        }


    </script>
<style type="text/css">
    
.ttFunctions {
	display: none;
	position: absolute;
	background: #ffffff;
	color: #666666;
	top: 10px;
	left: 20px;
	z-index: 1;
	white-space: nowrap;
	text-align: left;
	border: 1px solid #aaaaaa \9;
	-webkit-box-shadow: 0 2px 5px rgba(0,0,0,0.4);
	   -moz-box-shadow: 0 2px 5px rgba(0,0,0,0.4);
	        box-shadow: 0 2px 5px rgba(0,0,0,0.4);
	-webkit-border-radius: 5px;
	   -moz-border-radius: 5px;
	        border-radius: 5px;
    }

.ttFunction {
	cursor: pointer;
	padding: 3px 5px;
    }

.ttFunction:first-child {
	-webkit-border-top-right-radius: 5px;
	 -webkit-border-top-left-radius: 5px;
	    -moz-border-radius-topright: 5px;
	     -moz-border-radius-topleft: 5px;
	        border-top-right-radius: 5px;
	         border-top-left-radius: 5px;
    }

.ttFunction:last-child {
	-webkit-border-bottom-right-radius: 5px;
	 -webkit-border-bottom-left-radius: 5px;
	    -moz-border-radius-bottomright: 5px;
	     -moz-border-radius-bottomleft: 5px;
	        border-bottom-right-radius: 5px;
	         border-bottom-left-radius: 5px;
    }

.ttFunction:hover {
	background-color: #f5f5f5;
	}

</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">


<asp:Label runat="server" ID="lblResult" style="color: #990000;"></asp:Label>
    
	    <cms:PopupPanel runat="server" ID="pnlImportExport" Visible="false">
	        <ContentTemplate>
			    <div style="">
				    <!--Export Panel -->
				    <asp:Panel runat="server" ID="pnlExportSermons" Visible="false" style="padding: 20px;">
					    <h1>Export Sermons</h1>
					    Select which fields you would like to export.<br />
					    <input type="checkbox" id="cbSelectAll" checked="checked" /> Select/Unselect All
					    <div style="margin: 10px 0 10px 0; height: 236px; padding-right: 20px; overflow: auto; white-space: nowrap;">
						    <asp:CheckBoxList runat="server" ID="cbExportSermonFields" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList>
					    </div>
					    <asp:Button runat="server" ID="btnExportSermonFields" Text="Export" OnClick="btnExportSermonFields_Click" />
					    <asp:Button runat="server" ID="btnCancelExportSermonFields" Text="Close" OnClick="btnCancelImportExport_Click" />
				    </asp:Panel>
				    <!-- End Export Panel -->
				    <!--Select CSV Panel -->
				    <asp:Panel runat="server" ID="pnlUploadCSV" Visible="false">
					    <table style="width: 476px; height: 399px;" cellpadding="0" cellspacing="0">
						    <tr>
							    <td style="height: 27px;"></td>
						    </tr>
						    <tr>
							    <td style="width: 100%; height: 100%;" align="center" valign="middle">
								    <div style="width: 244px; text-align: left;">
									    <asp:Label runat="server" ID="lblUploadMessage"></asp:Label>
									    <h1>Select CSV file:</h1>
									    <asp:FileUpload runat="server" ID="fileUploadSermons" /><br />
									    <asp:Button runat="server" ID="btnUploadCSV" Text="Upload" OnClick="btnUploadCSV_Click" />
									    <asp:Button runat="server" ID="btnUploadSermonsCancel" OnClick="btnCancelImportExport_Click" Text="Cancel" />
								    </div>
							    </td>
						    </tr>
						    <tr>
							    <td style="height: 27px;"></td>
						    </tr>
					    </table>
				    </asp:Panel>
				    <!--End Select CSV Panel -->
				    <!-- Match Fields Panel -->
				    <asp:Panel runat="server" ID="pnlMatchFields" Visible="false">
					    <table style="width: 476px; height: 399px;" cellpadding="0" cellspacing="0">
						    <tr>
							    <td style="height: 27px; width: 150px; text-align: right;">
								    <div style="height: 21px; padding: 6px 0 0 0; border-right: #CCC 1px solid;">Sermon Fields&nbsp;&nbsp;&nbsp;</div>
							    </td>
							    <td style="height: 27px; width: 166px;">
								    <div style="height: 21px; padding: 6px 0 0 0; border-right: #CCC 1px solid;">&nbsp;&nbsp;Your Fields</div>
							    </td>
						    </tr>
						    <tr>
							    <td colspan="2" style="border-right: 1px solid #CCC;">
								    <div style="height: 345px; overflow: auto;">
									    <asp:Panel runat="server" ID="pnlSermonFields" style="width: 226px; text-align: right; float: left;"></asp:Panel>
									    <asp:Panel runat="server" ID="pnlFileFields" style="width: 234px; float: left;"></asp:Panel>
									    <div class="clearBoth"></div>
								    </div>
							    </td>
						    </tr>
						    <tr>
							    <td style="height: 27px;"></td>
							    <td style="height: 27px; padding: 0 10px 0 0;" align="right">
								    <asp:Button CssClass="importButtons" runat="server" ID="btnImportSermons" OnClick="importSermons_Click" Text="Import" />
								    <asp:Button CssClass="importButtons" runat="server" ID="btnImportCancel" OnClick="btnCancelImportExport_Click" Text="Cancel" />
							    </td>
						    </tr>
					    </table>
				    </asp:Panel>
				    <!-- End Match Fields Panel -->           
			    </div>
			</ContentTemplate>
		</cms:PopupPanel>
	



<div class="topBarSearch">
    <cms:DelayedSubmitExtender ID="DelayedSubmitExtender1" runat="server" Timeout="400" TargetControlID="txtSearch"/>
    <asp:TextBox ID="txtSearch" CssClass="search" Text="Search Sermons" onfocus="if(this.value == 'Search Sermons'){this.value='';}" onblur="if(this.value==''){this.value='Search Sermons';}" runat="server" AutoPostBack="True" OnTextChanged="txtSearch_OnTextChanged" />
</div>
<div class="sectionContent clearfix">
	<div class="leftColumnWrapper" style="width:280px;overflow:hidden">
		<div class="treeColumn">
			<div>
				<div style="padding: 8px 20px 0">
					<a href="/backstageIncludes/BackstageSermons/modules/Sermons/EditSeries.aspx" id="addSeries" class="bxButton2 green" style="float:none;width:100%;padding-left:0;padding-right:0">+ Add Series</a>
				</div>
			</div>
            <h1>Series</h1>
            <asp:Panel runat="server" ID="pnlSeries" style="padding: 0 15px"></asp:Panel>
		</div>
	</div>
	
	<div class="rightColumnWrapper" style="width: 700px;">
		<div class="rightColumn">
			<div class="toolbarContainer">
				<div class="toolbar clearfix">
					<div class="leftButtons clearfix" style="position: relative;">
					    <asp:CheckBox runat="server" ID="cbSelectAllSermons" AutoPostBack="true" OnCheckedChanged="lbSelectAllSermons_Click" style="position: absolute; z-index: 1; left: 5px; top: 3px; margin: 0; border: none;" />
					    <asp:LinkButton runat="server" ID="lbSelectAll" CssClass="bxButton2" OnClick="lbSelectAllSermons_Click" style="padding-left: 24px;">
				                <asp:Label runat="server" ID="lblSelectAllSermons" Text="Select All"></asp:Label>
				        </asp:LinkButton>
						<asp:LinkButton runat="server" data-icon="minuscircle" ID="lbDeleteSermon" CssClass="bxButton2 bleft" Text="Delete Selected" OnClick="btnDeleteSermon_Click" OnClientClick="return confirm('Are you sure you wish to delete all the selected sermons?');" />
						<div class="bxButton2 bright" data-icon="eye" data-dropdown="yes">Visibility
						    <div class="dropdownMenu">
						        <asp:LinkButton runat="server" ID="lbMarkVisible" OnClick="lbVisibility_Click" CommandArgument="Show">Show Selected</asp:LinkButton>
						        <asp:LinkButton runat="server" ID="lbMarkInvisible" OnClick="lbVisibility_Click" CommandArgument="Hide">Hide Selected</asp:LinkButton>
						    </div>
						</div>
					</div>
					<div class="rightButtons clearfix">
						<asp:LinkButton data-icon="tableexport" Text="Export List" runat="server" ID="lbExportSermons" CssClass="bxButton2" OnClick="lbExportSermons_Click" />
						<asp:LinkButton runat="server" ID="lbPrint" data-icon="printer" CssClass="bxButton2" OnClick="lbPrint_Click">Print List</asp:LinkButton>
						<a data-icon="printer" id="A1" data-icon="printer" class="bxButton2 printTrigger" style="display: none;" href="Print.aspx">Print List</a>
						<!--<asp:LinkButton runat="server" Text="Import" CssClass="bxButton2 green bleft" ID="lbImportSermons" OnClick="lbImportSermons_Click" />-->
						<a href="EditSermon.aspx?SeriesID=<%= strSeriesID %>" class="bxButton2 green">+ Add New Sermon</a>
					</div>
				</div>
			</div>
			<div class="tableContainer">
				<asp:UpdatePanel runat="server" ID="updateSermons" ChildrenAsTriggers="True" UpdateMode="Conditional">
                    <ContentTemplate>
                        
                        <asp:GridView runat="server" ID="gridSermons" AllowPaging="True" CssClass="list" AllowSorting="True" OnRowCommand="gridSermons_OnRowCommand"
                            AutoGenerateColumns="False" CellPadding="0" DataKeyNames="SermonID" GridLines="None" OnPageIndexChanging="gridSermons_PageIndexChanging"
                            OnRowCreated="gridSermons_OnRowCreated" OnDataBound="gridSermons_DataBound" OnSorting="gridSermons_Sorting" PageSize="12" PagerSettings-PageButtonCount="15">
                            <EmptyDataTemplate>
                            	<div style="padding:30px;text-align:center;font-size:20px;color:#999999">There are no sermons in this series.</div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField>
            	                    <HeaderTemplate></HeaderTemplate>
                                    <ItemTemplate>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="border-right: none;">
                                                    <asp:CheckBox runat="server" ID="cbSermon" cID='<%# Eval("SermonID") %>' />
                                                    <asp:HiddenField runat="server" ID="hfSermonID" Value='<%# Eval("SermonID") %>' />
                                                </td>
                                                <td>
                                                    <div class="SermonInfoView">
                    	                                <asp:ImageButton runat="server" ID="btnVisibility" CommandName="ChangeVisibility" CommandArgument='<%# Eval("SermonID") %>' ImageUrl='<%# (bool)Eval("Available") ? "/backstage/images/icons/eyeIcon.png" : "/backstage/images/icons/eyeIconClose.png" %>' />
                    	                            </div>
                    	                            <div class="SermonEditView" style="display: none;">
							                            <img style="border-width:0px; float:left; display:block" onclick="quickEditChangeVisibility(this)" src="<%# (bool)Eval("Available") ? "/backstage/images/icons/eyeIcon.png" : "/backstage/images/icons/eyeIconClose.png" %>" alt="visibility" />
                    	                                <asp:CheckBox class="pmUpdate"  runat="server" ID="cbEditActive" Checked='<%# (bool)Eval("Available") %>' style="display: none;" />
                    	                            </div>
                                                </td>
                                            </tr>
                                        </table> 
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Title" SortExpression="Title">
                                    <ItemTemplate>
                                        <div class="SermonInfoView">
                                            <a href="EditSermon.aspx?SermonID=<%# Eval("SermonID") %>&SeriesID=<%# strSeriesID %>">
                                                <%# backstage.Classes.StringEditor.Truncate(Eval("Title").ToString(), 32, "...", false) %>
                                            </a>
                                        </div>
                                        <div class="SermonEditView" style="display: none;">
                                            <asp:TextBox runat="server" ID="txtEditTitle" style="width: 190px;" Text='<%# Eval("Title") %>'></asp:TextBox>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Passage" SortExpression="Passage">
                                    <ItemTemplate>
                                        <div class="SermonInfoView">
						                    <%# Eval("PassageFull") %>
						                </div>
						                <div class="SermonEditView" style="display: none;">
                                            <asp:Label runat="server" ID="lblPassageBook" Style="padding:0">Book:</asp:Label>
                                            <asp:DropDownList runat="server" ID="ddlPassageBook" DataSource='<%# books %>' selectedValue='<%# Eval("PassageBook") %>' DataTextField="Name" DataValueField="Name" style="width: 110px;" AppendDataBoundItems="True">
			                                    <asp:ListItem Value="" Text="--" ></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label runat="server" ID="lblPassageChapter" Style="padding:0">Chapter:</asp:Label>
                                            <asp:TextBox runat="server" ID="txtPassageChapter" Text='<%# Eval("PassageChapter") %>' CssClass="regField" style="width: 110px;"></asp:TextBox>
                                            <asp:Label runat="server" ID="lblPassageVerse" Style="padding:0">Verse:</asp:Label>
                                            <asp:TextBox runat="server" ID="txtPassageVerse" Text='<%# Eval("PassageVerse") %>' CssClass="regField" style="width: 110px;"></asp:TextBox>
						                </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Speaker" SortExpression="Speaker">
                                    <ItemTemplate>
                                        <div class="SermonInfoView">
                                            <%# Eval("SpeakerName") %>
                                        </div>
                                        <div class="SermonEditView" style="display: none;">
                                            <asp:DropDownList runat="server" ID="ddlSpeaker" DataSource='<%# speakers %>' selectedValue='<%# GetSpeakerSelectedValue(Eval("Speaker").ToString()) %>' DataTextField="Name" DataValueField="SpeakerID" style="width: 120px;" AppendDataBoundItems="true">
                                                <asp:ListItem Value="" Text="--Select Speaker--"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div class="functions" style="position:relative">
                                        	<div class="SermonInfoView">
						                        <img alt="edit" title="Edit this sermon" src="/backstage/images/icon-edit.png" onclick="showHideEditControls(this)" />
						                        <asp:ImageButton runat="server" ID="btnDeleteSermon" CommandName="DeleteSermon" CommandArgument='<%# Eval("SermonID") %>' ImageUrl="/backstage/images/icon-trash.png" ToolTip="Delete this sermon" OnClientClick="return confirm('Are you sure you wish to delete this sermon?');" />
						                    </div>
						                    <div class="SermonEditView" style="display: none;">
						                        <asp:LinkButton runat="server" ID="lbUpdateSermon" Text="Update" class="pmUpdateBtn" CommandName="UpdateSermon" CommandArgument='<%# Eval("SermonID") %>'><img alt="save" src="/backstage/images/icon-save.png" /></asp:LinkButton>
						                        <img alt="cancel" src="/backstage/images/icon-cancel.png" onclick="closeSermonEditView(this)" />
						                    </div>
                                            <div class="ttFunctions sermonEditControls">
                                                <div class="ttFunction" onclick="toggleSermonEdit(this)">Quick Edit</div>
                                                <div class="ttFunction" onclick="window.location='EditSermon.aspx?SermonID=<%# Eval("SermonID") %>&SeriesID=<%# strSeriesID %>';">Edit Sermon</div>
                                                <div class="ttFunction" onclick="closeEditControls(this)">Close</div>
                                            </div>
						                </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle CssClass="trPager" />
                            <RowStyle CssClass="trContent" />
                        </asp:GridView>
                    
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="txtSearch" EventName="TextChanged" />
                        <asp:AsyncPostBackTrigger ControlID="lbDeleteSermon" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="lbMarkVisible" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="lbMarkInvisible" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="lbPrint" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
			</div>
		</div>
	</div>
</div>

<asp:UpdateProgress ID="udProgress" runat="server" DisplayAfter="10" Visible="true" DynamicLayout="true">
    <ProgressTemplate>
        <div style="position: absolute;">
            <div class="processing">
                <img alt="Processing" src="/backstage/images/NewButtons/ajax-loader.gif">
            </div>
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>

</asp:Content>