﻿using System;
using System.Text;

namespace BackstageSermons
{
    public class WebPage : backstage.WebPage
    {
        private int _SermonID;
        public int SermonID {
            get {
                if (_SermonID == 0)
                {
                    string strSermonID = Request.QueryString["SermonID"];
                    if (!string.IsNullOrEmpty(strSermonID))
                        _SermonID = int.Parse(strSermonID);
                }
                return _SermonID;
            }
        }

        public bool SermonDetailView;

        private SermonML _sermon;
        public SermonML sermon {
            get {
                if (_sermon == null)
                    _sermon = BackstageSermons.SermonsBLL.Instance.LoadSermon(SermonID, true);
                return _sermon;
            }
        }

        public override void SetSEO()
        {
            #region GetSEO from Sermon
            Trace.Write("SetSEO", SermonDetailView.ToString());
            if (SermonDetailView)
            {
                if (string.IsNullOrEmpty(sermon.SEOTitle))
                    PageTitle = sermon.Title;
                else
                    PageTitle = sermon.SEOTitle;
                PageDescription = sermon.SEODescription;

                StringBuilder sb = new StringBuilder();
                foreach (TopicML t in sermon.Topics)
                {
                    if (sb.Length > 0)
                        sb.Append(", ");
                    sb.Append(t.Title);
                }
                PageKeywords = sb.ToString();
                if (!string.IsNullOrEmpty(sermon.TitleGraphic))
                {
                    backstage.security.ImageML img = new backstage.security.ImageML(sermon.TitleGraphic);
                    if (img != null && !string.IsNullOrEmpty(img.Src))
                        PageImageSrc = img.Src;
                }
                if (string.IsNullOrEmpty(PageImageSrc) && sermon.SeriesID > 0)
                {
                    try
                    {
                        SeriesML series = SermonsBLL.Instance.LoadSeries(sermon.SeriesID, false);
                        if (series.Images != null && series.Images.Count > 0 && series.Images[0] != null && !string.IsNullOrEmpty(series.Images[0].Src))
                            PageImageSrc = series.Images[0].Src;
                    }
                    catch { }
                }
                if (string.IsNullOrEmpty(PageImageSrc) && sermon.Speaker > 0)
                {
                    try
                    {
                        SpeakerML speaker = SermonsBLL.Instance.LoadSpeaker(sermon.Speaker);
                        PageImageSrc = speaker.Photo;
                    }
                    catch { }
                }
                if (string.IsNullOrEmpty(PageImageSrc))
                {
                    backstage.security.ImageML img = new backstage.security.ImageML(BackstageSermons.Settings.Podcast.ImageID);
                    if (img != null && !string.IsNullOrEmpty(img.Src))
                        PageImageSrc = img.Src;
                }
            }
            #endregion
            base.SetSEO();
        }
    }
}
