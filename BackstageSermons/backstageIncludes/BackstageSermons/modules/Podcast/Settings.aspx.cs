﻿using System;
using backstage.Classes;
using BackstageSermons.Settings;

namespace BackstageSermons.modules.Podcast
{
    public partial class Settings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetDefaults();
        }

        protected void SetDefaults()
        {
            txtID.Text = BackstageSermons.Settings.Podcast.ID;
            txtTitle.Text = BackstageSermons.Settings.Podcast.Title;
            txtLink.Text = BackstageSermons.Settings.Podcast.Link;
            ddlLanguage.SelectedValue = BackstageSermons.Settings.Podcast.Language;
            txtCopyright.Text = BackstageSermons.Settings.Podcast.Copyright;
            txtSubtitle.Text = BackstageSermons.Settings.Podcast.Subtitle;
            txtAuthor.Text = BackstageSermons.Settings.Podcast.Author;
            txtSummary.Text = BackstageSermons.Settings.Podcast.Summary;
            txtDescription.Text = BackstageSermons.Settings.Podcast.Description;
            txtOwnerName.Text = BackstageSermons.Settings.Podcast.OwnerName;
            txtOwnerEmail.Text = BackstageSermons.Settings.Podcast.OwnerEmail;
            //imgPhoto.ImageUrl = BackstageSermons.Settings.Podcast.Image;
            imagePicker.ImageID = BackstageSermons.Settings.Podcast.ImageID;
            //txtImgSrc.Text = BackstageSermons.Settings.Podcast.Image;

            txtMaxSermons.Text = BackstageSermons.Settings.Podcast.MaxSermons > 0 ? BackstageSermons.Settings.Podcast.MaxSermons.ToString() : "";
            ddlDateRange.SelectedValue = BackstageSermons.Settings.Podcast.DateRange.ToString();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            BackstageSermons.Settings.Podcast.ID = txtID.Text;
            BackstageSermons.Settings.Podcast.Title = txtTitle.Text;
            BackstageSermons.Settings.Podcast.Link = txtLink.Text;
            BackstageSermons.Settings.Podcast.Language = ddlLanguage.SelectedValue;
            BackstageSermons.Settings.Podcast.Copyright = txtCopyright.Text;
            BackstageSermons.Settings.Podcast.Subtitle = txtSubtitle.Text;
            BackstageSermons.Settings.Podcast.Author = txtAuthor.Text;
            BackstageSermons.Settings.Podcast.Summary = txtSummary.Text;
            BackstageSermons.Settings.Podcast.Description = txtDescription.Text;
            BackstageSermons.Settings.Podcast.OwnerName = txtOwnerName.Text;
            BackstageSermons.Settings.Podcast.OwnerEmail = txtOwnerEmail.Text;
            //BackstageSermons.Settings.Podcast.Image = txtImgSrc.Text;
            BackstageSermons.Settings.Podcast.ImageID = imagePicker.ImageID;

            BackstageSermons.Settings.Podcast.MaxSermons = StringEditor.ToInt32(txtMaxSermons.Text);
            BackstageSermons.Settings.Podcast.DateRange = (BackstageSermons.Settings.Podcast.PodcastDateRanges)StringEditor.ToEnum(ddlDateRange.SelectedValue, typeof(BackstageSermons.Settings.Podcast.PodcastDateRanges));

            BackstageSermons.Settings.Podcast.Update();

            RefreshPage();
        }

        protected void RefreshPage()
        {
            Response.Redirect("Settings.aspx");
        }
    }
}
