﻿using System;

namespace BackstageSermons.modules.Sermons
{
    public partial class Print : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            gridSermons.DataSource = Session["SermonCollection"];
            gridSermons.DataBind();
        }
    }
}
