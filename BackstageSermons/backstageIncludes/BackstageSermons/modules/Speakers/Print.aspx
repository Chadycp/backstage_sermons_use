﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Print.aspx.cs" Inherits="BackstageSermons.modules.Speakers.Print" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>Backstage Sermons :: Sermon Manager</title>
<link href="/backstage/css/styles.common.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    
table.list tr th a {
    background-image: none;
}

</style>
</head>
<body>
    <form id="form1" runat="server">

        <asp:GridView runat="server" ID="gridSpeakers" CssClass="list" AllowSorting="True"
            AutoGenerateColumns="False" CellPadding="0" DataKeyNames="SpeakerID" GridLines="None">
            <EmptyDataTemplate>
            	<div style="padding:30px;text-align:center;font-size:20px;color:#999999">There are currently no speakers to show.</div>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate></HeaderTemplate>
                    <ItemTemplate>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="border-right: none;">
                                    <asp:CheckBox runat="server" ID="cbSpeaker" cID='<%# Eval("SpeakerID") %>' />
                                    <asp:HiddenField runat="server" ID="hfSpeakerID" Value='<%# Eval("SpeakerID") %>' />
                                </td>
                            </tr>
                        </table> 
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <span><img src="<%# Eval("Photo") %>" width="40" /></span>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Name" SortExpression="Name">
                    <ItemTemplate>
                        <%# backstage.Classes.StringEditor.Truncate(Eval("Name").ToString(), 40, "...", false) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Bio">
                    <ItemTemplate>
                        <%# backstage.Classes.StringEditor.Truncate(backstage.Classes.StringEditor.RemoveHTML(Eval("Bio").ToString()), 80, "...", true) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <div class="functions">
						</div>
                    </ItemTemplate>
                </asp:TemplateField>
	    
	    
	    </Columns>
            <PagerStyle CssClass="trPager" />
            <RowStyle CssClass="trContent" />
        </asp:GridView>
    </form>
</body>
</html>

