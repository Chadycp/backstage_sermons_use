﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;

namespace BackstageSermons
{
    public class ConnectionBAC
    {
        #region Constructors

        #region ConnectionBAC()

        /// <summary>
        /// Default constructor
        /// </summary>
        public ConnectionBAC()
        {
        }

        #endregion

        #endregion

        #region Instance data

        private static readonly ConnectionBAC instance = new ConnectionBAC();

        /// <summary>
        /// returns the ConnectionBAC singleton
        /// </summary>
        public static ConnectionBAC Instance
        {
            get { return instance; }
        }

        #endregion

        #region Private member data

        #endregion

        #region Public properties

        public string ConnectionString
        {
            get
            {
                //System.Diagnostics.Trace.WriteLine(string.Format("Connection [{0}]", ConfigurationManager.ConnectionStrings["backstage"].ConnectionString));
                return ConfigurationManager.ConnectionStrings["backstage"].ConnectionString;
            }
        }

        #endregion
    }
}
