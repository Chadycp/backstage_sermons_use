﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using backstage.Classes;
using BackstageSermons.Settings;

namespace BackstageSermons.modules.Podcast
{
    public partial class Default : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //Stop tracking podcast hits. Not very helpful with aggregated feeds checking for updates...
            //try
            //{
                //ActivityML a = new ActivityML();
                //a.ActivityDate = DateTime.Now;
                //a.ActivityType = ActivityTypes.Podcast;
                //SermonsBLL.Instance.InsertActivity(a);
            //}
            //catch { }
            XmlDocument doc = new XmlDocument();
            doc.Load(Server.MapPath("PodcastTemplate.xml"));

            string sIDs = Request.QueryString["SeriesIDs"];
            List<int> seriesIDs = null;
            if (!string.IsNullOrEmpty(sIDs))
                seriesIDs = StringEditor.StringToIntList(sIDs, ",");

            XmlNode ndChannel = doc.SelectSingleNode("rss/channel");

            string title = BackstageSermons.Settings.Podcast.Title;
            string subtitle = BackstageSermons.Settings.Podcast.Subtitle;
            string description = BackstageSermons.Settings.Podcast.Description;
            string imgSrc = new backstage.security.ImageML(BackstageSermons.Settings.Podcast.ImageID).Src;
            if (seriesIDs != null && seriesIDs.Count == 1)
            {
                try
                {
                    SeriesML s = SermonsBLL.Instance.LoadSeries(seriesIDs[0], false);
                    if (string.IsNullOrEmpty(s.PodcastTitle))
                        title += " :: " + s.Title;
                    else
                        title = s.PodcastTitle;
                    if (!string.IsNullOrEmpty(s.PodcastSubtitle))
                        subtitle = s.PodcastSubtitle;
                    if (!string.IsNullOrEmpty(s.PodcastDescription))
                        description = s.PodcastDescription;
                    if (s.PodcastImageID > 0)
                    {
                        string imgs = new backstage.security.ImageML(s.PodcastImageID).Src;
                        if (!string.IsNullOrEmpty(imgs))
                            imgSrc = imgs;
                    }
                }
                catch { }
            }
            XmlHelper.GetSingleNode(ndChannel, "title").InnerText = title;
            //XmlHelper.GetSingleNode(ndChannel, "link").InnerText = BackstageSermons.Settings.Podcast.Link;
            XmlHelper.GetSingleNode(ndChannel, "link").InnerText = Request.Url.ToString();
            XmlHelper.GetSingleNode(ndChannel, "language").InnerText = BackstageSermons.Settings.Podcast.Language;
            XmlHelper.GetSingleNode(ndChannel, "copyright").InnerText = "&#xA9; " + BackstageSermons.Settings.Podcast.Copyright;

            XmlNamespaceManager mgr = new XmlNamespaceManager(doc.NameTable);
            mgr.AddNamespace("itunes", "http://www.itunes.com/dtds/podcast-1.0.dtd");

            ndChannel.SelectSingleNode("itunes:subtitle", mgr).InnerText = subtitle;
            ndChannel.SelectSingleNode("itunes:author", mgr).InnerText = BackstageSermons.Settings.Podcast.Author;
            ndChannel.SelectSingleNode("itunes:summary", mgr).InnerText = BackstageSermons.Settings.Podcast.Summary;
            ndChannel.SelectSingleNode("description").InnerText = description;
            ndChannel.SelectSingleNode("itunes:owner/itunes:name", mgr).InnerText = BackstageSermons.Settings.Podcast.OwnerName;
            ndChannel.SelectSingleNode("itunes:owner/itunes:email", mgr).InnerText = BackstageSermons.Settings.Podcast.OwnerEmail;
            ndChannel.SelectSingleNode("itunes:image/@href", mgr).InnerText = backstage.Classes.UrlHelper.FullDomain + imgSrc;
            XmlNode ndImage = ndChannel.SelectSingleNode("itunes:image", mgr);

            XmlNode ndSub = doc.CreateElement("url");
            ndSub.InnerText = backstage.Classes.UrlHelper.FullDomain + new backstage.security.ImageML(BackstageSermons.Settings.Podcast.ImageID).Src;
            ndImage.AppendChild(ndSub);


            ndSub = doc.CreateElement("title");
            ndSub.InnerText = "";
            ndImage.AppendChild(ndSub);

            ndSub = doc.CreateElement("link");
            ndSub.InnerText = "";
            ndImage.AppendChild(ndSub);

            int maxSermons = BackstageSermons.Settings.Podcast.MaxSermons > 0 ? BackstageSermons.Settings.Podcast.MaxSermons : 10000;
            DateTime? sDate = null;
            DateTime? eDate = null;

            SermonCollectionML sc = null;
            
            //if (BackstageSermons.Settings.Podcast.MaxSermons > 0 && BackstageSermons.Settings.Podcast.DateRange == BackstageSermons.Settings.Podcast.PodcastDateRanges.None)
                //maxSermons = BackstageSermons.Settings.Podcast.MaxSermons;
                //sc = SermonsBLL.Instance.LoadLatestVisibleSermons(BackstageSermons.Settings.Podcast.MaxSermons, false);
            //else if (BackstageSermons.Settings.Podcast.DateRange != BackstageSermons.Settings.Podcast.PodcastDateRanges.None)
            if (BackstageSermons.Settings.Podcast.DateRange != BackstageSermons.Settings.Podcast.PodcastDateRanges.None)
            {
                eDate = DateTime.Now;
                sDate = BackstageSermons.Settings.Podcast.DateRange == BackstageSermons.Settings.Podcast.PodcastDateRanges.Month ? DateTime.Now.AddDays(-30) :
                    BackstageSermons.Settings.Podcast.DateRange == BackstageSermons.Settings.Podcast.PodcastDateRanges.ThreeMonth ? DateTime.Now.AddDays(-90) :
                    BackstageSermons.Settings.Podcast.DateRange == BackstageSermons.Settings.Podcast.PodcastDateRanges.SixMonth ? DateTime.Now.AddDays(-180) :
                    DateTime.Now.AddYears(-1);
                /*
                sc = SermonsBLL.Instance.LoadAllVisibleSermonsByDateRange(sDate, eDate, false);

                if (BackstageSermons.Settings.Podcast.MaxSermons > 0)
                {
                    List<SermonML> listToAdd = sc.Take(BackstageSermons.Settings.Podcast.MaxSermons).ToList();
                    sc = new SermonCollectionML();
                    foreach (SermonML s in listToAdd)
                    {
                        sc.Add(s);
                    }
                }
                 * */

            }
            //else
                //sc = SermonsBLL.Instance.LoadAllVisibleSermons(false);


            sc = SermonsBLL.Instance.SearchSermons(false, null, null, null, 0, seriesIDs, null, null, null, sDate, eDate, null, null, null, maxSermons, 1, false);

            foreach (SermonML s in sc) {
                XmlNode ndItem = doc.CreateElement("item");
                ndChannel.AppendChild(ndItem);

                XmlNode nd = doc.CreateElement("title");
                nd.InnerText = s.Title;
                ndItem.AppendChild(nd);

                nd = doc.CreateElement("itunes", "author", "http://www.itunes.com/dtds/podcast-1.0.dtd");
                nd.InnerText = s.SpeakerFullDisplayName;
                ndItem.AppendChild(nd);

                nd = doc.CreateElement("itunes", "subtitle", "http://www.itunes.com/dtds/podcast-1.0.dtd");
                nd.InnerText = "";
                ndItem.AppendChild(nd);

                nd = doc.CreateElement("itunes", "summary", "http://www.itunes.com/dtds/podcast-1.0.dtd");
                nd.InnerText = "";
                XmlCDataSection cData = doc.CreateCDataSection(s.Description);
                nd.AppendChild(cData);
                ndItem.AppendChild(nd);

                nd = doc.CreateElement("enclosure");
                XmlAttribute attr = doc.CreateAttribute("url");
                attr.Value = s.AudioURI;
                /*
                if(string.IsNullOrEmpty(s.SermonAudioLink))
                    attr.Value = Request.Url.Scheme + "://" + Request.Url.Host + s.AudioFile;
                else
                    attr.Value = s.SermonAudioLink + ".mp3";
                 */
                nd.Attributes.Append(attr);

                attr = doc.CreateAttribute("length");
                attr.Value = "";
                nd.Attributes.Append(attr);

                attr = doc.CreateAttribute("type");
                attr.Value = "audio/mpeg";
                nd.Attributes.Append(attr);

                ndItem.AppendChild(nd);

                nd = doc.CreateElement("guid");
                //nd.InnerText = Request.Url.Scheme + "://" + Request.Url.Host + s.AudioFile;
                ndItem.AppendChild(nd);

                nd = doc.CreateElement("pubDate");
                nd.InnerText = s.Date.ToString("ddd, dd MMM yyyy hh:mm:ss 'GMT'");
                //nd.InnerText = "Wed, 15 Jun 2005 19:00:00 GMT";
                ndItem.AppendChild(nd);

                nd = doc.CreateElement("itunes", "duration", "http://www.itunes.com/dtds/podcast-1.0.dtd");
                nd.InnerText = "";
                ndItem.AppendChild(nd);

                nd = doc.CreateElement("itunes", "keywords", "http://www.itunes.com/dtds/podcast-1.0.dtd");
                string keywords = "";
                /* Too slow for time being. Need to add as a comma separated list in query
                foreach (TopicML topic in SermonsBLL.Instance.LoadTopicsBySermonID(s.SermonID)) {
                    if (keywords.Length > 0)
                        keywords += ", ";
                    keywords += topic.Title;
                }
                 */
                nd.InnerText = keywords;
                ndItem.AppendChild(nd);


                nd = doc.CreateElement("itunes", "image", "http://www.itunes.com/dtds/podcast-1.0.dtd");
                //nd.InnerText = Request.Url.Scheme + "://" + Request.Url.Host + s.TitleGraphic;
                nd.InnerText = backstage.Classes.UrlHelper.FullDomain + new backstage.security.ImageML(BackstageSermons.Settings.Podcast.ImageID).Src;
                ndSub = doc.CreateElement("url");
                //ndSub.InnerText = Request.Url.Scheme + "://" + Request.Url.Host + s.TitleGraphic;
                ndSub.InnerText = backstage.Classes.UrlHelper.FullDomain + new backstage.security.ImageML(BackstageSermons.Settings.Podcast.ImageID).Src;
                nd.AppendChild(ndSub);

                ndSub = doc.CreateElement("title");
                ndSub.InnerText = "";
                nd.AppendChild(ndSub);

                ndSub = doc.CreateElement("link");
                ndSub.InnerText = "";
                nd.AppendChild(ndSub);

                ndItem.AppendChild(nd);


            }


            /*
       <item>
          <guid>http://example.com/podcasts/archive/aae20050615.m4a</guid>
          <pubDate>Wed, 15 Jun 2005 19:00:00 GMT</pubDate>
          <itunes:duration>7:04</itunes:duration>
          <itunes:keywords>salt, pepper, shaker, exciting</itunes:keywords>
        </item>
             */

            Response.ContentType = "text/xml";
            Response.Write(doc.OuterXml);
        }
    }
}
