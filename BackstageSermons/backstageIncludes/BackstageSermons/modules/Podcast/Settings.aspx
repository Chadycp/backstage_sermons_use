﻿<%@ Page Language="C#" MasterPageFile="~/backstageIncludes/BackstageSermons/Sermons.Master" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="BackstageSermons.modules.Podcast.Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <title>Backstage Sermons :: Podcast Manager</title>
    <link href="/backstageIncludes/BackstageSermons/modules/Sermons/css/SermonStyles.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
	    .copy p {
	    	line-height: 20px;
	    	}
	    .copy ul, .copy ol {
	    	line-height: 20px;
	    	padding-left: 25px;
	    	}
	    .leftColumn input[type="text"] {
	    	width: 200px;
	    	}
	</style>
    <script src="/backstageIncludes/BackstageSermons/modules/Podcast/js/podcast.js" type="text/javascript"></script>
    <script type="text/javascript">
        function equalHeight(group) {
            var tallest = 0;
            group.each(function () {
                var thisHeight = $(this).height();
                if (thisHeight > tallest) {
                    tallest = thisHeight;
                }
            });
            group.height(tallest);
        }
        $(document).ready(function () {
            equalHeight($("div.sectionContent").children('div').children('div'));
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
<asp:Label runat="server" ID="lblResult" style="color: #990000;"></asp:Label>

<div class="sectionContent clearfix">
	<div class="leftColumnWrapper" style="width:270px">
		<div class="leftColumn">
			<div class="padding20">
				<label class="full">
				    <asp:Label ID="lblPhoto" runat="server">Podcast Artwork <span style="font-size: 11px;">(600px x 600px)</span></asp:Label>
				</label>
                <bx:ImagePicker runat="server" ID="imagePicker"></bx:ImagePicker>
		        
			    <label class="full"><asp:Label ID="Label9" runat="server" Text="Podcast ID (Given by iTunes)"></asp:Label></label>
			    <asp:TextBox runat="server" ID="txtID"></asp:TextBox>
			    <label class="full"><asp:Label ID="Label1" runat="server" Text="Title"></asp:Label></label>
			    <asp:TextBox runat="server" ID="txtTitle"></asp:TextBox>
			    <label class="full"><asp:Label ID="Label2" runat="server" Text="Link"></asp:Label></label>
			    <asp:TextBox runat="server" ID="txtLink"></asp:TextBox>
			    <label class="full"><asp:Label ID="Label3" runat="server" Text="Language"></asp:Label></label>
			    <asp:DropDownList runat="server" ID="ddlLanguage">
			        <asp:ListItem Text="en-us"></asp:ListItem>
			    </asp:DropDownList>
			    <label class="full"><asp:Label ID="Label4" runat="server" Text="Copyright"></asp:Label></label>
			    <asp:Label runat="server" ID="lblCopyrightStart"></asp:Label> <asp:TextBox runat="server" ID="txtCopyright"></asp:TextBox>
			    <label class="full"><asp:Label ID="Label5" runat="server" Text="Subtitle"></asp:Label></label>
			    <asp:TextBox runat="server" ID="txtSubtitle"></asp:TextBox>
			    <label class="full"><asp:Label ID="Label6" runat="server" Text="Author"></asp:Label></label>
			    <asp:TextBox runat="server" ID="txtAuthor"></asp:TextBox>
			    <label class="full"><asp:Label ID="Label7" runat="server" Text="Summary"></asp:Label></label>
			    <asp:TextBox runat="server" ID="txtSummary"></asp:TextBox>
			    <label class="full"><asp:Label ID="Label8" runat="server" Text="Description"></asp:Label></label>
			    <asp:TextBox runat="server" ID="txtDescription"></asp:TextBox>
			    <label class="full"><asp:Label ID="Label11" runat="server" Text="Owner Name"></asp:Label></label>
			    <asp:TextBox runat="server" ID="txtOwnerName"></asp:TextBox>
			    <label class="full"><asp:Label ID="Label10" runat="server" Text="Owner Email"></asp:Label></label>
			    <asp:TextBox runat="server" ID="txtOwnerEmail"></asp:TextBox>

                <hr style="margin-top: 17px;"/>
                <label class="full" style="color: #848484"><asp:Label ID="Label14" runat="server" Text="RSS Feed Settings"></asp:Label></label>

                <label class="full"><asp:Label ID="Label12" runat="server" Text="Max Sermons in Podcast"></asp:Label></label>
                <asp:TextBox runat="server" ID="txtMaxSermons"></asp:TextBox>

                <label class="full"><asp:Label ID="Label13" runat="server" Text="Date Range"></asp:Label></label>
                <asp:DropDownList runat="server" ID="ddlDateRange">
                    <asp:ListItem Value="None" Text="--Choose Range--"></asp:ListItem>
			        <asp:ListItem Value="Month" Text="Last Month"></asp:ListItem>
                    <asp:ListItem Value="ThreeMonth" Text="Last 3 Months"></asp:ListItem>
                    <asp:ListItem Value="SixMonth" Text="Last 6 Months"></asp:ListItem>
                    <asp:ListItem Value="Year" Text="Last Year"></asp:ListItem>
			    </asp:DropDownList>
			</div>
		</div>
	</div>
	<div class="rightColumnWrapper" style="width:710px">
		<div class="rightColumn">
			<div class="toolbarContainer">
				<div class="toolbar clearfix">
					<span class="sectionTitle">
						<span>Customize your podcast</span>
					</span>
					<div class="rightButtons clearfix">
						<asp:LinkButton runat="server" ID="btnUpdate" CssClass="bxButton2 green" OnClick="btnUpdate_Click" Text="Save Changes" />
					</div>
				</div>
			</div>
			<div class="tableContainer padding20">
				<div class="copy">
					<h1>Setting up a new Podcast</h1>
					<ol>
						<li>Select artwork for your podcast that is easy to recognize when scaled down to 50x50 pixels. We recommend a 600 x 600 pixel JPG for cover art. The image associated with your podcast may require additional time to appear, because images are edge-cached by iTunes and must propagate across the caching servers.</li>
						<li>Leave Podcast ID blank for now.</li>
						<li>Fill in all additional fields.</li>
						<li>Save data.</li>
					</ol>
					<h1>Testing Your Feed.</h1>
					<p>Once you have finished setting up your new podcast, you should test your feed to see if it works with iTunes.</p>
					<ol>
						<li>Launch iTunes.</li>
						<li>In the Advanced menu, select Subscribe to Podcast.</li>
						<li>Enter the following url in the text box and click OK.<br /><%= backstage.Classes.UrlHelper.FullDomain + "/backstageIncludes/BackstageSermons/modules/Podcast" %></li>
					</ol>
					<p>iTunes displays your Podcast playlist, which shows all of the podcasts to which you have subscribed. Next to the new podcast subscription, you should see an orange circle, which indicates that iTunes is downloading your most recent episode. When the orange circle disappears, you should be able to see your podcast title, a list of all the episodes referenced in your feed, and a check next to the most recent episode, indicating that it has been successfully downloaded. Double-click on the episode to play it in iTunes. If you can successfully play the episode, then your feed is working and you can submit your podcast to iTunes.</p>
					<p>If the orange circle is replaced by an exclamation point (!) in a black circle, iTunes encountered a problem with your feed or episode. You should troubleshoot your episode and feed before submitting it. Please do not submit your feed until you can successfully subscribe using the Advanced menu.</p>
					<h1>Submitting Your Podcast to the iTunes Store.</h1>
					<p>If your site is LIVE, is no longer under a staging domain, and you can successfully subscribe to your feed using the Advanced menu in iTunes, you’re ready to submit your feed:</p>
					<ol>
						<li>Launch iTunes.</li>
						<li>In the left navigation column, click on iTunes Store to open the store.</li>
						<li>Once the store loads, click on Podcasts along the top navigation bar to go to the Podcasts page.</li>
						<li>In the right column of the Podcasts page, click on the Submit a Podcast link.</li>
						<li>Follow the instructions on the <a style="font-weight: bold; text-decoration: underline;" href="https://phobos.apple.com/WebObjects/MZFinance.woa/wa/publishPodcast" class="external">Submit a Podcast</a> page.</li>
					</ol>
					<p>Note that to submit a podcast you will need a valid iTunes account, and you will need to be logged into iTunes. If you are not logged in, iTunes will prompt you to do so before accepting your submission. By requiring you to log in, iTunes increases the likelihood of valid contact information for each submission. Your credit card will not be charged for submission of a podcast.</p>
					<p>You should see a summary page immediately after you submit your feed URL.</p>
				</div>
			</div>
		</div>
	</div>
</div>
</asp:Content>