﻿<%@ Page Language="C#" MasterPageFile="~/backstageIncludes/BackstageSermons/SermonsNew.Master" AutoEventWireup="True" CodeBehind="EditSermon.aspx.cs" Inherits="BackstageSermons.modules.Sermons.EditSermon" EnableEventValidation="false" MaintainScrollPositionOnPostback="true" ValidateRequest="false" %>
<%@ Register TagPrefix="ctrl" TagName="SEOEditor" Src="~/backstage/controls/SEOEditor/SEOEditor.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <title>Backstage Sermons :: Sermon Manager</title>
</asp:Content>

<asp:Content ID="contentStyles" ContentPlaceHolderID="stylesheets" runat="server">
    <link rel="stylesheet" type="text/css" href="css/SermonStyles.css" />
    <style type="text/css">
        .leftColumn .chosen-container {
            width: 215px !important;
        }

        h1 img.icon {
            display: block;
            float: left;
            margin-right: 8px;
            }
            .seoField
            {
                width: 246px;
            }

        .panelLeft {
            padding: 10px 15px 20px 15px;
            background-color: rgba(255,255,255,0.4);
            background-color: #e9eef2 \9;
            }

        .slideDown h1 {
            cursor: pointer;
            padding: 12px 15px 12px 15px;
    
            -webkit-transition: background-color 100ms;
            -moz-transition: background-color 100ms;
            transition: background-color 100ms;
            }

        .slideDown h1:hover {
            background-color: rgba(255,255,255,0.4);
    
            }

        .slideDown h1.active {
            color: #ffffff;
            text-shadow: 0 1px 0 #4f6c80;
            border-bottom: 1px solid #50738b;
            background: #84adc8;
            }

        .slideDown input[type="text"] {
            background-image: none;
            }

        .leftColumn input[type="checkbox"] {
            margin-left: 0px;
            }

        .hr {
            height: 1px;
            background: #c0c0c0 !important;
            margin: 15px 0;
            }

        .dragPanel {
	        background: white;
	        -webkit-box-shadow: 0 5px 17px rgba(0,0,0,0.35);
	           -moz-box-shadow: 0 5px 17px rgba(0,0,0,0.35);
	                box-shadow: 0 5px 17px rgba(0,0,0,0.35);
	        border: 1px solid #666666 \9;
	        }

        .regField {
	        width: 92%;
        }

    </style>
</asp:Content>

<asp:Content ID="contentScripts" ContentPlaceHolderID="scripts" runat="server">
    <script type="text/javascript" src="js/EditSermon.js"></script>
    <script type="text/javascript" src="js/topicselector.js"></script>

    <script type="text/javascript">
        var visibleImage = '<%= visibleImage%>';
        var invisibleImage = '<%= invisibleImage %>';
        var visibleLabel = '<%= visibleLabel %>';
        var invisibleLabel = '<%= invisibleLabel %>';
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">

<div style="display:none;" id="upButtons" role="status" aria-hidden="true">
		
    <div class="processing">
        <img alt="Processing" src="/backstage/images/NewButtons/ajax-loader.gif">
    </div>
                                    
</div>
<asp:Label runat="server" ID="lblResult" style="color: #990000;"></asp:Label>
<div class="sectionContent clearfix">
	<div class="leftColumnWrapper" style="width:25%">
		<div class="leftColumn">
			<!--Digital Sermons-->
			<asp:Panel runat="server" ID="pnlDigital" CssClass="slideDown">
				<h1><span class="bxIcon bxIcon-File"></span>Files</h1>
				<asp:Panel runat="server" ID="pnlDigitalFields" class="goHidden panelLeft" style="display:none">
					<label class="full">Where is your audio file?</label>
					<asp:DropDownList runat="server" ID="ddlAudioLocation" CssClass="ddlAudioLocation chosen-select" style="width: 213px" onchange="audioLocationChange(this)">
						<asp:ListItem Value="Bx" Text="Backstage"></asp:ListItem>
						<asp:ListItem Value="External" Text="Remote server"></asp:ListItem>
						<asp:ListItem Value="SermonAudio" Text="SermonAudio"></asp:ListItem>
					</asp:DropDownList>
					
					<asp:Panel runat="server" ID="pnlBxAudioFile" CssClass="pnlBxAudioFile" style="display: none;">
						<label class="full">Choose an Audio File</label>
						<asp:TextBox runat="server" ID="txtBxAudioFile" CssClass="regField" onclick="$(this).next().click();" style="width: 100%;"></asp:TextBox>
						<asp:LinkButton runat="server" ID="lbSelectAudioFile" CssClass="bxButton2" OnClientClick="openAudioPicker(this)" style="display: none;">Select File</asp:LinkButton>
					</asp:Panel>
					
					<asp:Panel runat="server" ID="pnlExternalAudioLink" CssClass="pnlExternalAudioLink" style="display: none;">
						<label class="full">External Audio Location</label>
						<asp:TextBox runat="server" ID="txtExternalAudioLink" CssClass="regField" style="width: 100%;"></asp:TextBox>
					</asp:Panel>
					
					<asp:Panel runat="server" ID="pnlSermonAudioID" CssClass="pnlSermonAudioID" style="display: none;">
						<label class="full">Sermon Audio ID</label>
						<asp:TextBox runat="server" ID="txtSermonAudioID" CssClass="regField" style="width: 100%;"></asp:TextBox>
					</asp:Panel>
					
					<label class="full">Where is your video file?</label>
					<asp:DropDownList runat="server" ID="ddlVideoLocation" CssClass="ddlVideoLocation chosen-select" style="width: 213px" onchange="videoLocationChange(this)">
						<asp:ListItem Value="Bx" Text="Backstage"></asp:ListItem>
						<asp:ListItem Value="External" Text="Remote server"></asp:ListItem>
						<asp:ListItem Value="YouTube" Text="YouTube"></asp:ListItem>
					</asp:DropDownList>
					
					<asp:Panel runat="server" ID="pnlBxVideoFile" CssClass="pnlBxVideoFile" style="display: none;">
						<label class="full">Choose a Video File</label>
						<asp:TextBox runat="server" ID="txtBxVideoFile" CssClass="regField" onclick="$(this).next().click();" style="width: 100%;"></asp:TextBox>
						<asp:LinkButton runat="server" ID="lbSelectVideoFile" CssClass="bxButton2" OnClientClick="openVideoPicker(this)" style="display: none;">Select File</asp:LinkButton>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlExternalVideoLink" CssClass="pnlExternalVideoLink" style="display: none;">
						<label class="full">External Video Location</label>
						<asp:TextBox runat="server" ID="txtExternalVideoLink" CssClass="regField" style="width: 100%;"></asp:TextBox>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlYouTubeID" CssClass="pnlYouTubeID" style="display: none;">
						<label class="full">YouTube ID or Link</label>
					    <asp:TextBox runat="server" ID="txtYouTubeID" CssClass="regField" style="width: 100%;"></asp:TextBox>
                        <cms:YoutubeValidator runat="server" ID="rfvYouTubeID" EnableClientScript="false" ControlToValidate="txtYouTubeID" Text="Must insert a valid ID/URL"></cms:YoutubeValidator>
					</asp:Panel>
					<label class="full">Bulletin/Outline:</label>
					<asp:TextBox runat="server" ID="txtOutline" CssClass="regField" onclick="$(this).next().click();" style="width: 100%;"></asp:TextBox>
					<asp:LinkButton runat="server" ID="lbSelectFile" CssClass="bxButton2" OnClientClick="openFilePicker(this)" style="display: none;">Select File</asp:LinkButton>
				</asp:Panel>
			</asp:Panel>
			<!--Series-->
			<asp:Panel runat="server" ID="pnlSeries" CssClass="slideDown">
				<asp:UpdatePanel runat="server" ID="updateSeries" UpdateMode="Conditional" OnLoad="updateSeries_Load">
					<ContentTemplate>
						<h1><span class="bxIcon bxIcon-Grid"></span>Series</h1>
						<div class="goHidden panelLeft" style="display:none">
							<div style="margin-left:-20px;padding: 0 0 16px 0;">
								<asp:Panel runat="server" ID="pnlSeriesHTML"></asp:Panel>
							</div>
							<div id="addSeries" class="bxButton2 green" style="float:none;width:100%;padding-left:0;padding-right:0">+ Add New Series</div>
							<div id="seriesTitle" style="display:none">
								<label class="full">Series Title</label>
								<asp:TextBox runat="server" ID="txtNewSeries" style="width: 232px;"></asp:TextBox>
								<label class="full">Parent Series</label>
								<asp:DropDownList runat="server" ID="ddlSeries" style="width: 240px;"></asp:DropDownList>
								<div class="clearfix" style="margin-top:10px">
									<asp:Button runat="server" CssClass="bxButton2 green" ID="btnNewSeries" OnClick="btnNewSeries_Click" Text="Create Series" />
								</div>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>  
			</asp:Panel>
			<!--Topics-->
			<asp:Panel runat="server" ID="pnlVariants" CssClass="slideDown">
				<h1><span class="bxIcon bxIcon-Tag"></span>Topics/Tags</h1>
				<div class="goHidden panelLeft" style="display:none">
					<asp:UpdatePanel runat="server" ID="updateTopics">
						<ContentTemplate>
							<div id="selectedTopics">
								<cms:SmartRepeater runat="server" ID="rptSelectedTopics">
									<ItemTemplate>
										<asp:Panel ID="Panel1" CssClass="tags" runat="server"><asp:LinkButton runat="server" ID="btnDeleteSermonTopic" OnClick="btnDeleteSermonTopic_Click" OnClientClick="notifyRemove(this)" CommandArgument='<%# Eval("TopicID") %>'><%# Server.UrlDecode(Eval("Title").ToString())%> <span></span></asp:LinkButton></asp:Panel>
									</ItemTemplate>
								</cms:SmartRepeater>
							</div>
							<div style="margin: 8px 0 8px 0; clear: both;">
							<a class="tooltipTrigger">Suggestions</a>
							<div class="divTooltip">Start typing a topic in the textbox below. Select an item from the auto-complete menu to choose a pre-existing topic and hit "Add New Topic" to create a new one.</div>
							</div>
							<div style="position: relative;" id="topicSearch">
								<div class="clearfix">
									<asp:TextBox runat="server" ID="txtTopic" style="float:left;margin-right:8px;margin-bottom: 5px; width: 100%;" onkeydown="return topickeydown(event)" onkeyup="topickeyup(this, event)" autocomplete="off"></asp:TextBox>
									<asp:Button runat="server" ID="btnNewTopic" style="margin-top:2px;margin-right: 0px;" Text="+ Add New Topic" OnClick="btnNewTopic_Click" CssClass="bxButton2 green btnNewTopic" />
									<div id="topicMenu" style="display: none; width: 136px;">
									    <cms:SmartRepeater runat="server" ID="rptTopics">
										    <ItemTemplate>
										        <div style="display: none;">
													<asp:LinkButton runat="server" style="display:block;" ID="lbTopic" OnClick="lbTopic_Click" CssClass="aTopic" CommandName='<%# Eval("Title") %>' CommandArgument='<%# Eval("TopicID") %>' onmouseover="topichover(this)"><%# Eval("Title") %></asp:LinkButton>
												</div>
										    </ItemTemplate>
									    </cms:SmartRepeater>
								    </div>
								</div>
								
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>
				<script type="text/javascript">
				    var xPos, yPos;
				    var prm = Sys.WebForms.PageRequestManager.getInstance();
				    prm.add_beginRequest(BeginRequestHandler);
				    prm.add_endRequest(EndRequestHandler);
				    function BeginRequestHandler(sender, args) {
				        xPos = $(window).scrollLeft();
				        yPos = $(window).scrollTop();
				    }
				    function EndRequestHandler(sender, args) {
				        setTimeout("window.scrollTo(xPos,yPos)", 0);
				        tooltipInit();
				    }
				</script>
			</asp:Panel>
			<!--SEO-->
            <asp:Panel runat="server" ID="pnlSEO" CssClass="slideDown">
    	        <h1><span class="bxIcon bxIcon-Search"></span>Optimize for SEO</h1>
    	        <div class="goHidden panelLeft" style="display:none">
    		        <p style="margin-bottom:10px">Include custom page titles and descriptions for this page.</p>
                    <ctrl:SEOEditor runat="server" ID="ctrlSEOEditor" HideKeywords="true" HideUrlRewrite="true" />
		        </div>
	        </asp:Panel>
		</div>
	</div>
	<div class="rightColumnWrapper" style="width:75%">
		<div class="rightColumn">
			<div class="toolbarContainer">
				<div class="toolbar clearfix">
					<span class="sectionTitle">
                        <span runat="server" id="h1Title" class="sermon-edit"></span>
                        <p runat="server" id="spanTitle" class="sermon-title"></p>
					</span>
					<div class="rightButtons clearfix">
						<asp:LinkButton runat="server" ID="btnUpdate1" OnClick="btnUpdate_Click" Visible="false" Text="Save and Close" CssClass="bxButton2 green" />
            			<asp:LinkButton runat="server" ID="btnCreate1" OnClick="btnCreate_Click" Visible="false" Text="Save and Close" CssClass="bxButton2 green" />
			            <asp:LinkButton runat="server" ID="btnCancel" OnClick="btnCancel_Click" CssClass="bxButton2" Text="Cancel" />
					</div>
				</div>
			</div>
			<div class="tableContainer">
				<div class="padding20">
					<h1 style="margin-top:0">Sermon Details</h1>
					<div class="clearfix">
						<div id="divVisibility" class="divVisibility" style="float:left">
							<asp:Image runat="server" ID="imgVisibility" CssClass="imgVisibility" style="border-width:0px;float:left;display:block" /> <asp:Label runat="server" ID="lblVisibility" CssClass="lblVisibility"></asp:Label>
							<asp:TextBox runat="server" ID="txtVisibility" CssClass="txtVisibility" style="display: none;"></asp:TextBox>
						</div>
						<asp:PlaceHolder runat="server" ID="phFeatured">
							<asp:CheckBox runat="server" ID="cbFeatured" Text="Featured?" style="margin: 0 0 0 10px;" />
						</asp:PlaceHolder>
					</div>

                    <div class="grid">
                        <div class="col-8-12">
                            <label class="full"><asp:Label runat="server" ID="lblTitle">Title</asp:Label></label>
					        <asp:TextBox runat="server" ID="txtTitle"></asp:TextBox>
                        </div>
                        <div class="col-4-12">
                            <asp:UpdatePanel ID="updatePnlEditSpeaker" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="True">
						        <ContentTemplate>
							        <asp:Panel runat="server" ID="pnlAddSpeaker" Visible="false" CssClass="dragPanel" style="z-index: 910; position: fixed; top: 50%;left:50%; margin: -226px 0 0 -269px;">
								        <div style="width: 476px; margin: 0 20px 20px;">
									        <table>
										        <tr>
											        <td>
                                                        <bx:ImagePicker runat="server" ID="ipSpeaker" ChildOfUpdatePanel="true"></bx:ImagePicker>
											        </td>
											        <td style="padding: 16px 0 0 20px; vertical-align: top;">
                                                        Prefix:<br />
                                                        <asp:DropDownList runat="server" ID="ddlAddSpeakerNamePrefix">
                                                            <asp:ListItem Value="" Text="--"></asp:ListItem>
                                                            <asp:ListItem Value="Dr" Text="Dr."></asp:ListItem>
                                                            <asp:ListItem Value="Mr" Text="Mr."></asp:ListItem>
                                                            <asp:ListItem Value="Mrs" Text="Mrs."></asp:ListItem>
                                                            <asp:ListItem Value="Pastor" Text="Pastor"></asp:ListItem>
                                                            <asp:ListItem Value="Rev" Text="Rev."></asp:ListItem>
                                                        </asp:DropDownList><br />
												        First Name:<br />
												        <asp:TextBox runat="server" ID="txtAddSpeakerFirstName" style="width: 202px;"></asp:TextBox><br />
                                                        Last Name:<br />
												        <asp:TextBox runat="server" ID="txtAddSpeakerLastName" style="width: 202px;"></asp:TextBox>
											        </td>
										        </tr>
									        </table>

									        <div style="margin: 16px 0 0 0; width: 432px;">
										        Bio:<br />
                                                <cms:CKEditor runat="server" ID="ckEditAddSpeakerBio" Toolbar="Basic" Height="200"></cms:CKEditor>
										        <div style="margin: 10px 0 0 0;">
                                                    <asp:LinkButton runat="server" ID="ImageButton1" CssClass="bxButton2" Text="Update" OnClick="btnAddSpeaker_Click" />
						                            <asp:LinkButton runat="server" ID="btnCancelEdit" CssClass="bxButton2" Text="Cancel" OnClick="btnCancelAddSpeaker_Click" />
										        </div>
									        </div>
									        <div class="clearBoth"></div>
								        </div>
							        </asp:Panel>
							        <label class="full"><asp:Label runat="server" ID="lblSpeaker">Speaker</asp:Label></label>
							        <asp:DropDownList runat="server" ID="ddlSpeaker" CssClass="chosen-select" style="width: 210px" DataTextField="FullDisplayName" DataValueField="SpeakerID"></asp:DropDownList>
							        <asp:ImageButton runat="server" ID="btnShowAddSpeaker" OnClick="btnShowAddSpeaker_Click" style="margin: 0px; padding: 0px;" ImageUrl="../../images/addNew.jpg" />
						        </ContentTemplate>
					        </asp:UpdatePanel>
                        </div>
                    </div>
					<div class="grid">
                        <div class="col-8-12">
                            <label class="full"><asp:Label runat="server" ID="lblTime">Service</asp:Label></label>
					        <asp:TextBox runat="server" ID="txtTime" CssClass="regField"></asp:TextBox>
                        </div>
                        <div class="col-4-12" style="height: 70px;">
                            <label class="full"><asp:Label runat="server" ID="lblDate">Date</asp:Label></label>
					        <asp:TextBox runat="server" ID="txtDate" CssClass="icon-left"></asp:TextBox>
                            <button id="dateTrigger" class="dateTrigger" style="vertical-align:middle;"></button>
					        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDate" PopupButtonID="dateTrigger"></asp:CalendarExtender>
                        </div>
                    </div>
                    <div class="grid">
                        <div class="col-6-12">
                            <label class="full"><asp:Label runat="server" ID="lblPassageBook">Book</asp:Label></label>
                            <asp:DropDownList runat="server" ID="ddlPassageBook" CssClass="chosen-select" style="width: 327px" DataTextField="Name" DataValueField="Name" AppendDataBoundItems="true">
								<asp:ListItem Value="" Text="--"></asp:ListItem>
							</asp:DropDownList>
                        </div>
                        <div class="col-3-12">
                            <label class="full"><asp:Label runat="server" ID="lblPassageChapter">Chapter</asp:Label></label>
                            <asp:TextBox runat="server" ID="txtPassageChapter"></asp:TextBox>
                        </div>
                        <div class="col-3-12">
                            <label class="full"><asp:Label runat="server" ID="lblPassageVerse">Verse</asp:Label></label>
                            <asp:TextBox runat="server" ID="txtPassageVerse"></asp:TextBox>
                        </div>
                    </div>
                    <div class="grid" style="margin-top: 10px;">
                        <div class="col-1-1">
                            <label class="full"><asp:Label runat="server" ID="Label1">Passage Copy</asp:Label></label>
                            <a class="bxButton2" style="margin-left: 0px; float: right; margin-bottom: 7px; margin-top: -24px;" onclick="getScripturePassage('<%= BackstageSermons.Settings.Admin.BibleVersion.ToString() %>', '<%= ckPassageCopy.ClientID %>', $('#<%= ddlPassageBook.ClientID %>').val(), $('#<%= txtPassageChapter.ClientID %>').val(), $('#<%= txtPassageVerse.ClientID %>').val())">Auto Fill (<%= BackstageSermons.Settings.Admin.BibleVersion.ToString() %>)</a>
                            <cms:CKEditor runat="server" ID="ckPassageCopy" Toolbar="Basic"></cms:CKEditor>
                        </div>
                    </div>
                    <div class="grid" style="margin-top: 15px;">
                        <div class="col-1-1">
                            <bx:ImagePicker runat="server" ID="ipTitleGraphic" Title="Image:"></bx:ImagePicker>
                        </div>
                    </div>
                    
					<h1>Sermon Notes & Additional Information</h1>
					<cms:CKEditor ID="ckSermonNotes" Toolbar="Basic" runat="server" height="316"></cms:CKEditor>


                    <asp:Panel runat="server" ID="pnlCustomFields">
		                <asp:Repeater runat="server" ID="rptCustomFields" OnItemDataBound="rptCustomFields_ItemDataBound">
			                <ItemTemplate>
					            <h1><%# Eval("Title") %></h1>
					            <p><%# Eval("Description") %></p>
					            <div class="clearBoth"></div>
					            <cms:CKEditor ID="ckCustom" Visible="false" runat="server" height="400" Text='<%# Eval("Value") %>'></cms:CKEditor>
					            <asp:TextBox runat="server" ID="txtCustom" Visible="false" CssClass="regField" Text='<%# Eval("Value") %>' />
					            <asp:RadioButtonList runat="server" ID="rblBool" Visible="false" RepeatDirection="Horizontal" SelectedValue='<%# Eval("Value").ToString() == "False" ? "False" : "True" %>'>
					                <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
					                <asp:ListItem Text="No" Value="False"></asp:ListItem>
					            </asp:RadioButtonList>
					            <bx:FilePicker runat="server" ID="fpCustom" Visible="false" FileID='<%# Eval("Value") %>'></bx:FilePicker>
					            <bx:ImagePicker runat="server" ID="ipCustom" Visible="false" ImageID='<%# Eval("Value") %>'></bx:ImagePicker>
                                <bx:MultipleImagePicker runat="server" ID="mipCustom" Visible="false" ImageIDs='<%# StringEditor.StringToList(Eval("Value").ToString(), ",") %>'></bx:MultipleImagePicker>
					            <sermons:CustomField runat="server" ID="cfCustom" Visible="false" Value='<%# Eval("Value") %>' Src='<%# Eval("Location") %>'></sermons:CustomField>
			                </ItemTemplate>
		                </asp:Repeater>
		            </asp:Panel>
				</div>
			</div>
		</div>
	</div>
</div>
</asp:Content>
