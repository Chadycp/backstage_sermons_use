﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace BackstageSermons
{
    public class SermonCollectionML : List<SermonML>
    {
        public SermonML FindSermonByID(int SermonID)
        {
            SermonML sermon = null;
            foreach (SermonML s in this)
            {
                if (s.SermonID == SermonID)
                {
                    sermon = s;
                    break;
                }
            }

            return sermon;
        }
    }
}
