﻿using System;
using System.Web.UI;
using backstage.Classes;
using BackstageSermons.Settings;

namespace BackstageSermons
{
    public partial class SermonsMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                SetDefaults();
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "init", "if(typeof init != 'undefined') init();", true);
        }

        protected void SetDefaults()
        {
            SetVisibility();
            string url = Request.Url.AbsolutePath.ToLower();
            if (url.StartsWith("/backstageincludes/backstagesermons/modules/sermons"))
                WebControlHelper.AddCssClass(hlNavSermons, "active");
            if (url.StartsWith("/backstageincludes/backstagesermons/modules/speakers"))
                WebControlHelper.AddCssClass(hlNavSpeakers, "active");
            if (url.StartsWith("/backstageincludes/backstagesermons/modules/topics"))
                WebControlHelper.AddCssClass(hlNavTopics, "active");
            if (url.StartsWith("/backstageincludes/backstagesermons/modules/podcast/settings.aspx"))
                WebControlHelper.AddCssClass(hlNavPodcast, "active");
            if (url.StartsWith("/backstageincludes/backstagesermons/modules/livestream"))
                WebControlHelper.AddCssClass(hlNavLivestream, "active");
            if (url.StartsWith("/backstageincludes/backstagesermons/modules/reports"))
                WebControlHelper.AddCssClass(hlNavReports, "active");
            if (url.StartsWith("/backstageincludes/backstagesermons/modules/settings"))
                WebControlHelper.AddCssClass(hlNavSettings, "active");
        }

        protected void SetVisibility()
        {
            phLivestream.Visible = Admin.EnableLivestream;
        }
    }
}
