﻿using System;
using System.Text;

namespace BackstageSermons
{
    public class SeriesML
    {
        public int SeriesID { get; set; }
        public string Title { get; set; }
        public bool Active { get; set; }
        public int ParentSeriesID { get; set; }
        public decimal UnitPrice { get; set; }
        public bool IsOpen { get; set; }
        public backstage.security.ImageCollectionML Images { get; set; }
        public int ImageID
        {
            get
            {
                if (Images != null && Images.Count > 0)
                    return Images[0].ID;
                return -1;
            }
        }
        public string Description { get; set; }
        public bool Featured { get; set; }
        public int SermonCount { get; set; }
        public SeriesCollectionML ChildSeries { get; set; }
        public SermonCollectionML Sermons { get; set; }

        public int PodcastImageID { get; set; }
        public string PodcastID { get; set; }
        public string PodcastTitle { get; set; }
        public string PodcastSubtitle { get; set; }
        public string PodcastDescription { get; set; }

        public string SEOTitle { get; set; }
        public string SEODescription { get; set; }

        private SeriesFieldValueCollectionML _FieldValues = null;
        public SeriesFieldValueCollectionML FieldValues
        {
            get
            {
                if (_FieldValues == null)
                    _FieldValues = SermonsBLL.Instance.LoadAllSeriesFieldValuesBySeriesID(SeriesID);
                return _FieldValues;
            }
            set { _FieldValues = value; }
        }

        public string FieldValue(int fieldID)
        {
            SeriesFieldValueML val = FieldValues.FindBySeriesIDAndFieldID(SeriesID, fieldID);

            if (val != null)
                return val.Value;
            return "";
        }
    }
}
