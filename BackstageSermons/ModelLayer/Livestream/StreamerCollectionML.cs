﻿using System.Collections.Generic;

namespace BackstageSermons.Livestream
{
    public class StreamerCollectionML : List<StreamerML>
    {
        public StreamerML GetStreamerBySessionID(string sessionID)
        {
            foreach (StreamerML s in this)
            {
                if (s.SessionID == sessionID)
                    return s;
            }
            return null;
        }
    }
}
