﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeriesFinder.ascx.cs" Inherits="BackstageSermons.components.SeriesFinder.SeriesFinder" %>

<script type="text/javascript">
    function seriesHover(tr) {
        $tr = $(tr);
        $tr.addClass("trFinderHover");
        $arrow = $tr.find(".childSeriesArrow");
        $arrow.attr("src", "/backstage/modules/CatalogManager/components/CategoryFinder/images/" + arrowDirection($arrow) + "ArrowHover.png");
        $tr.find(".divSeriesFinderActionBtns").css("visibility", "");
    }
    function seriesHoverOut(tr) {
        $tr = $(tr);
        $tr.removeClass("trFinderHover");
        $arrow = $tr.find(".childSeriesArrow");
        $arrow.attr("src", "/backstage/modules/CatalogManager/components/CategoryFinder/images/" + arrowDirection($arrow) + "Arrow.png");
        $tr.find(".divSeriesFinderActionBtns").css("visibility", "hidden");
    }
    function arrowDirection(img) {
        var direction = "right";
        if (img.attr("is_open") == "True")
            direction = "down";
        return direction;
    }
    var scrollEnabled = false;
    function <%= this.ClientID %>_initJquery() {
    $(document).ready(function () {
        scrollEnabled = false;
        $hf = $("#<%= hfScrollPosition.ClientID %>");
        $pnlSeries = $("#<%= pnlSeries.ClientID %>");
        $finder = $("#<%= pnlSeriesFinder.ClientID %>");

        $pnlSeries.scroll(function (e) {
            if (scrollEnabled)
                $hf.val(e.currentTarget.scrollTop);
        });
        $("#<%= TriggerClientID %>").click(function () {
            $("#<%= pnlSeriesFinder.ClientID %>").css("display", "block");
            scrollToPos();
        });
        scrollToPos();
    });
}

function scrollToPos() {
    var pnlSeries = document.getElementById("<%= pnlSeries.ClientID %>");

    if ($hf.val() != "") {
        pnlSeries.scrollTop = $hf.val();
        if ($finder.css("display") != "none")
            scrollEnabled = true;
    }
    else {
        var curSeries = $("tr.curSeries");
        if (curSeries.length > 0) {
            var disp = $finder.css("display");
            $finder.css("display", "block");
            var curSeriesOffset = curSeries.offset();
            var pnlSeriesOffset = $pnlSeries.offset();
            var ofs = (curSeriesOffset.top - pnlSeriesOffset.top);
            $hf.val(ofs);
            pnlSeries.scrollTop = ofs;
            if (disp == "none")
                $finder.css("display", "none");
            else
                scrollEnabled = true;
        }
    }
}

var scrollPos;
var moveID;
function showMoveOpts(img, id) {
    moveID = id;
    $('#<%= hfFolderToMove.ClientID %>').val(id);
    $moveOpts = $("#divMoveWhere");
    if (!$moveOpts.is(":visible")) {
        $img = $(img);
        var parentOffset = $moveOpts.parent().offset();
        var imgOffset = $img.offset();
        var left = imgOffset.left - parentOffset.left;
        var top = imgOffset.top - parentOffset.top + $img.outerHeight();

        $moveOpts.css({ "left": left.toString() + "px", "top": top.toString() + "px", display: "block", opacity: "0" });
        var w = $moveOpts.width();
        var h = $moveOpts.height();
        $moveOpts.css({ width: "0px", height: "0px" });
        $moveOpts.animate({ left: (left - w).toString() + "px", top: (top - h).toString() + "px", width: w.toString() + "px", height: h.toString() + "px", opacity: "1" }, 120, function () {
            $moveOpts.css({ width: "", height: "" });
        });
        $series = $("#ctrlSeriesFinder_pnlSeries");
        scrollPos = $series.scrollTop();
        $series.scroll(function () {
            $(this).scrollTop(scrollPos);
        });
    }
}

function closeMoveOpts() {
    $series = $("#ctrlSeriesFinder_pnlSeries");
    $series.unbind();
    $moveOpts = $("#divMoveWhere");
    $moveOpts.hide();
}

var curIndent = 0;
function chooseMoveOpt(type) {
    $('.btnMoveFolder' + type).show();
    $('.divSeriesFinderActionBtns').children().hide();
    $cancel = $('#moveCancelBtn' + moveID);
    $cancel.show().siblings().hide();
    $folderTr = $cancel.parent().parent();
    curIndent = GetIndent($folderTr);

    $prevFolder = $folderTr.prev();
    if ($prevFolder.length > 0) {
        //if parent folder
        var ind = GetIndent($prevFolder);
        if (ind < curIndent)
            HideAllMoveBtns($prevFolder);
    }

    $nextFolder = $folderTr.next();
    if ($nextFolder.length > 0)
        CheckMoveInto($nextFolder);
    closeMoveOpts();
}

function GetIndent(folderTr) {
    return parseInt(folderTr.children("td:first").children("a:first").children(".childSeriesArrow").css("margin-left").replace("px", ""));
}

function CheckMoveInto(folderTr) {
    var ind = GetIndent(folderTr);
    if (ind > curIndent) {
        HideAllMoveBtns(folderTr);
        $nextFolder = folderTr.next();
        if ($nextFolder.length > 0)
            CheckMoveInto($nextFolder);
    }
}

function moveHover(img, type) {
    $tr = $(img).parent().parent();
    if (type == "After")
        $tr.children("td").css("borderBottom", "1px solid #990000");
    else if (type == "Before")
        $tr.children("td").css("borderTop", "1px solid #990000");
}
function moveHoverOut(img) {
    $tr = $(img).parent().parent();
    $tr.children("td").css({ borderBottom: "", borderTop: "" });
}

function cancelMoveFolder() {
    $('.tdSeriesFinderMoveBtns').children().hide();
    $(".divSeriesFinderActionBtns").children().show();
}

function HideAllMoveBtns(folderTr) {
    folderTr.children(".tdSeriesFinderMoveBtns").children().hide();
}

Sys.Application.add_load(<%= this.ClientID %>_FirejQuery);
function <%= this.ClientID %>_FirejQuery() {
        <%= this.ClientID %>_initJquery();
}
</script>

<asp:HiddenField runat="server" ID="hfScrollPosition" />
<asp:UpdatePanel runat="server" ID="updateSeriesFinder" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:HiddenField runat="server" ID="hfFolderToMove" />
        <style type="text/css">
            .tblSeries{ margin: 0; }
            .tblSeries td{ padding: 2px 0; }
        </style>

        <asp:Panel runat="server" ID="pnlSeriesFinder" style="position: absolute; z-index: 700; left: 0px; display: none;">
            <asp:Panel runat="server" ID="pnlFinderArrow" CssClass="finderArrow"></asp:Panel>
            
            <table cellpadding="0" cellspacing="0" class="tblFinder">
                <tr>
                    <td style="background: url(/backstage/modules/CatalogManager/components/CategoryFinder/images/finderTopLeft.png) no-repeat;"><div style="width: 21px; height: 19px;"></div></td>
                    <td style="height: 19px; background: url(/backstage/modules/CatalogManager/components/CategoryFinder/images/finderTopMiddle.png) repeat-x;"></td>
                    <td style="background: url(/backstage/modules/CatalogManager/components/CategoryFinder/images/finderTopRight.png);"><div style="width: 21px; height: 19px;"></div></td>
                </tr>
                <tr>
                    <td style="width: 21px; background: url(/backstage/modules/CatalogManager/components/CategoryFinder/images/finderMiddleLeft.png) repeat-y;"></td>
                    <td style="background: #363636;">
                        <asp:Panel runat="server" ID="pnlMiddle">
                
                            <div style="height: 28px;">
                                <button runat="server" id="buttonAddFolder" class="bxButton" style="padding: 0 6px !important; height: 24px; line-height: 22px !important; float: left; margin: 0 4px 0 0;">Add Series</button>
                                <asp:Panel runat="server" ID="pnlCurrentSeries" Visible="false" style="float: left; width: 174px; overflow: hidden; margin: 5px 0 0 0;">
                                    <asp:Label runat="server" ID="lblCurrentSeries" ForeColor="White" Text="Select Main Level"></asp:Label>
                                    <asp:ImageButton runat="server" ID="btnUseSeries" Visible="false" CssClass="imageButton" ImageUrl="/backstage/images/useIcon.png" AlternateText="Use Series" OnClick="btnUseSeries_Click" style="margin: 0 0 2px 0; vertical-align: middle;" />
                                </asp:Panel>
                                
                                <%--
                                <div style="float: right; color: #FFF; cursor: pointer; margin: 4px 0 0 0;">
                                    <asp:LinkButton runat="server" ID="lbClose" OnClick="lbClose_Click" ForeColor="White">Close</asp:LinkButton>
                                </div>
                                --%>
                                <div style="float: right; color: #FFF; cursor: pointer; margin: 4px 0 0 0;" onclick="$('#<%= pnlSeriesFinder.ClientID %>').hide()">
                                    Close
                                </div>
                                
                            </div>
                            <div style="position: relative;">
                                <asp:UpdateProgress ID="updateProgressSeries" runat="server" AssociatedUpdatePanelID="updateSeriesFinder">
                                    <ProgressTemplate>
                                        <asp:Panel runat="server" ID="pnlProgress" CssClass="pnlProgress" style="position: absolute; background: url(/backstage/modules/ContactManager/images/whiteCover.png) repeat; width: 300px; z-index: 10;">
                                            <img src="/backstage/modules/PublishManager/images/loader.gif" style="position: absolute; top: 50%; left: 50%; margin: -8px 0 0 -8px;" alt="Progress Loader" />
                                        </asp:Panel>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <div id="divMoveWhere" style="display: none; background: black; padding: 2px; position: absolute; color: #FFF; z-index: 1; white-space: nowrap;">
                                    <div onmouseover="this.style.background = '#666';" onmouseout="this.style.background = '';" style="padding: 4px; cursor: pointer; white-space: nowrap;" onclick="chooseMoveOpt('Into');">Move into another folder</div>
                                    <div onmouseover="this.style.background = '#666';" onmouseout="this.style.background = '';" style="padding: 4px; cursor: pointer; white-space: nowrap;" onclick="chooseMoveOpt('Before');">Move before another folder</div>
                                    <div onmouseover="this.style.background = '#666';" onmouseout="this.style.background = '';" style="padding: 4px; cursor: pointer; white-space: nowrap;" onclick="chooseMoveOpt('After');">Move after another folder</div>
                                    <div onmouseover="this.style.background = '#666';" onmouseout="this.style.background = '';" style="padding: 4px; cursor: pointer; white-space: nowrap;" onclick="closeMoveOpts()">Close</div>
                                </div>
                                <asp:Panel runat="server" ID="pnlSeries" CssClass="pnlFinderSeries" style="min-height: 100px;">
                                    <table class="tblSeries" cellpadding="0" cellspacing="0">
                                        <asp:Repeater runat="server" ID="rptSeries">
                                            <ItemTemplate>
                                                <tr class="trSeries <%# Eval("ID").ToString() == SeriesID.ToString() ? "curSeries" : Container.ItemIndex % 2 == 0 ? "" : "trSeriesAlternate" %>" onmouseover="seriesHover(this)" onmouseout="seriesHoverOut(this)">
                                                    <td style="white-space: nowrap; padding-right: 10px; min-width: 200px;">
                                                        <asp:LinkButton runat="server" ID="lblSeriesArrow" ToolTip='View Child Series' CommandArgument='<%# Eval("ID") %>' OnClick="lblSeriesArrow_Click" CommandName='<%# Eval("Name") %>' OnClientClick='<%# (int)Eval("ChildCount") > 0 ? "" : "return false;" %>'>
                                                            <img is_open="<%# Eval("IsOpen") %>" class="childSeriesArrow" alt="show child seriesegories" style="vertical-align: middle; margin: 0 4px 0 <%# (4 + (int)Eval("Indent") * 16).ToString() %>px;<%# (int)Eval("ChildCount") > 0 ? "" : " visibility: hidden;" %>" src="/backstage/modules/CatalogManager/components/CategoryFinder/images/<%# (bool)Eval("IsOpen") ? "down" : "right" %>Arrow.png" />
                                                        </asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="lbVisibility" OnClick="lbVisibility_Click" CommandArgument='<%# Eval("ID") %>'>
                                                            <img src='<%# (bool)Eval("Visible") ? VISIBILITY_VISIBLE : VISIBILITY_HIDDEN %>'  style='margin: 0 0 0 0; vertical-align: middle;' alt="visibility" />
                                                        </asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="lbSeries" ToolTip='<%# Eval("Name") %>' CommandArgument='<%# Eval("ID") %>' OnClick="btnUseSeries_Click" CommandName='<%# Eval("Name") %>'>
                                                            <img alt="folder icon" src="<%# GetFolderImage(Eval("ID").ToString()) %>" style="margin: 0 4px 0 0px; vertical-align: middle;" /> 
                                                            <%# backstage.Classes.StringEditor.Truncate(Eval("Name").ToString(), 100, "...", false) %>
                                                        </asp:LinkButton>
                                                    </td>
                                                    <td class="tdSeriesFinderMoveBtns">
                                                        <img id='moveCancelBtn<%# Eval("ID") %>' class="moveCancelBtn" style="display: none; cursor: pointer; margin-right: 10px;" alt="cancel move" src="/backstage/images/cancel.png" onclick="cancelMoveFolder()" />
                                                        <asp:ImageButton runat="server" ID="btnMoveFolderBefore" CommandName="Before" CommandArgument='<%# Eval("ID") %>' CssClass='btnMoveFolderBefore' style="display: none; margin-right: 10px;" AlternateText="move before" ImageUrl="/backstage/images/moveBefore.png" OnClick="btnMoveFolderObject_Click" onmouseover="moveHover(this, 'Before')" onmouseout="moveHoverOut(this, 'Before')" />
                                                        <asp:ImageButton runat="server" ID="btnMoveFolderAfter" CommandName="After" CommandArgument='<%# Eval("ID") %>' CssClass='btnMoveFolderAfter' style="display: none; margin-right: 10px;" AlternateText="move after" ImageUrl="/backstage/images/moveAfter.png" OnClick="btnMoveFolderObject_Click" onmouseover="moveHover(this, 'After')" onmouseout="moveHoverOut(this, 'After')" />
                                                        <asp:ImageButton runat="server" ID="btnMoveFolderInto" CommandName="Into" CommandArgument='<%# Eval("ID") %>' CssClass='btnMoveFolderInto' style="display: none; margin-right: 10px;" AlternateText="move into" ImageUrl="/backstage/images/moveInto.png" OnClick="btnMoveFolderObject_Click" />
                                                        
                                                    </td>
                                                    <td align="left" class="tdSeriesFinderActionBtns">
                                                        <div style="visibility: hidden; white-space: nowrap; vertical-align: middle;" class="divSeriesFinderActionBtns">
                                                            
                                                            <asp:PlaceHolder runat="server" ID="phMoveBtn" Visible='<%# FinderType != FinderTypes.Picker %>'>
                                                                <img alt="move folder" id='moveFolderBtn<%# Eval("ID") %>' class="moveFolderBtn" onclick="showMoveOpts(this, '<%# Eval("ID") %>')" src="/backstage/images/moveArrows.png" style="vertical-align: middle; margin-right: 8px;" />
                                                            </asp:PlaceHolder>
                                                            <asp:ImageButton runat="server" ID="btnUseSeries" Visible='<%# FinderType == FinderTypes.Picker %>' CssClass="imageButton" ImageUrl="/backstage/images/useIcon.png" AlternateText="Use Series" ToolTip="Use Series" OnClick="btnUseSeries_Click" CommandArgument='<%# Eval("ID") %>' CommandName='<%# Eval("Name") %>' style="margin: 0 0 0 6px; vertical-align: middle;" />
                                                            <asp:ImageButton runat="server" ID="btnEditSeries" style="margin: 0 6px 0 0; vertical-align: middle;" Visible='<%# FinderType != FinderTypes.Picker %>' ImageUrl="/backstage/images/ePencil.png" AlternateText="edit series" ToolTip="edit series" OnClick="btnEditSeries_Click" CommandArgument='<%# Eval("ID") %>' />
                                                            <asp:ImageButton runat="server" ID="btnDeleteSeries" style="margin: 0 20px 0 0; vertical-align: middle;" Visible='<%# FinderType != FinderTypes.Picker %>' ImageUrl="/backstage/modules/CatalogManager/images/multiDelete.png" AlternateText="Delete Series" OnClick="btnDeleteSeries_Click" CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you sure you wish to delete this series?');" ToolTip="Delete Series" />
                                                            
                                                        </div>
                                                    </td>
                                                    <td class="tdSeriesFinderScrollSpacer"></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                </asp:Panel>
                            </div>
                        </asp:Panel>
                    </td>
                    <td style="width: 21px; background: url(/backstage/modules/CatalogManager/components/CategoryFinder/images/finderMiddleRight.png) repeat-y;"></td>
                </tr>
                <tr>
                    <td style="width: 21px; height: 27px; background: url(/backstage/modules/CatalogManager/components/CategoryFinder/images/finderBottomLeft.png);"></td>
                    <td style="height: 27px; background: url(/backstage/modules/CatalogManager/components/CategoryFinder/images/finderBottomMiddle.png) repeat-x;"></td>
                    <td style="width: 21px; height: 27px; background: url(/backstage/modules/CatalogManager/components/CategoryFinder/images/finderBottomRight.png);"></td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>

<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_initializeRequest(
        function () {
            $pnlSeries = $("#<%= pnlSeries.ClientID %>");
            var w = $pnlSeries.outerWidth();
            var h = $pnlSeries.outerHeight();
            $(".pnlProgress").css({ width: w + "px", height: h + "px" });
        }
    );
</script>