﻿using System;
using backstage.Classes;

namespace BackstageSermons.Settings
{
    public static class Admin
    {
        public static bool EnableFeatured
        {
            get
            {
                bool bl = true;
                try {
                    bl = bool.Parse(SermonsCache.SettingCollection.Setting("Admin_EnableFeatured").Value);
                } catch { }
                return bl;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("Admin_EnableFeatured").Value = value.ToString();
            }
        }

        public static bool EnableSharing
        {
            get
            {
                bool bl = true;
                try {
                    bl = bool.Parse(SermonsCache.SettingCollection.Setting("Admin_EnableSharing").Value);
                } catch { }
                return bl;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("Admin_EnableSharing").Value = value.ToString();
            }
        }

        public static bool EnableLivestream
        {
            get
            {
                bool bl = true;
                try {
                    bl = bool.Parse(SermonsCache.SettingCollection.Setting("Admin_EnableLivestream").Value);
                } catch { }
                return bl;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("Admin_EnableLivestream").Value = value.ToString();
            }
        }

        public static TimeZoneInfo TimeZone
        {
            get
            {
                string tz = SermonsCache.SettingCollection.Setting("Admin_TimeZone").Value;
                if (!string.IsNullOrEmpty(tz))
                    return TimeZoneInfo.FindSystemTimeZoneById(tz);
                else
                    return TimeZoneInfo.Local;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("Admin_TimeZone").Value = ((TimeZoneInfo)value).Id;
            }
        }

        public static int SeriesImageWidth
        {
            get
            {
                return StringEditor.ToInt32(SermonsCache.SettingCollection.Setting("Admin_SeriesImageWidth").Value);
            }
            set
            {
                SermonsCache.SettingCollection.Setting("Admin_SeriesImageWidth").Value = value.ToString();
            }
        }

        public static int SeriesImageHeight
        {
            get
            {
                return StringEditor.ToInt32(SermonsCache.SettingCollection.Setting("Admin_SeriesImageHeight").Value);
            }
            set
            {
                SermonsCache.SettingCollection.Setting("Admin_SeriesImageHeight").Value = value.ToString();
            }
        }

        public static int SermonsPerPageModule
        {
            get
            {
                int num = StringEditor.ToInt32(SermonsCache.SettingCollection.Setting("Admin_SermonsPerPageModule").Value);
                if (num == 0)
                    num = 12;
                return num;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("Admin_SermonsPerPageModule").Value = value.ToString();
            }
        }

        public static int SermonsPerPageSite
        {
            get
            {
                int num = StringEditor.ToInt32(SermonsCache.SettingCollection.Setting("Admin_SermonsPerPageSite").Value);
                if (num == 0)
                    num = 12;
                return num;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("Admin_SermonsPerPageSite").Value = value.ToString();
            }
        }

        public static string SermonDetailPage
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("Admin_SermonDetailPage").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("Admin_SermonDetailPage").Value = value;
            }
        }

        public static string SeriesDetailPage
        {
            get
            {
                return SermonsCache.SettingCollection.Setting("Admin_SeriesDetailPage").Value;
            }
            set
            {
                SermonsCache.SettingCollection.Setting("Admin_SeriesDetailPage").Value = value;
            }
        }

        public enum BibleVersions{ KJV, ASV, AKJV, WEB, rv1909, NASB };
        public static BibleVersions BibleVersion
        {
            get
            {
                return (BibleVersions)StringEditor.ToEnum(SermonsCache.SettingCollection.Setting("Admin_BibleVersion").Value, typeof(BibleVersions));
            }
            set
            {
                SermonsCache.SettingCollection.Setting("Admin_BibleVersion").Value = value.ToString();
            }
        }

        public static void Update()
        {
            SettingCollectionML sc = new SettingCollectionML();

            sc.Add(SermonsCache.SettingCollection.Setting("Admin_EnableFeatured"));
            sc.Add(SermonsCache.SettingCollection.Setting("Admin_EnableSharing"));
            sc.Add(SermonsCache.SettingCollection.Setting("Admin_EnableLivestream"));
            sc.Add(SermonsCache.SettingCollection.Setting("Admin_TimeZone"));
            sc.Add(SermonsCache.SettingCollection.Setting("Admin_SeriesImageWidth"));
            sc.Add(SermonsCache.SettingCollection.Setting("Admin_SeriesImageHeight"));
            sc.Add(SermonsCache.SettingCollection.Setting("Admin_SermonsPerPageModule"));
            sc.Add(SermonsCache.SettingCollection.Setting("Admin_SermonsPerPageSite"));
            sc.Add(SermonsCache.SettingCollection.Setting("Admin_SermonDetailPage"));
            sc.Add(SermonsCache.SettingCollection.Setting("Admin_SeriesDetailPage"));
            sc.Add(SermonsCache.SettingCollection.Setting("Admin_BibleVersion"));

            SermonsBLL.Instance.UpdateSettingCollection(sc);
        }
    }
}
