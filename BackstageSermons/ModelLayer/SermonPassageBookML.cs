﻿using System;
using System.Text;

namespace BackstageSermons
{
    public class SermonPassageBookML
    {
        public string Name { get; set; }
        public string SortName
        {
            get
            {
                if (Name.StartsWith("1 "))
                    return Name.Replace("1 ", "") + "1";
                else if (Name.StartsWith("2 "))
                    return Name.Replace("2 ", "") + "2";
                return Name;
            }
        }
        public int SermonCount { get; set; }
    }
}
