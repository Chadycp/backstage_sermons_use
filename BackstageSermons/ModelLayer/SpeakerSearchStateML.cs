﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Remember set of selections used for a search
/// </summary>

namespace BackstageSermons
{
    public class SpeakerSearchStateML
    {
        #region Public fields

        public string SortField = string.Empty;
        public SortDirection SortDirection = SortDirection.Ascending;
        public int PageIndex = 0;

        #endregion

        #region Constructors

        #region SpeakerSearchState()

        public SpeakerSearchStateML()
        {
        }

        #endregion

        #endregion

        #region Static properties

        #region Current

        public static SpeakerSearchStateML Current
        {
            get
            {
                SpeakerSearchStateML s = (SpeakerSearchStateML)System.Web.HttpContext.Current.Session["SpeakerSearchStateML"];
                if (s == null)
                    s = new SpeakerSearchStateML();
                return s;
            }

            set
            {
                System.Web.HttpContext.Current.Session["SpeakerSearchStateML"] = value;
            }
        }

        #endregion

        #endregion
    }
}