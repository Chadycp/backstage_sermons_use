﻿using System;

namespace BackstageSermons.modules.Sermons.controls
{
    public partial class BreadCrumbNavigation : System.Web.UI.UserControl
    {
        private int _SeriesID;
        public int SeriesID
        {
            get { return _SeriesID; }
            set
            {
                _SeriesID = value;
                ctrlSeriesFinder.SeriesID = SeriesID;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}