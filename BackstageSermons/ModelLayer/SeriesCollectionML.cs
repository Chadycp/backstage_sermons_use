﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace BackstageSermons
{
    public class SeriesCollectionML : List<SeriesML>
    {
        public SeriesML FindSeriesByID(int SeriesID)
        {
            SeriesML Series = FindSeriesByID(this, SeriesID);
            return Series;
        }

        private SeriesML FindSeriesByID(SeriesCollectionML sc, int SeriesID)
        {
            SeriesML s = null;
            foreach (SeriesML series in sc)
            {
                if (series.SeriesID == SeriesID)
                {
                    s = series;
                    break;
                }
                else
                {
                    if (series.ChildSeries != null)
                    {
                        SeriesML s1 = FindSeriesByID(series.ChildSeries, SeriesID);
                        if (s1 != null)
                            s = s1;
                    }
                }
            }
            return s;
        }

    }
}
