﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace BackstageSermons
{
    public enum AudioLocation { Bx, External, SermonAudio }
    public enum VideoLocation { Bx, External, YouTube, Vimeo }
    public class SermonML
    {
        public int SermonID { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string Time { get; set; }
        public string PassageBook { get; set; }
        public string PassageChapter { get; set; }
        public string PassageVerse { get; set; }
        public string PassageCopy { get; set; }
        public string PassageFull
        {
            get
            {
                string pf = PassageBook;
                if (!string.IsNullOrEmpty(PassageChapter))
                    pf += " " + PassageChapter;
                if (!string.IsNullOrEmpty(PassageVerse))
                    pf += ":" + PassageVerse;
                return pf;
            }
        }
        public int Speaker { get; set; }
        public NamePrefixes? SpeakerNamePrefix { get; set; }
        public string SpeakerFirstName { get; set; }
        public string SpeakerLastName { get; set; }

        public string SpeakerFullName(bool startWithFirstName, bool displayPrefix)
        {
            string n = "";
            string fName = displayPrefix && SpeakerNamePrefix != null ? backstage.Classes.TypeHelper.GetEnumDescription(SpeakerNamePrefix.Value) + " " + SpeakerFirstName : SpeakerFirstName;
            if (startWithFirstName)
            {
                n = fName;
                if (!string.IsNullOrEmpty(SpeakerLastName) && !string.IsNullOrEmpty(n))
                    n += " ";
                n += SpeakerLastName;
            }
            else
            {
                n = SpeakerLastName;
                if (!string.IsNullOrEmpty(fName) && !string.IsNullOrEmpty(n))
                    n += ", ";
                n += fName;
            }
            return n;
        }

        public string SpeakerFullDisplayName
        {
            get
            {
                return SpeakerFullName(true, true);
            }
        }

        public AudioLocation AudioLocation { get; set; }
        public string BxAudioFile { get; set; }
        public string ExternalAudioLink { get; set; }
        public string SermonAudioID { get; set; }
        public string SermonAudioLink
        {
            get
            {
                string strReturn = string.Empty;
                if (!string.IsNullOrEmpty(SermonAudioID))
                    strReturn = string.Format("http://playmp3.sa-media.com/filearea/{0}/{0}.mp3", SermonAudioID);
                return strReturn;
            }
        }
        //Read-only uses SermonAudioLink if exists, otherwise uses Audio File
        private string _AudioURI;
        public string AudioURI
        {
            get
            {
                if (string.IsNullOrEmpty(_AudioURI))
                {
                    //Added by DAD on 07/05/2011. Uses AudioLocation enum to decide where to pull the file from.
                    switch (AudioLocation.ToString()) {
                        case "Bx":
                            if(!string.IsNullOrEmpty(BxAudioFile))
                                _AudioURI = SermonsBLL.Instance.domain + BxAudioFile;
                            break;
                        case "External":
                            _AudioURI = ExternalAudioLink;
                            break;
                        case "SermonAudio":
                            _AudioURI = SermonAudioLink;
                            break;
                    }
                    //Commented out by DAD on 07/05/2011
                    /*
                    _AudioURI = SermonAudioLink;
                    if (string.IsNullOrEmpty(_AudioURI))
                        if(!string.IsNullOrEmpty(BxAudioFile))
                            _AudioURI = SermonsBLL.Instance.domain + BxAudioFile;
                    */
                }
                return _AudioURI;
            }
        }
        public VideoLocation VideoLocation { get; set; }
        public string BxVideoFile { get; set; }
        public string ExternalVideoLink { get; set; }
        public string YouTubeID { get; set; }
        public string VimeoID { get; set; }

        private string _VideoURI;
        public string VideoURI
        {
            get
            {
                if (string.IsNullOrEmpty(_VideoURI)) {
                    switch (VideoLocation.ToString()) {
                        case "Bx":
                            if(!string.IsNullOrEmpty(BxVideoFile))
                                _VideoURI = SermonsBLL.Instance.domain + BxVideoFile;
                            break;
                        case "External":
                            _VideoURI = ExternalVideoLink;
                            break;
                        case "YouTube":
                            _VideoURI = "https://www.youtube.com/embed/" + YouTubeID;
                            break;
                        case "Vimeo":
                            _VideoURI = "https://player.vimeo.com/video/" + VimeoID;
                            break;
                    }
                }
                return _VideoURI;
            }
        }

        public string Outline { get; set; }
        public string Bulletin { get; set; }
        public string Description { get; set; }
        public string TitleGraphic { get; set; }
        public bool Available { get; set; }
        public bool Featured { get; set; }
        public decimal UnitPrice { get; set; }
        public SeriesCollectionML Series { get; set; }
        public TopicCollectionML Topics { get; set; }

        public int SeriesID { get; set; }
        public string SeriesTitle { get; set; }

        private string _SEOTitle;
        public string SEOTitle
        {
            get
            {
                if (string.IsNullOrEmpty(_SEOTitle))
                    _SEOTitle = Title;
                return _SEOTitle;
            }
            set { _SEOTitle = value; }
        }

        private string _SEODescription;
        public string SEODescription
        {
            get
            {
                if (string.IsNullOrEmpty(_SEODescription))
                    _SEODescription = backstage.Classes.StringEditor.Truncate(backstage.Classes.StringEditor.ReplaceLineBreaks(backstage.Classes.StringEditor.RemoveHTML(Description), ""), 155, "", true);
                return _SEODescription;
            }
            set { _SEODescription = value; }
        }

        private SermonFieldValueCollectionML _FieldValues = null;
        public SermonFieldValueCollectionML FieldValues
        {
            get
            {
                if (_FieldValues == null)
                    _FieldValues = SermonsBLL.Instance.LoadAllSermonFieldValuesBySermonID(SermonID);
                return _FieldValues;
            }
            set { _FieldValues = value; }
        }

        public string FieldValue(int fieldID)
        {
            SermonFieldValueML val = FieldValues.FindBySermonIDAndFieldID(SermonID, fieldID);

            if (val != null)
                return val.Value;
            return "";
        }
    }
}
