﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackstageSermons
{
    public class SermonFieldCollectionML : List<SermonFieldML>
    {
        public SermonFieldML FindByFieldID(int fieldID)
        {
            foreach (SermonFieldML sf in this) {
                if (
                   (fieldID == sf.FieldID)
                ) {
                    return sf;
                }
            }
            return null;
        }
    }
}