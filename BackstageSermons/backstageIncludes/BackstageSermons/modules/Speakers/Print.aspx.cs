﻿using System;

namespace BackstageSermons.modules.Speakers
{
    public partial class Print : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            gridSpeakers.DataSource = Session["SpeakerCollection"];
            gridSpeakers.DataBind();
        }
    }
}
