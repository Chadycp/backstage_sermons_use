﻿using System;

namespace BackstageSermons
{
    public class ReportSermonML
    {
        public int SermonID { get; set; }
        public string SermonTitle { get; set; }
        public int TotalDownloads { get; set; }
        public int TotalListens { get; set; }
    }
}