﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using backstage.bxpages;
using backstage.Classes.CommonInfo;

namespace BackstageSermons.modules.Sermons
{
    public partial class Default : BackstagePage, IComparer<SermonML>
    {
        #region Private Objects
        private static SermonCollectionML searchSC;
        private static SermonCollectionML sc;
        private static SeriesCollectionML series;
        private static SpeakerCollectionML _speakers;
        private static BibleBookCollectionML _books;
        #endregion

        #region Public Objects
        public string SortExpression
        {
            get
            {
                return backstage.modules.MemberManager.StringHelper.NZ(ViewState["SortExpression"]);
            }
            set
            {
                ViewState["SortExpression"] = value;
            }
        }

        public SortDirection SortDirection
        {
            get
            {
                if (ViewState["SortDirection"] == null)
                    return SortDirection.Ascending;
                return (SortDirection)ViewState["SortDirection"];
            }
            set
            {
                ViewState["SortDirection"] = value;
            }
        }

        public SermonSearchStateML searchState
        {
            get
            {
                SermonSearchStateML _searchState = (SermonSearchStateML)Session["SermonSearchStateML"];
                if (_searchState == null) {
                    _searchState = new SermonSearchStateML();
                    Session["SermonSearchStateML"] = _searchState;
                }
                return _searchState;
            }
            set
            {
                Session["SermonSearchStateML"] = value;
            }
        }

        public static string strSeriesID = string.Empty;
        public static int SeriesID;
        public static string SeriesTitle;
        public SpeakerCollectionML speakers
        {
            get
            {
                if (_speakers == null)
                    _speakers = SermonsBLL.Instance.LoadAllSpeakers();
                return _speakers;
            }
            set { _speakers = value; }
        }
        public BibleBookCollectionML books
        {
            get
            {
                if (_books == null)
                    _books = new BibleBookCollectionML();
                return _books;
            }
            set { _books = value; }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) {
                ResetPaging();
                ResetSorting();

                SetSeriesDefaults();
                SetDefaults();
                BindSermons();
            } else
                UpdateSermonPrintList();
            LoadSeries();
        }

        protected void SetSeriesDefaults()
        {
            series = SermonsBLL.Instance.LoadSeriesTree(false);

            if (series != null) {
                if (series.Count > 0) {
                    //pnlSermonButtons.Visible = true;
                    strSeriesID = Request.QueryString["SeriesID"];
                    if (string.IsNullOrEmpty(strSeriesID)) {
                        SeriesID = series[0].SeriesID;
                        strSeriesID = SeriesID.ToString();
                        SeriesTitle = series[0].Title;
                    } else {
                        SeriesID = int.Parse(strSeriesID);
                        SeriesML s = series.FindSeriesByID(SeriesID);
                        if (s != null)
                            SeriesTitle = s.Title;
                    }


                    series.Sort(delegate(SeriesML s1, SeriesML s2) { return s1.Title.CompareTo(s2.Title); });
                }
            }
        }

        protected void SetDefaults()
        {
            if (series != null) {
                if (series.Count > 0) {
                    sc = SermonsBLL.Instance.LoadSermonsBySeriesID(SeriesID, true);
                    sc.Sort(delegate(SermonML s1, SermonML s2) { return s2.SermonID.CompareTo(s1.SermonID); });
                }
            }
            if (sc == null)
                sc = new SermonCollectionML();
        }

        protected void LoadSeries()
        {
            SeriesCollectionML series = SermonsBLL.Instance.LoadSeriesTree(false);
            RenderSeriesCollection(series, "");

            pnlSeries.Controls.Clear();
            pnlSeries.Controls.Add(new LiteralControl("<ul id=\"browser\" class=\"filetree\">"));
            foreach (SeriesML s in series) {
                BuildSeriesHtml(s, 0);
            }
            pnlSeries.Controls.Add(new LiteralControl("</ul>"));
            //pnlSeries.Controls = seriesControls;
        }

        private void BuildSeriesHtml(SeriesML s, int level)
        {
            bool VisibilityControl = true;
            string title = s.Title;
            string id = s.SeriesID.ToString();
            bool isSelected = (strSeriesID != null && s.SeriesID == SeriesID);
            string cssClass = "";

            // The series HTML list is a HTML bullet list using css.
            // The CSS class can be open, closed, bullet, openSelected, closedSelected, bulletSelected.
            if (s.ChildSeries.Count > 0) {
                cssClass = (isSelected) ? "open" : "closed";
                //cssClass = "open";
            } else {
                cssClass = "bullet";
            }
            cssClass += (isSelected) ? "Selected" : "";

            string url = "?SeriesID=" + id;
            if (s.ChildSeries.Count > 0) {
                //url += "&toggle=true";
            }
            // NOTE: Don't use line breaks here. If this output is used in a Javascript variable, 
            // (such as a scroller of some sort) the line breaks will break the code.

            if (VisibilityControl) {
                pnlSeries.Controls.Add(new LiteralControl("<li class=\"" + cssClass + "\"><span class=\"visibleFolder\">"));
                string visibleImage = "<img id=\"seriesFolder" + id + "\" onclick=\"SetVisibility('" + id + "')\" visibility=\"True\" src=\"/backstage/images/icons/eyeIcon.png\" />";
                if (s.Active == false)
                    visibleImage = "<img id=\"seriesFolder" + id + "\" onclick=\"SetVisibility('" + id + "')\" visibility=\"False\" src=\"/backstage/images/icons/eyeIconClose.png\" />";
                pnlSeries.Controls.Add(new LiteralControl(visibleImage));
            } else {
                pnlSeries.Controls.Add(new LiteralControl("<li style=\"position: relative;\" class=\"" + cssClass + "\"><span class=\"folder\">"));
            }
            pnlSeries.Controls.Add(new LiteralControl("<span><a title=\"" + title + "\" href=\"" + url + "\">" + backstage.Classes.StringEditor.Truncate(title, 22 - (level * 3), "...", false) + "</a>"));

            pnlSeries.Controls.Add(new LiteralControl("<img class=\"imgSeriesAction pencil\" onclick=\"window.location = 'EditSeries.aspx?SeriesID=" + id + "';\" src=\"/backstage/images/icon-edit.png\">"));

            ImageButton btnDeleteSeries = new ImageButton();
            btnDeleteSeries.ID = "btnDeleteSeries" + s.SeriesID.ToString();
            btnDeleteSeries.ImageUrl = "/backstage/images/icon-trash.png";
            btnDeleteSeries.CssClass = "imgSeriesAction delete";
            btnDeleteSeries.Attributes.Add("style", "display: none;");
            btnDeleteSeries.CommandName = "DeleteSeries";
            btnDeleteSeries.CommandArgument = s.SeriesID.ToString();
            btnDeleteSeries.Click += new ImageClickEventHandler(lbUpdateSeries_Click);
            btnDeleteSeries.OnClientClick = "return confirm('Are you sure you wish to delete \"" + s.Title + "\"?');";
            pnlSeries.Controls.Add(btnDeleteSeries);

            pnlSeries.Controls.Add(new LiteralControl("</span></span>"));

            





            // Show sub-series if series is open.
            //if (series.ChildSeries.Count > 0 && series.IsOpen) 
            if (s.ChildSeries.Count > 0) {
                pnlSeries.Controls.Add(new LiteralControl("<ul>"));
                level++;
                foreach (SeriesML subSeries in s.ChildSeries) {
                    BuildSeriesHtml(subSeries, level);
                }
                pnlSeries.Controls.Add(new LiteralControl("</ul>"));
            }
            pnlSeries.Controls.Add(new LiteralControl("</li>"));

        }

        protected void RenderSeriesCollection(SeriesCollectionML series, string preSpaces)
        {
            foreach (SeriesML s in series) {
                ListItem li = new ListItem(preSpaces + s.Title, s.SeriesID.ToString());
                if (!Page.IsPostBack)
                    if (s.SeriesID == SeriesID)
                        li.Selected = true;

                s.ChildSeries.Sort(delegate(SeriesML s1, SeriesML s2) { return s1.Title.CompareTo(s2.Title); });
                RenderSeriesCollection(s.ChildSeries, preSpaces + "--");
            }
        }

        protected void ReloadPage()
        {
            Response.Redirect("Default.aspx?SeriesID=" + strSeriesID);
        }

        #region Select/Deselect All
        private void SelectAll()
        {
            try {
                SermonCollectionML scTemp = (SermonCollectionML)Session["SermonCollection"];
                if (scTemp == null)
                    return;
                SermonPrintListCollectionML splc = (SermonPrintListCollectionML)Session["SermonPrintListCollectionML"];

                if (splc == null) {
                    splc = new SermonPrintListCollectionML();
                    Session["SermonPrintListCollectionML"] = splc;
                }

                foreach (SermonML s in scTemp) {
                    SermonPrintListML spl = new SermonPrintListML();
                    spl.SermonID = s.SermonID;
                    splc.Add(spl);
                }
            } catch (Exception err) {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "errAlert", "alert('" + err.Message.ToString() + "');", true);
            }
            BindSermons();
        }

        private void DeselectAll()
        {
            try {
                Session.Remove("SermonPrintListCollectionML");
            } catch (Exception err) {
                AlertMessage(err.Message);
            }
            BindSermons();
        }

        protected void lbSelectAllSermons_Click(object sender, EventArgs e)
        {
            bool chckd = false;
            if (sender.GetType() == typeof(CheckBox))
                chckd = !cbSelectAllSermons.Checked;
            else
                chckd = (cbSelectAllSermons.Checked);

            if (chckd) {
                DeselectAll();
                lblSelectAllSermons.Text = "Select All";
                cbSelectAllSermons.Checked = false;
            } else {
                SelectAll();
                lblSelectAllSermons.Text = "Deselect All";
                cbSelectAllSermons.Checked = true;
            }
            SermonPrintListCollectionML slc = (SermonPrintListCollectionML)Session["SermonPrintListCollectionML"];
            /*
            if (slc == null)
                lblSelectedSermons.Text = "0";
            else
                lblSelectedSermons.Text = plc.Count.ToString();
             */

            updateSermons.Update();
        }
        #endregion

        private void UpdateSermonPrintList()
        {
            SermonCollectionML scTemp = (SermonCollectionML)Session["SermonCollection"];
            if (scTemp == null)
                return;
            SermonPrintListCollectionML splc = (SermonPrintListCollectionML)Session["SermonPrintListCollectionML"];

            if (splc == null) {
                splc = new SermonPrintListCollectionML();
                Session["SermonPrintListCollectionML"] = splc;
            }

            foreach (GridViewRow gvr in gridSermons.Rows) {
                CheckBox cb = (CheckBox)gvr.FindControl("cbSermon");
                HiddenField hf = (HiddenField)gvr.FindControl("hfSermonID");
                if (cb != null) {
                    SermonML s = scTemp.FindSermonByID(int.Parse(hf.Value));

                    if (cb.Checked) {
                        SermonPrintListML spl = new SermonPrintListML();
                        spl.SermonID = s.SermonID;
                        splc.Add(spl);
                    } else {
                        SermonPrintListML spl = splc.GetBySermonID(s.SermonID);
                        if (spl != null) {
                            splc.Remove(spl);
                        }
                    }
                }
            }
        }

        /*
        protected void AlertMessage(string msg)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alertMessage", "alert(\"" + msg + "\");", true);
        }
        */
         

        protected void lbUpdateSeries_Click(object sender, EventArgs e)
        {
            string ca = "";
            string cn = "";
            if (sender.GetType() == typeof(LinkButton)) {
                LinkButton lb = (LinkButton)sender;
                ca = lb.CommandArgument;
                cn = lb.CommandName;
            } else {
                ImageButton ib = (ImageButton)sender;
                ca = ib.CommandArgument;
                cn = ib.CommandName;
            }
            int sID = int.Parse(ca);
            if (cn == "DeleteSeries") {
                SeriesML s = series.FindSeriesByID(sID);
                SermonsBLL.Instance.DeleteSeries(s);
            }
            ReloadPage();
        }

        protected void btnDeleteSermon_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow gvRow in gridSermons.Rows) {
                try {
                    CheckBox cb = (CheckBox)gvRow.FindControl("cbSermon");
                    HiddenField hb = (HiddenField)gvRow.FindControl("hfSermonID");
                    if (cb.Checked) {
                        int SermonID = int.Parse(hb.Value);
                        SermonsBLL.Instance.DeleteSermon(SermonID);
                    }
                } catch (Exception err) {
                    lblResult.Text = err.Message.ToString();
                }
            }
            SetDefaults();
            BindSermons();
        }

        [System.Web.Services.WebMethod]
        public static string ChangeVisibility(string seriesID, bool visible)
        {
            SeriesML series = BackstageSermons.SermonsBLL.Instance.LoadSeries(int.Parse(seriesID), false);
            series.Active = visible;
            SermonsBLL.Instance.UpdateSeries(series);
            return "Success";
        }

        protected void gridSermons_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try {
                if (e.CommandName == "UpdateSermon") {
                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                    int sID = int.Parse(e.CommandArgument.ToString());
                    SermonML sermon = SermonsBLL.Instance.LoadSermon(sID, false);
                    if (sermon != null) {
                        CheckBox cbEditActive = (CheckBox)row.FindControl("cbEditActive");
                        TextBox txtEditName = (TextBox)row.FindControl("txtEditTitle");
                        DropDownList ddlBooks = (DropDownList)row.FindControl("ddlPassageBook");
                        TextBox txtPassageChapter = (TextBox)row.FindControl("txtPassageChapter");
                        TextBox txtPassageVerse = (TextBox)row.FindControl("txtPassageVerse");
                        DropDownList ddlSpeaker = (DropDownList)row.FindControl("ddlSpeaker");

                        sermon.Available = cbEditActive.Checked;
                        sermon.Title = txtEditName.Text;
                        sermon.PassageBook = ddlBooks.SelectedValue;
                        sermon.PassageChapter = txtPassageChapter.Text;
                        sermon.PassageVerse = txtPassageVerse.Text;
                        if (string.IsNullOrEmpty(ddlSpeaker.SelectedValue))
                            throw new Exception("Must select a speaker");
                        sermon.Speaker = int.Parse(ddlSpeaker.SelectedValue);

                        SermonsBLL.Instance.UpdateSermon(sermon);
                        SetDefaults();
                        BindSermons();
                    }
                }

                if (e.CommandName == "DeleteSermon") {
                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                    int sID = int.Parse(e.CommandArgument.ToString());
                    SermonsBLL.Instance.DeleteSermon(sID);
                    SetDefaults();
                    BindSermons();
                }

                if (e.CommandName == "ChangeVisibility") {
                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                    int sID = int.Parse(e.CommandArgument.ToString());
                    SermonML sermon = SermonsBLL.Instance.LoadSermon(sID, false);
                    ImageButton btn = (ImageButton)row.FindControl("btnVisibility");
                    if (sermon != null) {
                        if (sermon.Available) {
                            sermon.Available = false;
                            btn.ImageUrl = "/backstage/images/icons/eyeIconClose.png";
                        } else {
                            sermon.Available = true;
                            btn.ImageUrl = "/backstage/images/icons/eyeIcon.png";
                        }

                        SermonsBLL.Instance.UpdateSermon(sermon);
                        //SetDefaults();
                        //BindSermons();
                    }
                }
            } catch (Exception err) {
                AlertMessage(err.Message);
            }
        }

        protected void lbVisibility_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            bool visible = lb.CommandArgument == "Show";
            string strResult = string.Empty;
            foreach (GridViewRow row in gridSermons.Rows) {
                try {
                    CheckBox cb = (CheckBox)row.FindControl("cbSermon");
                    HiddenField hf = (HiddenField)row.FindControl("hfSermonID");
                    if (cb.Checked) {
                        int SermonID = int.Parse(hf.Value);
                        SermonML sermon = SermonsBLL.Instance.LoadSermon(SermonID, false);
                        sermon.Available = visible;
                        SermonsBLL.Instance.UpdateSermon(sermon);
                    }
                } catch (Exception err) {
                    AlertMessage(err.Message);
                    strResult += err.Message.ToString() + "<br/>";
                }
            }
            if (string.IsNullOrEmpty(strResult)) {
                SetDefaults();
                BindSermons();
            } else
                AlertMessage(strResult);
        }

        protected void lbPrint_Click(object sender, EventArgs e)
        {
            if (Session["SermonCollection"] == null)
                BindSermons();
            ScriptManager.RegisterStartupScript(this, typeof(Page), "printTrigger", "$(document).ready(function(){$('#A1').click(); });", true);
        }

        protected void BindSermons()
        {
            searchSC = new SermonCollectionML();
            string search = txtSearch.Text.ToLower();
            if (search == "search sermons")
                search = "";

            foreach (SermonML sermon in sc) {
                if (sermon.SermonID.ToString().ToLower().Contains(search) || sermon.Title.ToLower().Contains(search))
                    searchSC.Add(sermon);
            }

            SortSermons();

            gridSermons.DataSource = searchSC;
            gridSermons.DataBind();

            Session["SermonCollection"] = searchSC;
        }

        protected void SortSermons()
        {
            if (SortExpression.Length > 0) {

                searchState.SortField = SortExpression;
                searchState.SortDirection = SortDirection;
                searchSC.Sort(this);
            }
        }

        protected void txtSearch_OnTextChanged(object sender, EventArgs e)
        {
            BindSermons();
        }

        #region Popup Panels

        #region Common Methods

        protected void CloseAllPopups()
        {
            pnlImportExport.Visible = false;
            pnlExportSermons.Visible = false;
        }

        protected void btnCancelImportExport_Click(object sender, EventArgs e)
        {
            CloseAllPopups();
        }

        #endregion

        #region Export Methods

        protected void lbExportSermons_Click(object sender, EventArgs e)
        {
            pnlImportExport.Visible = true;
            pnlExportSermons.Visible = true;

            Type myObjectType = typeof(SermonML);
            System.Reflection.PropertyInfo[] propertyInfo = myObjectType.GetProperties();
            cbExportSermonFields.Items.Clear();
            foreach (System.Reflection.PropertyInfo info in propertyInfo) {
                ListItem li = new ListItem(info.Name);
                li.Selected = true;
                cbExportSermonFields.Items.Add(li);
            }


            ScriptManager.RegisterStartupScript(this, this.GetType(), "cbScript", "$('#cbSelectAll').click(function(){var status = this.checked;$(this).next().find('input:checkbox').each(function(){this.checked = status;});});", true);



            //ExportSermons(DateTime.Now.ToString("yyyyMMdd") + "_Sermons_" + backstage.Classes.StringEditor.RemoveSpecialCharacters(SeriesTitle));
        }

        protected void btnExportSermonFields_Click(object sender, EventArgs e)
        {
            ExportSermons(DateTime.Now.ToString("yyyyMMdd") + "_Sermons_" + backstage.Classes.StringEditor.RemoveSpecialCharacters(SeriesTitle));
            CloseAllPopups();
        }

        public void ExportSermons(string fileName)
        {
            BindSermons();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            // First we will write the headers.
            foreach (ListItem li in cbExportSermonFields.Items) {
                if (li.Selected) {
                    if (sb.Length > 0)
                        sb.Append(",");
                    sb.Append(li.Value);
                }
            }
            sb.AppendLine("");
            // Now write all the rows.

            foreach (SermonML p in searchSC) {
                Type myObjectType = typeof(SermonML);
                System.Reflection.PropertyInfo[] propertyInfo = myObjectType.GetProperties();

                int col = 0;
                foreach (ListItem li in cbExportSermonFields.Items) {
                    if (li.Selected) {
                        if (col != 0)
                            sb.Append(",");

                        System.Reflection.PropertyInfo info = myObjectType.GetProperty(li.Value);
                        string s = "";
                        try {
                            s = info.GetValue(p, null).ToString();
                        } catch { }

                        if (s.IndexOfAny("\",\x0A\x0D".ToCharArray()) > -1)
                            s = "\"" + s.Replace("\"", "\"\"") + "\"";
                        sb.Append(s);
                        col++;
                    }
                }
                sb.AppendLine("");
            }

            System.Web.HttpContext.Current.Response.ContentType = "text/csv";
            System.Web.HttpContext.Current.Response.AddHeader("content-disposition",
                "attachment; filename=" + fileName + ".csv");

            System.Web.HttpContext.Current.Response.Write(sb);
            System.Web.HttpContext.Current.Response.End();
        }

        #endregion

        #region Import Methods

        protected void lbImportSermons_Click(object sender, EventArgs e)
        {
            pnlImportExport.Visible = true;
            pnlUploadCSV.Visible = true;
        }

        protected void btnUploadCSV_Click(object sender, EventArgs e)
        {
            FillInImportFields();
            FillInSermonFields();


            pnlUploadCSV.Visible = false;
            pnlMatchFields.Visible = true;
        }

        protected void FillInImportFields()
        {

            System.Threading.Thread.Sleep(5000);
            if (fileUploadSermons.HasFile) {
                string importFileName = Guid.NewGuid().ToString() + ".csv";
                string importFilePath = Server.MapPath("/site/user/files/" + importFileName);
                fileUploadSermons.SaveAs(importFilePath);
                DataTable dtImportSermons = GetDataTable(importFilePath);
                Session["dtImportSermons"] = dtImportSermons;
                FileInfo fi = new FileInfo(importFilePath);
                fi.Delete();
                int i = 0;
                int c = 0;
                foreach (DataColumn dc in dtImportSermons.Columns) {
                    Label lblSermon = new Label();
                    lblSermon.ID = "lblImportSermon" + i.ToString();
                    lblSermon.Text = backstage.Classes.StringEditor.Truncate(dc.ColumnName, 18, "...");
                    lblSermon.ToolTip = dc.ColumnName;
                    if (c == 0) {
                        pnlFileFields.Controls.Add(new LiteralControl("<div style=\"padding: 9px 4px 4px 10px; height: 19px;\">"));
                        c++;
                    } else {
                        pnlFileFields.Controls.Add(new LiteralControl("<div style=\"background: #F3F3F3; padding: 9px 4px 4px 10px; height: 19px;\">"));
                        c = 0;
                    }
                    pnlFileFields.Controls.Add(lblSermon);
                    pnlFileFields.Controls.Add(new LiteralControl("</div>"));
                    i++;
                }
            }
        }

        public DataTable GetDataTable(string strFileName)
        {
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OleDb.4.0; Data Source = " + System.IO.Path.GetDirectoryName(strFileName) + "; Extended Properties = \"Text;HDR=YES;FMT=Delimited\"");
            conn.Open();
            string strQuery = "SELECT * FROM [" + System.IO.Path.GetFileName(strFileName) + "]";
            System.Data.OleDb.OleDbDataAdapter adapter = new System.Data.OleDb.OleDbDataAdapter(strQuery, conn);
            System.Data.DataSet ds = new System.Data.DataSet("CSV File");
            adapter.Fill(ds);
            return ds.Tables[0];
        }

        protected void FillInSermonFields()
        {
            System.Web.HttpContext.Current.Trace.Write("Filling in Sermon Fields");
            List<Control> controls = new List<Control>();
            DataTable dtImportSermons = (DataTable)HttpContext.Current.Session["dtImportSermons"];
            System.Web.HttpContext.Current.Trace.Write("dtImportSermons", (dtImportSermons != null).ToString());

            int i = 0;
            int c = 0;
            foreach (DataColumn column in dtImportSermons.Columns) {
                DropDownList ddl = new DropDownList();
                ddl.ID = "ddlSermon" + i.ToString();
                ddl.Width = 180;
                ddl.CssClass = "ddlSermon";
                ddl.Items.Add(new ListItem("Do not import", ""));

                Type myObjectType = typeof(SermonML);
                System.Reflection.PropertyInfo[] propertyInfo = myObjectType.GetProperties();

                foreach (System.Reflection.PropertyInfo pi in propertyInfo) {
                    ddl.Items.Add(new ListItem(pi.Name, pi.Name));
                }
                if (c == 0) {
                    controls.Add(new LiteralControl("<div style=\"padding: 4px 10px 4px 4px; height: 24px; border-right: #CCC 1px solid;\">"));
                    c++;
                } else {
                    controls.Add(new LiteralControl("<div style=\"background: #F3F3F3; padding: 4px 10px 4px 4px; border-right: #CCC 1px solid; height: 24px;\">"));
                    c = 0;
                }

                ddl.Text = dtImportSermons.Columns[i].ColumnName;
                controls.Add(ddl);
                controls.Add(new LiteralControl("</div>"));
                i++;
            }


            foreach (Control control in controls) {
                pnlSermonFields.Controls.Add(control);
            }
        }

        protected void importSermons_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #endregion

        protected string GetSpeakerSelectedValue(string sID)
        {
            //Make sure the SpeakerID stored in the sermon actually exists in the speaker list, so an error is not thrown.
            string strReturn = string.Empty;

            foreach (SpeakerML s in speakers) {

                if (s.SpeakerID.ToString() == sID)

                    strReturn = s.SpeakerID.ToString();

            }

            return strReturn;

        }

        protected void gridSermons_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridSermons.PageIndex = e.NewPageIndex;
            //BindData();
            BindSermons();
        }

        public void gridSermons_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row != null && e.Row.RowType == DataControlRowType.Header) {
                foreach (TableCell cell in e.Row.Cells) {
                    if (cell.HasControls()) {
                        LinkButton button = cell.Controls[0] as LinkButton;

                        if (button != null) {
                            if (SortExpression == button.CommandArgument) {
                                if (SortDirection == SortDirection.Ascending)
                                    cell.CssClass = "asc";
                                else
                                    cell.CssClass = "desc";
                            }
                        }
                    }
                }
            }
        }

        protected void gridSermons_DataBound(object sender, System.EventArgs e)
        {
            SermonPrintListCollectionML splc = (SermonPrintListCollectionML)Session["SermonPrintListCollectionML"];

            if (splc == null) {
                //lblSelected.Text = "0";
                return;
            }

            foreach (GridViewRow gvRow in gridSermons.Rows) {
                CheckBox cb = (CheckBox)gvRow.Cells[0].FindControl("cbSermon");
                if (cb != null) {
                    SermonCollectionML sc = (SermonCollectionML)gridSermons.DataSource;
                    //throw new Exception(gvRow.DataItemIndex.ToString());
                    int sID = sc[gvRow.DataItemIndex].SermonID;
                    SermonPrintListML spl = splc.GetBySermonID(sID);
                    cb.Checked = (spl != null);
                }
            }
            //lblSelected.Text = plc.Count.ToString();
        }

        protected void gridSermons_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (SortExpression == e.SortExpression) {
                SortDirection = (SortDirection == SortDirection.Ascending) ? SortDirection.Descending : SortDirection.Ascending;
            } else {
                SortExpression = e.SortExpression;
                SortDirection = SortDirection.Ascending;
            }
            ResetPaging();
            BindSermons();
            //BindData();
            updateSermons.Update();
        }

        private void ResetPaging()
        {
            try {
                gridSermons.PageIndex = 0;
            } catch {
            }
        }

        private void ResetSorting()
        {
            try {
                SortExpression = "Title";
                SortDirection = SortDirection.Ascending;
            } catch {
            }
        }

        #region Sorting Methods
        public int Compare(SermonML x, SermonML y)
        {
            try {
                int result = GetCompareResult(x, y);

                #region Handle sorting direction

                if (searchState.SortDirection == SortDirection.Descending)
                    result = result * -1;

                #endregion


                return result;

            } catch (Exception err) {
                System.Web.HttpContext.Current.Trace.Write(err.ToString());
            }

            return 0;
        }

        public int GetCompareResult(SermonML x, SermonML y)
        {
            int result = 0;

            if (searchState.SortField == "CreationDate") {
                #region Sort by date

                DateTime dx = DateTime.MinValue;
                if (x.Date != null)
                    dx = x.Date;

                DateTime dy = DateTime.MinValue;
                if (y.Date != null)
                    dy = y.Date;

                if (dx < dy)
                    return -1;
                else if (dx > dy)
                    return 1;
                else
                    return 0;

                #endregion
            } else if (searchState.SortField == "Title") {
                #region Sort by FirstName
                result = CompareStrings(x.Title, y.Title);
                #endregion
            } else if (searchState.SortField == "Passage") {
                #region Sort by LastName
                result = CompareStrings(x.PassageFull, y.PassageFull);
                #endregion
            } else if (searchState.SortField == "Speaker") {
                #region Sort By Email
                result = CompareStrings(x.SpeakerLastName, y.SpeakerLastName);
                #endregion
            }
            return result;
        }

        public int CompareStrings(string s1, string s2)
        {
            int result = 0;
            if (string.IsNullOrEmpty(s1)) {
                if (string.IsNullOrEmpty(s2)) {
                    return 0;
                } else {
                    return -1;
                }
            } else if (string.IsNullOrEmpty(s2)) {
                return 1;
            }

            int s1Length = s1.Length;
            int s2Length = s2.Length;

            bool sp1 = char.IsLetterOrDigit(s1[0]);
            bool sp2 = char.IsLetterOrDigit(s2[0]);

            if (sp1 && !sp2) {
                return 1;
            }
            if (!sp1 && sp2) {
                return -1;
            }

            char c1, c2;
            int i1 = 0, i2 = 0;
            int r = 0;
            bool letter1, letter2;

            while (true) {
                c1 = s1[i1];
                c2 = s2[i2];

                sp1 = char.IsDigit(c1);
                sp2 = char.IsDigit(c2);

                if (!sp1 && !sp2) {
                    if (c1 != c2) {
                        letter1 = char.IsLetter(c1);
                        letter2 = char.IsLetter(c2);

                        if (letter1 && letter2) {
                            c1 = char.ToUpper(c1);
                            c2 = char.ToUpper(c2);

                            r = c1 - c2;
                            if (0 != r) {
                                return r;
                            }
                        } else if (!letter1 && !letter2) {
                            r = c1 - c2;
                            if (0 != r) {
                                return r;
                            }
                        } else if (letter1) {
                            return 1;
                        } else if (letter2) {
                            return -1;
                        }
                    }

                } else if (sp1 && sp2) {
                    r = CompareNumbers(s1, s1Length, ref i1, s2, s2Length, ref i2);
                    if (0 != r) {
                        result = r;
                        break;
                        //return r;
                    }
                } else if (sp1) {
                    return -1;
                } else if (sp2) {
                    return 1;
                }

                i1++;
                i2++;

                if (i1 >= s1Length) {
                    if (i2 >= s2Length) {
                        return 0;
                    } else {
                        return -1;
                    }
                } else if (i2 >= s2Length) {
                    return 1;
                }
            }
            return result;
        }

        private static int CompareNumbers(
        string s1, int s1Length, ref int i1,
        string s2, int s2Length, ref int i2)
        {
            int nzStart1 = i1, nzStart2 = i2;
            int end1 = i1, end2 = i2;

            ScanNumber(s1, s1Length, i1, ref nzStart1, ref end1);
            ScanNumber(s2, s2Length, i2, ref nzStart2, ref end2);

            int start1 = i1;
            i1 = end1 - 1;
            int start2 = i2;
            i2 = end2 - 1;

            int length1 = end2 - nzStart2;
            int length2 = end1 - nzStart1;

            if (length1 == length2) {
                int r;
                for (int j1 = nzStart1, j2 = nzStart2; j1 <= i1; j1++, j2++) {
                    r = s1[j1] - s2[j2];
                    if (0 != r) return r;
                }

                length1 = end1 - start1;
                length2 = end2 - start2;

                if (length1 == length2) return 0;
            }

            if (length1 > length2) return -1;
            return 1;
        }

        private static void ScanNumber(string s, int length, int start, ref int nzStart, ref int end)
        {
            nzStart = start;
            end = start;

            bool countZeros = true;
            char c = s[end];

            while (true) {
                if (countZeros) {
                    if ('0' == c) {
                        nzStart++;
                    } else {
                        countZeros = false;
                    }
                }

                end++;
                if (end >= length) break;

                c = s[end];
                if (!char.IsDigit(c)) break;
            }
        }
        #endregion
    }
}
