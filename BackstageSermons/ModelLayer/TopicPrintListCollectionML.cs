﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Security.Cryptography;
using backstage.security;

namespace BackstageSermons
{
    public class TopicPrintListCollectionML : List<TopicPrintListML>
    {
        #region Construction

        #region TopicPrintListCollectionML()

        public TopicPrintListCollectionML()
        {
        }

        #endregion

        #endregion

        #region Public methods

        #region public void Add(TopicPrintList tpl)

        public new void Add(TopicPrintListML tpl)
        {
            foreach (TopicPrintListML s in this) {
                if (s.TopicID == tpl.TopicID)
                    return;
            }
            base.Add(tpl);
        }

        #endregion

        #region TopicPrintListML GetByTopicID(int TopicID)

        public TopicPrintListML GetByTopicID(int TopicID)
        {
            foreach (TopicPrintListML tpl in this)
                if (tpl.TopicID == TopicID)
                    return tpl;

            return null;
        }

        #endregion

        #endregion
    }
}